package com.zhjoe.filenet;

import com.zhjoe.ccpws.asynthread.AsyncFactory;
import com.zhjoe.ccpws.asynthread.AsyncManager;
import com.zhjoe.ccpws.asynthread.TransferBehaviorLock;
import com.zhjoe.ccpws.service.TransferService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import java.io.IOException;

@Controller
public class TransferController {
    @Autowired
    TransferService transferService;
    @RequestMapping(value = "/downloadDocuments", method = RequestMethod.GET)
    @ResponseBody
    public String downloadDocuments() throws Exception {
        // 开启二十个线程跑下载
        for(int i=0; i<2; i++){
            AsyncManager.me().execute(AsyncFactory.transferDataToTempStorage());
        }
        return "转移文档到临时存储任务已启动";
    }
    
    @RequestMapping(value = "/uploadDocuments", method = RequestMethod.GET)
    @ResponseBody
    public String uploadDocuments() throws Exception{
        for(int i=0; i<2; i++){
            AsyncManager.me().execute(AsyncFactory.transferDataToNewFileNet());
        }
    	return "上传文档到新FileNet任务已启动";
    }

    @RequestMapping(value = "/checkInconsistant", method = RequestMethod.GET)
    @ResponseBody
    public String checkInconsistant() throws IOException {
        transferService.checkInConsistant();
        return "检测数据库不一致已启动，请检查日志，查看输出！";
    }
    @RequestMapping(value = "/clearContentRepeatData", method = RequestMethod.GET)
    @ResponseBody
    public String clearContentRepeatData() throws IOException {
        transferService.clearContentRepeatData();
        return "清空重复数据";
    }
}
