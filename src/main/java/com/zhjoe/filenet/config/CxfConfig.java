package com.zhjoe.filenet.config;

import com.zhjoe.filenet.webservice.ICommonContentManageBusinessService;
import com.zhjoe.filenet.webservice.IHelloWorld;
import com.zhjoe.filenet.webservice.ISessionManageBusinessService;
import org.apache.cxf.Bus;
import org.apache.cxf.jaxws.EndpointImpl;
import org.apache.cxf.transport.servlet.CXFServlet;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.web.servlet.ServletRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import javax.xml.ws.Endpoint;

/**
 * @ClassName:CxfConfig
 * @Description:cxf发布webservice配置
 * @author Maple
 * @date:2018年4月10日下午4:12:24
 */
@Configuration
public class CxfConfig {
	@Autowired
	private Bus bus;

	@Autowired
	IHelloWorld helloWorld;
	@Autowired
	ICommonContentManageBusinessService commonContentManageBusinessService;
	@Autowired
	ISessionManageBusinessService sessionManageBusinessService;

	/**
	 * 此方法作用是改变项目中服务名的前缀名，此处127.0.0.1或者localhost不能访问时，请使用ipconfig查看本机ip来访问
	 * 去掉注释后：wsdl访问地址为：http://127.0.0.1:8080/ws
	 */
	@Bean
	public ServletRegistrationBean cxfSpringDispatcherServlet() {
		return new ServletRegistrationBean(new CXFServlet(), "/ws/*");
	}

	/** JAX-WS
	 * 站点服务
	 * **/
	@Bean
	public Endpoint helloEndpoint() {
		EndpointImpl endpoint = new EndpointImpl(bus, helloWorld);
		endpoint.publish("/hello");
		return endpoint;
	}
	@Bean
	public Endpoint commonEndpoint() {
		EndpointImpl endpoint = new EndpointImpl(bus, commonContentManageBusinessService);
		endpoint.publish("/common");
		return endpoint;
	}
	@Bean
	public Endpoint sessionEndpoint() {
		EndpointImpl endpoint = new EndpointImpl(bus, sessionManageBusinessService);
		endpoint.publish("/session");
		return endpoint;
	}

}
