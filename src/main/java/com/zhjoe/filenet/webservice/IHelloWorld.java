/**
 * @Title: HelloWorld.java
 * @Package com.zhjoe.filenet.webservice
 * @Description: TODO(用一句话描述该文件做什么)
 * @author Changling Jiang
 * @date 2014-2-11 下午04:19:43
 * @version V1.0
 */
package com.zhjoe.filenet.webservice;

import com.zhjoe.ccpws.vo.UserVo;

import javax.jws.WebParam;
import javax.jws.WebService;

/**
 * @ClassName: HelloWorld
 * @Description: TODO(这里用一句话描述这个类的作用)
 * @author Changling Jiang
 * @date 2014-2-11 下午04:19:43
 *
 */
@WebService
public interface IHelloWorld {

	String sayHello(@WebParam(name = "username") String username);

	String sayUser(@WebParam(name = "user") UserVo user);

}
