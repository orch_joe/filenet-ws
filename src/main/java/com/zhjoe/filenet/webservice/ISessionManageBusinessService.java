/**
 * @Title: ISessionManageBusinessService.java
 * @Package com.zhjoe.filenet.webservice
 * @Description: TODO(用一句话描述该文件做什么)
 * @author Changling Jiang
 * @date 2014-2-13 下午03:42:04
 * @version V1.0
 */
package com.zhjoe.filenet.webservice;

import com.zhjoe.ccpws.vo.UserVo;
import com.zhjoe.ccpws.vo.WSSMLoginResultVo;
import com.zhjoe.ccpws.vo.WSSMLogoffResultVo;

import javax.jws.WebParam;
import javax.jws.WebService;


/**
 * @ClassName: ISessionManageBusinessService
 * @Description: TODO(这里用一句话描述这个类的作用)
 * @author Changling Jiang
 * @date 2014-2-13 下午03:42:04
 *
 */
@WebService
public interface ISessionManageBusinessService {

	public WSSMLoginResultVo login(@WebParam(name = "callId") String callId, @WebParam(name = "user") UserVo userVo);

	public WSSMLogoffResultVo logoff(@WebParam(name = "callId") String callId, @WebParam(name = "sessionId") String sessionId);
}
