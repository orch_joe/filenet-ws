/**
 * @Title: HelloWorldImpl.java
 * @Package com.zhjoe.filenet.webservice.impl
 * @Description: TODO(用一句话描述该文件做什么)
 * @author Changling Jiang
 * @date 2014-2-11 下午04:20:37
 * @version V1.0
 */
package com.zhjoe.filenet.webservice.impl;

import com.zhjoe.ccpws.vo.UserVo;
import com.zhjoe.filenet.webservice.IHelloWorld;
import org.springframework.stereotype.Service;

import javax.jws.WebService;

@WebService(serviceName = "helloService",//对外发布的服务名
		targetNamespace = "http://service.hello.filenet.com",//指定你想要的名称空间，通常使用使用包名反转
		endpointInterface = "com.zhjoe.filenet.webservice.IHelloWorld")//服务接口全路径, 指定做SEI（Service EndPoint Interface）服务端点接口
@Service
public class HelloWorldImpl implements IHelloWorld {

	/* (非 Javadoc)
	 * <p>Title: sayHello</p>
	 * <p>Description: </p>
	 * @param username
	 * @return
	 * @see com.zhjoe.filenet.webservice.HelloWorld#sayHello(java.lang.String)
	 */
	public String sayHello(String username) {
		return "Hello " + username;
	}

	/* (非 Javadoc)
	 * <p>Title: sayHello</p>
	 * <p>Description: </p>
	 * @param user
	 * @return
	 * @see com.zhjoe.filenet.webservice.HelloWorld#sayUser(com.ccpws.vo.UserVo)
	 */
	public String sayUser(UserVo user) {
		return user.getShortName() + " " + user.getPassword();
	}

}
