/**
 * @Title: SessionManageBusinessService.java
 * @Package com.zhjoe.filenet.webservice.impl
 * @Description: TODO(用一句话描述该文件做什么)
 * @author Changling Jiang
 * @date 2014-2-13 下午03:44:28
 * @version V1.0
 */
package com.zhjoe.filenet.webservice.impl;

import java.util.List;
import java.util.UUID;

import javax.jws.WebService;

import com.zhjoe.ccpws.util.SessionManageUtil;
import com.zhjoe.ccpws.vo.*;
import com.zhjoe.strongit.filenet.service.ce.CEObjectStore;
import com.zhjoe.strongit.filenet.service.ce.SecurityHelper;
import com.zhjoe.strongit.filenet.service.exception.CustomWebServiceException;
import com.zhjoe.strongit.filenet.service.util.CEObjectStoreUtil;
import com.zhjoe.strongit.filenet.service.util.FileNetConfig;
import org.apache.log4j.Logger;

import com.zhjoe.filenet.webservice.ISessionManageBusinessService;
import com.filenet.api.security.User;
import org.springframework.stereotype.Service;

/**
 * @ClassName: SessionManageBusinessService
 * @Description: TODO(这里用一句话描述这个类的作用)
 * @author Changling Jiang
 * @date 2014-2-13 下午03:44:28
 *
 */
@WebService(serviceName = "sessionService",//对外发布的服务名
		targetNamespace = "http://service.session.filenet.com",//指定你想要的名称空间，通常使用使用包名反转
		endpointInterface = "com.zhjoe.filenet.webservice.ISessionManageBusinessService")//服务接口全路径, 指定做SEI（Service EndPoint Interface）服务端点接口
@Service
public class SessionManageBusinessService implements ISessionManageBusinessService {

	private static Logger log = Logger.getLogger(SessionManageBusinessService.class);

	private static long timeOut = 1000 * 60 * 30;//超时时间


	/* (非 Javadoc)
	 * <p>Title: login</p>
	 * <p>Description: </p>
	 * @param callId
	 * @param userVo
	 * @return
	 * @see com.zhjoe.filenet.webservice.ISessionManageService#login(java.lang.String, com.ccpws.vo.UserVo)
	 */
	public WSSMLoginResultVo login(String callId, UserVo userVo) {

		WSSMLoginResultVo result = new WSSMLoginResultVo();
		WSSMLoginBodyVo body = new WSSMLoginBodyVo();
		WSHeaderVo header = result.getHeader();
		result.getHeader().setCallId(callId);
		result.setBody(body);
		try {
			log.debug("登录WS");
			UserSessionVo userSessionVo = new UserSessionVo();
			FNSessionVo fnSessionVo = new FNSessionVo();
			userSessionVo.setUserVo(userVo);
			CEObjectStore ceObjectStore = CEObjectStoreUtil.getCEObjectStore(userVo.getShortName(), userVo.getPassword(), false);
			User user = SecurityHelper.getUser(ceObjectStore.getOs());
			fnSessionVo.setCeObjectStore(ceObjectStore);
			String sessionId = UUID.randomUUID().toString();
			userSessionVo.setSessionId(sessionId);
			userSessionVo.setFnSessionVo(fnSessionVo);
			SessionManageUtil.getInstance().putSession(sessionId, userSessionVo);
			body.setSessionId(sessionId);
			log.debug("登录WS完成");
		} catch (Exception e) {
			e.printStackTrace();
			log.error(e);
			header.setResultCode("EC10001");
			header.setMessage(e.getLocalizedMessage());
		}

		return result;
	}

	/* (非 Javadoc)
	 * <p>Title: logoff</p>
	 * <p>Description: </p>
	 * @param callId
	 * @param sessionId
	 * @see com.zhjoe.filenet.webservice.ISessionManageService#logoff(java.lang.String, java.lang.String)
	 */
	public WSSMLogoffResultVo logoff(String callId, String sessionId) {

		WSSMLogoffResultVo result = new WSSMLogoffResultVo();
		WSSMLogoffBodyVo body = new WSSMLogoffBodyVo();
		WSHeaderVo header = result.getHeader();
		result.getHeader().setCallId(callId);
		result.setBody(body);
		try {
			log.debug("登出WS");
			SessionManageUtil.getInstance().removeSession(sessionId);
			log.debug("sessionId = " + sessionId);
			log.debug("登出WS完成");
		} catch (Exception e) {
			e.printStackTrace();
			log.error(e);
			header.setResultCode("EC10001");
			header.setMessage(e.getLocalizedMessage());
		}
		return result;
	}

}
