package com.zhjoe.filenet.webservice.impl;

import java.io.InputStream;
import java.util.*;

import javax.activation.DataHandler;
import javax.jws.WebService;

import com.filenet.api.admin.Choice;
import com.filenet.api.collection.*;
import com.filenet.api.constants.ChoiceType;
import com.filenet.api.constants.ComponentRelationshipType;
import com.filenet.api.constants.VersionBindType;
import com.filenet.api.core.*;
import com.filenet.api.security.User;
import com.zhjoe.ccpws.constant.Constants;
import com.zhjoe.ccpws.service.ContentManageService;
import com.zhjoe.ccpws.util.SessionManageUtil;
import com.zhjoe.ccpws.vo.*;
import com.zhjoe.strongit.filenet.service.ce.CEHelper;

import com.zhjoe.strongit.filenet.service.ce.SecurityHelper;
import com.zhjoe.strongit.filenet.service.comparator.ComparatorFolder;
import com.zhjoe.strongit.filenet.service.datatype.*;
import com.zhjoe.strongit.filenet.service.exception.CustomWebServiceException;
import com.zhjoe.strongit.filenet.service.util.*;
import net.sf.ezmorph.object.DateMorpher;
import net.sf.json.JsonConfig;
import net.sf.json.util.JSONUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.time.DateFormatUtils;
import org.apache.cxf.jaxrs.ext.multipart.InputStreamDataSource;
import org.apache.log4j.Logger;

import com.zhjoe.filenet.webservice.ICommonContentManageBusinessService;
import com.filenet.api.constants.RefreshMode;
import com.filenet.api.property.Properties;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

@WebService(serviceName = "commonService",//对外发布的服务名
        targetNamespace = "http://service.common.filenet.com",//指定你想要的名称空间，通常使用使用包名反转
        endpointInterface = "com.zhjoe.filenet.webservice.ICommonContentManageBusinessService")//服务接口全路径, 指定做SEI（Service EndPoint Interface）服务端点接口
@Service
public class CommonContentManageBusinessService implements ICommonContentManageBusinessService {
    // 常量
    protected static final String D = "document";
    protected static final String F = "folder";
    protected static final String A = "all";

    private static Logger log = Logger.getLogger(CommonContentManageBusinessService.class);
    @Autowired
    private ContentManageService contentManageService;

    /*
     * (非 Javadoc) <p>Title: createFolder</p> <p>Description: </p>
     *
     * @param callId
     *
     * @param sessionId
     *
     * @param path
     *
     * @param name
     *
     * @return
     *
     * @see
     * com.ccpws.business.ICommonContentManageBusinessService#createFolder(java.
     * lang.String, java.lang.String, java.lang.String, java.lang.String)
     */
    public WSCSCreateFolderResultVo createFolder(String callId, String sessionId, String path, String name) {
        String message = "正在创建文件夹,请注意:";
        log.debug(message);
        WSCSCreateFolderResultVo result = new WSCSCreateFolderResultVo();
        WSCSCreateFolderBodyVo body = new WSCSCreateFolderBodyVo();
        WSHeaderVo header = result.getHeader();
        result.getHeader().setCallId(callId);
        result.setBody(body);

        try {
            // 参数校验区域
            if (StringUtils.isEmpty(path)) {
                throw new CustomWebServiceException(message + "父路径path不能为空");
            }
            if (StringUtils.isEmpty(name)) {
                throw new CustomWebServiceException(message + "文件夹名称name不能为空");
            }
            ObjectStore os = SessionManageUtil.getInstance().getSession(sessionId).getFnSessionVo().getCeObjectStore()
                    .getOs();
            Folder folder = CEHelper.createSubFolderInfo(os, path, name);
            result.getBody().setFolderId(folder.get_Id().toString());
            result.getBody().setFolderName(folder.get_FolderName());
            result.getBody().setFolderPath(folder.get_PathName());
        }  catch (Exception e) {
            e.printStackTrace();
            log.error(e);
            header.setResultCode("EC20101");
            header.setMessage(e.getLocalizedMessage());
        }
        return result;
    }

    /**
     *
     * @Title: retrievePropertyInfosBySymbolicName @Description:
     *         根据类名称读取文档类的属性 @param @param callId @param @param
     *         sessionId @param @param symbolicName @param @return 设定文件 @return
     *         WSCSRetrieveDocumentpropertysResultVo 返回类型 @throws
     */
    public WSCSRetrieveDocumentpropertysResultVo retrievePropertyInfosBySymbolicName(String callId, String sessionId,
                                                                                     String symbolicName) {
        String message = "正在查询文档类" + symbolicName + "的相关属性,请注意:";
        log.debug(message);
        WSCSRetrieveDocumentpropertysResultVo result = new WSCSRetrieveDocumentpropertysResultVo();
        WSCSRetrieveDocumentpropertysBodyVo body = new WSCSRetrieveDocumentpropertysBodyVo();
        List<PropertyInfo> propertyInfos = new ArrayList<PropertyInfo>();
        WSHeaderVo header = result.getHeader();
        result.getHeader().setCallId(callId);
        result.setBody(body);
        try {
            ObjectStore os = SessionManageUtil.getInstance().getSession(sessionId).getFnSessionVo().getCeObjectStore()
                    .getOs();
            propertyInfos = ClassPropertiesUtil.getClassPropertyDescriptions(os, symbolicName);
            body.setPropertyInfos(propertyInfos);
        } catch (Exception e) {
            e.printStackTrace();
            log.error(e);
            header.setResultCode("EC20202");
            header.setMessage(e.getLocalizedMessage());
        }
        return result;
    }

    /*
     * (非 Javadoc) <p>Title: retrieveDocumentInfos</p> <p>Description: </p>
     *
     * @param callId
     *
     * @param sessionId
     *
     * @param path
     *
     * @param pagination
     *
     * @return
     *
     * @see com.ccpws.business.ICommonContentManageBusinessService#
     * retrieveDocumentInfos(java.lang.String, java.lang.String,
     * java.lang.String, com.ccpws.service.datatype.Pagination)
     */
    public WSCSRetrieveDocumentResultVo retrieveDocumentInfos(String callId, String sessionId, String path,
                                                              Pagination pagination) {
        String message = "正在查询文件夹下文档列表,请注意:";
        log.debug(message);
        WSCSRetrieveDocumentResultVo result = new WSCSRetrieveDocumentResultVo();
        WSCSRetrieveDocumentBodyVo body = new WSCSRetrieveDocumentBodyVo();
        List<DocumentInfo> documents = new ArrayList<DocumentInfo>();
        body.setDocument(documents);
        WSHeaderVo header = result.getHeader();
        result.getHeader().setCallId(callId);
        result.setBody(body);

        try {
            ObjectStore os = SessionManageUtil.getInstance().getSession(sessionId).getFnSessionVo().getCeObjectStore()
                    .getOs();
            // 参数校验区域
            if (StringUtils.isEmpty(path)) {
                throw new CustomWebServiceException(message + "父路径path不能为空");
            }
            if (pagination == null) {
                pagination = new Pagination();
                pagination.setCurrentPage(1);
                pagination.setObjectsPerpage(5000);
            } else {
                if (pagination.getCurrentPage() == 0 || pagination.getObjectsPerpage() == 0) {
                    throw new CustomWebServiceException(
                            message + "在pagination != null的情况下currentPage和objectsPerpage应不等于0");
                }
            }
            log.debug(message + "参数校验通过");
            String whereClause = "IsCurrentVersion = TRUE AND this INFOLDER '" + path + "'";
            log.debug(message + "查询当前目录下的文档,查询条件:" + whereClause);
            RetrievalResult retrievalResult = CEHelper.retrieveDocumentInfos(os, "Document", true, "*", null,
                    whereClause, "DocumentTitle", pagination);
            List objectList = retrievalResult.getObjectList();
            log.debug(message + "查询当前目录下的文档,待返回数量:" + objectList.size() + ",实际数量:" + pagination.getTotalObjects());
            for (int i = 0; i < objectList.size(); i++) {
                Document document = (Document) objectList.get(i);
                DocumentInfo documentInfo = new DocumentInfo();
                documentInfo.fromDocument(document);
                documents.add(documentInfo);
            }
        } catch (Exception e) {
            e.printStackTrace();
            log.error(e);
            header.setResultCode("EC20203");
            header.setMessage(e.getLocalizedMessage());
        }
        return result;
    }

    /**
     * @Title: retrieveDocumentInfos @Description:
     *         根据文档类查询文件夹下的文档列表 @param @param callId @param @param
     *         sessionId @param @param className @param @param
     *         path @param @param pagination @param @return 设定文件 @return
     *         WSCSRetrieveDocumentResultVo 返回类型 @throws
     */
    public WSCSRetrieveDocumentAndPropertyInfosResultVo retrieveDocumentInfosByClassName(String callId,
                                                                                         String sessionId, String className, String path, String docName, Pagination pagination) {
        String message = "正在根据文档类查询文件夹下文档列表,请注意:";
        log.debug(message);
        WSCSRetrieveDocumentAndPropertyInfosResultVo result = new WSCSRetrieveDocumentAndPropertyInfosResultVo();
        WSCSRetrieveDocumentAndPropertyInfosBodyVo body = new WSCSRetrieveDocumentAndPropertyInfosBodyVo();
        List<ArchivesDocumentInfo> documents = new ArrayList<ArchivesDocumentInfo>();
        body.setDocument(documents);
        WSHeaderVo header = result.getHeader();
        result.getHeader().setCallId(callId);
        result.setBody(body);
        try {
            ObjectStore os = SessionManageUtil.getInstance().getSession(sessionId).getFnSessionVo().getCeObjectStore()
                    .getOs();
            // 参数校验区域
            if (StringUtils.isEmpty(path)) {
                throw new CustomWebServiceException(message + "父路径path不能为空");
            }
            if (pagination == null) {
                pagination = new Pagination();
                pagination.setCurrentPage(1);
                pagination.setObjectsPerpage(99999999);
            } else {
                if (pagination.getCurrentPage() == 0 || pagination.getObjectsPerpage() == 0) {
                    throw new CustomWebServiceException(
                            message + "在pagination != null的情况下currentPage和objectsPerpage应不等于0");
                }
            }
            log.debug(message + "参数校验通过");
            String whereClause = "IsCurrentVersion = TRUE AND this INFOLDER '" + path + "'";
            if (docName != null && !"".equals(docName)) {
                whereClause += " AND DocumentTitle like '%" + docName + "%'";
            }
            log.debug(message + "查询当前目录下的文档,查询条件:" + whereClause);
            RetrievalResult retrievalResult = CEHelper.retrieveDocumentInfos(os, className, true, "*", null,
                    whereClause, "DocumentTitle", pagination);
            List objectList = retrievalResult.getObjectList();
            log.debug(message + "查询当前目录下的文档,待返回数量:" + objectList.size() + ",实际数量:" + pagination.getTotalObjects());
            for (int i = 0; i < objectList.size(); i++) {
                Document document = (Document) objectList.get(i);
                ArchivesDocumentInfo documentInfo = new ArchivesDocumentInfo();
                documentInfo.fromDocument(document);
                WSCSRetrieveDocumentpropertysResultVo propertysResultVo = retrieveDocumentProperties(callId, sessionId,
                        document.get_Id().toString());
                documentInfo.setPropertyInfos(propertysResultVo.getBody().getPropertyInfos());
                documents.add(documentInfo);
            }
            result.setPagination(pagination);
        } catch (Exception e) {
            e.printStackTrace();
            log.error(e);
            header.setResultCode("EC20204");
            header.setMessage(e.getLocalizedMessage());
        }
        return result;
    }

    /*
     * (非 Javadoc) <p>Title: retrieveFolderInfos</p> <p>Description: </p>
     *
     * @param callId
     *
     * @param sessionId
     *
     * @param path
     *
     * @param pagination
     *
     * @return
     *
     * @see com.ccpws.business.ICommonContentManageBusinessService#
     * retrieveFolderInfos(java.lang.String, java.lang.String, java.lang.String,
     * com.ccpws.service.datatype.Pagination)
     */
    public WSCSRetrieveFolderResultVo retrieveFolderInfos(String callId, String sessionId, String path,
                                                          Pagination pagination) {
        String message = "正在查询文件夹下列表,请注意:";
        log.debug(message);
        WSCSRetrieveFolderResultVo result = new WSCSRetrieveFolderResultVo();
        WSCSRetrieveFolderBodyVo body = new WSCSRetrieveFolderBodyVo();
        List<FolderInfo> folders = new ArrayList<FolderInfo>();
        body.setFolder(folders);
        WSHeaderVo header = result.getHeader();
        result.getHeader().setCallId(callId);
        result.setBody(body);

        try {
            ObjectStore os = SessionManageUtil.getInstance().getSession(sessionId).getFnSessionVo().getCeObjectStore()
                    .getOs();
            // 参数校验区域
            if (StringUtils.isEmpty(path)) {
                throw new CustomWebServiceException(message + "父路径path不能为空");
            }
            if (pagination == null) {
                pagination = new Pagination();
                pagination.setCurrentPage(1);
                pagination.setObjectsPerpage(5000);
            } else {
                if (pagination.getCurrentPage() == 0 || pagination.getObjectsPerpage() == 0) {
                    throw new CustomWebServiceException(
                            message + "在pagination != null的情况下currentPage和objectsPerpage应不等于0");
                }
            }
            log.debug(message + "参数校验通过");
            String whereClause = "this INFOLDER '" + path + "'";
            log.debug(message + "查询当前目录下的文件夹,查询条件:" + whereClause);
            RetrievalResult retrievalResult = CEHelper.retrieveFolderInfos(os, Constants.folderClass, false, "*",
                    whereClause, "FolderName", pagination);
            List objectList = retrievalResult.getObjectList();
            log.debug(message + "查询当前目录下的文件夹,待返回数量:" + objectList.size() + ",实际数量:" + pagination.getTotalObjects());
            for (int i = 0; i < objectList.size(); i++) {
                Folder folder = (Folder) objectList.get(i);
                FolderInfo folderInfo = new FolderInfo();
                folderInfo.fromFolder(folder);
                folders.add(folderInfo);
            }
        } catch (Exception e) {
            e.printStackTrace();
            log.error(e);
            header.setResultCode("EC20205");
            header.setMessage(e.getLocalizedMessage());
        }
        return result;
    }

    /*
     * (非 Javadoc) <p>Title: bulkDownload</p> <p>Description: </p>
     *
     * @param callId
     *
     * @param sessionId
     *
     * @param ids
     *
     * @return
     *
     * @see
     * com.ccpws.business.ICommonContentManageBusinessService#bulkDownload(java.
     * lang.String, java.lang.String, java.util.List)
     */
    public WSCSBulkDownloadResultVo bulkDownload(String callId, String sessionId, List<String> ids) {
        String message = "正在批量下载文档,请注意:";
        log.debug(message);
        WSCSBulkDownloadResultVo result = new WSCSBulkDownloadResultVo();
        WSCSBulkDownloadBodyVo body = new WSCSBulkDownloadBodyVo();
        List<WSCSBulkUDDocumentVo> files = new ArrayList<WSCSBulkUDDocumentVo>();
        body.setFileDatas(files);
        WSHeaderVo header = result.getHeader();
        result.getHeader().setCallId(callId);
        result.setBody(body);
        try {
            ObjectStore os = SessionManageUtil.getInstance().getSession(sessionId).getFnSessionVo().getCeObjectStore()
                    .getOs();
            // 参数校验区域
            if (ids == null || ids.isEmpty()) {
                throw new CustomWebServiceException(message + "ids 不能为空");
            }
            log.debug(message + "参数校验通过");
            for (int i = 0; i < ids.size(); i++) {
                WSCSBulkUDDocumentVo file = new WSCSBulkUDDocumentVo();
                file.setOperation("download");
                file.setStatus("success");
                String id = ids.get(i);
                try {
                    Document document = CEHelper.getDocumentInfo(os, id);
                    String documentTitle = document.getProperties().getStringValue("DocumentTitle");
                    file.setFileName(documentTitle);
                    ContentElementList contentElements = document.get_ContentElements();
                    if (contentElements != null && !contentElements.isEmpty()) {
                        ContentTransfer contentTransfer = (ContentTransfer) contentElements.get(0);
                        String contentType = contentTransfer.get_ContentType();
                        InputStream accessContentStream = contentTransfer.accessContentStream();
                        InputStreamDataSource inputStreamDataSource = new InputStreamDataSource(accessContentStream,
                                contentType);
                        file.setFileType(contentTransfer.get_ContentType());
                        file.setFileData(new DataHandler(inputStreamDataSource));
                    } else {
                        file.setMessage(message + id + "没有内容");
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                    log.error(e);
                    file.setStatus("failure");
                    file.setMessage(message + id + "没有该文档");
                }
                files.add(file);
            }
        } catch (Exception e) {
            e.printStackTrace();
            log.error(e);
            header.setResultCode("EC20301");
            header.setMessage(e.getLocalizedMessage());
        }
        return result;
    }


    /**
     *
     * @Title: bulkUpload4DocumentWithProps @Description: 上传 @param @param
     *         callId @param @param sessionId @param @param
     *         symbolicName @param @param path @param @param
     *         uploadType @param @param propertyInfos @param @param
     *         files @param @return 设定文件 @return WSCSBulkUploadResultVo
     *         返回类型 @throws
     */
    public WSCSBulkUploadResultVo bulkUpload4DocumentWithProps(String callId, String sessionId, String symbolicName,
                                                               String path, String uploadType, List<PropertyInfo> propertyInfos, List<WSCSBulkUDDocumentVo> files,
                                                               String documentTitle, List<Grantee> granteeList) {
        if (symbolicName == null || symbolicName == "") {
            throw new CustomWebServiceException("上传类型SymbolicName 不能为空！");
        }
        String message = "正在批量上传" + symbolicName + "类型的文档,请注意:";
        log.debug(message);
        WSCSBulkUploadResultVo result = new WSCSBulkUploadResultVo();
        WSCSBulkUploadBodyVo body = new WSCSBulkUploadBodyVo();
        WSHeaderVo header = result.getHeader();
        result.getHeader().setCallId(callId);
        body.setFiles(files);
        result.setBody(body);
        try {
            ObjectStore os = SessionManageUtil.getInstance().getSession(sessionId).getFnSessionVo().getCeObjectStore()
                    .getOs();
            String shortName = SessionManageUtil.getInstance().getSession(sessionId).getShortName();
            Map<String, String> documentIds = new HashMap<String, String>();
            if (uploadType == null) {
                throw new CustomWebServiceException(message + "上传类型不能为空");
            }
            // 参数校验区域
            if (files == null) {
                throw new CustomWebServiceException(message + "files 不能为空");
            } else {
                for (int i = 0; i < files.size(); i++) {
                    WSCSBulkUDDocumentVo fileVo = files.get(i);
                    if (fileVo != null) {
                        if (StringUtils.isEmpty(fileVo.getFileName())) {
                            throw new CustomWebServiceException(message + "files--file--fileName 不能为空");
                        }
                    } else {
                        throw new CustomWebServiceException(message + "files--file 不能为空");
                    }
                }
            }
            // 把属性提取出来
            Map<String, Object> props = new HashMap<String, Object>();
            if (propertyInfos != null && propertyInfos.size() > 0) {
                props = ClassPropertiesUtil.getPropertyDescriptions(propertyInfos);
            }
            Folder folder = null;
            if (StringUtils.isEmpty(path)) {
                // path参数为空时通过分类和属性带出路径
                String docClassName = Factory.ClassDefinition.fetchInstance(os, symbolicName, null).get_Id().toString();
                String folderPath = Constants.ROOT_PATH;
                // 路劲不为空
                if (!"".equals(folderPath)) {

                } else {// 取默认路劲
                    folderPath = Constants.ROOT_PATH;
                }
                folder = CEHelper.retrieveFolder(os, folderPath);
                if (folder == null) {
                    throw new CustomWebServiceException(message + "path 路径不存在");
                }
            } else {
                folder = CEHelper.retrieveFolder(os, path);
                if (folder == null) {
                    throw new CustomWebServiceException(message + "path 路径不存在");
                }
            }
            log.debug(message + "参数校验通过");
            if (uploadType.equalsIgnoreCase("ordinary")) {// 普通上传
                for (int i = 0; i < files.size(); i++) {
                    WSCSBulkUDDocumentVo bulkUploadVo = files.get(i);
                    String fileName = bulkUploadVo.getFileName();
                    props.put("DocumentTitle", fileName);
                    List<ContentTransferInfo> contents = new ArrayList<ContentTransferInfo>();
                    if (bulkUploadVo.getFileData() != null) {
                        ContentTransferInfo contentTransferInfo = new ContentTransferInfo();
                        contentTransferInfo.setRetrievalName(fileName);
                        contentTransferInfo.setInputStream(bulkUploadVo.getFileData().getInputStream());
                        contentTransferInfo.setContentType(MIMETypeUtil.getMIMEType(bulkUploadVo.getFileName()));
                        contents.add(contentTransferInfo);
                    }
                    Document newDoc = CEHelper.createDocumentInfo(os, symbolicName, 0, folder, props, contents);
                    if (i == 0) {
                        documentIds.put(newDoc.get_Name(), newDoc.get_Id().toString());

                        if (granteeList != null) {

                            if (granteeList.size() > 0) {

                                newDoc.refresh();
                                for (Grantee grantee : granteeList) {
                                    Document doc = CEHelper.getDocumentInfo(os, newDoc.get_Id().toString());
                                    List<Grantee> list = new ArrayList<Grantee>();
                                    list.add(grantee);
                                    SecurityHelper.setDocumentPermissions(doc, list);
                                }
                            }
                        }

                    }

                    bulkUploadVo.setStatus("success");
                    bulkUploadVo.setOperation("create");
                    bulkUploadVo.setFileData(null);
                }

            } else if (uploadType.equalsIgnoreCase("moreContent")) {// 多内容上传
                List<ContentTransferInfo> contents = new ArrayList<ContentTransferInfo>();
                WSCSBulkUDDocumentVo bulkUploadVo = new WSCSBulkUDDocumentVo();
                for (int i = 0; i < files.size(); i++) {
                    bulkUploadVo = files.get(i);
                    String fileName = bulkUploadVo.getFileName();
                    if (i == 0) {
                        if (!"".equals(documentTitle) && documentTitle != null) {
                            props.put("DocumentTitle", documentTitle);
                        } else {
                            props.put("DocumentTitle", fileName);
                        }

                    }
                    if (bulkUploadVo.getFileData() != null) {
                        ContentTransferInfo contentTransferInfo = new ContentTransferInfo();
                        contentTransferInfo.setRetrievalName(fileName);
                        contentTransferInfo.setInputStream(bulkUploadVo.getFileData().getInputStream());
                        contentTransferInfo.setContentType(MIMETypeUtil.getMIMEType(bulkUploadVo.getFileName()));
                        contents.add(contentTransferInfo);
                    }
                }
                Document newDoc = CEHelper.createDocumentInfo(os, symbolicName, 0, folder, props, contents);
                documentIds.put(newDoc.get_Name(), newDoc.get_Id().toString());
                if (granteeList != null) {

                    if (granteeList.size() > 0) {
                        newDoc.refresh();
                        for (Grantee grantee : granteeList) {
                            Document doc = CEHelper.getDocumentInfo(os, newDoc.get_Id().toString());
                            List<Grantee> list = new ArrayList<Grantee>();
                            list.add(grantee);
                            SecurityHelper.setDocumentPermissions(doc, list);
                        }
                    }
                }

                bulkUploadVo.setStatus("success");
                bulkUploadVo.setOperation("create");
                bulkUploadVo.setFileData(null);
            } else {
                List<Document> childs = new ArrayList<Document>();
                Document parent = null;
                for (int i = 0; i < files.size(); i++) {
                    WSCSBulkUDDocumentVo bulkUploadVo = files.get(i);
                    String fileName = bulkUploadVo.getFileName();
                    props.put("DocumentTitle", fileName);
                    List<ContentTransferInfo> contents = new ArrayList<ContentTransferInfo>();
                    if (bulkUploadVo.getFileData() != null) {
                        ContentTransferInfo contentTransferInfo = new ContentTransferInfo();
                        contentTransferInfo.setRetrievalName(fileName);
                        contentTransferInfo.setInputStream(bulkUploadVo.getFileData().getInputStream());
                        contentTransferInfo.setContentType(MIMETypeUtil.getMIMEType(bulkUploadVo.getFileName()));
                        contents.add(contentTransferInfo);
                    }
                    if (i == 0) {
                        parent = CEHelper.createDocumentInfo(os, symbolicName, 1, folder, props, contents);
                        documentIds.put(parent.get_Name(), parent.get_Id().toString());

                        bulkUploadVo.setStatus("success");
                        bulkUploadVo.setOperation("create");
                        bulkUploadVo.setFileData(null);

                        if (granteeList != null) {

                            if (granteeList.size() > 0) {
                                parent.refresh();
                                for (Grantee grantee : granteeList) {
                                    Document doc = CEHelper.getDocumentInfo(os, parent.get_Id().toString());
                                    List<Grantee> list = new ArrayList<Grantee>();
                                    list.add(grantee);
                                    SecurityHelper.setDocumentPermissions(doc, list);
                                }
                            }
                        }
                    } else {
                        Document child = CEHelper.createDocumentInfo(os, symbolicName, 0, folder, props, contents);
                        if (granteeList != null) {

                            if (granteeList.size() > 0) {
                                child.refresh();
                                for (Grantee grantee : granteeList) {
                                    Document doc = CEHelper.getDocumentInfo(os, child.get_Id().toString());
                                    List<Grantee> list = new ArrayList<Grantee>();
                                    list.add(grantee);
                                    SecurityHelper.setDocumentPermissions(doc, list);
                                }
                            }
                        }
                        documentIds.put(child.get_Name(), child.get_Id().toString());
                        bulkUploadVo.setStatus("success");
                        bulkUploadVo.setOperation("create");
                        bulkUploadVo.setFileData(null);
                        childs.add(child);
                    }
                }
                parent.refresh();
                // 为父文档和子文档添加关系
                CEHelper.createCompoundDocument(parent, childs, ComponentRelationshipType.DYNAMIC_CR,
                        VersionBindType.LATEST_MAJOR_VERSION);
            }
            if(documentIds.size() > 0){
                JSONObject jsonObject = JSONObject.fromObject(documentIds);
                body.setDocumentIds(jsonObject.toString());
            }
        } catch (Exception e) {
            e.printStackTrace();
            log.error(e);
            header.setResultCode("EC20402");
            header.setMessage(e.getLocalizedMessage());
        }
        return result;
    }

    public WSCSRetrieveDocumentpropertysResultVo retrieveDocumentProperties(String callId, String sessionId,
                                                                            String id) {
        WSCSRetrieveDocumentpropertysResultVo vo = new WSCSRetrieveDocumentpropertysResultVo();
        WSCSRetrieveDocumentpropertysBodyVo body = new WSCSRetrieveDocumentpropertysBodyVo();
        WSHeaderVo header = vo.getHeader();
        vo.getHeader().setCallId(callId);
        try {
            ObjectStore os = SessionManageUtil.getInstance().getSession(sessionId).getFnSessionVo().getCeObjectStore()
                    .getOs();
            Document document = CEHelper.getDocumentInfo(os, id);
            body.setPropertyInfos(ClassPropertiesUtil.getDocumentPropertyDescriptions(document));
            vo.setBody(body);
        }  catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            log.error(e);
            header.setResultCode("EC20601");
            header.setMessage(e.getLocalizedMessage());
        }
        return vo;
    }

    public WSCSRetrieveDocumentpropertysResultVo updateDocumentProperties(String callId, String sessionId, String id,
                                                                          List<PropertyInfo> propertyInfos) {
        WSCSRetrieveDocumentpropertysResultVo result = new WSCSRetrieveDocumentpropertysResultVo();
        ObjectStore os = SessionManageUtil.getInstance().getSession(sessionId).getFnSessionVo().getCeObjectStore()
                .getOs();
        WSHeaderVo header = result.getHeader();
        result.getHeader().setCallId(callId);
        try {
            Document document = CEHelper.getDocumentInfo(os, id);
            ClassPropertiesUtil.updateProperties(document, propertyInfos);
        }  catch (Exception e) {
            e.printStackTrace();
            log.error(e);
            header.setResultCode("EC20701");
            header.setMessage(e.getLocalizedMessage());
        }
        return result;
    }

    public WSCSRetrieveDocumentpropertysResultVo bulkUpdateDocumentPropertiesById(String callId, String sessionId,
                                                                                  String Ids, String propertys) {
        WSCSRetrieveDocumentpropertysResultVo result = new WSCSRetrieveDocumentpropertysResultVo();
        ObjectStore os = SessionManageUtil.getInstance().getSession(sessionId).getFnSessionVo().getCeObjectStore()
                .getOs();
        WSHeaderVo header = result.getHeader();
        result.getHeader().setCallId(callId);
        try {
            List<List<PropertyInfo>> propertyList = new ArrayList<List<PropertyInfo>>();
            List<String> idList = null;
            // 解析id json串
            JSONArray jsonArray_ids = JSONArray.fromObject(Ids);
            idList = JSONArray.toList(jsonArray_ids, new String(), new JsonConfig());
            // 解析属性 json串
            JSONUtils.getMorpherRegistry()
                    .registerMorpher(new DateMorpher(new String[] { "yyyy-MM-dd", "yyyy-MM-dd HH:mm:ss" }));
            JSONArray jsonArray_propertys = JSONArray.fromObject(propertys);
            for (int i = 0; i < jsonArray_propertys.size(); i++) {
                List<PropertyInfo> list = JSONArray.toList(JSONArray.fromObject(jsonArray_propertys.get(i)),
                        PropertyInfo.class);
                propertyList.add(list);
            }

            for (int i = 0; i < idList.size(); i++) {
                Document document = CEHelper.getDocumentInfo(os, idList.get(i));
                ClassPropertiesUtil.updateProperties(document, propertyList.get(i));
            }
        } catch (Exception e) {
            e.printStackTrace();
            log.error(e);
            header.setResultCode("EC20701");
            header.setMessage(e.getLocalizedMessage());
        }
        return result;
    }

    private void reUpdateProperties(Document document, List<PropertyInfo> propertyInfos) throws Exception {
        Properties properties = document.getProperties();
        for (int i = 0; i < propertyInfos.size(); i++) {
            PropertyInfo propertyInfo = propertyInfos.get(i);
            Integer dataType = propertyInfo.getDataType();
            Integer cardinality = propertyInfo.getCardinality();
            String symbolicName = propertyInfo.getSymbolicName();
            switch (dataType) {
                case 2:
                    if (cardinality == 0) {
                        Boolean propertyBoolean = propertyInfo.getPropertyBoolean();
                        if (propertyBoolean != null) {
                            properties.putValue(symbolicName, propertyBoolean);
                        }
                    } else {
                        List<Boolean> propertyBooleans = propertyInfo.getPropertyBooleans();
                        BooleanList booleanList = Factory.BooleanList.createList();
                        if (propertyBooleans != null && !propertyBooleans.isEmpty()) {
                            booleanList.addAll(propertyBooleans);
                            properties.putValue(symbolicName, booleanList);
                        }
                    }
                    break;
                case 3:
                    if (cardinality == 0) {
                        Date propertyDateTime = propertyInfo.getPropertyDateTime();
                        if (propertyDateTime != null) {
                            properties.putValue(symbolicName, propertyDateTime);
                        }
                    } else {
                        Date[] propertyDateTimes = propertyInfo.getPropertyDateTimes();
                        DateTimeList dateTimeList = Factory.DateTimeList.createList();
                        if (propertyDateTimes != null) {
                            dateTimeList.addAll(Arrays.asList(propertyDateTimes));
                            properties.putValue(symbolicName, dateTimeList);
                        }
                    }
                    break;
                case 4:
                    if (cardinality == 0) {
                        Double propertyFloat64 = propertyInfo.getPropertyFloat64();
                        if (propertyFloat64 != null) {
                            properties.putValue(symbolicName, propertyFloat64);
                        }
                    } else {
                        List<Double> propertyFloat64s = propertyInfo.getPropertyFloat64s();
                        Float64List float64List = Factory.Float64List.createList();
                        if (propertyFloat64s != null && !propertyFloat64s.isEmpty()) {
                            for (int j = 0; j < propertyFloat64s.size(); j++) {
                                float64List.add(Double.parseDouble(propertyFloat64s.get(j) + ""));
                            }
                            properties.putValue(symbolicName, float64List);
                        }
                    }
                    break;
                case 6:
                    if (cardinality == 0) {
                        Integer propertyInteger32 = propertyInfo.getPropertyInteger32();
                        if (propertyInteger32 != null) {
                            properties.putValue(symbolicName, propertyInteger32);
                        }
                    } else {
                        List<Integer> propertyInteger32s = propertyInfo.getPropertyInteger32s();
                        Integer32List integer32List = Factory.Integer32List.createList();
                        if (propertyInteger32s != null && !propertyInteger32s.isEmpty()) {
                            integer32List.addAll(propertyInteger32s);
                            properties.putValue(symbolicName, integer32List);
                        }
                    }
                    break;
                case 8:
                    if (cardinality == 0) {
                        String propertyString = propertyInfo.getPropertyString();
                        if (propertyString != null && !"".equals(propertyString)) {
                            properties.putValue(symbolicName, propertyString);
                        }
                    } else {
                        List<String> propertyStrings = propertyInfo.getPropertyStrings();
                        StringList stringList = Factory.StringList.createList();
                        if (propertyStrings != null && !propertyStrings.isEmpty()) {
                            stringList.addAll(propertyStrings);
                            properties.putValue(symbolicName, stringList);
                        }
                    }
                    break;
            }
        }
        document.save(RefreshMode.REFRESH);
    }

    public WSCSDeleteDocumentResultVo deleteDocument(String callId, String sessionId, String documentId) {
        String message = "正在删除文档,请注意:";
        log.debug(message);
        WSCSDeleteDocumentResultVo result = new WSCSDeleteDocumentResultVo();
        WSCSDeleteDocumentBodyVo body = new WSCSDeleteDocumentBodyVo();
        WSHeaderVo header = result.getHeader();
        result.getHeader().setCallId(callId);
        result.setBody(body);
        try {
            // 参数校验区域
            if (StringUtils.isEmpty(documentId)) {
                throw new CustomWebServiceException(message + "documentId不能为空");
            }
            log.debug(message + "参数校验通过");

            ObjectStore os = SessionManageUtil.getInstance().getSession(sessionId).getFnSessionVo().getCeObjectStore()
                    .getOs();
            Document document = CEHelper.getDocumentInfo(os, documentId);
            CEHelper.deleteDocument(document);
        } catch (Exception e) {
            e.printStackTrace();
            log.error("删除文档异常：" + e.getMessage(), e);
            header.setResultCode("EC20202");
            header.setMessage(e.getLocalizedMessage());
        }
        return result;
    }

    public WSCSSearchDocumentResultVo searchDocumentInfos(String callId, String sessionId,
                                                          List<com.ccpws.vo.SearchConditionVo> searchConditionVos, Pagination pagination, String symbolicName) {
        String message = "正在查询按照文档类的属性查询相应的文档列表,请注意:";
        log.debug(message);
        WSCSSearchDocumentResultVo result = new WSCSSearchDocumentResultVo();
        WSCSSearchDocumentBodyVo body = new WSCSSearchDocumentBodyVo();
        List<DocumentInfo> documents = new ArrayList<DocumentInfo>();
        body.setDocument(documents);
        WSHeaderVo header = result.getHeader();
        result.getHeader().setCallId(callId);
        result.setBody(body);

        try {
            ObjectStore os = SessionManageUtil.getInstance().getSession(sessionId).getFnSessionVo().getCeObjectStore()
                    .getOs();
            // 参数校验区域
            if (pagination == null) {
                pagination = new Pagination();
                pagination.setCurrentPage(1);
                pagination.setObjectsPerpage(5000);
            } else {
                if (pagination.getCurrentPage() == 0 || pagination.getObjectsPerpage() == 0) {
                    throw new CustomWebServiceException(
                            message + "在pagination != null的情况下currentPage和objectsPerpage应不等于0");
                }
            }
            body.setPagination(pagination);
            log.debug(message + "参数校验通过");
            if (!searchConditionVos.isEmpty()) {
                String whereClause = whereClause(null, searchConditionVos);
                log.debug(message + "查询当前目录下的文档,查询条件:" + whereClause);
                RetrievalResult retrievalResult = CEHelper.retrieveDocumentInfos(os, symbolicName, true, "*", null,
                        whereClause + " and IsCurrentVersion = TRUE", null, pagination);
                List objectList = retrievalResult.getObjectList();
                log.debug(message + "查询当前目录下的文档,待返回数量:" + objectList.size() + ",实际数量:" + pagination.getTotalObjects());
                for (int i = 0; i < objectList.size(); i++) {
                    Document document = (Document) objectList.get(i);
                    DocumentInfo documentInfo = new DocumentInfo();
                    documentInfo.fromDocument(document);
                    documents.add(documentInfo);
                }
            }
        }  catch (Exception e) {
            e.printStackTrace();
            log.error(e);
            header.setResultCode("EC20802");
            header.setMessage(e.getLocalizedMessage());
        }
        return result;
    }

    private String whereClause(Folder folder, List<com.ccpws.vo.SearchConditionVo> conditions) {
        String whereClause = "";
        if (folder != null) {
            whereClause = "this INFOLDER " + "'" + folder.get_Id() + "'";
        } else {
            whereClause = "1 = 1";
        }
        if (conditions != null && !conditions.isEmpty()) {
            for (int i = 0; i < conditions.size(); i++) {
                com.ccpws.vo.SearchConditionVo searchConditionVo = conditions.get(i);

                String propertyName = searchConditionVo.getPropertyName();
                String condition = searchConditionVo.getCondition();
                Integer dataType = searchConditionVo.getDataType();
                switch (dataType) {
                    case 2:
                        Boolean propertyBoolean = searchConditionVo.getPropertyBoolean();
                        whereClause += " AND " + propertyName + " " + condition + " " + propertyBoolean;
                        break;
                    case 3:
                        Date propertyDateTime = searchConditionVo.getPropertyDateTime();
                        whereClause += " AND " + propertyName + " " + condition + " "
                                + DateFormatUtils.format(propertyDateTime, "yyyy-MM-dd");
                        ;
                        break;
                    case 4:
                        Double propertyFloat64 = searchConditionVo.getPropertyFloat64();
                        whereClause += " AND " + propertyName + " " + condition + " " + propertyFloat64;
                        break;
                    case 6:
                        Integer propertyInteger32 = searchConditionVo.getPropertyInteger32();
                        whereClause += " AND " + propertyName + " " + condition + " " + propertyInteger32;
                        break;
                    case 8:
                        String propertyString = searchConditionVo.getPropertyString();
                        if (condition.equalsIgnoreCase("like")) {
                            whereClause += " AND " + propertyName + " " + condition + "'%" + propertyString + "%'";

                        } else {
                            whereClause += " AND " + propertyName + " " + condition + "'" + propertyString + "'";
                        }
                        break;

                }
            }
        }
        return whereClause;
    }

    public WSCSDocumentPermissionResultVo getDocumentPermission(String callId, String sessionId, String documentId) {
        String message = "正在查询文档的权限,请注意:";
        log.debug(message);
        WSCSDocumentPermissionResultVo result = new WSCSDocumentPermissionResultVo();
        WSCSDocumentPermissionBodyVo body = new WSCSDocumentPermissionBodyVo();
        List<Grantee> grantees = new ArrayList<Grantee>();
        body.setGrantees(grantees);
        WSHeaderVo header = result.getHeader();
        result.getHeader().setCallId(callId);
        result.setBody(body);
        try {
            ObjectStore os = SessionManageUtil.getInstance().getSession(sessionId).getFnSessionVo().getCeObjectStore()
                    .getOs();
            // 参数校验区域
            if (StringUtils.isEmpty(documentId)) {
                throw new CustomWebServiceException(message + "documentId不能为空");
            }
            log.debug(message + "参数校验通过");
            Document doc = CEHelper.getDocumentInfo(os, documentId);
            grantees.addAll(SecurityHelper.getDocumentPermissions(doc, null));
        } catch (Exception e) {
            e.printStackTrace();
            log.error(e);
            header.setResultCode("EC20207");
            header.setMessage(e.getLocalizedMessage());
        }
        return result;
    }

    public WSCSDocumentPermissionResultVo setDocumentPermission(String callId, String sessionId, String documentId,
                                                                List<Grantee> granteeList) {
        String message = "正在更改文档的权限,请注意:";
        log.debug(message);
        WSCSDocumentPermissionResultVo result = new WSCSDocumentPermissionResultVo();
        WSCSDocumentPermissionBodyVo body = new WSCSDocumentPermissionBodyVo();
        WSHeaderVo header = result.getHeader();
        result.getHeader().setCallId(callId);
        result.setBody(body);
        try {
            ObjectStore os = SessionManageUtil.getInstance().getSession(sessionId).getFnSessionVo().getCeObjectStore()
                    .getOs();
            // 参数校验区域
            if (StringUtils.isEmpty(documentId)) {
                throw new CustomWebServiceException(message + "documentId不能为空");
            }
            if (granteeList == null || granteeList.size() <= 0) {
                throw new CustomWebServiceException(message + "granteeList不能为空");
            }
            log.debug(message + "参数校验通过");

            for (Grantee grantee : granteeList) {
                List<Grantee> list = new ArrayList<Grantee>();
                Document doc = CEHelper.getDocumentInfo(os, documentId);
                list.add(grantee);
                boolean isSuc = SecurityHelper.setDocumentPermissions(doc, list);
            }
        } catch (Exception e) {
            e.printStackTrace();
            log.error(e);
            header.setResultCode("EC20208");
            header.setMessage(e.getLocalizedMessage());
        }
        return result;
    }

    public WSCSGetSubFolderResultVo getImmediateSubFolders(String callId, String sessionId, String path) {
        String message = "正在获取文件夹列表,请注意:";
        log.debug(message);
        WSCSGetSubFolderResultVo result = new WSCSGetSubFolderResultVo();
        WSCSGetSubFolderBodyVo body = new WSCSGetSubFolderBodyVo();
        WSHeaderVo header = result.getHeader();
        result.getHeader().setCallId(callId);
        result.setBody(body);
        try {
            ObjectStore os = SessionManageUtil.getInstance().getSession(sessionId).getFnSessionVo().getCeObjectStore()
                    .getOs();
            // 参数校验区域
            if (StringUtils.isEmpty(path)) {
                throw new CustomWebServiceException(message + "path不能为空");
            }
            log.debug(message + "参数校验通过");
            Folder folder = CEHelper.retrieveFolder(os, path);
            List<BaseInfo> folderList = searchChildFolders(folder, null, null);

            BaseInfo parentFolder = ConvertFolder2FolderInfoUtil.convertFolder2FolderInfo(folder);
            body.setParentFolder(parentFolder);
            body.setSubFolders(folderList);
        } catch (Exception e) {
            e.printStackTrace();
            log.error(e);
            header.setResultCode("EC20209");
            header.setMessage(e.getLocalizedMessage());
        }

        return result;
    }

    /**
     * @throws Exception
     * @Title: searchChildFolders
     * @Description: 获取文件夹对象下的文件夹列表
     * @param @param
     *            parentFolder
     * @param @return
     *            设定文件
     * @return List<BaseInfo> 返回类型
     */
    private List<BaseInfo> searchChildFolders(Folder parentFolder, String sort, String order) throws Exception {
        log.debug("查询文件夹下面的子文件夹");
        List<BaseInfo> list = new ArrayList<BaseInfo>();
        ComparatorFolder comparator = new ComparatorFolder();
        comparator.setOrder(order == null ? "asc" : order);
        comparator.setProperty(sort == null ? "FolderName" : sort);
        comparator.setType(1);
        List<Folder> subFolderList = CEHelper.retrieveSubFolderInfos(parentFolder, comparator);
        Iterator<Folder> folerListIterator = subFolderList.iterator();
        while (folerListIterator.hasNext()) {
            Folder folder = folerListIterator.next();
            FolderInfo folderInfo = ConvertFolder2FolderInfoUtil.convertFolder2FolderInfo(folder);
            list.add(folderInfo);
        }
        log.debug("查询文件夹下面的子文件夹完成，数量 " + list.size());
        return list;
    }

    public WSCSFolderPermissionResultVo getFolderPermission(String callId, String sessionId, String id) {
        String message = "正在获取文件夹列表,请注意:";
        log.debug(message);
        WSCSFolderPermissionResultVo result = new WSCSFolderPermissionResultVo();
        WSCSFolderPermissionBodyVo body = new WSCSFolderPermissionBodyVo();
        List<Grantee> granteeList = new ArrayList<Grantee>();
        body.setGranteeList(granteeList);
        WSHeaderVo header = result.getHeader();
        result.getHeader().setCallId(callId);
        result.setBody(body);
        try {
            ObjectStore os = SessionManageUtil.getInstance().getSession(sessionId).getFnSessionVo().getCeObjectStore()
                    .getOs();
            // 参数校验区域
            if (StringUtils.isEmpty(id)) {
                throw new CustomWebServiceException(message + "id不能为空");
            }
            log.debug(message + "参数校验通过");
            Folder folder = CEHelper.getFolderInfo(os, id);
            granteeList.addAll(SecurityHelper.getFolderPermissions(folder, null));
        } catch (Exception e) {
            e.printStackTrace();
            log.error(e);
            header.setResultCode("EC20210");
            header.setMessage(e.getLocalizedMessage());
        }

        return result;
    }

    public WSCSSearchDocumentResultVo searchPagingDocuments(String callId, String sessionId,
                                                            List<com.ccpws.vo.SearchConditionVo> searchConditionVos, Pagination pagination, String symbolicName,
                                                            String parentFolderId, String subFolderFlag) {
        String message = "正在查询按照文档类的属性查询相应的文档列表,请注意:";
        log.debug(message);
        WSCSSearchDocumentResultVo result = new WSCSSearchDocumentResultVo();
        WSCSSearchDocumentBodyVo body = new WSCSSearchDocumentBodyVo();
        List<DocumentInfo> documents = new ArrayList<DocumentInfo>();
        body.setDocument(documents);
        WSHeaderVo header = result.getHeader();
        result.getHeader().setCallId(callId);
        result.setBody(body);

        try {
            ObjectStore os = SessionManageUtil.getInstance().getSession(sessionId).getFnSessionVo().getCeObjectStore()
                    .getOs();
            UserVo userVo = SessionManageUtil.getInstance().getSession(sessionId).getUserVo();
            User user = SecurityHelper.findUser(os, userVo.getShortName());
            userVo.setMemberOfGroups(SecurityHelper.getMemberOfGroups(user));
            // 参数校验区域
            if (pagination == null) {
                pagination = new Pagination();
                pagination.setCurrentPage(1);
                pagination.setObjectsPerpage(5000);
            } else {
                if (pagination.getCurrentPage() == 0 || pagination.getObjectsPerpage() == 0) {
                    throw new CustomWebServiceException(
                            message + "在pagination != null的情况下currentPage和objectsPerpage应不等于0");
                }
            }
            if (!StringUtils.isEmpty(pagination.getSortProperty())) {
                if (StringUtils.isEmpty(pagination.getSortOrderBy())) {
                    throw new CustomWebServiceException("排序字段输入后，则必须选择其是否升序或降序（asc or desc).");
                }
            }
            body.setPagination(pagination);
            log.debug(message + "参数校验通过");
            if (!searchConditionVos.isEmpty()) {
                Folder parentFolder = CEHelper.getFolderInfo(os, parentFolderId);
                String whereClause = whereClause(parentFolder, searchConditionVos);

                if (!StringUtils.isEmpty(subFolderFlag) && subFolderFlag.equals("1")) {
                    whereClause = whereClause.replace("INFOLDER", "INSUBFOLDER");
                }
                log.debug(message + "查询当前目录下的文档,查询条件:" + whereClause);
                // CEHelper.retrieveDocumentInfos(os, className,
                // includeSubclasses, selectList, text, whereClause,
                // orderByClause, pagination)
                String sort = sort(D, pagination.getSortProperty(), pagination.getSortOrderBy());
                RetrievalResult retrievalResult = CEHelper.retrieveDocumentInfos(os, symbolicName, true, "*", null,
                        whereClause + " and IsCurrentVersion = TRUE", sort, pagination);

                List objectList = retrievalResult.getObjectList();
                log.debug(message + "查询当前目录下的文档,待返回数量:" + objectList.size() + ",实际数量:" + pagination.getTotalObjects());
                for (int i = 0; i < objectList.size(); i++) {
                    Document document = (Document) objectList.get(i);
                    DocumentInfo documentInfo = new DocumentInfo();
                    documentInfo.fromDocument(document);
                    List<Grantee> granteeList = SecurityHelper.getDocumentPermissions(document, null);
                    documentInfo.setGranteeList(granteeList);
                    documents.add(documentInfo);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
            log.error(e);
            header.setResultCode("EC20212");
            header.setMessage(e.getLocalizedMessage());
        }
        return result;
    }

    private String sort(String type, String sort, String order) {
        String sortorder = null;
        if (sort != null) {
            if ("name".equalsIgnoreCase(sort)) {
                if (D.equalsIgnoreCase(type)) {
                    sortorder = "DocumentTitle " + order;
                } else if (F.equalsIgnoreCase(type)) {
                    sortorder = "FolderName " + order;
                }
            } else if ("dateLastModified".equalsIgnoreCase(sort)) {
                sortorder = "DateLastModified " + order;
            } else {
                sortorder = sort + " " + order;
            }
        } else {
            if (D.equalsIgnoreCase(type)) {
                sortorder = "DocumentTitle ";
            } else if (F.equalsIgnoreCase(type)) {
                sortorder = "FolderName ";
            }
        }
        return sortorder;
    }

    public WSCSGetFolderResultVo getFolder(String callId, String sessionId, String folderId) {
        String message = "正在获取文件夹,请注意:";
        log.debug(message);
        WSCSGetFolderResultVo result = new WSCSGetFolderResultVo();
        WSCSGetFolderBodyVo body = new WSCSGetFolderBodyVo();
        WSHeaderVo header = result.getHeader();
        result.getHeader().setCallId(callId);
        result.setBody(body);
        try {
            ObjectStore os = SessionManageUtil.getInstance().getSession(sessionId).getFnSessionVo().getCeObjectStore()
                    .getOs();
            // 参数校验区域
            if (StringUtils.isEmpty(folderId)) {
                throw new CustomWebServiceException(message + "folderId不能为空");
            }
            log.debug(message + "参数校验通过");
            Folder folder = CEHelper.getFolderInfo(os, folderId);
            FolderInfo folderInfo = ConvertFolder2FolderInfoUtil.convertFolder2FolderInfo(folder);
            body.setFolder(folderInfo);
        } catch (Exception e) {
            e.printStackTrace();
            log.error(e);
            header.setResultCode("EC20213");
            header.setMessage(e.getLocalizedMessage());
        }

        return result;
    }

    public WSCSSearchDocumentResultVo fullTextSearch(String callId, String sessionId, String keyWords,
                                                     Pagination pagination, String symbolicName, String parentFolderId, String subFolderFlag) {
        String message = "正在查询按照文档类的属性查询相应的文档列表,请注意:";
        log.debug(message);
        WSCSSearchDocumentResultVo result = new WSCSSearchDocumentResultVo();
        WSCSSearchDocumentBodyVo body = new WSCSSearchDocumentBodyVo();
        List<DocumentInfo> documents = new ArrayList<DocumentInfo>();
        body.setDocument(documents);
        WSHeaderVo header = result.getHeader();
        result.getHeader().setCallId(callId);
        result.setBody(body);

        try {
            ObjectStore os = SessionManageUtil.getInstance().getSession(sessionId).getFnSessionVo().getCeObjectStore()
                    .getOs();
            UserVo userVo = SessionManageUtil.getInstance().getSession(sessionId).getUserVo();
            User user = SecurityHelper.findUser(os, userVo.getShortName());
            userVo.setMemberOfGroups(SecurityHelper.getMemberOfGroups(user));
            // 参数校验区域
            if (pagination == null) {
                pagination = new Pagination();
                pagination.setCurrentPage(1);
                pagination.setObjectsPerpage(5000);
            } else {
                if (pagination.getCurrentPage() == 0 || pagination.getObjectsPerpage() == 0) {
                    throw new CustomWebServiceException(
                            message + "在pagination != null的情况下currentPage和objectsPerpage应不等于0");
                }
            }
            if (!StringUtils.isEmpty(pagination.getSortProperty())) {
                if (StringUtils.isEmpty(pagination.getSortOrderBy())) {
                    throw new CustomWebServiceException("排序字段输入后，则必须选择其是否升序或降序（asc or desc).");
                }
            }
            body.setPagination(pagination);
            log.debug(message + "参数校验通过");
            Folder folder = CEHelper.getFolderInfo(os, parentFolderId);
            String whereClause = whereClause(folder, null);

            if (!StringUtils.isEmpty(subFolderFlag) && subFolderFlag.equals("1")) {
                whereClause = whereClause.replace("INFOLDER", "INSUBFOLDER");
            }
            String sort = sort(D, pagination.getSortProperty(), pagination.getSortOrderBy());

            RetrievalResult retrievalResult = CEHelper.retrieveDocumentInfosContentSummary(os,
                    Constants.ROOT_CLASS, true, "id,DocumentTitle", keyWords,
                    whereClause + " and IsCurrentVersion = TRUE", sort, pagination);
            List objectList = retrievalResult.getObjectList();
            log.debug(message + "查询当前目录下的文档,待返回数量:" + objectList.size() + ",实际数量:" + pagination.getTotalObjects());
            for (int i = 0; i < objectList.size(); i++) {
                Document document = (Document) objectList.get(i);
                DocumentInfo documentInfo = new DocumentInfo();
                documentInfo.fromDocument(document);
                List<Grantee> granteeList = SecurityHelper.getDocumentPermissions(document, null);
                documentInfo.setGranteeList(granteeList);
                documents.add(documentInfo);
            }

        }catch (Exception e) {
            e.printStackTrace();
            log.error(e);
            header.setResultCode("EC20202");
            header.setMessage(e.getLocalizedMessage());
        }
        return result;
    }
}
