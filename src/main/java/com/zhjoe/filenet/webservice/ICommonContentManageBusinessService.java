/**
 * @Title: ICommonContentManageBusinessService.java
 * @Package com.zhjoe.filenet.webservice
 * @Description: TODO(用一句话描述该文件做什么)
 * @author Changling Jiang
 * @date 2014-3-19 下午03:11:22
 * @version V1.0
 */
package com.zhjoe.filenet.webservice;

import com.zhjoe.ccpws.vo.*;
import com.zhjoe.strongit.filenet.service.datatype.DocumentInfo;
import com.zhjoe.strongit.filenet.service.datatype.Grantee;
import com.zhjoe.strongit.filenet.service.datatype.Pagination;
import com.zhjoe.strongit.filenet.service.datatype.PropertyInfo;

import java.util.List;
import java.util.Map;

import javax.jws.WebParam;
import javax.jws.WebService;


/**
 * @author Changling Jiang
 * @ClassName: ICommonContentManageBusinessService
 * @Description: TODO(这里用一句话描述这个类的作用)
 * @date 2014-3-19 下午03:11:22
 */
@WebService
public interface ICommonContentManageBusinessService {

    /**
     * @Title: createFolder
     * @Description: 创建文件夹
     * @param @param callId
     * @param @param sessionId
     * @param @param path 路径
     * @param @param name 文件夹名称
     * @param @return    设定文件
     * @return WSCSCreateFolderResultVo    返回类型
     * @throws
     */
    public WSCSCreateFolderResultVo createFolder(@WebParam(name="callId") String callId, @WebParam(name="sessionId") String sessionId, @WebParam(name="path") String path, @WebParam(name="name") String name);

    /**
     * @Title: retrieveFolderInfos
     * @Description: 查询文件夹下的文件夹列表
     * @param @param callId
     * @param @param sessionId
     * @param @param path
     * @param @param pagination
     * @param @return    设定文件
     * @return WSCSRetrieveFolderResultVo    返回类型
     * @throws
     */
    public WSCSRetrieveFolderResultVo retrieveFolderInfos(@WebParam(name="callId") String callId, @WebParam(name="sessionId") String sessionId, @WebParam(name="path") String path, @WebParam(name="pagination") Pagination pagination);

    public WSCSGetFolderResultVo getFolder(@WebParam(name="callId") String callId, @WebParam(name="sessionId") String sessionId, @WebParam(name="folderId") String folderId);

    /**
     * @Title: retrieveDocumentInfos
     * @Description: 查询文件夹下的文档列表
     * @param @param callId
     * @param @param sessionId
     * @param @param path
     * @param @param pagination
     * @param @return    设定文件
     * @return WSCSRetrieveDocumentResultVo    返回类型
     * @throws
     */
    public WSCSRetrieveDocumentResultVo retrieveDocumentInfos(@WebParam(name="callId") String callId, @WebParam(name="sessionId") String sessionId, @WebParam(name="path") String path, @WebParam(name="pagination") Pagination pagination);

    /**
     * @Title: retrieveDocumentInfos
     * @Description: 根据文档类查询文件夹下的文档列表
     * @param @param callId
     * @param @param sessionId
     * @param @param className
     * @param @param path
     * @param @param pagination
     * @param @return    设定文件
     * @return WSCSRetrieveDocumentResultVo    返回类型
     * @throws
     */
    public WSCSRetrieveDocumentAndPropertyInfosResultVo retrieveDocumentInfosByClassName(@WebParam(name="callId") String callId, @WebParam(name="sessionId") String sessionId, @WebParam(name="className") String className, @WebParam(name="path") String path, @WebParam(name="docName") String docName, @WebParam(name="pagination") Pagination pagination);

    /**
     * @Title: bulkDownload
     * @Description: 批量下载文档
     * @param @param callId
     * @param @param sessionId
     * @param @param ids
     * @param @return    设定文件
     * @return WSCSBulkDownloadResultVo    返回类型
     * @throws
     */
    public WSCSBulkDownloadResultVo bulkDownload(@WebParam(name="callId") String callId, @WebParam(name="sessionId") String sessionId, @WebParam(name="ids") List<String> ids);

    /**
     *
     * @Title: retrievePropertyInfosBySymbolicName
     * @Description: 根据类名称读取文档类的属性
     * @param @param callId
     * @param @param sessionId
     * @param @param symbolicName
     * @param @return    设定文件
     * @return WSCSRetrieveDocumentpropertysResultVo    返回类型
     * @throws
     */
    public WSCSRetrieveDocumentpropertysResultVo retrievePropertyInfosBySymbolicName(@WebParam(name="callId") String callId,@WebParam(name="sessionId") String sessionId, @WebParam(name="symbolicName")String symbolicName) ;

    /**
     * @Title: bulkUpload4DocumentWithProps
     * @Description: 上传
     * @param @param callId
     * @param @param sessionId
     * @param @param symbolicName
     * @param @param path
     * @param @param uploadType
     * @param @param propertyInfos
     * @param @param files
     * @param @return    设定文件
     * @return WSCSBulkUploadResultVo    返回类型
     * @throws
     */
    public WSCSBulkUploadResultVo bulkUpload4DocumentWithProps(@WebParam(name="callId") String callId, @WebParam(name="sessionId") String sessionId,
                                                               @WebParam(name="symbolicName") String symbolicName, @WebParam(name="path") String path,
                                                               @WebParam(name="uploadType") String uploadType, @WebParam(name="propertyInfos") List<PropertyInfo> propertyInfos,
                                                               @WebParam(name="files")  List<WSCSBulkUDDocumentVo> files, @WebParam(name="documentTitle") String documentTitle, List<Grantee> granteeList);

    /**
     * @Title: bulkUpload4ADC
     * @Description: 档案中心上传
     * @param @param callId
     * @param @param sessionId
     * @param @param symbolicName
     * @param @param uploadType
     * @param @param files
     * @param @return    设定文件
     * @return WSCSBulkUploadResultVo    返回类型
     */
    //public WSCSBulkUploadResultVo bulkUpload4ADC(@WebParam(name="callId") String callId,@WebParam(name="sessionId") String sessionId, @WebParam(name="symbolicName") String symbolicName, @WebParam(name="uploadType") String uploadType,@WebParam(name="files")  List<WSCSBulkUDDocumentVo> files);

    /**
     * @Title: retrieveDocumentProperties
     * @Description: TODO(这里用一句话描述这个方法的作用)
     * @param @param callId
     * @param @param sessionId
     * @param @param id
     * @param @return    设定文件
     * @return WSCSRetrieveDocumentpropertysResultVo    返回类型
     */
    public WSCSRetrieveDocumentpropertysResultVo retrieveDocumentProperties(@WebParam(name="callId") String callId,@WebParam(name="sessionId") String sessionId,@WebParam(name="id") String id);
    /**
     * @Title: updateDocumentProperties
     * @Description: TODO(这里用一句话描述这个方法的作用)
     * @param @param callId
     * @param @param sessionId
     * @param @param id    设定文件
     * @return void    返回类型
     */
    public WSCSRetrieveDocumentpropertysResultVo updateDocumentProperties(@WebParam(name="callId") String callId,@WebParam(name="sessionId") String sessionId,@WebParam(name="id") String id,@WebParam(name="propertyInfos") List<PropertyInfo> propertyInfos);

    public WSCSDeleteDocumentResultVo deleteDocument(@WebParam(name="callId") String callId, @WebParam(name="sessionId") String sessionId, @WebParam(name="documentId") String documentId);

    public WSCSSearchDocumentResultVo searchDocumentInfos(@WebParam(name="callId") String callId, @WebParam(name="sessionId") String sessionId, @WebParam(name="searchConditionVos")List<com.ccpws.vo.SearchConditionVo> searchConditionVos, @WebParam(name="pagination") Pagination pagination, @WebParam(name="symbolicName")String symbolicName);

    public WSCSDocumentPermissionResultVo getDocumentPermission(@WebParam(name="callId") String callId, @WebParam(name="sessionId") String sessionId, @WebParam(name="documentId") String documentId);

    public WSCSDocumentPermissionResultVo setDocumentPermission(@WebParam(name="callId") String callId, @WebParam(name="sessionId") String sessionId,@WebParam(name="documentId") String documentId, @WebParam(name="granteeList") List<Grantee> granteeList);

    public WSCSGetSubFolderResultVo getImmediateSubFolders(@WebParam(name="callId") String callId, @WebParam(name="sessionId") String sessionId, @WebParam(name="path") String path);


    public WSCSFolderPermissionResultVo getFolderPermission(@WebParam(name="callId") String callId, @WebParam(name="sessionId") String sessionId, @WebParam(name="id") String id);

    public WSCSSearchDocumentResultVo searchPagingDocuments(@WebParam(name="callId") String callId,@WebParam(name="sessionId") String sessionId,
                                                            @WebParam(name="searchConditionVos") List<com.ccpws.vo.SearchConditionVo> searchConditionVos, @WebParam(name="pagination") Pagination pagination,
                                                            @WebParam(name="symbolicName") String symbolicName, @WebParam(name="parentFolderId") String parentFolderId,
                                                            @WebParam(name="subFolderFlag") String subFolderFlag);

    public WSCSSearchDocumentResultVo fullTextSearch(@WebParam(name="callId") String callId,@WebParam(name="sessionId") String sessionId,
                                                     @WebParam(name="keyWords") String keyWords, @WebParam(name="pagination") Pagination pagination, @WebParam(name="symbolicName") String symbolicName,
                                                     @WebParam(name="parentFolderId") String parentFolderId, @WebParam(name="subFolderFlag") String subFolderFlag);

}
