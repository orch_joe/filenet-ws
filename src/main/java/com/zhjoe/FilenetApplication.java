package com.zhjoe;

import lombok.extern.slf4j.Slf4j;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.web.servlet.error.ErrorMvcAutoConfiguration;
import org.springframework.boot.web.servlet.ServletComponentScan;
import org.springframework.context.annotation.EnableAspectJAutoProxy;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.transaction.annotation.EnableTransactionManagement;

@SpringBootApplication(exclude = { ErrorMvcAutoConfiguration.class })
@EnableAspectJAutoProxy()
@ServletComponentScan
@EnableTransactionManagement
@EnableAsync
@EnableScheduling
public class FilenetApplication {
	private static Logger logger = LoggerFactory.getLogger(FilenetApplication.class);

	public static void main(String[] args) {
		SpringApplication.run(FilenetApplication.class, args);
		logger.info("filenet project start success");
	}

}
