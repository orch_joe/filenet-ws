/**
 * @Title: WSCSBulkUploadBodyVo.java
 * @Package com.ccpws.vo
 * @Description: TODO(用一句话描述该文件做什么)
 * @author Changling Jiang
 * @date 2014-3-20 下午12:02:13
 * @version V1.0
 */
package com.zhjoe.ccpws.vo;


/**
 * @ClassName: WSCSBulkUploadBodyVo
 * @Description: TODO(这里用一句话描述这个类的作用)
 * @author Changling Jiang
 * @date 2014-3-20 下午12:02:13
 *
 */
public class BulkUpload5BodyVo {

	private String docId;

	public String getDocId() {
		return docId;
	}

	public void setDocId(String docId) {
		this.docId = docId;
	}


}
