/**
 * @Title: WSCSBulkUploadResultVo.java
 * @Package com.ccpws.vo
 * @Description: TODO(用一句话描述该文件做什么)
 * @author Changling Jiang
 * @date 2014-3-20 下午12:01:44
 * @version V1.0
 */
package com.zhjoe.ccpws.vo;

/**
 * @ClassName: WSCSBulkUploadResultVo
 * @Description: TODO(这里用一句话描述这个类的作用)
 * @author Changling Jiang
 * @date 2014-3-20 下午12:01:44
 *
 */
public class WSCSBulkUploadResultVo extends WSBaseResultVo {

	private WSCSBulkUploadBodyVo body;

	/**
	 * @return body
	 */
	public WSCSBulkUploadBodyVo getBody() {
		return body;
	}

	/**
	 * @param body body to set
	 */
	public void setBody(WSCSBulkUploadBodyVo body) {
		this.body = body;
	}

}
