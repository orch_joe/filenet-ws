/**
 * @Title: PermittingDocumentInfo.java
 * @Package com.ccpws.vo
 * @Description: TODO(用一句话描述该文件做什么)
 * @author Changling Jiang
 * @date 2014-4-11 上午10:53:09
 * @version V1.0
 */
package com.zhjoe.ccpws.vo;

import java.util.Date;
import java.util.Map;

import com.filenet.api.core.Document;
import com.filenet.api.property.Properties;

/**
 * @ClassName: PermittingDocumentInfo
 * @Description: TODO(这里用一句话描述这个类的作用)
 * @author Changling Jiang
 * @date 2014-4-11 上午10:53:09
 *
 */
public class PermittingDocumentInfo extends RegularDocDocumentInfo {
	private String agencyName;
	private String documentationType;
	private Date expiredDate;
	private boolean inactive;
	private String permitId;
	private Date remindingDate;
	private String responsiblePerson;
	private String wellName;

	@Override
	public void fromVo(Map<String, Object> map) {
		super.fromVo(map);
		map.put("inactive", isInactive());
		if (getAgencyName() != null) {
			map.put("agencyName", getAgencyName());
		}
		if (getDocumentationType() != null) {
			map.put("documentationType", getDocumentationType());
		}
		if (getExpiredDate() != null) {
			map.put("expiredDate", getExpiredDate());
		}
		if (getPermitId() != null) {
			map.put("permitID", getPermitId());
		}
		if (getRemindingDate() != null) {
			map.put("remindingDate", getRemindingDate());
		}
		if (getResponsiblePerson() != null) {
			map.put("responsiblePerson", getResponsiblePerson());
		}
		if (getWellName() != null) {
			map.put("wellName", getWellName());
		}
	}

	@Override
	protected void fromCustom(Document document) {
		super.fromCustom(document);
		Properties properties = document.getProperties();
		setAgencyName(properties.getStringValue("agencyName"));
		setDocumentationType(properties.getStringValue("documentationType"));
		setExpiredDate(properties.getDateTimeValue("expiredDate"));
		setInactive(properties.getBooleanValue("inactive"));
		setPermitId(properties.getStringValue("permitID"));
		setRemindingDate(properties.getDateTimeValue("remindingDate"));
		setResponsiblePerson(properties.getStringValue("responsiblePerson"));
		setWellName(properties.getStringValue("wellName"));
	}

	/**
	 * @return agencyName
	 */
	public String getAgencyName() {
		return agencyName;
	}

	/**
	 * @param agencyName agencyName to set
	 */
	public void setAgencyName(String agencyName) {
		this.agencyName = agencyName;
	}

	/**
	 * @return documentationType
	 */
	public String getDocumentationType() {
		return documentationType;
	}

	/**
	 * @param documentationType documentationType to set
	 */
	public void setDocumentationType(String documentationType) {
		this.documentationType = documentationType;
	}

	/**
	 * @return expiredDate
	 */
	public Date getExpiredDate() {
		return expiredDate;
	}

	/**
	 * @param expiredDate expiredDate to set
	 */
	public void setExpiredDate(Date expiredDate) {
		this.expiredDate = expiredDate;
	}

	/**
	 * @return inactive
	 */
	public boolean isInactive() {
		return inactive;
	}

	/**
	 * @param inactive inactive to set
	 */
	public void setInactive(boolean inactive) {
		this.inactive = inactive;
	}

	/**
	 * @return permitId
	 */
	public String getPermitId() {
		return permitId;
	}

	/**
	 * @param permitId permitId to set
	 */
	public void setPermitId(String permitId) {
		this.permitId = permitId;
	}

	/**
	 * @return remindingDate
	 */
	public Date getRemindingDate() {
		return remindingDate;
	}

	/**
	 * @param remindingDate remindingDate to set
	 */
	public void setRemindingDate(Date remindingDate) {
		this.remindingDate = remindingDate;
	}

	/**
	 * @return responsiblePerson
	 */
	public String getResponsiblePerson() {
		return responsiblePerson;
	}

	/**
	 * @param responsiblePerson responsiblePerson to set
	 */
	public void setResponsiblePerson(String responsiblePerson) {
		this.responsiblePerson = responsiblePerson;
	}

	/**
	 * @return wellName
	 */
	public String getWellName() {
		return wellName;
	}

	/**
	 * @param wellName wellName to set
	 */
	public void setWellName(String wellName) {
		this.wellName = wellName;
	}

}
