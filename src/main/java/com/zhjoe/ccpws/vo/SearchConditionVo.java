package com.ccpws.vo;

import java.util.Date;

import org.apache.commons.lang.StringUtils;

/**
 * @ClassName: SearchConditionVo
 * @Description: TODO(这里用一句话描述这个类的作用)
 * @author Changling Jiang
 * @date 2014年9月9日 上午11:28:59
 * 
 */
public class SearchConditionVo {

    private String propertyName;
    private Integer dataType; //2 3 4 6 8
    private String condition;// = < <= > >= ...
    private Boolean propertyBoolean; //2
    private Date propertyDateTime; //3
    private Double propertyFloat64; //4
    private Integer propertyInteger32; //6
    private String propertyString; //8

    /**
     * @return propertyName
     */
    public String getPropertyName() {
        return propertyName;
    }
    /**
     * @param propertyName propertyName to set
     */
    public void setPropertyName(String propertyName) {
        this.propertyName = propertyName;
    }
    /**
     * @return dataType
     */
    public Integer getDataType() {
        return dataType;
    }
    /**
     * @param dataType dataType to set
     */
    public void setDataType(Integer dataType) {
        this.dataType = dataType;
    }
    /**
     * @return condition
     */
    public String getCondition() {
        return condition;
    }
    /**
     * @param condition condition to set
     */
    public void setCondition(String condition) {
        this.condition = condition;
    }
    /**
     * @return propertyBoolean
     */
    public Boolean getPropertyBoolean() {
        return propertyBoolean;
    }
    /**
     * @param propertyBoolean propertyBoolean to set
     */
    public void setPropertyBoolean(Boolean propertyBoolean) {
        this.propertyBoolean = propertyBoolean;
    }
    /**
     * @return propertyDateTime
     */
    public Date getPropertyDateTime() {
        return propertyDateTime;
    }
    /**
     * @param propertyDateTime propertyDateTime to set
     */
    public void setPropertyDateTime(Date propertyDateTime) {
        this.propertyDateTime = propertyDateTime;
    }
    /**
     * @return propertyFloat64
     */
    public Double getPropertyFloat64() {
        return propertyFloat64;
    }
    /**
     * @param propertyFloat64 propertyFloat64 to set
     */
    public void setPropertyFloat64(Double propertyFloat64) {
        this.propertyFloat64 = propertyFloat64;
    }
    /**
     * @return propertyInteger32
     */
    public Integer getPropertyInteger32() {
        return propertyInteger32;
    }
    /**
     * @param propertyInteger32 propertyInteger32 to set
     */
    public void setPropertyInteger32(Integer propertyInteger32) {
        this.propertyInteger32 = propertyInteger32;
    }
    /**
     * @return propertyString
     */
    public String getPropertyString() {
        return propertyString;
    }
    /**
     * @param propertyString propertyString to set
     */
    public void setPropertyString(String propertyString) {
        this.propertyString = propertyString;
    }
    
    
}

