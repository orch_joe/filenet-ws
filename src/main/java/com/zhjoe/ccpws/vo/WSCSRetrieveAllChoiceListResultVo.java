/**
 * @Title: WSCSFindUsersResultVo.java
 * @Package com.ccpws.vo
 * @Description: TODO(用一句话描述该文件做什么)
 * @author Changling Jiang
 * @date 2014年5月20日 下午2:26:31
 * @version V1.0
 */
package com.zhjoe.ccpws.vo;

/**
 * @ClassName: WSCSFindUsersResultVo
 * @Description: TODO(这里用一句话描述这个类的作用)
 * @author Changling Jiang
 * @date 2014年5月20日 下午2:26:31
 * 
 */
public class WSCSRetrieveAllChoiceListResultVo extends WSBaseResultVo {

	private WSCSRetrieveAllChoiceListBodyVo body;

	/**
	 * @return body
	 */
	public WSCSRetrieveAllChoiceListBodyVo getBody() {
		return body;
	}

	/**
	 * @param body body to set
	 */
	public void setBody(WSCSRetrieveAllChoiceListBodyVo body) {
		this.body = body;
	}

	
	
}
