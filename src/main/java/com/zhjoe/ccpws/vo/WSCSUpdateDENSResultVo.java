/**
 * @Title: WSCSUpdateDENSResultVo.java
 * @Package com.ccpws.vo
 * @Description: TODO(用一句话描述该文件做什么)
 * @author Changling Jiang
 * @date 2014-3-3 上午10:26:35
 * @version V1.0
 */
package com.zhjoe.ccpws.vo;

/**
 * @ClassName: WSCSUpdateDENSResultVo
 * @Description: TODO(这里用一句话描述这个类的作用)
 * @author Changling Jiang
 * @date 2014-3-3 上午10:26:35
 *
 */
public class WSCSUpdateDENSResultVo extends WSBaseResultVo {

	private WSCSUpdateDENSBodyVo body;

	/**
	 * @return body
	 */
	public WSCSUpdateDENSBodyVo getBody() {
		return body;
	}

	/**
	 * @param body body to set
	 */
	public void setBody(WSCSUpdateDENSBodyVo body) {
		this.body = body;
	}

}
