/**
 * @Title: WSBaseResultVo.java
 * @Package com.ccpws.vo
 * @Description: TODO(用一句话描述该文件做什么)
 * @author Changling Jiang
 * @date 2014-2-18 下午12:58:50
 * @version V1.0
 */
package com.zhjoe.ccpws.vo;

/**
 * @ClassName: WSBaseResultVo
 * @Description: TODO(这里用一句话描述这个类的作用)
 * @author Changling Jiang
 * @date 2014-2-18 下午12:58:50
 *
 */
public class WSBaseResultVo {

	private WSHeaderVo header = new WSHeaderVo();

	/**
	 * @return header
	 */
	public WSHeaderVo getHeader() {
		return header;
	}

	/**
	 * @param header header to set
	 */
	public void setHeader(WSHeaderVo header) {
		this.header = header;
	}

}
