/**
 * @Title: WSCSBulkDownloadBodyVo.java
 * @Package com.ccpws.vo
 * @Description: TODO(用一句话描述该文件做什么)
 * @author Changling Jiang
 * @date 2014-3-19 下午06:01:24
 * @version V1.0
 */
package com.zhjoe.ccpws.vo;

import java.util.List;

/**
 * @ClassName: WSCSBulkDownloadBodyVo
 * @Description: TODO(这里用一句话描述这个类的作用)
 * @author Changling Jiang
 * @date 2014-3-19 下午06:01:24
 *
 */
public class WSCSBulkDownloadBodyVo {

	private List<WSCSBulkUDDocumentVo> fileDatas;

	/**
	 * @return fileDatas
	 */
	public List<WSCSBulkUDDocumentVo> getFileDatas() {
		return fileDatas;
	}

	/**
	 * @param fileDatas fileDatas to set
	 */
	public void setFileDatas(List<WSCSBulkUDDocumentVo> fileDatas) {
		this.fileDatas = fileDatas;
	}

}
