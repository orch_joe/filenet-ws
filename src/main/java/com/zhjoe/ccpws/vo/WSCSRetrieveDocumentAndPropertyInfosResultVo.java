package com.zhjoe.ccpws.vo;

import com.zhjoe.strongit.filenet.service.datatype.Pagination;

public class WSCSRetrieveDocumentAndPropertyInfosResultVo extends WSBaseResultVo{
	private WSCSRetrieveDocumentAndPropertyInfosBodyVo body;
	private Pagination pagination;

	public WSCSRetrieveDocumentAndPropertyInfosBodyVo getBody() {
		return body;
	}

	public void setBody(WSCSRetrieveDocumentAndPropertyInfosBodyVo body) {
		this.body = body;
	}

	public Pagination getPagination() {
		return pagination;
	}

	public void setPagination(Pagination pagination) {
		this.pagination = pagination;
	}
	
}
