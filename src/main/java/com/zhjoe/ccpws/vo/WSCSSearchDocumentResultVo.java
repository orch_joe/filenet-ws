package com.zhjoe.ccpws.vo;

public class WSCSSearchDocumentResultVo extends WSBaseResultVo {
    private WSCSSearchDocumentBodyVo body;

    public WSCSSearchDocumentBodyVo getBody() {
        return body;
    }

    public void setBody(WSCSSearchDocumentBodyVo body) {
        this.body = body;
    }
}
