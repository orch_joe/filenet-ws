package com.zhjoe.ccpws.vo;

import java.util.List;

import com.zhjoe.strongit.filenet.service.datatype.Grantee;

public class WSCSDocumentPermissionBodyVo {
    private List<Grantee> grantees;

    public List<Grantee> getGrantees() {
        return grantees;
    }

    public void setGrantees(List<Grantee> grantees) {
        this.grantees = grantees;
    }
}
