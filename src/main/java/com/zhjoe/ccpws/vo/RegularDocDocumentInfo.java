/**
 * @Title: RegularDocDocumentInfo.java
 * @Package com.ccpws.vo
 * @Description: TODO(用一句话描述该文件做什么)
 * @author Changling Jiang
 * @date 2014-3-26 下午05:48:51
 * @version V1.0
 */
package com.zhjoe.ccpws.vo;

import java.util.Map;

import com.filenet.api.core.Document;

/**
 * @ClassName: RegularDocDocumentInfo
 * @Description: TODO(这里用一句话描述这个类的作用)
 * @author Changling Jiang
 * @date 2014-3-26 下午05:48:51
 *
 */
public class RegularDocDocumentInfo extends PbDocDocumentInfo {

	/* (非 Javadoc)
	 * <p>Title: fromCustom</p>
	 * <p>Description: </p>
	 * @param document
	 * @see com.ccpws.service.datatype.DocumentInfo#fromCustom(com.filenet.api.core.Document)
	 */
	@Override
	protected void fromCustom(Document document) {
		super.fromCustom(document);
	}

	/* (非 Javadoc)
	 * <p>Title: fromVo</p>
	 * <p>Description: </p>
	 * @param map
	 * @see com.ccpws.service.datatype.DocumentInfo#fromVo(java.util.Map)
	 */
	@Override
	public void fromVo(Map<String, Object> map) {
		super.fromVo(map);
	}

}
