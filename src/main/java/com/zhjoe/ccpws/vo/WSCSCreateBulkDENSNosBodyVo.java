/**
 * @Title: WSCSCreateBulkDENSNosBodyVo.java
 * @Package com.ccpws.vo
 * @Description: TODO(用一句话描述该文件做什么)
 * @author Changling Jiang
 * @date 2014-2-18 下午05:44:04
 * @version V1.0
 */
package com.zhjoe.ccpws.vo;

import java.util.HashMap;
import java.util.List;

/**
 * @ClassName: WSCSCreateBulkDENSNosBodyVo
 * @Description: TODO(这里用一句话描述这个类的作用)
 * @author Changling Jiang
 * @date 2014-2-18 下午05:44:04
 *
 */
public class WSCSCreateBulkDENSNosBodyVo {

	private List<String> seqno;

	/**
	 * @return seqno
	 */
	public List<String> getSeqno() {
		return seqno;
	}

	/**
	 * @param seqno seqno to set
	 */
	public void setSeqno(List<String> seqno) {
		this.seqno = seqno;
	}

}
