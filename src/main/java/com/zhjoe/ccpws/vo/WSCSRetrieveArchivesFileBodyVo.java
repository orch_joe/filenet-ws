package com.zhjoe.ccpws.vo;

import java.util.List;

public class WSCSRetrieveArchivesFileBodyVo {
	
	private List<ArchivesFileVo> archivesFiles;

	public List<ArchivesFileVo> getArchivesFiles() {
		return archivesFiles;
	}

	public void setArchivesFiles(List<ArchivesFileVo> archivesFiles) {
		this.archivesFiles = archivesFiles;
	}
	
}
