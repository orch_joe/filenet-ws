package com.zhjoe.ccpws.vo;

public class WSCSFolderPermissionResultVo extends WSBaseResultVo{
    private WSCSFolderPermissionBodyVo body;

    /**
     * @return body
     */
    public WSCSFolderPermissionBodyVo getBody() {
        return body;
    }

    /**
     * @param body body to set
     */
    public void setBody(WSCSFolderPermissionBodyVo body) {
        this.body = body;
    }
}
