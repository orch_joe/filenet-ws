/**
 * @Title: WSDBExecuteSQLQueryBatchProcessingResultVo.java
 * @Package com.ccpws.vo
 * @Description: TODO(用一句话描述该文件做什么)
 * @author Changling Jiang
 * @date 2014-4-2 下午06:55:48
 * @version V1.0
 */
package com.zhjoe.ccpws.vo;

/**
 * @ClassName: WSDBExecuteSQLQueryBatchProcessingResultVo
 * @Description: TODO(这里用一句话描述这个类的作用)
 * @author Changling Jiang
 * @date 2014-4-2 下午06:55:48
 *
 */
public class WSDBExecuteSQLQueryBatchProcessingResultVo extends WSBaseResultVo {

	private WSDBExecuteSQLQueryBatchProcessingBodyVo body;

	/**
	 * @return body
	 */
	public WSDBExecuteSQLQueryBatchProcessingBodyVo getBody() {
		return body;
	}

	/**
	 * @param body body to set
	 */
	public void setBody(WSDBExecuteSQLQueryBatchProcessingBodyVo body) {
		this.body = body;
	}

}
