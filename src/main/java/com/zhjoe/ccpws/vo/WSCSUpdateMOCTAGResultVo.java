/**
 * @Title: WSCSUpdateMOCTAGResultVo.java
 * @Package com.ccpws.vo
 * @Description: TODO(用一句话描述该文件做什么)
 * @author Changling Jiang
 * @date 2014-3-10 下午02:26:24
 * @version V1.0
 */
package com.zhjoe.ccpws.vo;

/**
 * @ClassName: WSCSUpdateMOCTAGResultVo
 * @Description: TODO(这里用一句话描述这个类的作用)
 * @author Changling Jiang
 * @date 2014-3-10 下午02:26:24
 *
 */
public class WSCSUpdateMOCTAGResultVo extends WSBaseResultVo {

	private WSCSUpdateMOCTAGBodyVo body;

	/**
	 * @return body
	 */
	public WSCSUpdateMOCTAGBodyVo getBody() {
		return body;
	}

	/**
	 * @param body body to set
	 */
	public void setBody(WSCSUpdateMOCTAGBodyVo body) {
		this.body = body;
	}

}
