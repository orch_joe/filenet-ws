/**
 * @Title: WSCSCreateDENSResultVo.java
 * @Package com.ccpws.vo
 * @Description: TODO(用一句话描述该文件做什么)
 * @author Changling Jiang
 * @date 2014-2-25 下午12:35:14
 * @version V1.0
 */
package com.zhjoe.ccpws.vo;

/**
 * @ClassName: WSCSCreateDENSResultVo
 * @Description: TODO(这里用一句话描述这个类的作用)
 * @author Changling Jiang
 * @date 2014-2-25 下午12:35:14
 *
 */
public class WSCSCreateDENSResultVo extends WSBaseResultVo {

	private WSCSCreateDENSBodyVo body;

	/**
	 * @return body
	 */
	public WSCSCreateDENSBodyVo getBody() {
		return body;
	}

	/**
	 * @param body body to set
	 */
	public void setBody(WSCSCreateDENSBodyVo body) {
		this.body = body;
	}

}
