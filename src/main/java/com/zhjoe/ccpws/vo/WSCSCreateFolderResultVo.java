/**
 * @Title: WSCSCreateFolderResultVo.java
 * @Package com.ccpws.vo
 * @Description: TODO(用一句话描述该文件做什么)
 * @author Changling Jiang
 * @date 2014-3-19 下午03:17:10
 * @version V1.0
 */
package com.zhjoe.ccpws.vo;

/**
 * @ClassName: WSCSCreateFolderResultVo
 * @Description: TODO(这里用一句话描述这个类的作用)
 * @author Changling Jiang
 * @date 2014-3-19 下午03:17:10
 *
 */
public class WSCSCreateFolderResultVo extends WSBaseResultVo {

	private WSCSCreateFolderBodyVo body;

	public WSCSCreateFolderBodyVo getBody() {
		return body;
	}

	public void setBody(WSCSCreateFolderBodyVo body) {
		this.body = body;
	}
}
