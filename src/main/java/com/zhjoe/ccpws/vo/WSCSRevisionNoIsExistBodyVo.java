/**
 * @Title: WSCSRevisionNoIsExistBodyVo.java
 * @Package com.ccpws.vo
 * @Description: TODO(用一句话描述该文件做什么)
 * @author Changling Jiang
 * @date 2014-4-3 下午04:35:48
 * @version V1.0
 */
package com.zhjoe.ccpws.vo;

/**
 * @ClassName: WSCSRevisionNoIsExistBodyVo
 * @Description: TODO(这里用一句话描述这个类的作用)
 * @author Changling Jiang
 * @date 2014-4-3 下午04:35:48
 *
 */
public class WSCSRevisionNoIsExistBodyVo {

	private boolean exist;

	/**
	 * @return exist
	 */
	public boolean isExist() {
		return exist;
	}

	/**
	 * @param exist exist to set
	 */
	public void setExist(boolean exist) {
		this.exist = exist;
	}


}
