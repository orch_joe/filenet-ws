/**
 * @Title: WSCSRetrieveFolderBodyVo.java
 * @Package com.ccpws.vo
 * @Description: TODO(用一句话描述该文件做什么)
 * @author Changling Jiang
 * @date 2014-3-25 上午10:22:35
 * @version V1.0
 */
package com.zhjoe.ccpws.vo;

import com.zhjoe.strongit.filenet.service.datatype.FolderInfo;

import java.util.List;


/**
 * @ClassName: WSCSRetrieveFolderBodyVo
 * @Description: TODO(这里用一句话描述这个类的作用)
 * @author Changling Jiang
 * @date 2014-3-25 上午10:22:35
 *
 */
public class WSCSRetrieveFolderBodyVo {

	private List<FolderInfo> folder;

	/**
	 * @return folder
	 */
	public List<FolderInfo> getFolder() {
		return folder;
	}

	/**
	 * @param folder folder to set
	 */
	public void setFolder(List<FolderInfo> folder) {
		this.folder = folder;
	}

}
