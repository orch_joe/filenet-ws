/**
 * @Title: WSCSChoiceListBodyVo.java
 * @Package com.ccpws.vo
 * @Description: TODO(用一句话描述该文件做什么)
 * @author Changling Jiang
 * @date 2014-2-18 下午01:57:50
 * @version V1.0
 */
package com.zhjoe.ccpws.vo;

import com.zhjoe.strongit.filenet.service.datatype.ChoiceInfo;

import java.util.List;


/**
 * @ClassName: WSCSChoiceListBodyVo
 * @Description: TODO(这里用一句话描述这个类的作用)
 * @author Changling Jiang
 * @date 2014-2-18 下午01:57:50
 *
 */
public class WSCSChoiceListBodyVo {

	private List<ChoiceInfo> choiceInfo;

	/**
	 * @return choiceInfo
	 */
	public List<ChoiceInfo> getChoiceInfo() {
		return choiceInfo;
	}

	/**
	 * @param choiceInfo choiceInfo to set
	 */
	public void setChoiceInfo(List<ChoiceInfo> choiceInfo) {
		this.choiceInfo = choiceInfo;
	}


}
