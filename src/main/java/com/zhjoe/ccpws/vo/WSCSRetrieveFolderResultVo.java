/**
 * @Title: WSCSRetrieveFolderResultVo.java
 * @Package com.ccpws.vo
 * @Description: TODO(用一句话描述该文件做什么)
 * @author Changling Jiang
 * @date 2014-3-25 上午10:22:05
 * @version V1.0
 */
package com.zhjoe.ccpws.vo;

/**
 * @ClassName: WSCSRetrieveFolderResultVo
 * @Description: TODO(这里用一句话描述这个类的作用)
 * @author Changling Jiang
 * @date 2014-3-25 上午10:22:05
 *
 */
public class WSCSRetrieveFolderResultVo extends WSBaseResultVo {

	private WSCSRetrieveFolderBodyVo body;

	/**
	 * @return body
	 */
	public WSCSRetrieveFolderBodyVo getBody() {
		return body;
	}

	/**
	 * @param body body to set
	 */
	public void setBody(WSCSRetrieveFolderBodyVo body) {
		this.body = body;
	}

}
