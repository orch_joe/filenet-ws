/**
 * @Title: WSCSCreateRevisionResultVo.java
 * @Package com.ccpws.vo
 * @Description: TODO(用一句话描述该文件做什么)
 * @author Changling Jiang
 * @date 2014-3-6 上午11:04:04
 * @version V1.0
 */
package com.zhjoe.ccpws.vo;

/**
 * @ClassName: WSCSCreateRevisionResultVo
 * @Description: TODO(这里用一句话描述这个类的作用)
 * @author Changling Jiang
 * @date 2014-3-6 上午11:04:04
 *
 */
public class WSCSCreateRevisionResultVo extends WSBaseResultVo {

	private WSCSCreateRevisionBodyVo body;

	/**
	 * @return body
	 */
	public WSCSCreateRevisionBodyVo getBody() {
		return body;
	}

	/**
	 * @param body body to set
	 */
	public void setBody(WSCSCreateRevisionBodyVo body) {
		this.body = body;
	}

}
