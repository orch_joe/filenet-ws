/**
 * @Title: WSCSCreateBulkDENSNosResultVo.java
 * @Package com.ccpws.vo
 * @Description: TODO(用一句话描述该文件做什么)
 * @author Changling Jiang
 * @date 2014-2-18 下午05:32:46
 * @version V1.0
 */
package com.zhjoe.ccpws.vo;

/**
 * @ClassName: WSCSCreateBulkDENSNosResultVo
 * @Description: TODO(这里用一句话描述这个类的作用)
 * @author Changling Jiang
 * @date 2014-2-18 下午05:32:46
 *
 */
public class WSCSCreateBulkDENSNosResultVo extends WSBaseResultVo {

	private WSCSCreateBulkDENSNosBodyVo body;

	/**
	 * @return body
	 */
	public WSCSCreateBulkDENSNosBodyVo getBody() {
		return body;
	}

	/**
	 * @param body body to set
	 */
	public void setBody(WSCSCreateBulkDENSNosBodyVo body) {
		this.body = body;
	}

}
