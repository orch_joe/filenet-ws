/**
 * @Title: MaintenanceDocumentInfo.java
 * @Package com.ccpws.vo
 * @Description: Maintenance文档类
 * @author Ke Cui
 * @date 2014年4月18日 下午4:00:22
 * @version V1.0
 */
package com.zhjoe.ccpws.vo;

import java.util.Date;
import java.util.Map;

import com.filenet.api.core.Document;

/**
 * @ClassName: MaintenanceDocumentInfo
 * @Description: Maintenance文档类
 * @author Ke Cui
 * @date 2014年4月18日 下午4:00:22
 *
 */
public class MaintenanceDocumentInfo extends RegularDocDocumentInfo {

	private String category;
	private String eventName;
	private String lvl2;
	private String lvl3;
	private String lvl4;
	private String lvl5;
	private String lvlDesc;
	private String notificationNo;
	private String orderNo;
	private Date workDate;

	@Override
	protected void fromCustom(Document document) {
		super.fromCustom(document);
		setCategory(document.getProperties().getStringValue("Category"));
		setEventName(document.getProperties().getStringValue("EventName"));
		setLvl2(document.getProperties().getStringValue("LVL2"));
		setLvl3(document.getProperties().getStringValue("LVL3"));
		setLvl4(document.getProperties().getStringValue("LVL4"));
		setLvl5(document.getProperties().getStringValue("LVL5"));
		setLvlDesc(document.getProperties().getStringValue("LVLDesc"));
		setNotificationNo(document.getProperties().getStringValue("NotificationNo"));
		setOrderNo(document.getProperties().getStringValue("OrderNo"));
		setWorkDate(document.getProperties().getDateTimeValue("WorkDate"));
	}


	public void fromVo(Map<String, Object> map) {
		super.fromVo(map);
		if (getCategory() != null) {
			map.put("Category", getCategory());
		}
		if (getEventName() != null) {
			map.put("EventName", getEventName());
		}
		if (getLvl2() != null) {
			map.put("LVL2", getLvl2());
		}
		if (getLvl3() != null) {
			map.put("LVL3", getLvl3());
		}
		if (getLvl4() != null) {
			map.put("LVL4", getLvl4());
		}
		if (getLvl5() != null) {
			map.put("LVL5", getLvl5());
		}
		if (getNotificationNo() != null) {
			map.put("NotificationNo", getNotificationNo());
		}
		if (getOrderNo() != null) {
			map.put("OrderNo", getOrderNo());
		}
		if (getWorkDate() != null) {
			map.put("WorkDate", getWorkDate());
		}
		if (getLvlDesc() != null) {
			map.put("LVLDesc", getLvlDesc());
		}
	}

	/**
	 * @return category
	 */
	public String getCategory() {
		return category;
	}

	/**
	 * @param category category to set
	 */
	public void setCategory(String category) {
		this.category = category;
	}

	/**
	 * @return eventName
	 */
	public String getEventName() {
		return eventName;
	}

	/**
	 * @param eventName eventName to set
	 */
	public void setEventName(String eventName) {
		this.eventName = eventName;
	}

	/**
	 * @return lvl2
	 */
	public String getLvl2() {
		return lvl2;
	}

	/**
	 * @param lvl2 lvl2 to set
	 */
	public void setLvl2(String lvl2) {
		this.lvl2 = lvl2;
	}

	/**
	 * @return lvl3
	 */
	public String getLvl3() {
		return lvl3;
	}

	/**
	 * @param lvl3 lvl3 to set
	 */
	public void setLvl3(String lvl3) {
		this.lvl3 = lvl3;
	}

	/**
	 * @return lvl4
	 */
	public String getLvl4() {
		return lvl4;
	}

	/**
	 * @param lvl4 lvl4 to set
	 */
	public void setLvl4(String lvl4) {
		this.lvl4 = lvl4;
	}

	/**
	 * @return lvl5
	 */
	public String getLvl5() {
		return lvl5;
	}

	/**
	 * @param lvl5 lvl5 to set
	 */
	public void setLvl5(String lvl5) {
		this.lvl5 = lvl5;
	}

	/**
	 * @return notificationNo
	 */
	public String getNotificationNo() {
		return notificationNo;
	}

	/**
	 * @param notificationNo notificationNo to set
	 */
	public void setNotificationNo(String notificationNo) {
		this.notificationNo = notificationNo;
	}

	/**
	 * @return orderNo
	 */
	public String getOrderNo() {
		return orderNo;
	}

	/**
	 * @param orderNo orderNo to set
	 */
	public void setOrderNo(String orderNo) {
		this.orderNo = orderNo;
	}

	/**
	 * @return workDate
	 */
	public Date getWorkDate() {
		return workDate;
	}

	/**
	 * @param workDate workDate to set
	 */
	public void setWorkDate(Date workDate) {
		this.workDate = workDate;
	}


	public String getLvlDesc() {
		return lvlDesc;
	}


	public void setLvlDesc(String lvlDesc) {
		this.lvlDesc = lvlDesc;
	}

}
