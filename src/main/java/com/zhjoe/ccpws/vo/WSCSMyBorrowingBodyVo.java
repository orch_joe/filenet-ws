package com.zhjoe.ccpws.vo;

public class WSCSMyBorrowingBodyVo extends WSBaseResultVo {
    private BorrowVo myBorrowVo;
    public BorrowVo getMyBorrowVo() {
        return myBorrowVo;
    }
    public void setMyBorrowVo(BorrowVo myBorrowVo) {
        this.myBorrowVo = myBorrowVo;
    }
}
