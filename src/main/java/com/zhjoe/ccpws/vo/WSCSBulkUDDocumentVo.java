/**
 * @Title: WSCSBulkUDDocumentVo.java
 * @Package com.ccpws.vo
 * @Description: TODO(用一句话描述该文件做什么)
 * @author Changling Jiang
 * @date 2014-3-21 下午12:21:05
 * @version V1.0
 */
package com.zhjoe.ccpws.vo;

import javax.activation.DataHandler;

/**
 * @ClassName: WSCSBulkUDDocumentVo
 * @Description: TODO(这里用一句话描述这个类的作用)
 * @author Changling Jiang
 * @date 2014-3-21 下午12:21:05
 *
 */
public class WSCSBulkUDDocumentVo {
	private String fileName;
	private String fileType;
	private DataHandler fileData;

	private String status;//success||failure
	private String operation;
	private String message;

	/**
	 * @return fileName
	 */
	public String getFileName() {
		return fileName;
	}

	/**
	 * @param fileName fileName to set
	 */
	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	/**
	 * @return fileType
	 */
	public String getFileType() {
		return fileType;
	}

	/**
	 * @param fileType fileType to set
	 */
	public void setFileType(String fileType) {
		this.fileType = fileType;
	}

	/**
	 * @return fileData
	 */
	public DataHandler getFileData() {
		return fileData;
	}

	/**
	 * @param fileData fileData to set
	 */
	public void setFileData(DataHandler fileData) {
		this.fileData = fileData;
	}

	/**
	 * @return status
	 */
	public String getStatus() {
		return status;
	}

	/**
	 * @param status status to set
	 */
	public void setStatus(String status) {
		this.status = status;
	}

	/**
	 * @return operation
	 */
	public String getOperation() {
		return operation;
	}

	/**
	 * @param operation operation to set
	 */
	public void setOperation(String operation) {
		this.operation = operation;
	}

	/**
	 * @return message
	 */
	public String getMessage() {
		return message;
	}

	/**
	 * @param message message to set
	 */
	public void setMessage(String message) {
		this.message = message;
	}

}
