package com.zhjoe.ccpws.vo;

import java.util.List;
import java.util.Map;
import com.filenet.api.core.Document;
import com.zhjoe.strongit.filenet.service.datatype.DocumentInfo;
import com.zhjoe.strongit.filenet.service.datatype.PropertyInfo;

public class ArchivesDocumentInfo extends DocumentInfo {
	
	List<PropertyInfo> propertyInfos;
	
	/* (非 Javadoc) 
	 * <p>Title: fromCustom4CommonDisplay</p> 
	 * <p>Description: </p> 
	 * @param document 
	 * @see com.strongit.filenet.service.datatype.AbstractDocumentInfo#fromCustom(com.filenet.api.core.Document) 
	 */
	
	protected void fromCustom(Document document) {
	}

	/* (非 Javadoc) 
	 * <p>Title: convertProperties2Map</p> 
	 * <p>Description: </p> 
	 * @param map
	 * @param abstractDocumentInfo 
	 * @see com.strongit.filenet.service.datatype.AbstractDocumentInfo#fromVo(java.util.Map, com.strongit.filenet.service.datatype.AbstractDocumentInfo) 
	 */
	
	public void fromVo(Map<String, Object> map) {
		super.fromVo(map);
	}

	public List<PropertyInfo> getPropertyInfos() {
		return propertyInfos;
	}

	public void setPropertyInfos(List<PropertyInfo> propertyInfos) {
		this.propertyInfos = propertyInfos;
	}
}
