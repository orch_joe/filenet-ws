/**
 * @Title: WSCSBulkUploadResultVo.java
 * @Package com.ccpws.vo
 * @Description: TODO(用一句话描述该文件做什么)
 * @author Changling Jiang
 * @date 2014-3-20 下午12:01:44
 * @version V1.0
 */
package com.zhjoe.ccpws.vo;

/**
 * @ClassName: WSCSBulkUploadResultVo
 * @Description: TODO(这里用一句话描述这个类的作用)
 * @author Changling Jiang
 * @date 2014-3-20 下午12:01:44
 *
 */
public class WSCSBulkUpload5ResultVo extends WSBaseResultVo {

	private BulkUpload5BodyVo body;

	public BulkUpload5BodyVo getBody() {
		return body;
	}

	public void setBody(BulkUpload5BodyVo body) {
		this.body = body;
	}


}
