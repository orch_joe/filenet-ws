/**
 * @Title: Xml4SPOVo.java
 * @Package com.ccp.vo
 * @Description: TODO(用一句话描述该文件做什么)
 * @author Shushuai Gu
 * @date 2014年6月10日 下午6:14:32
 * @version V1.0
 */
package com.zhjoe.ccpws.vo;

import java.util.Date;

/**
 * @ClassName: Xml4SPOVo
 * @Description: TODO(这里用一句话描述这个类的作用)
 * @author Shushuai Gu
 * @date 2014年6月10日 下午6:14:32
 *
 */
public class Xml4SPOVo {
	private Integer id;
	private String ceId;
	private String xml;
	private int messageState;
	private Date saveTime;
	private Date sendTime;
	private String lable;

	/**
	 * @return id
	 */
	public Integer getId() {
		return id;
	}

	/**
	 * @param id id to set
	 */
	public void setId(Integer id) {
		this.id = id;
	}

	/**
	 * @return ceId
	 */
	public String getCeId() {
		return ceId;
	}

	/**
	 * @param ceId ceId to set
	 */
	public void setCeId(String ceId) {
		this.ceId = ceId;
	}

	/**
	 * @return xml
	 */
	public String getXml() {
		return xml;
	}

	/**
	 * @param xml xml to set
	 */
	public void setXml(String xml) {
		this.xml = xml;
	}

	/**
	 * @return messageState
	 */
	public int getMessageState() {
		return messageState;
	}

	/**
	 * @param messageState messageState to set
	 */
	public void setMessageState(int messageState) {
		this.messageState = messageState;
	}

	/**
	 * @return the saveTime
	 */
	public Date getSaveTime() {
		return saveTime;
	}

	/**
	 * @param saveTime the saveTime to set
	 */
	public void setSaveTime(Date saveTime) {
		this.saveTime = saveTime;
	}

	/**
	 * @return the sendTime
	 */
	public Date getSendTime() {
		return sendTime;
	}

	/**
	 * @param sendTime the sendTime to set
	 */
	public void setSendTime(Date sendTime) {
		this.sendTime = sendTime;
	}

	/**
	 * @return the lable
	 */
	public String getLable() {
		return lable;
	}

	/**
	 * @param lable the lable to set
	 */
	public void setLable(String lable) {
		this.lable = lable;
	}


}
