package com.zhjoe.ccpws.vo;

import java.util.Date;


/**
 * @ClassName: EmpowerAgentVo
 * @Description: 授权代理人信息类
 * @author Cui Ke
 * @date 2014年10月24日 下午5:02:38
 */
public class EmpowerAgentVo {
	private Integer id;// 主键ID
	private String startDate;// 授权开始时间
	private Date startTime;// 授权开始时间（数据库）
	private String endDate;// 授权截止时间
	private Date endTime;// 授权截止时间（数据库）
	private String agent;// 代理人
	private String type;// 授权类型	日常工作/流程审批/采办业务系统/其他
	private Integer empowerId;// 授权信息表外键关联ID
	private String workflowNum; //流程号

	/**
	 * @return id
	 */
	public Integer getId() {
		return id;
	}

	/**
	 * @param id id to set
	 */
	public void setId(Integer id) {
		this.id = id;
	}

	/**
	 * @return startDate
	 */
	public String getStartDate() {
		return startDate;
	}

	/**
	 * @param startDate startDate to set
	 */
	public void setStartDate(String startDate) {
		this.startDate = startDate;
	}

	/**
	 * @return endDate
	 */
	public String getEndDate() {
		return endDate;
	}

	/**
	 * @param endDate endDate to set
	 */
	public void setEndDate(String endDate) {
		this.endDate = endDate;
	}

	/**
	 * @return agent
	 */
	public String getAgent() {
		return agent;
	}

	/**
	 * @param agent agent to set
	 */
	public void setAgent(String agent) {
		this.agent = agent;
	}

	/**
	 * @return type
	 */
	public String getType() {
		return type;
	}

	/**
	 * @param type type to set
	 */
	public void setType(String type) {
		this.type = type;
	}

	/**
	 * @return empowerId
	 */
	public Integer getEmpowerId() {
		return empowerId;
	}

	/**
	 * @param empowerId empowerId to set
	 */
	public void setEmpowerId(Integer empowerId) {
		this.empowerId = empowerId;
	}

	/**
	 * @return startTime
	 */

	public Date getStartTime() {
		return startTime;
	}

	/**
	 * @param startTime startTime to set
	 */
	public void setStartTime(Date startTime) {
		this.startTime = startTime;
	}

	/**
	 * @return endTime
	 */

	public Date getEndTime() {
		return endTime;
	}

	/**
	 * @param endTime endTime to set
	 */
	public void setEndTime(Date endTime) {
		this.endTime = endTime;
	}

	/**
	 * @return workflowNum
	 */
	public String getWorkflowNum() {
		return workflowNum;
	}

	/**
	 * @param workflowNum workflowNum to set
	 */
	public void setWorkflowNum(String workflowNum) {
		this.workflowNum = workflowNum;
	}
}
