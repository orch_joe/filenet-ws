/**
 * @Title: UserSessionVo.java
 * @Package com.ccpws.vo
 * @Description: TODO(用一句话描述该文件做什么)
 * @author Changling Jiang
 * @date 2014-2-14 上午11:18:59
 * @version V1.0
 */
package com.zhjoe.ccpws.vo;

import com.filenet.api.core.ObjectStore;
import com.zhjoe.strongit.filenet.service.util.UserContextUtil;


/**
 * @ClassName: UserSessionVo
 * @Description: WS UserSession
 * @author Changling Jiang
 * @date 2014-2-14 上午11:18:59
 *
 */
public class UserSessionVo {

	private UserVo userVo;
	private FNSessionVo fnSessionVo;
	private String sessionId;
	private String shortName;

	/**
	 * @return userVo
	 */
	public UserVo getUserVo() {
		return userVo;
	}

	/**
	 * @param userVo userVo to set
	 */
	public void setUserVo(UserVo userVo) {
		this.userVo = userVo;
	}

	/**
	 * @return fnSessionVo
	 */
	public FNSessionVo getFnSessionVo() {
		return fnSessionVo;
	}

	/**
	 * @param fnSessionVo fnSessionVo to set
	 */
	public void setFnSessionVo(FNSessionVo fnSessionVo) {
		this.fnSessionVo = fnSessionVo;
	}

	/**
	 * @return sessionId
	 */
	public String getSessionId() {
		return sessionId;
	}

	/**
	 * @param sessionId sessionId to set
	 */
	public void setSessionId(String sessionId) {
		this.sessionId = sessionId;
	}

	/**
	 * @return shortName
	 */
	public String getShortName() {
		return shortName;
	}

	/**
	 * @param shortName shortName to set
	 */
	public void setShortName(String shortName) {
		this.shortName = shortName;
	}

	/**
	 * @Title: getObjectStore
	 * @Description: 获取ObjectStore对象
	 * @param @return    设定文件
	 * @return ObjectStore    返回类型
	 * @throws
	 */
	public ObjectStore getObjectStore() {
		if (fnSessionVo == null || fnSessionVo.getCeObjectStore() == null) {
			return null;
		}
		UserContextUtil.pushSubject(fnSessionVo.getCeObjectStore().getSubject());
		return fnSessionVo.getCeObjectStore().getOs();
	}

	public void free() {
		if (fnSessionVo != null) {
			fnSessionVo.free();
		}
	}

}
