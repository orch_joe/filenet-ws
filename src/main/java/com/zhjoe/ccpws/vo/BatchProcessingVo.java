/**
 * @Title: BatchProcessingVo.java
 * @Package com.ccpws.vo
 * @Description: TODO(用一句话描述该文件做什么)
 * @author Changling Jiang
 * @date 2014-4-2 上午11:22:35
 * @version V1.0
 */
package com.zhjoe.ccpws.vo;

import java.util.Date;

/**
 * @ClassName: BatchProcessingVo
 * @Description: TODO(这里用一句话描述这个类的作用)
 * @author Changling Jiang
 * @date 2014-4-2 上午11:22:35
 *
 */
public class BatchProcessingVo {

	private String batchId;
	private Date started;
	private Date ended;
	private String batchFile;
	private Integer processed;
	private Integer created;
	private Integer updated;
	private Integer upload;
	private Integer deleted;
	private Integer errors;
	private String status;

	/**
	 * @return batchId
	 */
	public String getBatchId() {
		return batchId;
	}

	/**
	 * @param batchId batchId to set
	 */
	public void setBatchId(String batchId) {
		this.batchId = batchId;
	}

	/**
	 * @return started
	 */
	public Date getStarted() {
		return started;
	}

	/**
	 * @param started started to set
	 */
	public void setStarted(Date started) {
		this.started = started;
	}

	/**
	 * @return ended
	 */
	public Date getEnded() {
		return ended;
	}

	/**
	 * @param ended ended to set
	 */
	public void setEnded(Date ended) {
		this.ended = ended;
	}

	/**
	 * @return batchFile
	 */
	public String getBatchFile() {
		return batchFile;
	}

	/**
	 * @param batchFile batchFile to set
	 */
	public void setBatchFile(String batchFile) {
		this.batchFile = batchFile;
	}

	/**
	 * @return processed
	 */
	public Integer getProcessed() {
		return processed;
	}

	/**
	 * @param processed processed to set
	 */
	public void setProcessed(Integer processed) {
		this.processed = processed;
	}

	/**
	 * @return created
	 */
	public Integer getCreated() {
		return created;
	}

	/**
	 * @param created created to set
	 */
	public void setCreated(Integer created) {
		this.created = created;
	}

	/**
	 * @return updated
	 */
	public Integer getUpdated() {
		return updated;
	}

	/**
	 * @param updated updated to set
	 */
	public void setUpdated(Integer updated) {
		this.updated = updated;
	}

	/**
	 * @return upload
	 */
	public Integer getUpload() {
		return upload;
	}

	/**
	 * @param upload upload to set
	 */
	public void setUpload(Integer upload) {
		this.upload = upload;
	}

	/**
	 * @return deleted
	 */
	public Integer getDeleted() {
		return deleted;
	}

	/**
	 * @param deleted deleted to set
	 */
	public void setDeleted(Integer deleted) {
		this.deleted = deleted;
	}

	/**
	 * @return errors
	 */
	public Integer getErrors() {
		return errors;
	}

	/**
	 * @param errors errors to set
	 */
	public void setErrors(Integer errors) {
		this.errors = errors;
	}

	/**
	 * @return status
	 */
	public String getStatus() {
		return status;
	}

	/**
	 * @param status status to set
	 */
	public void setStatus(String status) {
		this.status = status;
	}


}
