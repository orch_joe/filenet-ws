/**
 * @Title: WSCSDeleteDENSResultVo.java
 * @Package com.ccpws.vo
 * @Description: TODO(用一句话描述该文件做什么)
 * @author Changling Jiang
 * @date 2014-2-19 下午04:48:50
 * @version V1.0
 */
package com.zhjoe.ccpws.vo;

/**
 * @ClassName: WSCSDeleteDENSResultVo
 * @Description: TODO(这里用一句话描述这个类的作用)
 * @author Changling Jiang
 * @date 2014-2-19 下午04:48:50
 *
 */
public class WSCSDeleteDENSResultVo extends WSBaseResultVo {

	private WSCSDeleteDENSBodyVo body;

	/**
	 * @return body
	 */
	public WSCSDeleteDENSBodyVo getBody() {
		return body;
	}

	/**
	 * @param body body to set
	 */
	public void setBody(WSCSDeleteDENSBodyVo body) {
		this.body = body;
	}

}
