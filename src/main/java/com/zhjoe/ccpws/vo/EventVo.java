/**
 * @Title: EventVo.java
 * @Package com.ccp.vo
 * @Description: TODO(用一句话描述该文件做什么)
 * @author Shushuai Gu
 * @date 2014年5月6日 下午3:59:36
 * @version V1.0
 */
package com.zhjoe.ccpws.vo;

import java.util.Date;


/**
 * @ClassName: EventVo
 * @Description: TODO(这里用一句话描述这个类的作用)
 * @author Shushuai Gu
 * @date 2014年5月6日 下午3:59:36
 *
 */
public class EventVo {
	//自增序列
	private int id;
	//ID
	private String ceID;
	//类别
	private String type;
	//VSID
	private String vsId;
	//事件
	private String event;
	//操作人
	private String operator;
	//时间
	private Date operationTime;

	/**
	 * @return id
	 */
	public int getId() {
		return id;
	}

	/**
	 * @param id id to set
	 */
	public void setId(int id) {
		this.id = id;
	}

	/**
	 * @return ceID
	 */
	public String getCeID() {
		return ceID;
	}

	/**
	 * @param ceID ceID to set
	 */
	public void setCeID(String ceID) {
		this.ceID = ceID;
	}

	/**
	 * @return type
	 */
	public String getType() {
		return type;
	}

	/**
	 * @param type type to set
	 */
	public void setType(String type) {
		this.type = type;
	}

	/**
	 * @return vsId
	 */
	public String getVsId() {
		return vsId;
	}

	/**
	 * @param vsId vsId to set
	 */
	public void setVsId(String vsId) {
		this.vsId = vsId;
	}

	/**
	 * @return event
	 */
	public String getEvent() {
		return event;
	}

	/**
	 * @param event event to set
	 */
	public void setEvent(String event) {
		this.event = event;
	}

	/**
	 * @return operator
	 */
	public String getOperator() {
		return operator;
	}

	/**
	 * @param operator operator to set
	 */
	public void setOperator(String operator) {
		this.operator = operator;
	}

	/**
	 * @return operationTime
	 */

	public Date getOperationTime() {
		return operationTime;
	}

	/**
	 * @param operationTime operationTime to set
	 */
	public void setOperationTime(Date operationTime) {
		this.operationTime = operationTime;
	}


}
