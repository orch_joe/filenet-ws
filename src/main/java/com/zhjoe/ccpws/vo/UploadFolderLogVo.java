package com.ccpws.vo;

import java.util.Date;

public class UploadFolderLogVo {

	//自增序列
	private int id;
	//文档名称
	private String docName;
	//文档ID
	private String docId;
	//上传时间
	private Date upTime;
	//上传目标路径
	private String targetPath;
	//新创建路径
	private String createPath;
	//上传是否成功标识   0：成功  1:失败
	private int upFlag;
	//归档信息
	private String upMessage;
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getDocName() {
		return docName;
	}
	public void setDocName(String docName) {
		this.docName = docName;
	}
	public String getDocId() {
		return docId;
	}
	public void setDocId(String docId) {
		this.docId = docId;
	}
	
	public Date getUpTime() {
		return upTime;
	}
	public void setUpTime(Date upTime) {
		this.upTime = upTime;
	}
	public String getTargetPath() {
		return targetPath;
	}
	public void setTargetPath(String targetPath) {
		this.targetPath = targetPath;
	}
	public String getCreatePath() {
		return createPath;
	}
	public void setCreatePath(String createPath) {
		this.createPath = createPath;
	}
	public int getUpFlag() {
		return upFlag;
	}
	public void setUpFlag(int upFlag) {
		this.upFlag = upFlag;
	}
	public String getUpMessage() {
		return upMessage;
	}
	public void setUpMessage(String upMessage) {
		this.upMessage = upMessage;
	}
}
