/**
 * @Title: WSCSRetrieveDocumentpropertysResultVo.java
 * @Package com.ccpws.vo
 * @Description: TODO(用一句话描述该文件做什么)
 * @author Changling Jiang
 * @date 2014-3-25 上午10:23:19
 * @version V1.0
 */
package com.zhjoe.ccpws.vo;

import java.util.List;

import com.zhjoe.strongit.filenet.service.datatype.PropertyInfo;

/**
 * @ClassName: WSCSRetrieveDocumentpropertysResultVo
 * @Description: TODO(这里用一句话描述这个类的作用)
 * @author Changling Jiang
 * @date 2014-3-25 上午10:23:19
 * 
 */
public class WSCSRetrieveDocumentpropertysBodyVo{

	private List<PropertyInfo> propertyInfos;

	/**
	 * @return propertyInfos
	 */
	public List<PropertyInfo> getPropertyInfos() {
		return propertyInfos;
	}

	/**
	 * @param propertyInfos propertyInfos to set
	 */
	public void setPropertyInfos(List<PropertyInfo> propertyInfos) {
		this.propertyInfos = propertyInfos;
	}

	
	
}
