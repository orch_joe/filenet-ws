package com.zhjoe.ccpws.vo;

public class WSCSGetSubFolderResultVo extends WSBaseResultVo {
    private WSCSGetSubFolderBodyVo body;

    /**
     * @return body
     */
    public WSCSGetSubFolderBodyVo getBody() {
        return body;
    }

    /**
     * @param body body to set
     */
    public void setBody(WSCSGetSubFolderBodyVo body) {
        this.body = body;
    }
}
