/**
 * @Title: ShortCutDocumentInfo.java
 * @Package com.ccp.vo
 * @Description: Short Cut
 * @author Changling Jiang
 * @date 2014-4-11 上午09:27:36
 * @version V1.0
 */
package com.zhjoe.ccpws.vo;

import java.util.Map;

import com.filenet.api.core.Document;

/**
 * @ClassName: ShortCutDocumentInfo
 * @Description: TODO(这里用一句话描述这个类的作用)
 * @author Changling Jiang
 * @date 2014-4-11 上午09:27:36
 *
 */
public class ShortCutDocumentInfo extends PbDocDocumentInfo {


	private String sctype;//标识ShortCut的类型：Folder || Document
	private String scclass;//标识Shortcut对应的FEM中的类Folder || Document || DocumentandDrawing
	private String scid;//标识Shortcut的对象id

	@Override
	protected void fromCustom(Document document) {
		super.fromCustom(document);
		setSctype(document.getProperties().getStringValue("sctype"));
		setScclass(document.getProperties().getStringValue("scclass"));
		setScid(document.getProperties().getStringValue("scid"));
	}

	@Override
	public void fromVo(Map<String, Object> map) {
		super.fromVo(map);
		if (getSctype() != null) {
			map.put("sctype", getSctype());
		}
		if (getScclass() != null) {
			map.put("scclass", getScclass());
		}
		if (getScid() != null) {
			map.put("scid", getScid());
		}
	}

	/**
	 * @return sctype
	 */
	public String getSctype() {
		return sctype;
	}

	/**
	 * @param sctype sctype to set
	 */
	public void setSctype(String sctype) {
		this.sctype = sctype;
	}

	/**
	 * @return scclass
	 */
	public String getScclass() {
		return scclass;
	}

	/**
	 * @param scclass scclass to set
	 */
	public void setScclass(String scclass) {
		this.scclass = scclass;
	}

	/**
	 * @return scid
	 */
	public String getScid() {
		return scid;
	}

	/**
	 * @param scid scid to set
	 */
	public void setScid(String scid) {
		this.scid = scid;
	}

}
