/**
 * @Title: WSSMLogoffResultVo.java
 * @Package com.ccpws.vo
 * @Description: TODO(用一句话描述该文件做什么)
 * @author Changling Jiang
 * @date 2014-2-19 下午03:12:32
 * @version V1.0
 */
package com.zhjoe.ccpws.vo;

/**
 * @ClassName: WSSMLogoffResultVo
 * @Description: TODO(这里用一句话描述这个类的作用)
 * @author Changling Jiang
 * @date 2014-2-19 下午03:12:32
 *
 */
public class WSSMLogoffResultVo extends WSBaseResultVo {

	private WSSMLogoffBodyVo body;

	/**
	 * @return body
	 */
	public WSSMLogoffBodyVo getBody() {
		return body;
	}

	/**
	 * @param body body to set
	 */
	public void setBody(WSSMLogoffBodyVo body) {
		this.body = body;
	}

}
