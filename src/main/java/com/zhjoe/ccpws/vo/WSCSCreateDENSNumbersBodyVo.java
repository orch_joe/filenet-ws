/**
 * @Title: WSCSCreateDENSNumbersBodyVo.java
 * @Package com.ccpws.vo
 * @Description: TODO(用一句话描述该文件做什么)
 * @author Changling Jiang
 * @date 2014-3-7 下午01:14:20
 * @version V1.0
 */
package com.zhjoe.ccpws.vo;

import java.util.List;

/**
 * @ClassName: WSCSCreateDENSNumbersBodyVo
 * @Description: TODO(这里用一句话描述这个类的作用)
 * @author Changling Jiang
 * @date 2014-3-7 下午01:14:20
 *
 */
public class WSCSCreateDENSNumbersBodyVo {

	private List<String> densNumber;

	/**
	 * @return densNumber
	 */
	public List<String> getDensNumber() {
		return densNumber;
	}

	/**
	 * @param densNumber densNumber to set
	 */
	public void setDensNumber(List<String> densNumber) {
		this.densNumber = densNumber;
	}
}
