/**
 * @Title: WSCSBulkDownloadResultVo.java
 * @Package com.ccpws.vo
 * @Description: TODO(用一句话描述该文件做什么)
 * @author Changling Jiang
 * @date 2014-3-19 下午06:00:53
 * @version V1.0
 */
package com.zhjoe.ccpws.vo;

/**
 * @ClassName: WSCSBulkDownloadResultVo
 * @Description: TODO(这里用一句话描述这个类的作用)
 * @author Changling Jiang
 * @date 2014-3-19 下午06:00:53
 *
 */
public class WSCSBulkDownloadResultVo extends WSBaseResultVo {

	private WSCSBulkDownloadBodyVo body;

	/**
	 * @return body
	 */
	public WSCSBulkDownloadBodyVo getBody() {
		return body;
	}

	/**
	 * @param body body to set
	 */
	public void setBody(WSCSBulkDownloadBodyVo body) {
		this.body = body;
	}


}
