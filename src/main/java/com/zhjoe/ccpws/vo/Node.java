/**
 * @Title: Node.java
 * @Package com.ccp.vo
 * @Description: TODO(用一句话描述该文件做什么)
 * @author Shushuai Gu
 * @date 2014年1月10日 下午3:56:10
 * @version V1.0
 */
package com.zhjoe.ccpws.vo;

import java.util.List;
import java.util.Map;

/**
 * @ClassName: Node
 * @Description: 树节点
 * @author Shushuai Gu
 * @date 2014年1月10日 下午3:56:10
 * 
 */
public class Node {
	private String id;
	private String text;
	private String state;
	private String path;
	private Map<String, Object> attributes;
	private List<Node> children;
	/**
	 * @return id
	 */
	public String getId() {
		return id;
	}
	/**
	 * @param id id to set
	 */
	public void setId(String id) {
		this.id = id;
	}
	/**
	 * @return text
	 */
	public String getText() {
		return text;
	}
	/**
	 * @param text text to set
	 */
	public void setText(String text) {
		this.text = text;
	}
	/**
	 * @return state
	 */
	public String getState() {
		return state;
	}
	/**
	 * @param state state to set
	 */
	public void setState(String state) {
		this.state = state;
	}
	/**
	 * @return attributes
	 */
	public Map<String, Object> getAttributes() {
		return attributes;
	}
	/**
	 * @param attributes attributes to set
	 */
	public void setAttributes(Map<String, Object> attributes) {
		this.attributes = attributes;
	}
	/**
	 * @return the children
	 */
	public List<Node> getChildren() {
		return children;
	}
	/**
	 * @param children the children to set
	 */
	public void setChildren(List<Node> children) {
		this.children = children;
	}
	public String getPath() {
		return path;
	}
	public void setPath(String path) {
		this.path = path;
	}
	
	
}
