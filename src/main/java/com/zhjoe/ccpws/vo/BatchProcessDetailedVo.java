/**
 * @Title: BatchProcessDetailedVo.java
 * @Package com.ccpws.vo
 * @Description: TODO(用一句话描述该文件做什么)
 * @author Changling Jiang
 * @date 2014-4-2 上午11:29:46
 * @version V1.0
 */
package com.zhjoe.ccpws.vo;

import java.util.Date;

/**
 * @ClassName: BatchProcessDetailedVo
 * @Description: TODO(这里用一句话描述这个类的作用)
 * @author Changling Jiang
 * @date 2014-4-2 上午11:29:46
 *
 */
public class BatchProcessDetailedVo {

	private int id;
	private String batchId;
	private String location;
	private Date operationDate;
	private String operation;
	private String msg;
	private String name;

	/**
	 * @return id
	 */
	public int getId() {
		return id;
	}

	/**
	 * @param id id to set
	 */
	public void setId(int id) {
		this.id = id;
	}

	/**
	 * @return batchId
	 */
	public String getBatchId() {
		return batchId;
	}

	/**
	 * @param batchId batchId to set
	 */
	public void setBatchId(String batchId) {
		this.batchId = batchId;
	}

	/**
	 * @return location
	 */
	public String getLocation() {
		return location;
	}

	/**
	 * @param location location to set
	 */
	public void setLocation(String location) {
		this.location = location;
	}

	/**
	 * @return operationDate
	 */
	public Date getOperationDate() {
		return operationDate;
	}

	/**
	 * @param operationDate operationDate to set
	 */
	public void setOperationDate(Date operationDate) {
		this.operationDate = operationDate;
	}

	/**
	 * @return operation
	 */
	public String getOperation() {
		return operation;
	}

	/**
	 * @param operation operation to set
	 */
	public void setOperation(String operation) {
		this.operation = operation;
	}

	/**
	 * @return msg
	 */
	public String getMsg() {
		return msg;
	}

	/**
	 * @param msg msg to set
	 */
	public void setMsg(String msg) {
		this.msg = msg;
	}

	/**
	 * @return name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param name name to set
	 */
	public void setName(String name) {
		this.name = name;
	}


}
