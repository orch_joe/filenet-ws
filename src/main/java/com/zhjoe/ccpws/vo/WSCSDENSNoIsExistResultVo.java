/**
 * @Title: WSCSDENSNoIsExistResultVo.java
 * @Package com.ccpws.vo
 * @Description: TODO(用一句话描述该文件做什么)
 * @author Changling Jiang
 * @date 2014-2-18 下午03:39:52
 * @version V1.0
 */
package com.zhjoe.ccpws.vo;

/**
 * @ClassName: WSCSDENSNoIsExistResultVo
 * @Description: TODO(这里用一句话描述这个类的作用)
 * @author Changling Jiang
 * @date 2014-2-18 下午03:39:52
 *
 */
public class WSCSDENSNoIsExistResultVo extends WSBaseResultVo {

	private WSCSDENSNoIsExistBodyVo body;

	/**
	 * @return body
	 */
	public WSCSDENSNoIsExistBodyVo getBody() {
		return body;
	}

	/**
	 * @param body body to set
	 */
	public void setBody(WSCSDENSNoIsExistBodyVo body) {
		this.body = body;
	}

}
