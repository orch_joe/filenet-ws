/**
 * @Title: WSCSChoiceListResultVo.java
 * @Package com.ccpws.vo
 * @Description: TODO(用一句话描述该文件做什么)
 * @author Changling Jiang
 * @date 2014-2-18 下午01:05:22
 * @version V1.0
 */
package com.zhjoe.ccpws.vo;

/**
 * @ClassName: WSCSChoiceListResultVo
 * @Description: TODO(这里用一句话描述这个类的作用)
 * @author Changling Jiang
 * @date 2014-2-18 下午01:05:22
 *
 */
public class WSCSChoiceListResultVo extends WSBaseResultVo {

	private WSCSChoiceListBodyVo body;

	/**
	 * @return body
	 */
	public WSCSChoiceListBodyVo getBody() {
		return body;
	}

	/**
	 * @param body body to set
	 */
	public void setBody(WSCSChoiceListBodyVo body) {
		this.body = body;
	}

}
