/**
 * @Title: DeleteDENSVo.java
 * @Package com.ccp.vo
 * @Description: TODO(用一句话描述该文件做什么)
 * @author chao zhang
 * @date 2014年11月11日 上午11:02:57
 * @version V1.0
 */
package com.zhjoe.ccpws.vo;

import java.util.Date;


/**
 * @ClassName: delCompoundDocVO
 * @Description: 删除顶级DENS文档 对象
 * @author qiaolei
 * @date 2013年12月14日
 *
 */
public class DeleteVo {
	/**
	 *	序号
	 */
	private Integer primary_id;

	/**
	 *	文档ID
	 */
	private String id;

	/**
	 *	文档名称
	 */
	private String name;

	/**
	 *	类型
	 */
	private String type;

	/**
	 *	原文档路径
	 */
	private String oldPath;

	/**
	 *	删除时间
	 */
	private Date delTime;

	/**
	 *	文件大小
	 */
	private String contentSize;

	/**
	 * 修改时间
	 */
	private String dateLastModified;
	/**
	 *
	 */
	private String deleter;
	/**
	 *	文件后缀类型
	 */
	private String mimeType;

	/**
	 *  是否是复合文档
	 *
	 */
	private Integer compoundDocumentState;

	private String copcrevisionno;

	private String className;

	private String densNumber;

	private String sheetNo;


	/**
	 * @return the primary_id
	 */
	public Integer getPrimary_id() {
		return primary_id;
	}

	/**
	 * @param primary_id the primary_id to set
	 */
	public void setPrimary_id(Integer primary_id) {
		this.primary_id = primary_id;
	}

	/**
	 * @return the id
	 */
	public String getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(String id) {
		this.id = id;
	}

	/**
	 * @return name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param name name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @return the type
	 */
	public String getType() {
		return type;
	}

	/**
	 * @param type the type to set
	 */
	public void setType(String type) {
		this.type = type;
	}

	/**
	 * @return oldPath
	 */
	public String getOldPath() {
		return oldPath;
	}

	/**
	 * @param oldPath oldPath to set
	 */
	public void setOldPath(String oldPath) {
		this.oldPath = oldPath;
	}

	/**
	 * @return the delTime
	 */

	public Date getDelTime() {
		return delTime;
	}

	/**
	 * @param delTime the delTime to set
	 */
	public void setDelTime(Date delTime) {
		this.delTime = delTime;
	}

	/**
	 * @return the dateLastModified
	 */
	public String getDateLastModified() {
		return dateLastModified;
	}

	/**
	 * @param dateLastModified the dateLastModified to set
	 */
	public void setDateLastModified(String dateLastModified) {
		this.dateLastModified = dateLastModified;
	}

	/**
	 * @return the contentSize
	 */
	public String getContentSize() {
		return contentSize;
	}

	/**
	 * @param contentSize the contentSize to set
	 */
	public void setContentSize(String contentSize) {
		this.contentSize = contentSize;
	}

	/**
	 * @return deleter
	 */
	public String getDeleter() {
		return deleter;
	}

	/**
	 * @param deleter deleter to set
	 */
	public void setDeleter(String deleter) {
		this.deleter = deleter;
	}

	/**
	 * @return the mimeType
	 */
	public String getMimeType() {
		return mimeType;
	}

	/**
	 * @param mimeType the mimeType to set
	 */
	public void setMimeType(String mimeType) {
		this.mimeType = mimeType;
	}

	/**
	 * @return the compoundDocumentState
	 */
	public Integer getCompoundDocumentState() {
		return compoundDocumentState;
	}

	/**
	 * @param compoundDocumentState the compoundDocumentState to set
	 */
	public void setCompoundDocumentState(Integer compoundDocumentState) {
		this.compoundDocumentState = compoundDocumentState;
	}

	/**
	 * @return the copcrevisionno
	 */
	public String getCopcrevisionno() {
		return copcrevisionno;
	}

	/**
	 * @param copcrevisionno the copcrevisionno to set
	 */
	public void setCopcrevisionno(String copcrevisionno) {
		this.copcrevisionno = copcrevisionno;
	}

	/**
	 * @return the className
	 */
	public String getClassName() {
		return className;
	}

	/**
	 * @param className the className to set
	 */
	public void setClassName(String className) {
		this.className = className;
	}

	/**
	 * @return the densNumber
	 */
	public String getDensNumber() {
		return densNumber;
	}

	/**
	 * @param densNumber the densNumber to set
	 */
	public void setDensNumber(String densNumber) {
		this.densNumber = densNumber;
	}

	/**
	 * @return the sheetNo
	 */
	public String getSheetNo() {
		return sheetNo;
	}

	/**
	 * @param sheetNo the sheetNo to set
	 */
	public void setSheetNo(String sheetNo) {
		this.sheetNo = sheetNo;
	}


}

