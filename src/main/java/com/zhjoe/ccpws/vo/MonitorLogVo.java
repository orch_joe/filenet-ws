/**
 * @Title: MonitorLogVo.java
 * @Package com.ccpws.vo
 * @Description: TODO(用一句话描述该文件做什么)
 * @author Changling Jiang
 * @date 2014-2-13 下午12:03:51
 * @version V1.0
 */
package com.zhjoe.ccpws.vo;

import java.sql.Timestamp;

/**
 * @ClassName: MonitorLogVo
 * @Description: TODO(这里用一句话描述这个类的作用)
 * @author Changling Jiang
 * @date 2014-2-13 下午12:03:51
 *
 */
public class MonitorLogVo {

	private String id;// 日志Id
	private String sessionId;// sessionId
	private String callId;// 调用Id
	private String remoteHost;// Ip地址
	private int remotePort;// 端口
	private String shortName;// 登录名
	private Timestamp operateTime;// 操作时间
	private int duration;// 操作耗时
	private String interfaceName;// 接口名称
	private int operateState;// 操作状态
	private String logContent;// 日志内容

	/**
	 * @return id
	 */
	public String getId() {
		return id;
	}

	/**
	 * @param id id to set
	 */
	public void setId(String id) {
		this.id = id;
	}

	/**
	 * @return sessionId
	 */
	public String getSessionId() {
		return sessionId;
	}

	/**
	 * @param sessionId sessionId to set
	 */
	public void setSessionId(String sessionId) {
		this.sessionId = sessionId;
	}

	/**
	 * @return callId
	 */
	public String getCallId() {
		return callId;
	}

	/**
	 * @param callId callId to set
	 */
	public void setCallId(String callId) {
		this.callId = callId;
	}

	/**
	 * @return remoteHost
	 */
	public String getRemoteHost() {
		return remoteHost;
	}

	/**
	 * @param remoteHost remoteHost to set
	 */
	public void setRemoteHost(String remoteHost) {
		this.remoteHost = remoteHost;
	}

	/**
	 * @return remotePort
	 */
	public int getRemotePort() {
		return remotePort;
	}

	/**
	 * @param remotePort remotePort to set
	 */
	public void setRemotePort(int remotePort) {
		this.remotePort = remotePort;
	}

	/**
	 * @return shortName
	 */
	public String getShortName() {
		return shortName;
	}

	/**
	 * @param shortName shortName to set
	 */
	public void setShortName(String shortName) {
		this.shortName = shortName;
	}

	/**
	 * @return operateTime
	 */
	public Timestamp getOperateTime() {
		return operateTime;
	}

	/**
	 * @param operateTime operateTime to set
	 */
	public void setOperateTime(Timestamp operateTime) {
		this.operateTime = operateTime;
	}

	/**
	 * @return duration
	 */
	public int getDuration() {
		return duration;
	}

	/**
	 * @param duration duration to set
	 */
	public void setDuration(int duration) {
		this.duration = duration;
	}

	/**
	 * @return interfaceName
	 */
	public String getInterfaceName() {
		return interfaceName;
	}

	/**
	 * @param interfaceName interfaceName to set
	 */
	public void setInterfaceName(String interfaceName) {
		this.interfaceName = interfaceName;
	}

	/**
	 * @return operateState
	 */
	public int getOperateState() {
		return operateState;
	}

	/**
	 * @param operateState operateState to set
	 */
	public void setOperateState(int operateState) {
		this.operateState = operateState;
	}

	/**
	 * @return logContent
	 */
	public String getLogContent() {
		return logContent;
	}

	/**
	 * @param logContent logContent to set
	 */
	public void setLogContent(String logContent) {
		this.logContent = logContent;
	}


}
