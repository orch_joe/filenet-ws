package com.zhjoe.ccpws.vo;

import java.util.List;

public class WSCSRetrieveDocumentAndPropertyInfosBodyVo {
	private List<ArchivesDocumentInfo> document;

	public List<ArchivesDocumentInfo> getDocument() {
		return document;
	}

	public void setDocument(List<ArchivesDocumentInfo> document) {
		this.document = document;
	}
}
