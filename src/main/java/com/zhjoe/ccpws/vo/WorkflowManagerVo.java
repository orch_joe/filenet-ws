package com.zhjoe.ccpws.vo;

/**
 * @ClassName: WorkflowManagerVo
 * @Description: 流程业务管理员类
 * @author Ke Cui
 * @date 2014年6月19日 下午4:02:57
 *
 */
public class WorkflowManagerVo {

	private int id;// ID
	private String businessNo;// 业务编号
	private String businessName;// 业务名称
	private String businessAdministrator;// 业务管理员

	/**
	 * @return id
	 */
	public int getId() {
		return id;
	}

	/**
	 * @param id id to set
	 */
	public void setId(int id) {
		this.id = id;
	}

	/**
	 * 业务编号
	 * @return businessNo
	 */
	public String getBusinessNo() {
		return businessNo;
	}

	/**
	 * 业务编号
	 * @param businessNo businessNo to set
	 */
	public void setBusinessNo(String businessNo) {
		this.businessNo = businessNo;
	}

	/**
	 * 业务名称
	 * @return businessName
	 */
	public String getBusinessName() {
		return businessName;
	}

	/**
	 * 业务名称
	 * @param businessName businessName to set
	 */
	public void setBusinessName(String businessName) {
		this.businessName = businessName;
	}

	/**
	 * 业务管理员
	 * @return businessAdministrator
	 */
	public String getBusinessAdministrator() {
		return businessAdministrator;
	}

	/**
	 * 业务管理员
	 * @param businessAdministrator businessAdministrator to set
	 */
	public void setBusinessAdministrator(String businessAdministrator) {
		this.businessAdministrator = businessAdministrator;
	}

}
