/**
 * @Title: WSCSDENSLocationBodyVo.java
 * @Package com.ccpws.vo
 * @Description: TODO(用一句话描述该文件做什么)
 * @author Changling Jiang
 * @date 2014-3-17 下午04:10:59
 * @version V1.0
 */
package com.zhjoe.ccpws.vo;

/**
 * @ClassName: WSCSDENSLocationBodyVo
 * @Description: TODO(这里用一句话描述这个类的作用)
 * @author Changling Jiang
 * @date 2014-3-17 下午04:10:59
 *
 */
public class WSCSDENSLocationBodyVo {

	private String location;

	/**
	 * @return location
	 */
	public String getLocation() {
		return location;
	}

	/**
	 * @param location location to set
	 */
	public void setLocation(String location) {
		this.location = location;
	}

}
