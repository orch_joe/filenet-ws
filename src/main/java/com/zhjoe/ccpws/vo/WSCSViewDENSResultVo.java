/**
 * @Title: WSCSViewDENSResultVo.java
 * @Package com.ccpws.vo
 * @Description: TODO(用一句话描述该文件做什么)
 * @author Changling Jiang
 * @date 2014-2-26 下午04:56:17
 * @version V1.0
 */
package com.zhjoe.ccpws.vo;

/**
 * @ClassName: WSCSViewDENSResultVo
 * @Description: TODO(这里用一句话描述这个类的作用)
 * @author Changling Jiang
 * @date 2014-2-26 下午04:56:17
 *
 */
public class WSCSViewDENSResultVo extends WSBaseResultVo {

	private WSCSViewDENSBodyVo body;

	/**
	 * @return body
	 */
	public WSCSViewDENSBodyVo getBody() {
		return body;
	}

	/**
	 * @param body body to set
	 */
	public void setBody(WSCSViewDENSBodyVo body) {
		this.body = body;
	}

}
