/**
 * @Title: WSCSCreateDENSNumbersResultVo.java
 * @Package com.ccpws.vo
 * @Description: TODO(用一句话描述该文件做什么)
 * @author Changling Jiang
 * @date 2014-3-7 下午01:13:49
 * @version V1.0
 */
package com.zhjoe.ccpws.vo;

/**
 * @ClassName: WSCSCreateDENSNumbersResultVo
 * @Description: TODO(这里用一句话描述这个类的作用)
 * @author Changling Jiang
 * @date 2014-3-7 下午01:13:49
 *
 */
public class WSCSCreateDENSNumbersResultVo extends WSBaseResultVo {

	private WSCSCreateDENSNumbersBodyVo body;

	/**
	 * @return body
	 */
	public WSCSCreateDENSNumbersBodyVo getBody() {
		return body;
	}

	/**
	 * @param body body to set
	 */
	public void setBody(WSCSCreateDENSNumbersBodyVo body) {
		this.body = body;
	}

}
