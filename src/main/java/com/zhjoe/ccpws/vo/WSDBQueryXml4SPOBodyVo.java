/**
 * @Title: WSDBQueryXml4SPOBodyVo.java
 * @Package com.ccpws.vo
 * @Description: TODO(用一句话描述该文件做什么)
 * @author Changling Jiang
 * @date 2014年6月27日 上午9:29:18
 * @version V1.0
 */
package com.zhjoe.ccpws.vo;

import java.util.List;

/**
 * @ClassName: WSDBQueryXml4SPOBodyVo
 * @Description: TODO(这里用一句话描述这个类的作用)
 * @author Changling Jiang
 * @date 2014年6月27日 上午9:29:18
 *
 */
public class WSDBQueryXml4SPOBodyVo {
	private List<Xml4SPOVo> xml;

	/**
	 * @return the xml
	 */
	public List<Xml4SPOVo> getXml() {
		return xml;
	}

	/**
	 * @param xml the xml to set
	 */
	public void setXml(List<Xml4SPOVo> xml) {
		this.xml = xml;
	}

}
