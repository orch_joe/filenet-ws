/**
 * @Title: WSCSRetrieveChoiceListBodyVo.java
 * @Package com.ccpws.vo
 * @Description: TODO(用一句话描述该文件做什么)
 * @author Shushuai Gu
 * @date 2014-10-27 下午1:59:24
 * @version V1.0
 */
package com.zhjoe.ccpws.vo;

import java.util.List;

/**
 * @ClassName: WSCSRetrieveChoiceListBodyVo
 * @Description: TODO(这里用一句话描述这个类的作用)
 * @author Changling Jiang
 * @date 2014-10-27 下午1:59:24
 * 
 */
public class WSCSRetrieveChoiceListBodyVo {
	private List<Node> nodes;

	/**
	 * @return nodes
	 */
	public List<Node> getNodes() {
		return nodes;
	}

	/**
	 * @param nodes nodes to set
	 */
	public void setNodes(List<Node> nodes) {
		this.nodes = nodes;
	}
	
	
}
