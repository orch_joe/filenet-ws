/**
 * @Title: WSSMLoginBodyVo.java
 * @Package com.ccpws.vo
 * @Description: TODO(用一句话描述该文件做什么)
 * @author Changling Jiang
 * @date 2014-2-19 下午03:07:15
 * @version V1.0
 */
package com.zhjoe.ccpws.vo;

/**
 * @ClassName: WSSMLoginBodyVo
 * @Description: TODO(这里用一句话描述这个类的作用)
 * @author Changling Jiang
 * @date 2014-2-19 下午03:07:15
 *
 */
public class WSSMLoginBodyVo {

	private String sessionId;

	/**
	 * @return sessionId
	 */
	public String getSessionId() {
		return sessionId;
	}

	/**
	 * @param sessionId sessionId to set
	 */
	public void setSessionId(String sessionId) {
		this.sessionId = sessionId;
	}

}
