/**
 * @Title: WSCSMoveDENSResultVo.java
 * @Package com.ccpws.vo
 * @Description: TODO(用一句话描述该文件做什么)
 * @author Changling Jiang
 * @date 2014-3-4 下午07:34:16
 * @version V1.0
 */
package com.zhjoe.ccpws.vo;

/**
 * @ClassName: WSCSMoveDENSResultVo
 * @Description: TODO(这里用一句话描述这个类的作用)
 * @author Changling Jiang
 * @date 2014-3-4 下午07:34:16
 *
 */
public class WSCSMoveDENSResultVo extends WSBaseResultVo {

	private WSCSMoveDENSBodyVo body;

	/**
	 * @return body
	 */
	public WSCSMoveDENSBodyVo getBody() {
		return body;
	}

	/**
	 * @param body body to set
	 */
	public void setBody(WSCSMoveDENSBodyVo body) {
		this.body = body;
	}

}
