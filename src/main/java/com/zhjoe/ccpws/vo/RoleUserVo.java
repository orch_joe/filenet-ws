/**
 * @Title: RoleUserVo.java
 * @Package com.ccp.vo
 * @Description: TODO(用一句话描述该文件做什么)
 * @author Changling Jiang
 * @date 2013年11月20日 下午6:02:19
 * @version V1.0
 */
package com.zhjoe.ccpws.vo;

/**
 * @ClassName: RoleUserVo
 * @Description: TODO(这里用一句话描述这个类的作用)
 * @author Changling Jiang
 * @date 2013年11月20日 下午6:02:19
 *
 */
public class RoleUserVo {

	/**
	 * 序号
	 */
	private int id;
	/**
	 * ShortName
	 */
	private String shortName;
	/**
	 * 显示名称
	 */
	private String displayName;
	/**
	 * 类型 User || Group
	 */
	private int type;
	/**
	 * 角色Id
	 */
	private String roleId;

	/**
	 * @return id
	 */
	public int getId() {
		return id;
	}

	/**
	 * @param id id to set
	 */
	public void setId(int id) {
		this.id = id;
	}

	/**
	 * @return shortName
	 */
	public String getShortName() {
		return shortName;
	}

	/**
	 * @param shortName shortName to set
	 */
	public void setShortName(String shortName) {
		this.shortName = shortName;
	}

	/**
	 * @return displayName
	 */
	public String getDisplayName() {
		return displayName;
	}

	/**
	 * @param displayName displayName to set
	 */
	public void setDisplayName(String displayName) {
		this.displayName = displayName;
	}

	/**
	 * @return type
	 */
	public int getType() {
		return type;
	}

	/**
	 * @param type type to set
	 */
	public void setType(int type) {
		this.type = type;
	}

	/**
	 * @return roleId
	 */
	public String getRoleId() {
		return roleId;
	}

	/**
	 * @param roleId roleId to set
	 */
	public void setRoleId(String roleId) {
		this.roleId = roleId;
	}
}
