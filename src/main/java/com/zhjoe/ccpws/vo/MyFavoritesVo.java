package com.zhjoe.ccpws.vo;


/**
 * @ClassName: MyFavoritesVO
 * @Description: 我的收藏 对象
 * @author qiaolei
 * @date 2013年12月14日
 *
 */
public class MyFavoritesVo {
	/**
	 * 序号
	 */
	private Integer primary_id;
	/**
	 *	 对象ID
	 */
	private String id;
	/**
	 * VsId
	 *
	 */
	private String vsId;
	/**
	 * 名称
	 */
	private String name;

	/**
	 * 类型
	 */
	private String type;

	/**
	 * 是否为复合文档
	 */
	private Integer compoundDocumentState;
	/**
	 * 路径
	 */
	private String path;

	/**
	 *	收藏者
	 */
	private String favoritesUser;

	/**
	 * 大小
	 */
	private String contentSize;
	/**
	 * mimeType;
	 */
	private String mimeType;

	/**
	 * 主版本
	 */
	private Integer minorVersionNumber;
	/**
	 * 修改时间
	 */
	private String dateLastModified;

	/**
	 * 保留状态
	 */
	private String reservedBy;

	/**
	 * 来自（CE/DB）
	 */
	private String fromWhere;

	/**
	 * 父文件ID
	 */
	private Integer pId;

	/**
	 * 文档所属类型
	 *
	 */
	private String className;

	/**
	 * @return the id
	 */
	public String getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(String id) {
		this.id = id;
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @return the type
	 */
	public String getType() {
		return type;
	}

	/**
	 * @param type the type to set
	 */
	public void setType(String type) {
		this.type = type;
	}

	/**
	 * @return the compoundDocumentState
	 */
	public Integer getCompoundDocumentState() {
		return compoundDocumentState;
	}

	/**
	 * @param compoundDocumentState the compoundDocumentState to set
	 */
	public void setCompoundDocumentState(Integer compoundDocumentState) {
		this.compoundDocumentState = compoundDocumentState;
	}

	/**
	 * @return the path
	 */
	public String getPath() {
		return path;
	}

	/**
	 * @param path the path to set
	 */
	public void setPath(String path) {
		this.path = path;
	}

	/**
	 * @return the favoritesUser
	 */
	public String getFavoritesUser() {
		return favoritesUser;
	}

	/**
	 * @param favoritesUser the favoritesUser to set
	 */
	public void setFavoritesUser(String favoritesUser) {
		this.favoritesUser = favoritesUser;
	}


	/**
	 * @return contentSize
	 */
	public String getContentSize() {
		return contentSize;
	}

	/**
	 * @param contentSize contentSize to set
	 */
	public void setContentSize(String contentSize) {
		this.contentSize = contentSize;
	}

	/**
	 * @return mimeType
	 */
	public String getMimeType() {
		return mimeType;
	}

	/**
	 * @param mimeType mimeType to set
	 */
	public void setMimeType(String mimeType) {
		this.mimeType = mimeType;
	}

	/**
	 * @return dateLastModified
	 */
	public String getDateLastModified() {
		return dateLastModified;
	}

	/**
	 * @param dateLastModified dateLastModified to set
	 */
	public void setDateLastModified(String dateLastModified) {
		this.dateLastModified = dateLastModified;
	}


	/**
	 * @return vsId
	 */
	public String getVsId() {
		return vsId;
	}

	/**
	 * @param vsId vsId to set
	 */
	public void setVsId(String vsId) {
		this.vsId = vsId;
	}


	/**
	 * @return reservedBy
	 */
	public String getReservedBy() {
		return reservedBy;
	}

	/**
	 * @param reservedBy reservedBy to set
	 */
	public void setReservedBy(String reservedBy) {
		this.reservedBy = reservedBy;
	}

	/**
	 * @return primary_id
	 */
	public Integer getPrimary_id() {
		return primary_id;
	}

	/**
	 * @param primary_id primary_id to set
	 */
	public void setPrimary_id(Integer primary_id) {
		this.primary_id = primary_id;
	}

	/**
	 * @return fromWhere
	 */
	public String getFromWhere() {
		return fromWhere;
	}

	/**
	 * @param fromWhere fromWhere to set
	 */
	public void setFromWhere(String fromWhere) {
		this.fromWhere = fromWhere;
	}

	/**
	 * @return minorVersionNumber
	 */
	public Integer getMinorVersionNumber() {
		return minorVersionNumber;
	}

	/**
	 * @param minorVersionNumber minorVersionNumber to set
	 */
	public void setMinorVersionNumber(Integer minorVersionNumber) {
		this.minorVersionNumber = minorVersionNumber;
	}

	/**
	 * @return pId
	 */
	public Integer getpId() {
		return pId;
	}

	/**
	 * @param pId pId to set
	 */
	public void setpId(Integer pId) {
		this.pId = pId;
	}

	public String getClassName() {
		return className;
	}

	public void setClassName(String className) {
		this.className = className;
	}

}
