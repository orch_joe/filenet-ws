/**
 * @Title: WSCSRetrieveDocumentpropertysResultVo.java
 * @Package com.ccpws.vo
 * @Description: TODO(用一句话描述该文件做什么)
 * @author Changling Jiang
 * @date 2014-3-25 上午10:23:19
 * @version V1.0
 */
package com.zhjoe.ccpws.vo;

import java.util.List;

import com.zhjoe.strongit.filenet.service.datatype.PropertyInfo;

/**
 * @ClassName: WSCSRetrieveDocumentpropertysResultVo
 * @Description: TODO(这里用一句话描述这个类的作用)
 * @author Changling Jiang
 * @date 2014-3-25 上午10:23:19
 * 
 */
public class WSCSRetrieveDocumentpropertysResultVo extends WSBaseResultVo {

	private WSCSRetrieveDocumentpropertysBodyVo body;

	/**
	 * @return body
	 */
	public WSCSRetrieveDocumentpropertysBodyVo getBody() {
		return body;
	}

	/**
	 * @param body body to set
	 */
	public void setBody(WSCSRetrieveDocumentpropertysBodyVo body) {
		this.body = body;
	}

	
}
