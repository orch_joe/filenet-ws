package com.zhjoe.ccpws.vo;

public class WSCSDocumentPermissionResultVo extends WSBaseResultVo{
    
    private WSCSDocumentPermissionBodyVo body;
    
    /**
     * @return body
     */
    public WSCSDocumentPermissionBodyVo getBody() {
        return body;
    }

    /**
     * @param body body to set
     */
    public void setBody(WSCSDocumentPermissionBodyVo body) {
        this.body = body;
    }
}
