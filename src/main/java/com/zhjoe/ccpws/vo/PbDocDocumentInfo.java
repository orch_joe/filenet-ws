/**
 * @Title: PbDocDocumentInfo.java
 * @Package com.ccpws.vo
 * @Description: TODO(用一句话描述该文件做什么)
 * @author Changling Jiang
 * @date 2014-3-26 下午05:48:51
 * @version V1.0
 */
package com.zhjoe.ccpws.vo;

import java.util.Map;

import com.filenet.api.core.Document;
import com.zhjoe.strongit.filenet.service.datatype.DocumentInfo;

/**
 * @ClassName: PbDocDocumentInfo
 * @Description: TODO(这里用一句话描述这个类的作用)
 * @author Changling Jiang
 * @date 2014-3-26 下午05:48:51
 *
 */
public class PbDocDocumentInfo extends DocumentInfo {

	private String reservedBy;
	private boolean doNotOperateFlag;
	private String pbdesc;

	/* (非 Javadoc)
	 * <p>Title: fromCustom</p>
	 * <p>Description: </p>
	 * @param document
	 * @see com.ccpws.service.datatype.DocumentInfo#fromCustom(com.filenet.api.core.Document)
	 */
	@Override
	protected void fromCustom(Document document) {
		super.fromCustom(document);
		setPbdesc(document.getProperties().getStringValue("pbdesc"));
		setReservedBy(document.getProperties().getStringValue("reservedBy"));
	}

	/* (非 Javadoc)
	 * <p>Title: fromVo</p>
	 * <p>Description: </p>
	 * @param map
	 * @see com.ccpws.service.datatype.DocumentInfo#fromVo(java.util.Map)
	 */
	@Override
	public void fromVo(Map<String, Object> map) {
		super.fromVo(map);
		if (getPbdesc() != null) {
			map.put("pbdesc", getPbdesc());
		}
		if (getReservedBy() != null) {
			map.put("reservedBy", getReservedBy());
		}
	}

	/**
	 * @return reservedBy
	 */
	public String getReservedBy() {
		return reservedBy;
	}

	/**
	 * @param reservedBy reservedBy to set
	 */
	public void setReservedBy(String reservedBy) {
		this.reservedBy = reservedBy;
	}

	/**
	 * @return doNotOperateFlag
	 */
	public boolean isDoNotOperateFlag() {
		return doNotOperateFlag;
	}

	/**
	 * @param doNotOperateFlag doNotOperateFlag to set
	 */
	public void setDoNotOperateFlag(boolean doNotOperateFlag) {
		this.doNotOperateFlag = doNotOperateFlag;
	}

	/**
	 * @return pbdesc
	 */
	public String getPbdesc() {
		return pbdesc;
	}

	/**
	 * @param pbdesc pbdesc to set
	 */
	public void setPbdesc(String pbdesc) {
		this.pbdesc = pbdesc;
	}


}
