/**
 * @Title: ParameterVo.java
 * @Package com.ccpws.vo
 * @Description: TODO(用一句话描述该文件做什么)
 * @author Changling Jiang
 * @date 2014-2-13 下午12:46:22
 * @version V1.0
 */
package com.zhjoe.ccpws.vo;

/**
 * @ClassName: ParameterVo
 * @Description: TODO(这里用一句话描述这个类的作用)
 * @author Changling Jiang
 * @date 2014-2-13 下午12:46:22
 *
 */
public class ParameterVo {

	private int index;// 参数索引
	private Object object;// 参数对象

	/**
	 * @return index
	 */
	public int getIndex() {
		return index;
	}

	/**
	 * @param index index to set
	 */
	public void setIndex(int index) {
		this.index = index;
	}

	/**
	 * @return object
	 */
	public Object getObject() {
		return object;
	}

	/**
	 * @param object object to set
	 */
	public void setObject(Object object) {
		this.object = object;
	}


}
