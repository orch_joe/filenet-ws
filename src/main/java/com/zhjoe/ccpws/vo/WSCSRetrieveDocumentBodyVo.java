/**
 * @Title: WSCSRetrieveDocumentBodyVo.java
 * @Package com.ccpws.vo
 * @Description: TODO(用一句话描述该文件做什么)
 * @author Changling Jiang
 * @date 2014-3-25 上午10:23:47
 * @version V1.0
 */
package com.zhjoe.ccpws.vo;

import com.zhjoe.strongit.filenet.service.datatype.DocumentInfo;

import java.util.List;


/**
 * @ClassName: WSCSRetrieveDocumentBodyVo
 * @Description: TODO(这里用一句话描述这个类的作用)
 * @author Changling Jiang
 * @date 2014-3-25 上午10:23:47
 *
 */
public class WSCSRetrieveDocumentBodyVo {

	private List<DocumentInfo> document;

	/**
	 * @return document
	 */
	public List<DocumentInfo> getDocument() {
		return document;
	}

	/**
	 * @param document document to set
	 */
	public void setDocument(List<DocumentInfo> document) {
		this.document = document;
	}
}
