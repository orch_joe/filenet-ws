package com.zhjoe.ccpws.vo;

public class WSCSMyBorrowingResultVo extends WSBaseResultVo{
    private WSCSMyBorrowingBodyVo body;

    public WSCSMyBorrowingBodyVo getBody() {
        return body;
    }

    public void setBody(WSCSMyBorrowingBodyVo body) {
        this.body = body;
    }
}
