package com.zhjoe.ccpws.vo;

public class WSCSOperateResultVo extends WSBaseResultVo {
	private WSCSOperateBodyVo body;

	public WSCSOperateBodyVo getBody() {
		return body;
	}

	public void setBody(WSCSOperateBodyVo body) {
		this.body = body;
	}

}
