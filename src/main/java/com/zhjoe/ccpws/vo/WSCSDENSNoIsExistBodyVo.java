/**
 * @Title: WSCSDENSNoIsExistBodyVo.java
 * @Package com.ccpws.vo
 * @Description: TODO(用一句话描述该文件做什么)
 * @author Changling Jiang
 * @date 2014-2-18 下午03:40:20
 * @version V1.0
 */
package com.zhjoe.ccpws.vo;

/**
 * @ClassName: WSCSDENSNoIsExistBodyVo
 * @Description: TODO(这里用一句话描述这个类的作用)
 * @author Changling Jiang
 * @date 2014-2-18 下午03:40:20
 *
 */
public class WSCSDENSNoIsExistBodyVo {

	private boolean exist;

	/**
	 * @return exist
	 */
	public boolean isExist() {
		return exist;
	}

	/**
	 * @param exist exist to set
	 */
	public void setExist(boolean exist) {
		this.exist = exist;
	}

}
