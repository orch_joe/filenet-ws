package com.zhjoe.ccpws.vo;

public class WSCSGetFolderResultVo extends WSBaseResultVo  {
    private WSCSGetFolderBodyVo body;

    /**
     * @return body
     */
    public WSCSGetFolderBodyVo getBody() {
        return body;
    }

    /**
     * @param body body to set
     */
    public void setBody(WSCSGetFolderBodyVo body) {
        this.body = body;
    }
}
