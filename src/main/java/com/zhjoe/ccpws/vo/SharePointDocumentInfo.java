/**
 * @Title: PermittingDocumentInfo.java
 * @Package com.ccpws.vo
 * @Description: TODO(用一句话描述该文件做什么)
 * @author Changling Jiang
 * @date 2014-4-11 上午10:53:09
 * @version V1.0
 */
package com.zhjoe.ccpws.vo;

import java.util.Map;

import com.filenet.api.core.Document;
import com.filenet.api.property.Properties;

/**
 * @ClassName: PermittingDocumentInfo
 * @Description: TODO(这里用一句话描述这个类的作用)
 * @author Changling Jiang
 * @date 2014-4-11 上午10:53:09
 *
 */
public class SharePointDocumentInfo extends PbDocDocumentInfo {

	private String newsTitle;
	private String realCreater;
	private String spfilte;

	public String getNewsTitle() {
		return newsTitle;
	}

	public void setNewsTitle(String newsTitle) {
		this.newsTitle = newsTitle;
	}

	public String getRealCreater() {
		return realCreater;
	}

	public void setRealCreater(String realCreater) {
		this.realCreater = realCreater;
	}

	public String getSpfilte() {
		return spfilte;
	}

	public void setSpfilte(String spfilte) {
		this.spfilte = spfilte;
	}

	@Override
	public void fromVo(Map<String, Object> map) {
		super.fromVo(map);

		if (newsTitle != null) {
			map.put("newsTitle", newsTitle);
		}
		if (realCreater != null) {
			map.put("realCreater", realCreater);
		}
		if (spfilte != null) {
			map.put("spfilte", spfilte);
		}

	}

	@Override
	protected void fromCustom(Document document) {
		super.fromCustom(document);
		Properties properties = document.getProperties();
		setNewsTitle(properties.getStringValue("newsTitle"));
		setRealCreater(properties.getStringValue("realCreater"));
		setSpfilte(properties.getStringValue("spfilte"));

	}

}
