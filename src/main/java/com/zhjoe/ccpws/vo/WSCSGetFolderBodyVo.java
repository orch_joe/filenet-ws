package com.zhjoe.ccpws.vo;


import com.zhjoe.strongit.filenet.service.datatype.FolderInfo;

public class WSCSGetFolderBodyVo {
    private FolderInfo folder;

    public FolderInfo getFolder() {
        return folder;
    }

    public void setFolder(FolderInfo folder) {
        this.folder = folder;
    }
}
