package com.zhjoe.ccpws.vo;

public class WSCSRetrieveArchivesFileResultVo extends WSBaseResultVo{
	
	private WSCSRetrieveArchivesFileBodyVo body;

	public WSCSRetrieveArchivesFileBodyVo getBody() {
		return body;
	}

	public void setBody(WSCSRetrieveArchivesFileBodyVo body) {
		this.body = body;
	}
	
}
