package com.zhjoe.ccpws.vo;

import java.util.Map;

public class WSCSRetrieveAllChoiceListBodyVo {
	
	
	private Map<String,String> allNodes;

	private Map<String,String> treeNodes;

	/**
	 * @return allNodes
	 */
	public Map<String, String> getAllNodes() {
		return allNodes;
	}

	/**
	 * @param allNodes allNodes to set
	 */
	public void setAllNodes(Map<String, String> allNodes) {
		this.allNodes = allNodes;
	}

	/**
	 * @return treeNodes
	 */
	public Map<String, String> getTreeNodes() {
		return treeNodes;
	}

	/**
	 * @param treeNodes treeNodes to set
	 */
	public void setTreeNodes(Map<String, String> treeNodes) {
		this.treeNodes = treeNodes;
	}

	
	
	
}
