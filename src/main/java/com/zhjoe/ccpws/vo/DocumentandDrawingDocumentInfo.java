/**
 * @Title: DocumentandDrawingDocumentInfo.java
 * @Package com.ccpws.service.datatype
 * @Description: DENS文档类
 * @author Changling Jiang
 * @date 2013年11月15日 下午3:26:27
 * @version V1.0
 */
package com.zhjoe.ccpws.vo;

import java.util.Date;
import java.util.Map;


import com.filenet.api.core.Document;
import com.filenet.api.property.Properties;

/**
 * @ClassName: DocumentandDrawingDocumentInfo
 * @Description: DENS文档类
 * @author Changling Jiang
 * @date 2013年11月15日 下午3:26:27
 *
 */
public class DocumentandDrawingDocumentInfo extends PbDocDocumentInfo {

	/**
	 * Format
	 */
	private String format;
	/**
	 * Sheet No
	 */
	private String sheetNo;
	/**
	 * Country Code
	 */
	private String countryCode;
	/**
	 * DENS 1
	 */
	private String dens1;
	/**
	 * DENS 2
	 */
	private String dens2;
	/**
	 * DENS 3
	 */
	private String dens3;
	/**
	 * DENS 4
	 */
	private String dens4;
	/**
	 * DENS 5
	 */
	private String dens5;
	/**
	 * DENS Number
	 */
	private String densNumber;
	/**
	 * DENS SeqLen
	 */
	private String densSeqLen;
	/**
	 * DENS Type
	 */
	private String densType;
	/**
	 * Project No
	 */
	private String projectNo;
	/**
	 * Utilisation Category
	 */
	private String utilisationCategory;
	/**
	 * Supplier-External Doc-Dwg No
	 */
	private String supplierExternalDocDwgNo;
	/**
	 *
	 */
	private String title;
	/**
	 * Doc-Dwg Type
	 */
	private String docDwgType;
	/**
	 * Company
	 */
	private String company;
	/**
	 * Discipline
	 */
	private String discipline;
	/**
	 * Location Code
	 */
	private String locationCode;
	/**
	 * Transmittal Number
	 */
	private String transmittalNumber;
	/**
	 * Author
	 */
	private String author;
	/**
	 * Last Reviewed Date
	 */
	private Date lastReviewedDate;
	/**
	 * Approver
	 */
	private String approver;
	/**
	 * Region
	 */
	private String region;
	/**
	 * Issue-Revision Date
	 */
	private Date issueRevisionDate;
	/**
	 * Review Cycle
	 */
	private String reviewCycle;
	/**
	 * Status
	 */
	private String status;
	/**
	 * System Code
	 */
	private String systemCode;
	/**
	 * Purchase Order
	 */
	private String purchaseOrder;
	/**
	 * Legacy Doc-Dwg No
	 */
	private String legacyDocDwgNo;
	/**
	 * Facility
	 */
	private String facility;
	/**
	 * Retention Code
	 */
	private String retentionCode;
	/**
	 * COPC Revision No
	 */
	private String copcRevisionNo;
	/**
	 * Sheet Size
	 */
	private String sheetSize;
	/**
	 * DENS文档的序列号
	 */
	private int densSequence;
	/**
	 * DENS文档的层级
	 */
	private int densLevel;

	/* (非 Javadoc)
	 * <p>Title: fromCustom</p>
	 * <p>Description: </p>
	 * @param document
	 * @see com.ccpws.service.datatype.DefaultDocumentInfo#fromCustom(com.filenet.api.core.Document)
	 */
	@Override
	protected void fromCustom(Document document) {
		super.fromCustom(document);
		Properties properties = document.getProperties();
		setFormat(properties.getStringValue("Format"));
		setSheetNo(properties.getStringValue("SheetNo"));
		setCountryCode(properties.getStringValue("CountryCode"));
		setDens1(properties.getStringValue("DENS1"));
		setDens2(properties.getStringValue("DENS2"));
		setDens3(properties.getStringValue("DENS3"));
		setDens4(properties.getStringValue("DENS4"));
		setDens5(properties.getStringValue("DENS5"));
		setDensNumber(properties.getStringValue("DENSNumber"));
		setDensSeqLen(properties.getStringValue("DENSSeqLen"));
		setDensType(properties.getStringValue("DENSType"));
		setProjectNo(properties.getStringValue("ProjectNo"));
		setUtilisationCategory(properties.getStringValue("UtilisationCategory"));
		setSupplierExternalDocDwgNo(properties.getStringValue("SupplierExternalDocDwgNo"));
		setTitle(properties.getStringValue("Title"));
		setDocDwgType(properties.getStringValue("DocDwgType"));
		setCompany(properties.getStringValue("Company"));
		setDiscipline(properties.getStringValue("Discipline"));
		setLocationCode(properties.getStringValue("LocationCode"));
		setTransmittalNumber(properties.getStringValue("TransmittalNumber"));
		setAuthor(properties.getStringValue("Author"));
		setLastReviewedDate(properties.getDateTimeValue("LastReviewedDate"));
		setApprover(properties.getStringValue("Approver"));
		setRegion(properties.getStringValue("Region"));
		setIssueRevisionDate(properties.getDateTimeValue("IssueRevisionDate"));
		setReviewCycle(properties.getStringValue("ReviewCycle"));
		setStatus(properties.getStringValue("Status"));
		setSystemCode(properties.getStringValue("SystemCode"));
		setPurchaseOrder(properties.getStringValue("PurchaseOrder"));
		setLegacyDocDwgNo(properties.getStringValue("LegacyDocDwgNo"));
		setFacility(properties.getStringValue("Facility"));
		setRetentionCode(properties.getStringValue("RetentionCode"));
		setCopcRevisionNo(properties.getStringValue("COPCRevisionNo"));
		setSheetSize(properties.getStringValue("SheetSize"));
		setDensSequence(properties.getInteger32Value("DENSSequence") == null ? 0 : properties.getInteger32Value("DENSSequence"));
		setDensLevel(properties.getInteger32Value("DENSLevel") == null ? 0 : properties.getInteger32Value("DENSLevel"));
	}

	/* (非 Javadoc)
	 * <p>Title: convertProperties2Map</p>
	 * <p>Description: </p>
	 * @param map
	 * @param abstractDocumentInfo
	 * @see com.ccpws.service.datatype.DocumentInfo#fromVo(java.util.Map, com.ccpws.service.datatype.AbstractDocumentInfo)
	 */
	@Override
	public void fromVo(Map<String, Object> map) {
		super.fromVo(map);
		if (getFormat() != null) {
			map.put("Format", getFormat());
		}
		if (getSheetNo() != null) {
			map.put("SheetNo", getSheetNo());
		}
		if (getCountryCode() != null) {
			map.put("CountryCode", getCountryCode());
		}
		if (getDens1() != null) {
			map.put("DENS1", getDens1());
		}
		if (getDens2() != null) {
			map.put("DENS2", getDens2());
		}
		if (getDens3() != null) {
			map.put("DENS3", getDens3());
		}
		if (getDens4() != null) {
			map.put("DENS4", getDens4());
		}
		if (getDens5() != null) {
			map.put("DENS5", getDens5());
		}
		if (getDensNumber() != null) {
			map.put("DENSNumber", getDensNumber());
		}
		if (getDensSeqLen() != null) {
			map.put("DENSSeqLen", getDensSeqLen());
		}
		if (getDensType() != null) {
			map.put("DENSType", getDensType());
		}
		if (getProjectNo() != null) {
			map.put("ProjectNo", getProjectNo());
		}
		if (getUtilisationCategory() != null) {
			map.put("UtilisationCategory", getUtilisationCategory());
		}
		if (getSupplierExternalDocDwgNo() != null) {
			map.put("SupplierExternalDocDwgNo", getSupplierExternalDocDwgNo());
		}
		if (getTitle() != null) {
			map.put("Title", getTitle());
		}
		if (getDocDwgType() != null) {
			map.put("DocDwgType", getDocDwgType());
		}
		if (getCompany() != null) {
			map.put("Company", getCompany());
		}
		if (getDiscipline() != null) {
			map.put("Discipline", getDiscipline());
		}
		if (getLocationCode() != null) {
			map.put("LocationCode", getLocationCode());
		}
		if (getTransmittalNumber() != null) {
			map.put("TransmittalNumber", getTransmittalNumber());
		}
		if (getAuthor() != null) {
			map.put("Author", getAuthor());
		}
		if (getLastReviewedDate() != null) {
			map.put("LastReviewedDate", getLastReviewedDate());
		}
		if (getApprover() != null) {
			map.put("Approver", getApprover());
		}
		if (getRegion() != null) {
			map.put("Region", getRegion());
		}
		if (getIssueRevisionDate() != null) {
			map.put("IssueRevisionDate", getIssueRevisionDate());
		}
		if (getReviewCycle() != null) {
			map.put("ReviewCycle", getReviewCycle());
		}
		if (getStatus() != null) {
			map.put("Status", getStatus());
		}
		if (getSystemCode() != null) {
			map.put("SystemCode", getSystemCode());
		}
		if (getPurchaseOrder() != null) {
			map.put("PurchaseOrder", getPurchaseOrder());
		}
		if (getLegacyDocDwgNo() != null) {
			map.put("LegacyDocDwgNo", getLegacyDocDwgNo());
		}
		if (getFacility() != null) {
			map.put("Facility", getFacility());
		}
		if (getRetentionCode() != null) {
			map.put("RetentionCode", getRetentionCode());
		}
		if (getCopcRevisionNo() != null) {
			map.put("COPCRevisionNo", getCopcRevisionNo());
		}
		if (getSheetSize() != null) {
			map.put("SheetSize", getSheetSize());
		}
		if (getDensSequence() != 0) {
			map.put("DENSSequence", getDensSequence());
		}
		if (getDensLevel() != 0) {
			map.put("DENSLevel", getDensLevel());
		}
	}


	/**
	 * @return format
	 */
	public String getFormat() {
		return format;
	}

	/**
	 * @param format format to set
	 */
	public void setFormat(String format) {
		this.format = format;
	}

	/**
	 * @return sheetNo
	 */
	public String getSheetNo() {
		return sheetNo;
	}

	/**
	 * @param sheetNo sheetNo to set
	 */
	public void setSheetNo(String sheetNo) {
		this.sheetNo = sheetNo;
	}

	/**
	 * @return countryCode
	 */
	public String getCountryCode() {
		return countryCode;
	}

	/**
	 * @param countryCode countryCode to set
	 */
	public void setCountryCode(String countryCode) {
		this.countryCode = countryCode;
	}

	/**
	 * @return dens1
	 */
	public String getDens1() {
		return dens1;
	}

	/**
	 * @param dens1 dens1 to set
	 */
	public void setDens1(String dens1) {
		this.dens1 = dens1;
	}

	/**
	 * @return dens2
	 */
	public String getDens2() {
		return dens2;
	}

	/**
	 * @param dens2 dens2 to set
	 */
	public void setDens2(String dens2) {
		this.dens2 = dens2;
	}

	/**
	 * @return dens3
	 */
	public String getDens3() {
		return dens3;
	}

	/**
	 * @param dens3 dens3 to set
	 */
	public void setDens3(String dens3) {
		this.dens3 = dens3;
	}

	/**
	 * @return dens4
	 */
	public String getDens4() {
		return dens4;
	}

	/**
	 * @param dens4 dens4 to set
	 */
	public void setDens4(String dens4) {
		this.dens4 = dens4;
	}

	/**
	 * @return dens5
	 */
	public String getDens5() {
		return dens5;
	}

	/**
	 * @param dens5 dens5 to set
	 */
	public void setDens5(String dens5) {
		this.dens5 = dens5;
	}

	/**
	 * @return densNumber
	 */
	public String getDensNumber() {
		return densNumber;
	}

	/**
	 * @param densNumber densNumber to set
	 */
	public void setDensNumber(String densNumber) {
		this.densNumber = densNumber;
	}

	/**
	 * @return densSeqLen
	 */
	public String getDensSeqLen() {
		return densSeqLen;
	}

	/**
	 * @param densSeqLen densSeqLen to set
	 */
	public void setDensSeqLen(String densSeqLen) {
		this.densSeqLen = densSeqLen;
	}

	/**
	 * @return densType
	 */
	public String getDensType() {
		return densType;
	}

	/**
	 * @param densType densType to set
	 */
	public void setDensType(String densType) {
		this.densType = densType;
	}

	/**
	 * @return projectNo
	 */
	public String getProjectNo() {
		return projectNo;
	}

	/**
	 * @param projectNo projectNo to set
	 */
	public void setProjectNo(String projectNo) {
		this.projectNo = projectNo;
	}

	/**
	 * @return utilisationCategory
	 */
	public String getUtilisationCategory() {
		return utilisationCategory;
	}

	/**
	 * @param utilisationCategory utilisationCategory to set
	 */
	public void setUtilisationCategory(String utilisationCategory) {
		this.utilisationCategory = utilisationCategory;
	}

	/**
	 * @return supplierExternalDocDwgNo
	 */
	public String getSupplierExternalDocDwgNo() {
		return supplierExternalDocDwgNo;
	}

	/**
	 * @param supplierExternalDocDwgNo supplierExternalDocDwgNo to set
	 */
	public void setSupplierExternalDocDwgNo(String supplierExternalDocDwgNo) {
		this.supplierExternalDocDwgNo = supplierExternalDocDwgNo;
	}

	/**
	 * @return title
	 */
	public String getTitle() {
		return title;
	}

	/**
	 * @param title title to set
	 */
	public void setTitle(String title) {
		this.title = title;
	}

	/**
	 * @return docDwgType
	 */
	public String getDocDwgType() {
		return docDwgType;
	}

	/**
	 * @param docDwgType docDwgType to set
	 */
	public void setDocDwgType(String docDwgType) {
		this.docDwgType = docDwgType;
	}

	/**
	 * @return company
	 */
	public String getCompany() {
		return company;
	}

	/**
	 * @param company company to set
	 */
	public void setCompany(String company) {
		this.company = company;
	}

	/**
	 * @return discipline
	 */
	public String getDiscipline() {
		return discipline;
	}

	/**
	 * @param discipline discipline to set
	 */
	public void setDiscipline(String discipline) {
		this.discipline = discipline;
	}

	/**
	 * @return locationCode
	 */
	public String getLocationCode() {
		return locationCode;
	}

	/**
	 * @param locationCode locationCode to set
	 */
	public void setLocationCode(String locationCode) {
		this.locationCode = locationCode;
	}

	/**
	 * @return transmittalNumber
	 */
	public String getTransmittalNumber() {
		return transmittalNumber;
	}

	/**
	 * @param transmittalNumber transmittalNumber to set
	 */
	public void setTransmittalNumber(String transmittalNumber) {
		this.transmittalNumber = transmittalNumber;
	}

	/**
	 * @return author
	 */
	public String getAuthor() {
		return author;
	}

	/**
	 * @param author author to set
	 */
	public void setAuthor(String author) {
		this.author = author;
	}

	/**
	 * @return lastReviewedDate
	 */

	public Date getLastReviewedDate() {
		return lastReviewedDate;
	}

	/**
	 * @param lastReviewedDate lastReviewedDate to set
	 */
	public void setLastReviewedDate(Date lastReviewedDate) {
		this.lastReviewedDate = lastReviewedDate;
	}

	/**
	 * @return approver
	 */
	public String getApprover() {
		return approver;
	}

	/**
	 * @param approver approver to set
	 */
	public void setApprover(String approver) {
		this.approver = approver;
	}

	/**
	 * @return region
	 */
	public String getRegion() {
		return region;
	}

	/**
	 * @param region region to set
	 */
	public void setRegion(String region) {
		this.region = region;
	}

	/**
	 * @return issueRevisionDate
	 */

	public Date getIssueRevisionDate() {
		return issueRevisionDate;
	}

	/**
	 * @param issueRevisionDate issueRevisionDate to set
	 */
	public void setIssueRevisionDate(Date issueRevisionDate) {
		this.issueRevisionDate = issueRevisionDate;
	}

	/**
	 * @return reviewCycle
	 */
	public String getReviewCycle() {
		return reviewCycle;
	}

	/**
	 * @param reviewCycle reviewCycle to set
	 */
	public void setReviewCycle(String reviewCycle) {
		this.reviewCycle = reviewCycle;
	}

	/**
	 * @return status
	 */
	public String getStatus() {
		return status;
	}

	/**
	 * @param status status to set
	 */
	public void setStatus(String status) {
		this.status = status;
	}

	/**
	 * @return systemCode
	 */
	public String getSystemCode() {
		return systemCode;
	}

	/**
	 * @param systemCode systemCode to set
	 */
	public void setSystemCode(String systemCode) {
		this.systemCode = systemCode;
	}

	/**
	 * @return purchaseOrder
	 */
	public String getPurchaseOrder() {
		return purchaseOrder;
	}

	/**
	 * @param purchaseOrder purchaseOrder to set
	 */
	public void setPurchaseOrder(String purchaseOrder) {
		this.purchaseOrder = purchaseOrder;
	}

	/**
	 * @return legacyDocDwgNo
	 */
	public String getLegacyDocDwgNo() {
		return legacyDocDwgNo;
	}

	/**
	 * @param legacyDocDwgNo legacyDocDwgNo to set
	 */
	public void setLegacyDocDwgNo(String legacyDocDwgNo) {
		this.legacyDocDwgNo = legacyDocDwgNo;
	}

	/**
	 * @return facility
	 */
	public String getFacility() {
		return facility;
	}

	/**
	 * @param facility facility to set
	 */
	public void setFacility(String facility) {
		this.facility = facility;
	}

	/**
	 * @return retentionCode
	 */
	public String getRetentionCode() {
		return retentionCode;
	}

	/**
	 * @param retentionCode retentionCode to set
	 */
	public void setRetentionCode(String retentionCode) {
		this.retentionCode = retentionCode;
	}

	/**
	 * @return copcRevisionNo
	 */
	public String getCopcRevisionNo() {
		return copcRevisionNo;
	}

	/**
	 * @param copcRevisionNo copcRevisionNo to set
	 */
	public void setCopcRevisionNo(String copcRevisionNo) {
		this.copcRevisionNo = copcRevisionNo;
	}

	/**
	 * @return sheetSize
	 */
	public String getSheetSize() {
		return sheetSize;
	}

	/**
	 * @param sheetSize sheetSize to set
	 */
	public void setSheetSize(String sheetSize) {
		this.sheetSize = sheetSize;
	}

	/**
	 * @return densSequence
	 */
	public int getDensSequence() {
		return densSequence;
	}

	/**
	 * @param densSequence densSequence to set
	 */
	public void setDensSequence(int densSequence) {
		this.densSequence = densSequence;
	}

	/**
	 * @return densLevel
	 */
	public int getDensLevel() {
		return densLevel;
	}

	/**
	 * @param densLevel densLevel to set
	 */
	public void setDensLevel(int densLevel) {
		this.densLevel = densLevel;
	}
}
