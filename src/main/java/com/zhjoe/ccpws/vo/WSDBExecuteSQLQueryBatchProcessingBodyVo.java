/**
 * @Title: WSDBExecuteSQLQueryBatchProcessingBodyVo.java
 * @Package com.ccpws.vo
 * @Description: TODO(用一句话描述该文件做什么)
 * @author Changling Jiang
 * @date 2014-4-2 下午06:56:12
 * @version V1.0
 */
package com.zhjoe.ccpws.vo;

import java.util.List;

/**
 * @ClassName: WSDBExecuteSQLQueryBatchProcessingBodyVo
 * @Description: TODO(这里用一句话描述这个类的作用)
 * @author Changling Jiang
 * @date 2014-4-2 下午06:56:12
 *
 */
public class WSDBExecuteSQLQueryBatchProcessingBodyVo {

	private List<BatchProcessingVo> batchProcessing;

	/**
	 * @return batchProcessing
	 */
	public List<BatchProcessingVo> getBatchProcessing() {
		return batchProcessing;
	}

	/**
	 * @param batchProcessing batchProcessing to set
	 */
	public void setBatchProcessing(List<BatchProcessingVo> batchProcessing) {
		this.batchProcessing = batchProcessing;
	}


}
