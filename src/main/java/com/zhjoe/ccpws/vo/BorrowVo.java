package com.zhjoe.ccpws.vo;

import java.util.Date;


public class BorrowVo {
    /**
     * 主键id
     */
    private int id;
    /**
     * 文档CEid
     */
    private String CEid;
    /**
     * 文档版本id
     */
    private String VSid;
    /**
     * 名称
     */
    private String name;
    /**
     * 借阅时间
     */
    private Date borrowaTime;
    /**
     * 借阅人
     */
    private String borrower;
    /**
     * 借阅状态
     *-1--拒绝
     * 0--借阅车
     * 1--审批中
     * 2--借阅成功
     * 3--授权回收
     */
    private int borrowState;
    /**
     * 大小
     */
    private String contentSize;
    /**
     * mimeType; 
     */
    private String mimeType;
    
    /**
     * 到期日期
     */
    private Date expireDate;
    /**
     * 剩余天数
     */
    private Integer remainingDays;
    
    /**
     * @return id
     */
    public int getId() {
        return id;
    }
    /**
     * @param id id to set
     */
    public void setId(int id) {
        this.id = id;
    }
    /**
     * @return cEid
     */
    public String getCEid() {
        return CEid;
    }
    /**
     * @param cEid cEid to set
     */
    public void setCEid(String cEid) {
        CEid = cEid;
    }
    /**
     * @return vSid
     */
    public String getVSid() {
        return VSid;
    }
    /**
     * @param vSid vSid to set
     */
    public void setVSid(String vSid) {
        VSid = vSid;
    }
    /**
     * @return borrowaTime
     */
    public Date getBorrowaTime() {
        return borrowaTime;
    }
    /**
     * @param borrowaTime borrowaTime to set
     */
    public void setBorrowaTime(Date borrowaTime) {
        this.borrowaTime = borrowaTime;
    }
    /**
     * @return borrower
     */
    public String getBorrower() {
        return borrower;
    }
    /**
     * @param borrower borrower to set
     */
    public void setBorrower(String borrower) {
        this.borrower = borrower;
    }
    /**
     * @return borrowState
     */
    public int getBorrowState() {
        return borrowState;
    }
    /**
     * @param borrowState borrowState to set
     */
    public void setBorrowState(int borrowState) {
        this.borrowState = borrowState;
    }
    /**
     * @return name
     */
    public String getName() {
        return name;
    }
    /**
     * @param name name to set
     */
    public void setName(String name) {
        this.name = name;
    }
    /**
     * @return contentSize
     */
    public String getContentSize() {
        return contentSize;
    }
    /**
     * @param contentSize contentSize to set
     */
    public void setContentSize(String contentSize) {
        this.contentSize = contentSize;
    }
    /**
     * @return mimeType
     */
    public String getMimeType() {
        return mimeType;
    }
    /**
     * @param mimeType mimeType to set
     */
    public void setMimeType(String mimeType) {
        this.mimeType = mimeType;
    }
    /**
     * @return expireDate
     */
    public Date getExpireDate() {
        return expireDate;
    }
    /**
     * @param expireDate expireDate to set
     */
    public void setExpireDate(Date expireDate) {
        this.expireDate = expireDate;
    }
    /**
     * @return remainingDays
     */
    public Integer getRemainingDays() {
        return remainingDays;
    }
    /**
     * @param remainingDays remainingDays to set
     */
    public void setRemainingDays(Integer remainingDays) {
        this.remainingDays = remainingDays;
    }

    
}
