/**
 * @Title: WSCSLocationBodyVo.java
 * @Package com.ccpws.vo
 * @Description: TODO(用一句话描述该文件做什么)
 * @author Changling Jiang
 * @date 2014-2-20 上午10:30:12
 * @version V1.0
 */
package com.zhjoe.ccpws.vo;

import java.util.List;

/**
 * @ClassName: WSCSLocationBodyVo
 * @Description: TODO(这里用一句话描述这个类的作用)
 * @author Changling Jiang
 * @date 2014-2-20 上午10:30:12
 *
 */
public class WSCSLocationBodyVo {

	private List<String> location;

	/**
	 * @return location
	 */
	public List<String> getLocation() {
		return location;
	}

	/**
	 * @param location location to set
	 */
	public void setLocation(List<String> location) {
		this.location = location;
	}

}
