/**
 * @Title: WSSMLoginResultVo.java
 * @Package com.ccpws.vo
 * @Description: TODO(用一句话描述该文件做什么)
 * @author Changling Jiang
 * @date 2014-2-19 下午03:06:41
 * @version V1.0
 */
package com.zhjoe.ccpws.vo;

/**
 * @ClassName: WSSMLoginResultVo
 * @Description: TODO(这里用一句话描述这个类的作用)
 * @author Changling Jiang
 * @date 2014-2-19 下午03:06:41
 *
 */
public class WSSMLoginResultVo extends WSBaseResultVo {

	private WSSMLoginBodyVo body;

	/**
	 * @return body
	 */
	public WSSMLoginBodyVo getBody() {
		return body;
	}

	/**
	 * @param body body to set
	 */
	public void setBody(WSSMLoginBodyVo body) {
		this.body = body;
	}


}
