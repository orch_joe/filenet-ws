/**
 * @Title: UserVo.java
 * @Package com.ccpws.vo
 * @Description: TODO(用一句话描述该文件做什么)
 * @author Changling Jiang
 * @date 2014-2-13 下午02:54:07
 * @version V1.0
 */
package com.zhjoe.ccpws.vo;

import java.util.List;

/**
 * @ClassName: UserVo
 * @Description: TODO(这里用一句话描述这个类的作用)
 * @author Changling Jiang
 * @date 2014-2-13 下午02:54:07
 *
 */
public class UserVo {


	private String name;
	private String distinguishedName;
	private String shortName;
	private String password;
	private String displayName;
	private List<String> memberOfGroups;//LDAP中所属组
	public List<String> getMemberOfGroups() {
		return memberOfGroups;
	}
	public void setMemberOfGroups(List<String> memberOfGroups) {
		this.memberOfGroups = memberOfGroups;
	}
	/**
	 * @return name
	 */
	public String getName() {
		return name;
	}
	/**
	 * @param name name to set
	 */
	public void setName(String name) {
		this.name = name;
	}
	/**
	 * @return distinguishedName
	 */
	public String getDistinguishedName() {
		return distinguishedName;
	}
	/**
	 * @param distinguishedName distinguishedName to set
	 */
	public void setDistinguishedName(String distinguishedName) {
		this.distinguishedName = distinguishedName;
	}
	/**
	 * @return shortName
	 */
	public String getShortName() {
		return shortName;
	}
	/**
	 * @param shortName shortName to set
	 */
	public void setShortName(String shortName) {
		this.shortName = shortName;
	}
	/**
	 * @return password
	 */
	public String getPassword() {
		return password;
	}
	/**
	 * @param password password to set
	 */
	public void setPassword(String password) {
		this.password = password;
	}
	/**
	 * @return displayName
	 */
	public String getDisplayName() {
		return displayName;
	}
	/**
	 * @param displayName displayName to set
	 */
	public void setDisplayName(String displayName) {
		this.displayName = displayName;
	}

}
