/**
 * @Title: WSDBExecuteSQLQueryBatchProcessDetailedResultVo.java
 * @Package com.ccpws.vo
 * @Description: TODO(用一句话描述该文件做什么)
 * @author Changling Jiang
 * @date 2014-4-2 下午06:54:01
 * @version V1.0
 */
package com.zhjoe.ccpws.vo;

/**
 * @ClassName: WSDBExecuteSQLQueryBatchProcessDetailedResultVo
 * @Description: TODO(这里用一句话描述这个类的作用)
 * @author Changling Jiang
 * @date 2014-4-2 下午06:54:01
 *
 */
public class WSDBExecuteSQLQueryBatchProcessDetailedResultVo extends WSBaseResultVo {

	private WSDBExecuteSQLQueryBatchProcessDetailedBodyVo body;

	/**
	 * @return body
	 */
	public WSDBExecuteSQLQueryBatchProcessDetailedBodyVo getBody() {
		return body;
	}

	/**
	 * @param body body to set
	 */
	public void setBody(WSDBExecuteSQLQueryBatchProcessDetailedBodyVo body) {
		this.body = body;
	}


}
