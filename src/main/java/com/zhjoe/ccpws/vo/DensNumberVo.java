/**
 * @Title: DensNumberVo.java
 * @Package com.ccp.vo
 * @Description: TODO(用一句话描述该文件做什么)
 * @author Shushuai Gu
 * @date 2014年6月18日 下午2:47:05
 * @version V1.0
 */
package com.zhjoe.ccpws.vo;

/**
 * @ClassName: DensNumberVo
 * @Description: TODO(这里用一句话描述这个类的作用)
 * @author Shushuai Gu
 * @date 2014年6月18日 下午2:47:05
 *
 */
public class DensNumberVo {
	/**
	 * 主键ID
	 */
	public Integer id;
	/**
	 * DENS Type
	 */
	public String densType;
	/**
	 * 前缀
	 */
	public String prefix;
	/**
	 * 当前值
	 */
	public Integer currentValue;

	/**
	 * @return id
	 */
	public Integer getId() {
		return id;
	}

	/**
	 * @param id id to set
	 */
	public void setId(Integer id) {
		this.id = id;
	}

	/**
	 * @return densType
	 */
	public String getDensType() {
		return densType;
	}

	/**
	 * @param densType densType to set
	 */
	public void setDensType(String densType) {
		this.densType = densType;
	}

	/**
	 * @return prefix
	 */
	public String getPrefix() {
		return prefix;
	}

	/**
	 * @param prefix prefix to set
	 */
	public void setPrefix(String prefix) {
		this.prefix = prefix;
	}

	/**
	 * @return currentValue
	 */
	public Integer getCurrentValue() {
		return currentValue;
	}

	/**
	 * @param currentValue currentValue to set
	 */
	public void setCurrentValue(Integer currentValue) {
		this.currentValue = currentValue;
	}


}
