/**
 * @Title: WSDBExecuteSQLQueryLvlResultVo.java
 * @Package com.ccpws.vo
 * @Description: TODO(用一句话描述该文件做什么)
 * @author Changling Jiang
 * @date 2014年4月23日 上午10:12:54
 * @version V1.0
 */
package com.zhjoe.ccpws.vo;

/**
 * @ClassName: WSDBExecuteSQLQueryLvlResultVo
 * @Description: TODO(这里用一句话描述这个类的作用)
 * @author Changling Jiang
 * @date 2014年4月23日 上午10:12:54
 *
 */
public class WSDBExecuteSQLQueryLvlResultVo extends WSBaseResultVo {

	private WSDBExecuteSQLQueryLvlBodyVo body;

	/**
	 * @return body
	 */
	public WSDBExecuteSQLQueryLvlBodyVo getBody() {
		return body;
	}

	/**
	 * @param body body to set
	 */
	public void setBody(WSDBExecuteSQLQueryLvlBodyVo body) {
		this.body = body;
	}


}
