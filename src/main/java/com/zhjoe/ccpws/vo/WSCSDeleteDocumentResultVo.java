package com.zhjoe.ccpws.vo;

public class WSCSDeleteDocumentResultVo extends WSBaseResultVo {
    private WSCSDeleteDocumentBodyVo body;

    public WSCSDeleteDocumentBodyVo getBody() {
        return body;
    }

    public void setBody(WSCSDeleteDocumentBodyVo body) {
        this.body = body;
    }
}
