package com.zhjoe.ccpws.vo;

import java.util.List;

import com.zhjoe.strongit.filenet.service.datatype.DocumentInfo;
import com.zhjoe.strongit.filenet.service.datatype.Pagination;

public class WSCSSearchDocumentBodyVo {
    private List<DocumentInfo> document;
    private Pagination pagination;
    /**
     * @return document
     */
    public List<DocumentInfo> getDocument() {
        return document;
    }

    /**
     * @param document document to set
     */
    public void setDocument(List<DocumentInfo> document) {
        this.document = document;
    }

    public Pagination getPagination() {
        return pagination;
    }

    public void setPagination(Pagination pagination) {
        this.pagination = pagination;
    }
}
