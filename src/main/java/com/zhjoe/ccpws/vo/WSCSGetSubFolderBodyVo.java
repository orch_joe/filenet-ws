package com.zhjoe.ccpws.vo;

import java.util.List;

import com.zhjoe.strongit.filenet.service.datatype.BaseInfo;

public class WSCSGetSubFolderBodyVo {
    private BaseInfo parentFolder;
    private List<BaseInfo> subFolders;
    public BaseInfo getParentFolder() {
        return parentFolder;
    }
    public void setParentFolder(BaseInfo parentFolder) {
        this.parentFolder = parentFolder;
    }
    public List<BaseInfo> getSubFolders() {
        return subFolders;
    }
    public void setSubFolders(List<BaseInfo> subFolders) {
        this.subFolders = subFolders;
    }
}
