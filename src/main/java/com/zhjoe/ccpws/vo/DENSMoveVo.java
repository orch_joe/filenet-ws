/**
 * @Title: DENSMoveVo.java
 * @Package com.ccpws.vo
 * @Description: TODO(用一句话描述该文件做什么)
 * @author Changling Jiang
 * @date 2014-3-5 下午03:21:52
 * @version V1.0
 */
package com.zhjoe.ccpws.vo;

import com.filenet.api.core.Document;
import com.filenet.api.core.Folder;

/**
 * @ClassName: DENSMoveVo
 * @Description: TODO(这里用一句话描述这个类的作用)
 * @author Changling Jiang
 * @date 2014-3-5 下午03:21:52
 *
 */
public class DENSMoveVo {

	private Folder from;
	private String path;//原始路径
	private Folder to;
	private Document document;

	/**
	 * <p>Title: </p>
	 * <p>Description: </p>
	 * @param from
	 * @param to
	 * @param document
	 */
	public DENSMoveVo(Folder from, Folder to, Document document) {
		this.from = from;
		this.to = to;
		this.document = document;
	}

	/**
	 * @return from
	 */
	public Folder getFrom() {
		return from;
	}

	/**
	 * @param from from to set
	 */
	public void setFrom(Folder from) {
		this.from = from;
	}

	/**
	 * @return to
	 */
	public Folder getTo() {
		return to;
	}

	/**
	 * @param to to to set
	 */
	public void setTo(Folder to) {
		this.to = to;
	}

	/**
	 * @return document
	 */
	public Document getDocument() {
		return document;
	}

	/**
	 * @param document document to set
	 */
	public void setDocument(Document document) {
		this.document = document;
	}


}
