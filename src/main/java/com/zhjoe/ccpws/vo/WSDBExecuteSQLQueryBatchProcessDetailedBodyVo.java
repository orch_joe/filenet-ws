/**
 * @Title: WSDBExecuteSQLQueryBatchProcessDetailedBodyVo.java
 * @Package com.ccpws.vo
 * @Description: TODO(用一句话描述该文件做什么)
 * @author Changling Jiang
 * @date 2014-4-2 下午06:54:38
 * @version V1.0
 */
package com.zhjoe.ccpws.vo;

import java.util.List;

/**
 * @ClassName: WSDBExecuteSQLQueryBatchProcessDetailedBodyVo
 * @Description: TODO(这里用一句话描述这个类的作用)
 * @author Changling Jiang
 * @date 2014-4-2 下午06:54:38
 *
 */
public class WSDBExecuteSQLQueryBatchProcessDetailedBodyVo {

	private List<BatchProcessDetailedVo> batchProcessDetailed;

	/**
	 * @return batchProcessDetailed
	 */
	public List<BatchProcessDetailedVo> getBatchProcessDetailed() {
		return batchProcessDetailed;
	}

	/**
	 * @param batchProcessDetailed batchProcessDetailed to set
	 */
	public void setBatchProcessDetailed(List<BatchProcessDetailedVo> batchProcessDetailed) {
		this.batchProcessDetailed = batchProcessDetailed;
	}


}
