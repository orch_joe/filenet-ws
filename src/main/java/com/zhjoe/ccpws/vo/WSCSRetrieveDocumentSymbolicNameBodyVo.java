/**
 * @Title: WSCSRetrieveDocumentSymbolicNameBodyVo.java
 * @Package com.ccpws.vo
 * @Description: TODO(用一句话描述该文件做什么)
 * @author Shushuai Gu
 * @date 2014-10-27 下午2:59:55
 * @version V1.0
 */
package com.zhjoe.ccpws.vo;

import java.util.List;

/**
 * @ClassName: WSCSRetrieveDocumentSymbolicNameBodyVo
 * @Description: TODO(这里用一句话描述这个类的作用)
 * @author Changling Jiang
 * @date 2014-10-27 下午2:59:55
 * 
 */
public class WSCSRetrieveDocumentSymbolicNameBodyVo {
	private List<Node> nodes;

	/**
	 * @return nodes
	 */
	public List<Node> getNodes() {
		return nodes;
	}

	/**
	 * @param nodes nodes to set
	 */
	public void setNodes(List<Node> nodes) {
		this.nodes = nodes;
	}
	
	
}
