/**
 * @Title: WSCSFindUsersBodyVo.java
 * @Package com.ccpws.vo
 * @Description: TODO(用一句话描述该文件做什么)
 * @author Changling Jiang
 * @date 2014年5月20日 下午2:27:01
 * @version V1.0
 */
package com.zhjoe.ccpws.vo;

import java.util.List;

/**
 * @ClassName: WSCSFindUsersBodyVo
 * @Description: TODO(这里用一句话描述这个类的作用)
 * @author Changling Jiang
 * @date 2014年5月20日 下午2:27:01
 *
 */
public class WSCSFindUsersBodyVo {

	private List<UserVo> user;

	/**
	 * @return user
	 */
	public List<UserVo> getUser() {
		return user;
	}

	/**
	 * @param user user to set
	 */
	public void setUser(List<UserVo> user) {
		this.user = user;
	}

}
