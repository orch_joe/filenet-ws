/**
 * @Title: WSHeaderVo.java
 * @Package com.ccpws.vo
 * @Description: TODO(用一句话描述该文件做什么)
 * @author Changling Jiang
 * @date 2014-2-18 下午12:57:07
 * @version V1.0
 */
package com.zhjoe.ccpws.vo;

/**
 * @ClassName: WSHeaderVo
 * @Description: TODO(这里用一句话描述这个类的作用)
 * @author Changling Jiang
 * @date 2014-2-18 下午12:57:07
 *
 */
public class WSHeaderVo {

	private String callId;
	private String resultCode = "EC00000";//EC00000正常返回
	private String message = "";

	/**
	 * @return callId
	 */
	public String getCallId() {
		return callId;
	}

	/**
	 * @param callId callId to set
	 */
	public void setCallId(String callId) {
		this.callId = callId;
	}

	/**
	 * @return resultCode
	 */
	public String getResultCode() {
		return resultCode;
	}

	/**
	 * @param resultCode resultCode to set
	 */
	public void setResultCode(String resultCode) {
		this.resultCode = resultCode;
	}

	/**
	 * @return message
	 */
	public String getMessage() {
		return message;
	}

	/**
	 * @param message message to set
	 */
	public void setMessage(String message) {
		this.message = message;
	}


}
