/**
 * @Title: WSCSBulkUploadBodyVo.java
 * @Package com.ccpws.vo
 * @Description: TODO(用一句话描述该文件做什么)
 * @author Changling Jiang
 * @date 2014-3-20 下午12:02:13
 * @version V1.0
 */
package com.zhjoe.ccpws.vo;

import java.util.List;
import java.util.Map;

/**
 * @ClassName: WSCSBulkUploadBodyVo
 * @Description: TODO(这里用一句话描述这个类的作用)
 * @author Changling Jiang
 * @date 2014-3-20 下午12:02:13
 *
 */
public class WSCSBulkUploadBodyVo {

	private List<WSCSBulkUDDocumentVo> files;

	/**
	 * @return files
	 */
	public List<WSCSBulkUDDocumentVo> getFiles() {
		return files;
	}

	/**
	 * @param files files to set
	 */
	public void setFiles(List<WSCSBulkUDDocumentVo> files) {
		this.files = files;
	}

	public String getDocumentIds() {
		return documentIds;
	}

	public void setDocumentIds(String documentIds) {
		this.documentIds = documentIds;
	}

	private String documentIds;
}
