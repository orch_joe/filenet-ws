/**
 * @Title: WSCSViewDENSBodyVo.java
 * @Package com.ccpws.vo
 * @Description: TODO(用一句话描述该文件做什么)
 * @author Changling Jiang
 * @date 2014-2-26 下午04:57:03
 * @version V1.0
 */
package com.zhjoe.ccpws.vo;


/**
 * @ClassName: WSCSViewDENSBodyVo
 * @Description: TODO(这里用一句话描述这个类的作用)
 * @author Changling Jiang
 * @date 2014-2-26 下午04:57:03
 *
 */
public class WSCSViewDENSBodyVo {

	private DocumentandDrawingDocumentInfo dens;

	/**
	 * @return dens
	 */
	public DocumentandDrawingDocumentInfo getDens() {
		return dens;
	}

	/**
	 * @param dens dens to set
	 */
	public void setDens(DocumentandDrawingDocumentInfo dens) {
		this.dens = dens;
	}

}
