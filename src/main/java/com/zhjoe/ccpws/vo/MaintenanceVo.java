/**
 * @Title: MaintenanceVo.java
 * @Package com.ccpws.vo
 * @Description: TODO(用一句话描述该文件做什么)
 * @author Shushuai Gu
 * @date 2014年4月22日 下午3:10:13
 * @version V1.0
 */
package com.zhjoe.ccpws.vo;

/**
 * @ClassName: MaintenanceVo
 * @Description: TODO(这里用一句话描述这个类的作用)
 * @author Shushuai Gu
 * @date 2014年4月22日 下午3:10:13
 *
 */
public class MaintenanceVo {
	private String lvl;
	private String attName;
	private String description;

	/**
	 * @return lvl
	 */
	public String getLvl() {
		return lvl;
	}

	/**
	 * @param lvl lvl to set
	 */
	public void setLvl(String lvl) {
		this.lvl = lvl;
	}

	/**
	 * @return attName
	 */
	public String getAttName() {
		return attName;
	}

	/**
	 * @param attName attName to set
	 */
	public void setAttName(String attName) {
		this.attName = attName;
	}

	/**
	 * @return description
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * @param description description to set
	 */
	public void setDescription(String description) {
		this.description = description;
	}

}
