/**
 * @Title: WSCSRevisionNoIsExistResultVo.java
 * @Package com.ccpws.vo
 * @Description: TODO(用一句话描述该文件做什么)
 * @author Changling Jiang
 * @date 2014-4-3 下午04:35:16
 * @version V1.0
 */
package com.zhjoe.ccpws.vo;

/**
 * @ClassName: WSCSRevisionNoIsExistResultVo
 * @Description: TODO(这里用一句话描述这个类的作用)
 * @author Changling Jiang
 * @date 2014-4-3 下午04:35:16
 *
 */
public class WSCSRevisionNoIsExistResultVo extends WSBaseResultVo {

	private WSCSRevisionNoIsExistBodyVo body;

	/**
	 * @return body
	 */
	public WSCSRevisionNoIsExistBodyVo getBody() {
		return body;
	}

	/**
	 * @param body body to set
	 */
	public void setBody(WSCSRevisionNoIsExistBodyVo body) {
		this.body = body;
	}

}
