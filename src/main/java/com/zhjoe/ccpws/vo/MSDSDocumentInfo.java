/**
 * @Title: MSDSDocumentInfo.java
 * @Package com.ccpws.vo
 * @Description: TODO(用一句话描述该文件做什么)
 * @author Changling Jiang
 * @date 2014-4-11 上午10:53:52
 * @version V1.0
 */
package com.zhjoe.ccpws.vo;

import java.util.Date;
import java.util.Map;


import com.filenet.api.core.Document;
import com.filenet.api.property.Properties;

/**
 * @ClassName: MSDSDocumentInfo
 * @Description: TODO(这里用一句话描述这个类的作用)
 * @author Changling Jiang
 * @date 2014-4-11 上午10:53:52
 *
 */
public class MSDSDocumentInfo extends RegularDocDocumentInfo {

	private Integer f;
	private Integer h;
	private String isRemain;
	private Date lastRevisedDate;
	private String location1;
	private String location2;
	private String location3;
	private String location4;
	private String location5;
	private String location6;
	private String location7;
	private String materialId;
	private String msdsNo;
	private Integer r;
	private String s;
	private String supplier;
	private String unit;
	private Date remindingDate;
	private String responsiblePerson;

	@Override
	protected void fromCustom(Document document) {
		super.fromCustom(document);
		Properties properties = document.getProperties();
		setF(properties.getInteger32Value("F"));
		setH(properties.getInteger32Value("H"));
		setIsRemain(properties.getStringValue("isRemain"));
		setLastRevisedDate(properties.getDateTimeValue("lastRevisedDate"));
		setLocation1(properties.getStringValue("location1"));
		setLocation2(properties.getStringValue("location2"));
		setLocation3(properties.getStringValue("location3"));
		setLocation4(properties.getStringValue("location4"));
		setLocation5(properties.getStringValue("location5"));
		setLocation6(properties.getStringValue("location6"));
		setLocation7(properties.getStringValue("location7"));
		setMaterialId(properties.getStringValue("materialID"));
		setMsdsNo(properties.getStringValue("MSDSNO"));
		setR(properties.getInteger32Value("R"));
		setS(properties.getStringValue("S"));
		setSupplier(properties.getStringValue("supplier"));
		setUnit(properties.getStringValue("unit"));
		setRemindingDate(properties.getDateTimeValue("remindingDate"));
		setResponsiblePerson(properties.getStringValue("responsiblePerson"));
	}

	@Override
	public void fromVo(Map<String, Object> map) {
		super.fromVo(map);
		if (getF() != null) {
			map.put("F", getF());
		}
		if (getH() != null) {
			map.put("H", getH());
		}
		if (getIsRemain() != null) {
			map.put("isRemain", getIsRemain());
		}
		if (getLastRevisedDate() != null) {
			map.put("lastRevisedDate", getLastRevisedDate());
		}
		if (getLocation1() != null) {
			map.put("location1", getLocation1());
		}
		if (getLocation2() != null) {
			map.put("location2", getLocation2());
		}
		if (getLocation3() != null) {
			map.put("location3", getLocation3());
		}
		if (getLocation4() != null) {
			map.put("location4", getLocation4());
		}
		if (getLocation5() != null) {
			map.put("location5", getLocation5());
		}
		if (getLocation6() != null) {
			map.put("location6", getLocation6());
		}
		if (getLocation7() != null) {
			map.put("location7", getLocation7());
		}
		if (getMaterialId() != null) {
			map.put("materialID", getMaterialId());
		}
		if (getMsdsNo() != null) {
			map.put("MSDSNO", getMsdsNo());
		}
		if (getR() != null) {
			map.put("R", getR());
		}
		if (getS() != null) {
			map.put("S", getS());
		}
		if (getSupplier() != null) {
			map.put("supplier", getSupplier());
		}
		if (getUnit() != null) {
			map.put("unit", getUnit());
		}
		if (getRemindingDate() != null) {
			map.put("remindingDate", getRemindingDate());
		}
		if (getResponsiblePerson() != null) {
			map.put("responsiblePerson", getResponsiblePerson());
		}
	}

	/**
	 * @return f
	 */
	public Integer getF() {
		return f;
	}

	/**
	 * @param f f to set
	 */
	public void setF(Integer f) {
		this.f = f;
	}

	/**
	 * @return h
	 */
	public Integer getH() {
		return h;
	}

	/**
	 * @param h h to set
	 */
	public void setH(Integer h) {
		this.h = h;
	}

	/**
	 * @return isRemain
	 */
	public String getIsRemain() {
		return isRemain;
	}

	/**
	 * @param isRemain isRemain to set
	 */
	public void setIsRemain(String isRemain) {
		this.isRemain = isRemain;
	}

	/**
	 * @return lastRevisedDate
	 */

	public Date getLastRevisedDate() {
		return lastRevisedDate;
	}

	/**
	 * @param lastRevisedDate lastRevisedDate to set
	 */
	public void setLastRevisedDate(Date lastRevisedDate) {
		this.lastRevisedDate = lastRevisedDate;
	}

	/**
	 * @return location1
	 */
	public String getLocation1() {
		return location1;
	}

	/**
	 * @param location1 location1 to set
	 */
	public void setLocation1(String location1) {
		this.location1 = location1;
	}

	/**
	 * @return location2
	 */
	public String getLocation2() {
		return location2;
	}

	/**
	 * @param location2 location2 to set
	 */
	public void setLocation2(String location2) {
		this.location2 = location2;
	}

	/**
	 * @return location3
	 */
	public String getLocation3() {
		return location3;
	}

	/**
	 * @param location3 location3 to set
	 */
	public void setLocation3(String location3) {
		this.location3 = location3;
	}

	/**
	 * @return location4
	 */
	public String getLocation4() {
		return location4;
	}

	/**
	 * @param location4 location4 to set
	 */
	public void setLocation4(String location4) {
		this.location4 = location4;
	}

	/**
	 * @return location5
	 */
	public String getLocation5() {
		return location5;
	}

	/**
	 * @param location5 location5 to set
	 */
	public void setLocation5(String location5) {
		this.location5 = location5;
	}

	/**
	 * @return location6
	 */
	public String getLocation6() {
		return location6;
	}

	/**
	 * @param location6 location6 to set
	 */
	public void setLocation6(String location6) {
		this.location6 = location6;
	}

	/**
	 * @return location7
	 */
	public String getLocation7() {
		return location7;
	}

	/**
	 * @param location7 location7 to set
	 */
	public void setLocation7(String location7) {
		this.location7 = location7;
	}

	/**
	 * @return materialId
	 */
	public String getMaterialId() {
		return materialId;
	}

	/**
	 * @param materialId materialId to set
	 */
	public void setMaterialId(String materialId) {
		this.materialId = materialId;
	}

	/**
	 * @return msdsNo
	 */
	public String getMsdsNo() {
		return msdsNo;
	}

	/**
	 * @param msdsNo msdsNo to set
	 */
	public void setMsdsNo(String msdsNo) {
		this.msdsNo = msdsNo;
	}

	/**
	 * @return r
	 */
	public Integer getR() {
		return r;
	}

	/**
	 * @param r r to set
	 */
	public void setR(Integer r) {
		this.r = r;
	}

	/**
	 * @return s
	 */
	public String getS() {
		return s;
	}

	/**
	 * @param s s to set
	 */
	public void setS(String s) {
		this.s = s;
	}

	/**
	 * @return supplier
	 */
	public String getSupplier() {
		return supplier;
	}

	/**
	 * @param supplier supplier to set
	 */
	public void setSupplier(String supplier) {
		this.supplier = supplier;
	}

	/**
	 * @return unit
	 */
	public String getUnit() {
		return unit;
	}

	/**
	 * @param unit unit to set
	 */
	public void setUnit(String unit) {
		this.unit = unit;
	}

	/**
	 * @return remindingDate
	 */
	public Date getRemindingDate() {
		return remindingDate;
	}

	/**
	 * @param remindingDate remindingDate to set
	 */
	public void setRemindingDate(Date remindingDate) {
		this.remindingDate = remindingDate;
	}

	/**
	 * @return responsiblePerson
	 */
	public String getResponsiblePerson() {
		return responsiblePerson;
	}

	/**
	 * @param responsiblePerson responsiblePerson to set
	 */
	public void setResponsiblePerson(String responsiblePerson) {
		this.responsiblePerson = responsiblePerson;
	}

}
