package com.zhjoe.ccpws.vo;

import java.util.Date;

public class ArchivesFileVo {
	//自增序列
	private int id;
	//文档名称
	private String docName;
	//文档ID
	private String docId;
	//归档时间
	private Date archiveTime;
	//文档原始路径
	private String oldPath;
	//文档归档路径
	private String archivePath;
	//归档是否成功标识   0：成功  1:失败
	private int archiveFlag;
	//归档信息
	private String archiveMessage;
	
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getDocName() {
		return docName;
	}
	public void setDocName(String docName) {
		this.docName = docName;
	}
	public String getDocId() {
		return docId;
	}
	public void setDocId(String docId) {
		this.docId = docId;
	}
	public String getOldPath() {
		return oldPath;
	}
	public void setOldPath(String oldPath) {
		this.oldPath = oldPath;
	}
	public String getArchivePath() {
		return archivePath;
	}
	public void setArchivePath(String archivePath) {
		this.archivePath = archivePath;
	}
	public int getArchiveFlag() {
		return archiveFlag;
	}
	public void setArchiveFlag(int archiveFlag) {
		this.archiveFlag = archiveFlag;
	}
	public String getArchiveMessage() {
		return archiveMessage;
	}
	public void setArchiveMessage(String archiveMessage) {
		this.archiveMessage = archiveMessage;
	}
	public Date getArchiveTime() {
		return archiveTime;
	}
	public void setArchiveTime(Date archiveTime) {
		this.archiveTime = archiveTime;
	}
}
