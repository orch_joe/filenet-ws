/**
 * @Title: WSDBQueryXml4SPOResultVo.java
 * @Package com.ccpws.vo
 * @Description: TODO(用一句话描述该文件做什么)
 * @author Changling Jiang
 * @date 2014年6月27日 上午9:28:44
 * @version V1.0
 */
package com.zhjoe.ccpws.vo;

/**
 * @ClassName: WSDBQueryXml4SPOResultVo
 * @Description: TODO(这里用一句话描述这个类的作用)
 * @author Changling Jiang
 * @date 2014年6月27日 上午9:28:44
 *
 */
public class WSDBQueryXml4SPOResultVo extends WSBaseResultVo {

	private WSDBQueryXml4SPOBodyVo body;

	/**
	 * @return body
	 */
	public WSDBQueryXml4SPOBodyVo getBody() {
		return body;
	}

	/**
	 * @param body body to set
	 */
	public void setBody(WSDBQueryXml4SPOBodyVo body) {
		this.body = body;
	}

}
