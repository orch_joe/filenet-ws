/**
 * @Title: WSDBExecuteSQLQueryLvlBodyVo.java
 * @Package com.ccpws.vo
 * @Description: TODO(用一句话描述该文件做什么)
 * @author Changling Jiang
 * @date 2014年4月23日 上午10:13:22
 * @version V1.0
 */
package com.zhjoe.ccpws.vo;

import java.util.List;

/**
 * @ClassName: WSDBExecuteSQLQueryLvlBodyVo
 * @Description: TODO(这里用一句话描述这个类的作用)
 * @author Changling Jiang
 * @date 2014年4月23日 上午10:13:22
 *
 */
public class WSDBExecuteSQLQueryLvlBodyVo {

	private List<MaintenanceVo> lvl;

	/**
	 * @return lvl
	 */
	public List<MaintenanceVo> getLvl() {
		return lvl;
	}

	/**
	 * @param lvl lvl to set
	 */
	public void setLvl(List<MaintenanceVo> lvl) {
		this.lvl = lvl;
	}


}
