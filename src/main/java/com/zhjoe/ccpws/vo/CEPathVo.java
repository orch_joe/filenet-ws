/**
 * @Title: CEPathVo.java
 * @Package com.ccp.vo
 * @Description: TODO(用一句话描述该文件做什么)
 * @author Changling Jiang
 * @date 2013年12月2日 下午12:25:10
 * @version V1.0
 */
package com.zhjoe.ccpws.vo;

/**
 * @ClassName: CEPathVo
 * @Description: FileNet Path 对象
 * @author Changling Jiang
 * @date 2013年12月2日 下午12:25:10
 *
 */
public class CEPathVo {

	/**
	 * 对象路径信息
	 */
	private String path;
	/**
	 * 对象Id
	 */
	private String id;
	/**
	 * 对象类型 folder||compound document
	 */
	private String type;
	/**
	 * compoundDocumentState
	 */
	private Integer compoundDocumentState;

	/**
	 * @return path
	 */
	public String getPath() {
		return path;
	}

	/**
	 * @param path path to set
	 */
	public void setPath(String path) {
		this.path = path;
	}

	/**
	 * @return id
	 */
	public String getId() {
		return id;
	}

	/**
	 * @param id id to set
	 */
	public void setId(String id) {
		this.id = id;
	}

	/**
	 * @return type
	 */
	public String getType() {
		return type;
	}

	/**
	 * @param type type to set
	 */
	public void setType(String type) {
		this.type = type;
	}

	/**
	 * @return compoundDocumentState
	 */
	public Integer getCompoundDocumentState() {
		return compoundDocumentState;
	}

	/**
	 * @param compoundDocumentState compoundDocumentState to set
	 */
	public void setCompoundDocumentState(Integer compoundDocumentState) {
		this.compoundDocumentState = compoundDocumentState;
	}
}
