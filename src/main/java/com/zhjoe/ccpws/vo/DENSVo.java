/**
 * @Title: DENSVo.java
 * @Package com.ccpws.vo
 * @Description: TODO(用一句话描述该文件做什么)
 * @author Changling Jiang
 * @date 2014-2-26 下午02:52:20
 * @version V1.0
 */
package com.zhjoe.ccpws.vo;

import java.util.List;
import java.util.Map;

import com.filenet.api.core.Document;

/**
 * @ClassName: DENSVo
 * @Description: TODO(这里用一句话描述这个类的作用)
 * @author Changling Jiang
 * @date 2014-2-26 下午02:52:20
 *
 */
public class DENSVo extends DocumentandDrawingDocumentInfo {

	private String action;//事件
	private String location;//Location
	private String filePath;//文档所在磁盘路径
	private String moveTo;//移动到哪里

	private List mocNumber;//MOCNumber
	private List tagNumber;//TagNumber

	public void fromVo(Map<String, Object> map) {
		super.fromVo(map);
	}

	/* (非 Javadoc)
	 * <p>Title: fromCustom</p>
	 * <p>Description: </p>
	 * @param document
	 * @see com.ccpws.service.datatype.DocumentandDrawingDocumentInfo#fromCustom(com.filenet.api.core.Document)
	 */
	@Override
	protected void fromCustom(Document document) {
		super.fromCustom(document);
	}

	/**
	 * @return action
	 */
	public String getAction() {
		return action;
	}

	/**
	 * @param action action to set
	 */
	public void setAction(String action) {
		this.action = action;
	}

	/**
	 * @return location
	 */
	public String getLocation() {
		return location;
	}

	/**
	 * @param location location to set
	 */
	public void setLocation(String location) {
		this.location = location;
	}

	/**
	 * @return filePath
	 */
	public String getFilePath() {
		return filePath;
	}

	/**
	 * @param filePath filePath to set
	 */
	public void setFilePath(String filePath) {
		this.filePath = filePath;
	}

	/**
	 * @return moveTo
	 */
	public String getMoveTo() {
		return moveTo;
	}

	/**
	 * @param moveTo moveTo to set
	 */
	public void setMoveTo(String moveTo) {
		this.moveTo = moveTo;
	}

	/**
	 * @return mocNumber
	 */
	public List getMocNumber() {
		return mocNumber;
	}

	/**
	 * @param mocNumber mocNumber to set
	 */
	public void setMocNumber(List mocNumber) {
		this.mocNumber = mocNumber;
	}

	/**
	 * @return tagNumber
	 */
	public List getTagNumber() {
		return tagNumber;
	}

	/**
	 * @param tagNumber tagNumber to set
	 */
	public void setTagNumber(List tagNumber) {
		this.tagNumber = tagNumber;
	}

}
