/**
 * @Title: FNSessionVo.java
 * @Package com.ccpws.vo
 * @Description: TODO(用一句话描述该文件做什么)
 * @author Changling Jiang
 * @date 2014-2-14 上午11:18:38
 * @version V1.0
 */
package com.zhjoe.ccpws.vo;

import com.zhjoe.strongit.filenet.service.ce.CEObjectStore;
import com.zhjoe.strongit.filenet.service.util.UserContextUtil;

import java.util.Calendar;

/**
 * @ClassName: FNSessionVo
 * @Description: TODO(这里用一句话描述这个类的作用)
 * @author Changling Jiang
 * @date 2014-2-14 上午11:18:38
 *
 */
public class FNSessionVo {

	private CEObjectStore ceObjectStore;
	private long osLastOperateTimeStamp;//objectStore 最后操作时间戳
	private int osState;//objectStore 操作状态     状态为0表示正常

	/**
	 * @return ceObjectStore
	 */
	public CEObjectStore getCeObjectStore() {
		osLastOperateTimeStamp = Calendar.getInstance().getTimeInMillis();
		return ceObjectStore;
	}

	/**
	 * @param ceObjectStore ceObjectStore to set
	 */
	public void setCeObjectStore(CEObjectStore ceObjectStore) {
		osLastOperateTimeStamp = Calendar.getInstance().getTimeInMillis();
		this.ceObjectStore = ceObjectStore;
	}

	/**
	 * @return osLastOperateTimeStamp
	 */
	public long getOsLastOperateTimeStamp() {
		return osLastOperateTimeStamp;
	}

	/**
	 * @param osLastOperateTimeStamp osLastOperateTimeStamp to set
	 */
	public void setOsLastOperateTimeStamp(long osLastOperateTimeStamp) {
		this.osLastOperateTimeStamp = osLastOperateTimeStamp;
	}

	/**
	 * @return osState
	 */
	public int getOsState() {
		return osState;
	}

	/**
	 * @param osState osState to set
	 */
	public void setOsState(int osState) {
		this.osState = osState;
	}

	public void free() {
		UserContextUtil.popSubject();
	}


}
