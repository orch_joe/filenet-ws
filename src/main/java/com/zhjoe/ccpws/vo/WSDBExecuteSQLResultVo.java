/**
 * @Title: WSDBExecuteSQLResultVo.java
 * @Package com.ccpws.vo
 * @Description: TODO(用一句话描述该文件做什么)
 * @author Changling Jiang
 * @date 2014-3-21 上午10:54:35
 * @version V1.0
 */
package com.zhjoe.ccpws.vo;

/**
 * @ClassName: WSDBExecuteSQLResultVo
 * @Description: TODO(这里用一句话描述这个类的作用)
 * @author Changling Jiang
 * @date 2014-3-21 上午10:54:35
 *
 */
public class WSDBExecuteSQLResultVo extends WSBaseResultVo {

	private WSDBExecuteSQLBodyVo body;

	/**
	 * @return body
	 */
	public WSDBExecuteSQLBodyVo getBody() {
		return body;
	}

	/**
	 * @param body body to set
	 */
	public void setBody(WSDBExecuteSQLBodyVo body) {
		this.body = body;
	}

}
