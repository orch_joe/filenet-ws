/**
 * @Title: WSCSRetrieveDocumentSymbolicNameResultVo.java
 * @Package com.ccpws.vo
 * @Description: TODO(用一句话描述该文件做什么)
 * @author Changling Jiang
 * @date 2014-3-25 上午10:23:19
 * @version V1.0
 */
package com.zhjoe.ccpws.vo;


/**
 * @ClassName: WSCSRetrieveDocumentSymbolicNameResultVo
 * @Description: TODO(这里用一句话描述这个类的作用)
 * @author Changling Jiang
 * @date 2014-3-25 上午10:23:19
 * 
 */
public class WSCSRetrieveDocumentSymbolicNameResultVo extends WSBaseResultVo {

	private WSCSRetrieveDocumentSymbolicNameBodyVo body;

	/**
	 * @return body
	 */
	public WSCSRetrieveDocumentSymbolicNameBodyVo getBody() {
		return body;
	}

	/**
	 * @param body body to set
	 */
	public void setBody(WSCSRetrieveDocumentSymbolicNameBodyVo body) {
		this.body = body;
	}

	

	
}
