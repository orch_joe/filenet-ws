package com.zhjoe.ccpws.vo;

import java.util.List;

import com.zhjoe.strongit.filenet.service.datatype.Grantee;

public class WSCSFolderPermissionBodyVo {
    private List<Grantee> granteeList;

    public List<Grantee> getGranteeList() {
        return granteeList;
    }

    public void setGranteeList(List<Grantee> granteeList) {
        this.granteeList = granteeList;
    }
}
