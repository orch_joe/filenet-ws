/**
 * @Title: LogVo.java
 * @Package com.ccpws.vo
 * @Description: TODO(用一句话描述该文件做什么)
 * @author Changling Jiang
 * @date 2014-2-13 下午12:47:32
 * @version V1.0
 */
package com.zhjoe.ccpws.vo;

import java.util.List;

/**
 * @ClassName: LogVo
 * @Description: TODO(这里用一句话描述这个类的作用)
 * @author Changling Jiang
 * @date 2014-2-13 下午12:47:32
 *
 */
public class LogVo {

	private List<ParameterVo> params;// 参数对象
	private String errorMessage;

	/**
	 * @return params
	 */
	public List<ParameterVo> getParams() {
		return params;
	}

	/**
	 * @param params params to set
	 */
	public void setParams(List<ParameterVo> params) {
		this.params = params;
	}

	/**
	 * @return errorMessage
	 */
	public String getErrorMessage() {
		return errorMessage;
	}

	/**
	 * @param errorMessage errorMessage to set
	 */
	public void setErrorMessage(String errorMessage) {
		this.errorMessage = errorMessage;
	}

}
