/**
 * @Title: WSCSDENSLocationResultVo.java
 * @Package com.ccpws.vo
 * @Description: TODO(用一句话描述该文件做什么)
 * @author Changling Jiang
 * @date 2014-3-17 下午04:10:29
 * @version V1.0
 */
package com.zhjoe.ccpws.vo;

/**
 * @ClassName: WSCSDENSLocationResultVo
 * @Description: TODO(这里用一句话描述这个类的作用)
 * @author Changling Jiang
 * @date 2014-3-17 下午04:10:29
 *
 */
public class WSCSDENSLocationResultVo extends WSBaseResultVo {

	private WSCSDENSLocationBodyVo body;

	/**
	 * @return body
	 */
	public WSCSDENSLocationBodyVo getBody() {
		return body;
	}

	/**
	 * @param body body to set
	 */
	public void setBody(WSCSDENSLocationBodyVo body) {
		this.body = body;
	}

}
