/**
 * @Title: WSCSLocationResultVo.java
 * @Package com.ccpws.vo
 * @Description: TODO(用一句话描述该文件做什么)
 * @author Changling Jiang
 * @date 2014-2-20 上午10:29:43
 * @version V1.0
 */
package com.zhjoe.ccpws.vo;

/**
 * @ClassName: WSCSLocationResultVo
 * @Description: TODO(这里用一句话描述这个类的作用)
 * @author Changling Jiang
 * @date 2014-2-20 上午10:29:43
 *
 */
public class WSCSLocationResultVo extends WSBaseResultVo {

	private WSCSLocationBodyVo body;

	/**
	 * @return body
	 */
	public WSCSLocationBodyVo getBody() {
		return body;
	}

	/**
	 * @param body body to set
	 */
	public void setBody(WSCSLocationBodyVo body) {
		this.body = body;
	}


}
