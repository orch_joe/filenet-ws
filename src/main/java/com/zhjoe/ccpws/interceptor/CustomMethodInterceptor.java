/**
 * @Title: CustomMethodInterceptor.java
 * @Package com.ccpws.interceptor
 * @Description: TODO(用一句话描述该文件做什么)
 * @author Changling Jiang
 * @date 2014-2-12 下午04:05:28
 * @version V1.0
 */
package com.zhjoe.ccpws.interceptor;

import org.aopalliance.intercept.MethodInterceptor;
import org.aopalliance.intercept.MethodInvocation;
import org.springframework.stereotype.Component;

/**
 * @ClassName: CustomMethodInterceptor
 * @Description: TODO(这里用一句话描述这个类的作用)
 * @author Changling Jiang
 * @date 2014-2-12 下午04:05:28
 *
 */
@Component
public class CustomMethodInterceptor implements MethodInterceptor {

	/* (非 Javadoc)
	 * <p>Title: invoke</p>
	 * <p>Description: </p>
	 * @param arg0
	 * @return
	 * @throws Throwable
	 * @see org.aopalliance.intercept.MethodInterceptor#invoke(org.aopalliance.intercept.MethodInvocation)
	 */
	public Object invoke(MethodInvocation mi) throws Throwable {
		System.out.println("invoke before");
		Object object = mi.proceed();
		System.out.println("invoke over");
		return object;
	}

}
