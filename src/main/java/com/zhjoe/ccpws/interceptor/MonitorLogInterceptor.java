/**
 * @Title: MonitorLogInterceptor.java
 * @Package com.ccpws.interceptor
 * @Description: TODO(用一句话描述该文件做什么)
 * @author Changling Jiang
 * @date 2014-2-13 下午12:43:19
 * @version V1.0
 */
package com.zhjoe.ccpws.interceptor;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.UUID;

import javax.servlet.http.HttpServletRequest;

import com.zhjoe.ccpws.vo.LogVo;
import com.zhjoe.ccpws.vo.MonitorLogVo;
import com.zhjoe.ccpws.vo.ParameterVo;
import org.aopalliance.intercept.MethodInterceptor;
import org.aopalliance.intercept.MethodInvocation;
import org.apache.cxf.message.Message;
import org.apache.cxf.phase.PhaseInterceptorChain;
import org.apache.cxf.transport.http.AbstractHTTPDestination;
import org.springframework.aop.framework.ReflectiveMethodInvocation;
import org.springframework.stereotype.Component;


/**
 * @ClassName: MonitorLogInterceptor
 * @Description: TODO(这里用一句话描述这个类的作用)
 * @author Changling Jiang
 * @date 2014-2-13 下午12:43:19
 *
 */
@Component
public class MonitorLogInterceptor implements MethodInterceptor {

	/* (非 Javadoc)
	 * <p>Title: invoke</p>
	 * <p>Description: </p>
	 * @param arg0
	 * @return
	 * @throws Throwable
	 * @see org.aopalliance.intercept.MethodInterceptor#invoke(org.aopalliance.intercept.MethodInvocation)
	 */
	public Object invoke(MethodInvocation mi) throws Throwable {

		if (ReflectiveMethodInvocation.class.equals(mi.getClass()) == false) {
			MonitorLogVo monitorLogVo = new MonitorLogVo();
			LogVo logVo = new LogVo();
			long timeInMillis = Calendar.getInstance().getTimeInMillis();

			Object[] args = mi.getArguments();
			String callId = "";

			String methodName = mi.getMethod().getName();
			monitorLogVo.setInterfaceName(methodName);
			monitorLogVo.setOperateTime(new Timestamp(timeInMillis));

			if (args != null && args.length > 0) {
				List<ParameterVo> list = new ArrayList<ParameterVo>();
				logVo.setParams(list);
				for (int i = 0; i < args.length; i++) {
					ParameterVo parameterVo = new ParameterVo();
					parameterVo.setIndex(i);
					parameterVo.setObject(args[i]);

					list.add(parameterVo);
				}
				callId = args[0] + "";
			}
			Message message = PhaseInterceptorChain.getCurrentMessage();
			if (message != null) {
				HttpServletRequest httprequest = (HttpServletRequest) message.get(AbstractHTTPDestination.HTTP_REQUEST);
				if (httprequest != null) {
					monitorLogVo.setRemoteHost(httprequest.getRemoteHost());
					monitorLogVo.setRemotePort(httprequest.getRemotePort());
				}
			}
			if (args != null && args.length > 0) {
				callId = args[0] + "";
				monitorLogVo.setCallId(callId);
			}
			if (!"login".equals(methodName)) {
				if (args != null && args.length > 1) {
					String sessionId = args[1] + "";
					monitorLogVo.setSessionId(sessionId);
					/*UserSession session = SessionManage.getInstance().getSession(sessionId);
					if (session != null){
						monitorLogVo.setLoginName(session.getUserLoginName());
						monitorLogVo.setSystemCode(session.getSystemCode());
					}*/
				}
			} else {
				monitorLogVo.setShortName(args[1] + "");
			}
			monitorLogVo.setId(UUID.randomUUID().toString());


			System.out.println("MethodInterceptor before");
			MonitorLogVo log = new MonitorLogVo();
			Object object = mi.proceed();
			System.out.println("MethodInterceptor over");
			return object;

		} else {
			return mi.proceed();
		}

	}

}
