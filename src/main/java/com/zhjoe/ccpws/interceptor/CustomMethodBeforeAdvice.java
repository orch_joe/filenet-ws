/**
 * @Title: CustomMethodBeforeAdvice.java
 * @Package com.ccpws.interceptor
 * @Description: TODO(用一句话描述该文件做什么)
 * @author Changling Jiang
 * @date 2014-2-12 下午04:03:48
 * @version V1.0
 */
package com.zhjoe.ccpws.interceptor;

import java.lang.reflect.Method;

import org.springframework.aop.MethodBeforeAdvice;
import org.springframework.stereotype.Component;

/**
 * @ClassName: CustomMethodBeforeAdvice
 * @Description: TODO(这里用一句话描述这个类的作用)
 * @author Changling Jiang
 * @date 2014-2-12 下午04:03:48
 *
 */
@Component
public class CustomMethodBeforeAdvice implements MethodBeforeAdvice {

	/* (非 Javadoc)
	 * <p>Title: before</p>
	 * <p>Description: </p>
	 * @param arg0
	 * @param arg1
	 * @param arg2
	 * @throws Throwable
	 * @see org.springframework.aop.MethodBeforeAdvice#before(java.lang.reflect.Method, java.lang.Object[], java.lang.Object)
	 */
	public void before(Method arg0, Object[] arg1, Object arg2) throws Throwable {
		System.out.println("custom before");
	}

}
