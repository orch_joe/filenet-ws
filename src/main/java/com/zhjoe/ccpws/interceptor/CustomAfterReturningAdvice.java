/**
 * @Title: CustomAfterReturningAdvice.java
 * @Package com.ccpws.interceptor
 * @Description: TODO(用一句话描述该文件做什么)
 * @author Changling Jiang
 * @date 2014-2-12 下午04:05:04
 * @version V1.0
 */
package com.zhjoe.ccpws.interceptor;

import java.lang.reflect.Method;

import org.springframework.aop.AfterReturningAdvice;
import org.springframework.stereotype.Component;

/**
 * @ClassName: CustomAfterReturningAdvice
 * @Description: TODO(这里用一句话描述这个类的作用)
 * @author Changling Jiang
 * @date 2014-2-12 下午04:05:04
 *
 */
@Component
public class CustomAfterReturningAdvice implements AfterReturningAdvice {

	/* (非 Javadoc)
	 * <p>Title: afterReturning</p>
	 * <p>Description: </p>
	 * @param arg0
	 * @param arg1
	 * @param arg2
	 * @param arg3
	 * @throws Throwable
	 * @see org.springframework.aop.AfterReturningAdvice#afterReturning(java.lang.Object, java.lang.reflect.Method, java.lang.Object[], java.lang.Object)
	 */
	public void afterReturning(Object arg0, Method arg1, Object[] arg2, Object arg3) throws Throwable {
		System.out.println("custom after");
	}

}
