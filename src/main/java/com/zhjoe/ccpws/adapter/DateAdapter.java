/**
 * @Title: DateAdapter.java
 * @Package com.ccpws.adapter
 * @Description: TODO(用一句话描述该文件做什么)
 * @author Changling Jiang
 * @date 2014-2-25 下午06:23:20
 * @version V1.0
 */
package com.zhjoe.ccpws.adapter;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.xml.bind.annotation.adapters.XmlAdapter;

/**
 * @ClassName: DateAdapter
 * @Description: TODO(这里用一句话描述这个类的作用)
 * @author Changling Jiang
 * @date 2014-2-25 下午06:23:20
 *
 */
public class DateAdapter extends XmlAdapter<String, Date> {

	private static DateFormat df = new SimpleDateFormat("yyyyMMdd");

	@Override
	public String marshal(Date date) throws Exception {
		return df.format(date);
	}

	@Override
	public Date unmarshal(String date) throws Exception {
		return df.parse(date);
	}


}
