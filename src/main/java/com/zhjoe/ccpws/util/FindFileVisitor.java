package com.zhjoe.ccpws.util;

import org.apache.commons.lang3.StringUtils;

import java.nio.file.FileVisitResult;
import java.nio.file.Path;
import java.nio.file.SimpleFileVisitor;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.LinkedList;
import java.util.List;

public class FindFileVisitor extends SimpleFileVisitor<Path> {

    private List<Path> filenameList = new LinkedList<>();
    private String pattern = null;

    public FindFileVisitor(String pattern) {
        this.pattern = pattern;
    }

    public FileVisitResult visitFile(Path file, BasicFileAttributes attrs) {
        String absPath = file.toString();
        Path fileName = file.getFileName();
        if(StringUtils.isNotEmpty(pattern)
                && fileName.toString().matches(pattern)){
            //正则表达式+文件后缀名匹配整个文件名
            //例如pattern \\d{13}  后缀.wav组成新的表达式  "\\d{13}\\.wav"
            filenameList.add(file.normalize());
        }
        return FileVisitResult.CONTINUE;
    }

    public List<Path> getFilenameList() {
        return filenameList;
    }
}
