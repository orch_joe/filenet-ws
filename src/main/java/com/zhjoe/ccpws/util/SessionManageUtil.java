/**
 * @Title: SessionManageUtil.java
 * @Package com.ccpws.util
 * @Description: TODO(用一句话描述该文件做什么)
 * @author Changling Jiang
 * @date 2014-2-14 上午11:54:06
 * @version V1.0
 */
package com.zhjoe.ccpws.util;

import java.util.Date;
import java.util.HashMap;

import com.zhjoe.ccpws.vo.FNSessionVo;
import com.zhjoe.ccpws.vo.UserSessionVo;
import com.zhjoe.strongit.filenet.service.exception.CustomWebServiceException;
import org.apache.log4j.Logger;


/**
 * @ClassName: SessionManageUtil
 * @Description: TODO(这里用一句话描述这个类的作用)
 * @author Changling Jiang
 * @date 2014-2-14 上午11:54:06
 *
 */
public class SessionManageUtil {

	protected Logger log = Logger.getLogger(this.getClass());

	private static SessionManageUtil smt;
	private HashMap<String, UserSessionVo> map;
	private static long timeOut = 1000 * 60 * 120;//超时时间

	private SessionManageUtil() {
		map = new HashMap<String, UserSessionVo>();
	}

	//未分级 仅供显示使用
	private HashMap<String, String> choiceListSet;

	public HashMap<String, String> getChoiceListSet() {
		return choiceListSet;
	}

	public void setChoiceListSet(HashMap<String, String> choiceListSet) {
		this.choiceListSet = choiceListSet;
	}

	public HashMap<String, String> getChoiceList() {
		return choiceList;
	}

	public void setChoiceList(HashMap<String, String> choiceList) {
		this.choiceList = choiceList;
	}

	//分级  供初始化使用
	private HashMap<String, String> choiceList;

	public static SessionManageUtil getInstance() {
		if (smt == null)
			smt = new SessionManageUtil();
		return smt;
	}

	public UserSessionVo getSession(String sessionId) {
		CheckUtil.checkNull(sessionId, "EC000001", "no sessionId.");
		log.debug("Session Map=" + map + ",sessionid=" + sessionId);
		UserSessionVo userSessionVo = map.get(sessionId);

		if (userSessionVo != null) {
			FNSessionVo fnSessionVo = userSessionVo.getFnSessionVo();
			if (fnSessionVo != null) {
				long last = fnSessionVo.getOsLastOperateTimeStamp();

				long now = new Date().getTime();
				if (now - last > timeOut) {
					this.removeSession(sessionId);
					throw new CustomWebServiceException("EC000003", "time out");
				} else {
					fnSessionVo.setOsLastOperateTimeStamp(now);
				}
			}
		}

		CheckUtil.checkNull(userSessionVo, "EC000002", "no login.");
		return userSessionVo;
	}

	public void putSession(String sessionId, UserSessionVo userSessionVo) {
		map.put(sessionId, userSessionVo);
	}

	public void removeSession(String sessionId) {
		UserSessionVo userSessionVo = map.get(sessionId);
		if (userSessionVo != null) {
			map.remove(sessionId);
		}
	}

	/**
	 * @return map
	 */
	public HashMap<String, UserSessionVo> getMap() {
		return map;
	}

	/**
	 * @param map map to set
	 */
	public void setMap(HashMap<String, UserSessionVo> map) {
		this.map = map;
	}

	/**
	 * @return timeOut
	 */
	public static long getTimeOut() {
		return timeOut;
	}


}
