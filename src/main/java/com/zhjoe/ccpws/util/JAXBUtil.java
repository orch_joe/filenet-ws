/**
 * @Title: JAXBUtil.java
 * @Package com.ccpws.util
 * @Description: TODO(用一句话描述该文件做什么)
 * @author Changling Jiang
 * @date 2014-2-27 下午12:36:27
 * @version V1.0
 */
package com.zhjoe.ccpws.util;

import java.io.File;
import java.io.StringWriter;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;

/**
 * @ClassName: JAXBUtil
 * @Description: TODO(这里用一句话描述这个类的作用)
 * @author Changling Jiang
 * @date 2014-2-27 下午12:36:27
 *
 */
public class JAXBUtil {

	public static String convertObject2String(Object object) throws JAXBException {
		try {
			StringWriter sw = new StringWriter();
			JAXBContext ctx = JAXBContext.newInstance(object.getClass());
			Marshaller marshaller = ctx.createMarshaller();
			marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
			marshaller.setProperty(Marshaller.JAXB_FRAGMENT, true);
			marshaller.setProperty("com.sun.xml.bind.xmlHeaders", "<?xml version=\"1.0\" encoding=\"UTF-8\"?>");
			marshaller.marshal(object, sw);
			return sw.toString();
		} catch (JAXBException e) {
			e.printStackTrace();
			throw e;
		}
	}

	public static void convertObject2File(Object object, File file) throws JAXBException {

		try {
			JAXBContext ctx = JAXBContext.newInstance(object.getClass());
			Marshaller marshaller = ctx.createMarshaller();
			marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
			marshaller.setProperty(Marshaller.JAXB_FRAGMENT, true);
			marshaller.setProperty("com.sun.xml.bind.xmlHeaders", "<?xml version=\"1.0\" encoding=\"UTF-8\"?>");
			marshaller.marshal(object, file);
		} catch (JAXBException e) {
			e.printStackTrace();
			throw e;
		}
	}

	public static Object convertXML2Object(File file, Class c) throws JAXBException {
		try {
			JAXBContext ctx = JAXBContext.newInstance(c);
			Unmarshaller us = ctx.createUnmarshaller();
			Marshaller marshaller = ctx.createMarshaller();
			return us.unmarshal(file);
		} catch (JAXBException e) {
			e.printStackTrace();
			throw e;
		}
	}

}
