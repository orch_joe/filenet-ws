/**
 * @Title: CheckUtil.java
 * @Package com.ccpws.util
 * @Description: TODO(用一句话描述该文件做什么)
 * @author Changling Jiang
 * @date 2014-2-14 下午03:30:03
 * @version V1.0
 */
package com.zhjoe.ccpws.util;


import com.zhjoe.strongit.filenet.service.exception.CustomWebServiceException;

/**
 * @ClassName: CheckUtil
 * @Description: TODO(这里用一句话描述这个类的作用)
 * @author Changling Jiang
 * @date 2014-2-14 下午03:30:03
 *
 */
public class CheckUtil {

	public static void checkNull(Object object, String exceptionCode, String errorMessage) {
		if (object == null) {
			throw new CustomWebServiceException(exceptionCode, errorMessage);
		}
	}

}
