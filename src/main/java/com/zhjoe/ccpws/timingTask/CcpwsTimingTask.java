/**
 * @Title: CcpwsTimingTask.java
 * @Package com.ccpws.timingTask
 * @Description: TODO(用一句话描述该文件做什么)
 * @author Shushuai Gu
 * @date 2014年6月16日 下午6:10:42
 * @version V1.0
 */
package com.zhjoe.ccpws.timingTask;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import com.zhjoe.ccpws.util.SessionManageUtil;
import com.zhjoe.ccpws.vo.UserSessionVo;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Service;


/**
 * @ClassName: CcpwsTimingTask
 * @Description: TODO(这里用一句话描述这个类的作用)
 * @author Shushuai Gu
 * @date 2014年6月16日 下午6:10:42
 *
 */
@Service
public class CcpwsTimingTask {
	protected Logger log = Logger.getLogger(this.getClass());

	public void cleanSession4Map() {
		log.debug("请注意：定时清理session任务开始-------");
		SessionManageUtil sessionManageUtil = SessionManageUtil.getInstance();
		HashMap<String, UserSessionVo> sessionMap = sessionManageUtil.getMap();
		log.debug("获取sessionMap");
		if (!sessionMap.isEmpty()) {
			for (Map.Entry<String, UserSessionVo> entry : sessionMap.entrySet()) {
				long now = new Date().getTime();
				log.debug("获取session ID 为：" + entry.getKey());
				UserSessionVo sessionVo = (UserSessionVo) entry.getValue();
				if (sessionManageUtil.getTimeOut() < (now - sessionVo.getFnSessionVo().getOsLastOperateTimeStamp())) {
					log.debug("该session：" + entry.getKey() + "已过期！");
					sessionManageUtil.removeSession(entry.getKey());
					log.debug("该session：" + entry.getKey() + "已删除");
				} else {
					log.debug("该session：" + entry.getKey() + "没过期！");
				}
			}
		} else {
			log.debug("Map为空，说明当前时间无人登陆。");
		}
	}

}
