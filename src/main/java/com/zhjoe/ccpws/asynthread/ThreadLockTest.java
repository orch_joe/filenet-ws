package com.zhjoe.ccpws.asynthread;

import java.util.ArrayList;
import java.util.List;

public class ThreadLockTest {
    public static void main(String[] args){

        ThreadLockTest t = new ThreadLockTest();
        t.test3();

    }

    public void test3(){
        List<String> docIdList = new ArrayList<>();
        for(int i=1; i<1000; i++){
            docIdList.add("id"+i);
        }
        Runnable task = new Runnable() {
            TransferBehaviorLock transferLock = new TransferBehaviorLock();
            @Override
            public void run() {
                for(String docId : docIdList){
                    try{
                        boolean isLock = transferLock.lock(docId);
                        if(isLock){
                            System.out.println("threadName:"+Thread.currentThread().getName()+",get the docId:"+docId+" is working!:");
                        }
                    }finally {
                        transferLock.releaseLock(docId);
                    }
                }
                //transferLock.releaseLock();
            }
        };
        for(int i=0; i<5; i++){
            Thread t = new Thread(task);
            t.start();
        }
    }

}
