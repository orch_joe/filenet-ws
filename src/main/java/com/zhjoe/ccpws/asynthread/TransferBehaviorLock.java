package com.zhjoe.ccpws.asynthread;

import javax.sound.midi.Soundbank;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;
import java.util.concurrent.Semaphore;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class TransferBehaviorLock {
    private static ConcurrentMap<String, Long> signMap = new ConcurrentHashMap<>();

    public boolean lock(String docId){
        Long threadId = signMap.putIfAbsent(docId, Thread.currentThread().getId());
        if(threadId == null){
            return true;
        }else{
            return false;
        }
    }



    public void releaseLock(String docId){
        signMap.remove(docId);
    }
}
