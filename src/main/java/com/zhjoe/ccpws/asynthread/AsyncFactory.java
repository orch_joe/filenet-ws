package com.zhjoe.ccpws.asynthread;

import com.zhjoe.ccpws.service.TransferService;
import com.zhjoe.ccpws.util.SpringUtil;

import java.util.TimerTask;

public class AsyncFactory {
    public static TimerTask transferDataToTempStorage() {
        return new TimerTask() {
            @Override
            public void run() {
                SpringUtil.getBean(TransferService.class).downloadDocuments();
            }
        };
    }

    public static TimerTask transferDataToNewFileNet() {
        return new TimerTask() {
            @Override
            public void run() {
                SpringUtil.getBean(TransferService.class).uploadDocuments();
            }
        };
    }
}
