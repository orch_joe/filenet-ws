package com.zhjoe.ccpws.service;

import com.filenet.api.admin.ChoiceList;
import com.filenet.api.admin.PropertyTemplate;
import com.filenet.api.core.Factory;
import com.filenet.api.core.ObjectStore;
import com.filenet.api.util.Id;
import com.zhjoe.strongit.filenet.service.ce.CEObjectStore;
import com.zhjoe.strongit.filenet.service.util.CEObjectStoreUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


public class OldFileNetTest {
	private static Logger logger = LoggerFactory.getLogger(OldFileNetTest.class);

	public static void main(String[] args) {
		CEObjectStore ceObjectStore = CEObjectStoreUtil.getDefaultCEObjectStore();
		ObjectStore os = ceObjectStore.getOs();
		OldFileNetTest testSave = new OldFileNetTest();
		testSave.queryChoiceList(os, "");
	}

	private void queryChoiceList(ObjectStore oldOs, String propId) {
		PropertyTemplate oldPropertyTemplate = Factory.PropertyTemplate.fetchInstance(oldOs, new Id(propId), null);
		int typeId = oldPropertyTemplate.get_DataType().getValue();
		logger.info("执行id:{}------typeId为:{}-------SymbolicName:{}的执行信息", propId, typeId, oldPropertyTemplate.get_SymbolicName());
		ChoiceList oldChoiceList = oldPropertyTemplate.get_ChoiceList();
		System.out.println(oldPropertyTemplate.get_IsHidden());
		System.out.println(oldPropertyTemplate.getAccessAllowed());
		System.out.println(oldPropertyTemplate.get_SymbolicName());
		System.out.println(oldPropertyTemplate.get_IsHidden());
		System.out.println(oldPropertyTemplate.get_DisplayNames());
		System.out.println(oldPropertyTemplate.get_DescriptiveTexts());
		System.out.println(oldPropertyTemplate.get_Cardinality());
		System.out.println(oldPropertyTemplate.get_Settability());
		System.out.println(oldPropertyTemplate.get_IsValueRequired());
		System.out.println(oldPropertyTemplate.get_PersistenceType());
		System.out.println(oldPropertyTemplate.get_IsNameProperty());
		System.out.println(oldPropertyTemplate.get_RequiresUniqueElements());
		System.out.println(oldPropertyTemplate.get_SymbolicName());
		System.out.println(oldPropertyTemplate.get_PropertyDisplayCategory());
		//System.out.println(oldPropertyTemplate.get_AuditAs());

	}


}
