package com.zhjoe.ccpws.service;

import com.filenet.api.admin.*;
import com.filenet.api.collection.ClassDefinitionSet;
import com.filenet.api.collection.LocalizedStringList;
import com.filenet.api.collection.PropertyDefinitionList;
import com.filenet.api.constants.RefreshMode;
import com.filenet.api.core.Factory;
import com.filenet.api.core.ObjectStore;
import com.filenet.api.exception.EngineRuntimeException;
import com.filenet.api.util.Id;
import com.google.common.collect.Lists;
import com.zhjoe.ccpws.vo.TreeNode;
import com.zhjoe.strongit.filenet.service.ce.CEObjectStore;
import com.zhjoe.strongit.filenet.service.datatype.ClassDefinitionInfo;
import com.zhjoe.strongit.filenet.service.util.CEObjectStoreUtil;
import com.zhjoe.strongit.filenet.service.util.FileNetConfig;
import com.zhjoe.strongit.filenet.service.util.NewCEObjectStoreUtil;
import com.zhjoe.strongit.filenet.service.util.TreeUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.*;

public class DocumentClassTransferService1 {
	private static Logger logger = LoggerFactory.getLogger(DocumentClassTransferService1.class);

	public static void main(String[] args) {
		logger.info("=-=-=-开始=-=-==");
		DocumentClassTransferService1 service = new DocumentClassTransferService1();

		service.queryAllClassDefinition("Document");
		service.createNewClassDefinitonInNewFileNet();
		logger.info("=-=-=-结束=-=-==");
	}

	/**
	 * 1.按照根类型查找老的FileNet中所有的文档类
	 * 2.循环每个文档及其子类，获取文档类的符号名称，显示名称
	 * 3.进入新的FileNet根据符号名称和显示名称，转移文档类
	 * 4.获取老的FileNet下所有文档类的属性信息，属性中包含choiceList
	 * 5.给新的FileNet对应的文档类转移属性信息
	 */
	private List<String> denyClassNameList = new ArrayList<>();

	public DocumentClassTransferService1() {
		String denyClassNames = FileNetConfig.getValue("DenyClassNames");
		String[] denyClassNamesArray = denyClassNames.split(",");
		for (String denyClassName : denyClassNamesArray) {
			denyClassNameList.add(denyClassName);
		}

		ClassDefinitionInfo rootClassDefinitionInfo = new ClassDefinitionInfo();
		rootClassDefinitionInfo.setSymbolicName("Document");
		rootClassDefinitionInfo.setId(FileNetConfig.getValue("RootClassId"));
		rootClassDefinitionInfo.setName("文档");
		rootClassDefinitionInfo.setOsName(FileNetConfig.getValue("objectStoreName"));

		this.classDefinitionInfos.add(rootClassDefinitionInfo);
	}

	private List<ClassDefinitionInfo> classDefinitionInfos = new ArrayList<>();

	private Map<String, Set<String>> properties = new HashMap<>();

	/**
	 * 从老的FileNet中，提取所有的文档类型信息
	 * 从跟root document class开始
	 * allowedClassNameList指根类下的一级类名称组
	 *
	 * @return
	 */
	public void queryAllClassDefinition(String parentClassName) {

		ClassDefinition parentClassDefinition = Factory.ClassDefinition.fetchInstance(getOldOs(), parentClassName, null);

		String osName = getOldOs().get_SymbolicName();
		ClassDefinitionSet classDefinitionSet = parentClassDefinition.get_ImmediateSubclassDefinitions();

		Iterator it = classDefinitionSet.iterator();
		while (it.hasNext()) {
			ClassDefinition classDefinition = (ClassDefinition) it.next();
			String symbolicName = classDefinition.get_SymbolicName();
			String displayName = classDefinition.get_DisplayName();
			String classId = classDefinition.get_Id().toString();
			if (denyClassNameList.contains(symbolicName)) {
				continue;
			}

			ClassDefinitionInfo classDefinitionInfo = new ClassDefinitionInfo();
			classDefinitionInfo.setSymbolicName(symbolicName);
			classDefinitionInfo.setId(classId);
			classDefinitionInfo.setName(displayName);
			classDefinitionInfo.setOsName(osName);
			classDefinitionInfo.setParentClassName(parentClassName);

			classDefinitionInfos.add(classDefinitionInfo);

			List<PropertyTemplate> propertyTemplates = queryPropertyTemplate(classDefinition, parentClassDefinition);
			Set<String> propIds = new HashSet<>();
			if (null == propertyTemplates || propertyTemplates.isEmpty()) {
				continue;
			}
			logger.info("文档类名称:" + classDefinition.getClassName() + "---一共属性为:" + propertyTemplates.size());
			for (PropertyTemplate propertyTemplate : propertyTemplates) {
				if (null == propertyTemplate) {
					continue;
				}
				String propId = propertyTemplate.get_Id().toString();
				logger.info(String.format("propId:%s-----propName:%s-----displayName:%s", propId, propertyTemplate.get_Name(), propertyTemplate.get_DisplayName()));
				propIds.add(propId);
			}
			properties.put(symbolicName, propIds);

			boolean isEmpty = classDefinition.get_ImmediateSubclassDefinitions().isEmpty();
			if (!isEmpty) {
				queryAllClassDefinition(symbolicName);
			}
		}
	}

	private void createSubClass(ClassDefinition parentClassDefinition, String subClassName, String subClassDisplayName) {
		ClassDefinition classDefinitionSubclass = parentClassDefinition.createSubclass();
		classDefinitionSubclass.set_SymbolicName(subClassName);

		LocalizedString ls = Factory.LocalizedString.createInstance();
		ls.set_LocalizedText(subClassDisplayName + "2");
		ls.set_LocaleName("zh_cn");
		LocalizedStringList lsl = Factory.LocalizedString.createList();
		lsl.add(ls);
		classDefinitionSubclass.set_DisplayNames(lsl);

		PropertyDefinitionList propertyDefinitionList = classDefinitionSubclass.get_PropertyDefinitions();
		Set<String> propIds = properties.get(subClassName);

		for (String propId : propIds) {
			// 根据属性模板信息。在新的filenet中创建属性模板，
			//构建一个新的propertyTemplate
			PropertyTemplate newPropertyTemplate = buildNewPropertyTemplate(propId);
			PropertyDefinition propertyDefinition = newPropertyTemplate.createClassProperty();
			propertyDefinitionList.add(propertyDefinition);
		}
		classDefinitionSubclass.save(RefreshMode.REFRESH);
	}

	// 根据文档类拿到原来的老的filenet的属性模板id，连接老的filenet的通过老属性模板的id读取属性模板信息。
	private PropertyTemplate buildNewPropertyTemplate(String propId) {
		PropertyTemplate newPropertyTemplate = null;
		try {
			newPropertyTemplate = Factory.PropertyTemplate.fetchInstance(getNewOs(), new Id(propId), null);
		} catch (Exception e) {
			logger.info("在新的FileNet中没有找到id:{}的PropertyTemplate", propId);
		}
		if (null != newPropertyTemplate) {
			return newPropertyTemplate;
		}
		PropertyTemplate oldPropertyTemplate = Factory.PropertyTemplate.fetchInstance(getOldOs(), new Id(propId), null);
		int typeId = oldPropertyTemplate.get_DataType().getValue();
		logger.info("执行id:{}------typeId为:{}-------SymbolicName:{}的执行信息", propId,typeId, oldPropertyTemplate.get_SymbolicName());
		if (typeId == 1) {
			//double
			newPropertyTemplate = Factory.PropertyTemplateFloat64.createInstance(getNewOs());
		} else if (typeId == 2) {
			//guid
			newPropertyTemplate = Factory.PropertyTemplateId.createInstance(getNewOs());

		} else if (typeId == 3) {
			//boolean
			newPropertyTemplate = Factory.PropertyTemplateBoolean.createInstance(getNewOs());

		} else if (typeId == 4) {
			//BINARY
			newPropertyTemplate = Factory.PropertyTemplateBinary.createInstance(getNewOs());

		} else if (typeId == 5) {
			//object
			newPropertyTemplate = Factory.PropertyTemplateObject.createInstance(getNewOs());

		} else if (typeId == 6) {
			//date
			newPropertyTemplate = Factory.PropertyTemplateDateTime.createInstance(getNewOs());

		} else if (typeId == 7) {
			//long
			newPropertyTemplate = Factory.PropertyTemplateInteger32.createInstance(getNewOs());

		} else if (typeId == 8) {
			//String
			newPropertyTemplate = Factory.PropertyTemplateString.createInstance(getNewOs());
		}
		//构建一个新的choiceList
		ChoiceList oldChoiceList = oldPropertyTemplate.get_ChoiceList();
		ChoiceList newChoiceList = buildNewChoiceList(oldChoiceList);
		if (null != newChoiceList) {
			newPropertyTemplate.set_ChoiceList(newChoiceList);
		}
		System.out.println(oldPropertyTemplate.get_IsHidden());
		System.out.println(oldPropertyTemplate.getAccessAllowed());
		System.out.println(oldPropertyTemplate.get_SymbolicName());
		newPropertyTemplate.set_IsHidden(oldPropertyTemplate.get_IsHidden());
		LocalizedString locStr = Factory.LocalizedString.createInstance();
		locStr.set_LocalizedText(oldPropertyTemplate.get_Name());
		locStr.set_LocaleName(getNewOs().get_LocaleName());
		newPropertyTemplate.set_DisplayNames(Factory.LocalizedString.createList());
		newPropertyTemplate.get_DisplayNames().add(locStr);
		//newPropertyTemplate.set_DisplayNames(oldPropertyTemplate.get_DisplayNames());
		//newPropertyTemplate.set_DescriptiveTexts(oldPropertyTemplate.get_DescriptiveTexts());
		newPropertyTemplate.set_Cardinality(oldPropertyTemplate.get_Cardinality());
		newPropertyTemplate.set_Settability(oldPropertyTemplate.get_Settability());
		newPropertyTemplate.set_IsValueRequired(oldPropertyTemplate.get_IsValueRequired());
		newPropertyTemplate.set_PersistenceType(oldPropertyTemplate.get_PersistenceType());
		newPropertyTemplate.set_IsNameProperty(oldPropertyTemplate.get_IsNameProperty());
		//newPropertyTemplate.set_RequiresUniqueElements(oldPropertyTemplate.get_RequiresUniqueElements());
		newPropertyTemplate.set_SymbolicName(oldPropertyTemplate.get_SymbolicName());
		newPropertyTemplate.set_PropertyDisplayCategory(oldPropertyTemplate.get_PropertyDisplayCategory());
		//newPropertyTemplate.set_AuditAs(oldPropertyTemplate.get_AuditAs());
		newPropertyTemplate.save(RefreshMode.REFRESH);
		return newPropertyTemplate;
	}

	// 如果该模板关联了choiceList即选项列表，那么也要在新的filenet中创建一个choiceList，并与新的属性模板关联
	private ChoiceList buildNewChoiceList(ChoiceList oldChoiceList) {
		if (null == oldChoiceList) {
			return null;
		}
		ChoiceList newChoiceList = null;
		try {
			newChoiceList = Factory.ChoiceList.fetchInstance(getNewOs(), oldChoiceList.get_Id(), null);
		} catch (Exception e) {
			logger.info("在新的FileNet中没有找到id:{}的choiceList", oldChoiceList.get_Id().toString());
		}
		if (null != newChoiceList) {
			return newChoiceList;
		}
		newChoiceList = Factory.ChoiceList.createInstance(getNewOs());
		newChoiceList.set_DataType(oldChoiceList.get_DataType());
		newChoiceList.set_DisplayName(oldChoiceList.get_DisplayName() + "2");
		newChoiceList.set_DescriptiveText(oldChoiceList.get_DescriptiveText() + "2");

		com.filenet.api.collection.ChoiceList oldChoiceListChoiceValues = null;
		try {
			oldChoiceListChoiceValues = oldChoiceList.get_ChoiceValues();
		} catch (Exception e) {
			logger.error("没有从oldChildList[" + oldChoiceList.get_DisplayName() + "]中获取到choiceValues原因" + e.getMessage());
		}

		if (null != oldChoiceListChoiceValues && !oldChoiceListChoiceValues.isEmpty()) {
			buildNewChoiceValues(newChoiceList, oldChoiceListChoiceValues);
		}
		newChoiceList.save(RefreshMode.REFRESH);
		return newChoiceList;
	}

	private void buildNewChoiceValues(ChoiceList newChoiceList, com.filenet.api.collection.ChoiceList oldChoiceListChoiceValues) {
		Iterator<Choice> oldChoiceListIterator = oldChoiceListChoiceValues.iterator();
		if (oldChoiceListIterator.hasNext()) {
			newChoiceList.set_ChoiceValues(Factory.Choice.createList());
		} else {
			return;
		}
		while (oldChoiceListIterator.hasNext()) {
			Choice oldChoice = oldChoiceListIterator.next();
			//创建新的choiceList
			Choice newChoice = Factory.Choice.createInstance(getNewOs());
			newChoice.set_DisplayName(oldChoice.get_DisplayName() + "2");
			newChoice.set_DisplayNames(oldChoice.get_DisplayNames());
			newChoice.set_ChoiceType(oldChoice.get_ChoiceType());
			newChoice.set_ChoiceIntegerValue(oldChoice.get_ChoiceIntegerValue());
			newChoice.set_ChoiceStringValue(oldChoice.get_ChoiceStringValue());
			newChoiceList.get_ChoiceValues().add(newChoice);
		}
	}

	private void createNewClassDefinitonInNewFileNet() {
		List<ClassDefinitionInfo> classTree = TreeUtils.generateTrees(this.classDefinitionInfos);
		ClassDefinitionInfo rootNode = TreeUtils.getRoot(classTree);
		List<ClassDefinitionInfo> classDefinitionInfos = Lists.newArrayList();
		if (null == rootNode) {
			logger.info("rootNode为空不在执行");
			return;
		}
		logger.info(rootNode.getOsName() + "----" + rootNode.getSymbolicName());

		List<? extends TreeNode> children = rootNode.getChildren();
		if (null == children || children.isEmpty()) {
			logger.info(rootNode.getOsName() + "----" + rootNode.getSymbolicName() + "----children为空不在执行");
			return;
		}
		logger.info("需要执行的节点信息个数为:" + children.size());
		classDefinitionInfos = (List<ClassDefinitionInfo>) rootNode.getChildren();
		this.recursionCreateClassDefinition(classDefinitionInfos);
	}

	private void recursionCreateClassDefinition(List<ClassDefinitionInfo> childNodes) {
		for (ClassDefinitionInfo classDefinitionInfo : childNodes) {
			String parentClassName = classDefinitionInfo.getParentClassName();
			String symbolicName = classDefinitionInfo.getSymbolicName();
			String displayName = classDefinitionInfo.getName();

			ClassDefinition classDefinition = null;
			ClassDefinition parentClassDefinition = Factory.ClassDefinition.fetchInstance(getNewOs(), parentClassName, null);
			try {
				classDefinition = Factory.ClassDefinition.fetchInstance(getNewOs(), symbolicName, null);
			} catch (EngineRuntimeException e) {
				// do nothing
			}

			if (classDefinition == null && parentClassDefinition != null) {
				createSubClass(parentClassDefinition, symbolicName, displayName);
			}
			if (classDefinitionInfo.getChildren() != null && classDefinitionInfo.getChildren().size() > 0) {
				recursionCreateClassDefinition((List<ClassDefinitionInfo>) classDefinitionInfo.getChildren());
			}
		}
	}

	//public static List<String> excuteClass = Lists.newArrayList("DestinationDocuments");

	private List<PropertyTemplate> queryPropertyTemplate(ClassDefinition classDefinition, ClassDefinition parentClassDefinition) {
		List<PropertyTemplate> propertyTemplates = new ArrayList<>();
		PropertyDefinitionList propertyDefinitionList = classDefinition.get_PropertyDefinitions();

		Iterator it = propertyDefinitionList.iterator();
		while (it.hasNext()) {
			PropertyDefinition propertyDefinition = (PropertyDefinition) it.next();
			propertyDefinition.get_IsSystemOwned();
			propertyDefinition.get_IsNameProperty();

			if (!propertyDefinition.get_IsSystemOwned() && !propertyDefinition.get_IsHidden() && !existParentProperty(propertyDefinition, parentClassDefinition)) {
				PropertyTemplate propertyTemplate = propertyDefinition.get_PropertyTemplate();
				//				if (excuteClass.indexOf(propertyTemplate.getClassName()) >= 0) {
				//					continue;
				//				}
				propertyTemplates.add(propertyTemplate);
			}
		}
		return propertyTemplates;
	}

	//是否存在classdefinition和父类的classdefinition 一致的返回true,不一致返回false
	private boolean existParentProperty(PropertyDefinition propertyDefinition, ClassDefinition parentClassDefinition) {
		PropertyDefinitionList parentPropertyDefinitionList = parentClassDefinition.get_PropertyDefinitions();
		Iterator it = parentPropertyDefinitionList.iterator();
		while (it.hasNext()) {
			PropertyDefinition parentPropertyDefinition = (PropertyDefinition) it.next();
			logger.info("thisName:" + propertyDefinition.get_SymbolicName() + "------parentName:" + parentPropertyDefinition.get_SymbolicName());
			if (parentPropertyDefinition.get_SymbolicName().equals(propertyDefinition.get_SymbolicName())) {
				return true;
			}
		}

		return false;
	}


	private ObjectStore getNewOs() {
		CEObjectStore ceObjectStore = NewCEObjectStoreUtil.getDefaultCEObjectStore();
		ObjectStore os = ceObjectStore.getOs();
		return os;
	}

	private ObjectStore getOldOs() {
		CEObjectStore ceObjectStore = CEObjectStoreUtil.getDefaultCEObjectStore();
		ObjectStore os = ceObjectStore.getOs();
		return os;
	}
}

