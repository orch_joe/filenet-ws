package com.zhjoe.ccpws.service;

import com.filenet.api.collection.*;

import com.filenet.api.constants.FilteredPropertyType;
import com.filenet.api.core.ContentTransfer;
import com.filenet.api.core.Document;
import com.filenet.api.core.Factory;
import com.filenet.api.core.Folder;
import com.filenet.api.exception.EngineRuntimeException;
import com.filenet.api.property.FilterElement;
import com.filenet.api.property.PropertyFilter;
import com.filenet.api.query.SearchSQL;
import com.filenet.api.query.SearchScope;
import com.filenet.api.util.Id;
import com.google.common.collect.Lists;
import com.zhjoe.ccpws.asynthread.TransferBehaviorLock;
import com.zhjoe.ccpws.util.FindFileVisitor;
import com.zhjoe.strongit.filenet.service.ce.CEHelper;
import com.zhjoe.strongit.filenet.service.ce.CEObjectStore;
import com.zhjoe.strongit.filenet.service.datatype.*;
import com.zhjoe.strongit.filenet.service.util.*;

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.time.DateFormatUtils;


import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.core.support.AbstractLobCreatingPreparedStatementCallback;
import org.springframework.jdbc.core.support.AbstractLobStreamingResultSetExtractor;
import org.springframework.jdbc.support.lob.DefaultLobHandler;
import org.springframework.jdbc.support.lob.LobCreator;
import org.springframework.jdbc.support.lob.LobHandler;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.*;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.io.*;
import java.math.BigDecimal;
import java.nio.file.*;
import java.nio.file.attribute.BasicFileAttributes;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.stream.Collectors;

@Service
public class TransferService {
    private static final Logger log = Logger.getLogger(TransferService.class);
    @Autowired
    private JdbcTemplate jdbcTemplate;

    private static final String XML_PATH = "E:/transfer/xml/";
    private static final String DOC_PATH = "C:/transfer/doc/";
    private static final String DEFAULT_CONTENT_INTEGRATE_PATH = "/transfer";
    private TransferBehaviorLock downloadLock = new TransferBehaviorLock();
    private TransferBehaviorLock uploadLock = new TransferBehaviorLock();

    /**
     * 从老的文档库中，将根类型的文档下载到临时目录中，并把属性封装到xml中
     */
    @Transactional
    public void downloadDocuments() {
        try {
            //int index = 0;
            CEObjectStore ceObjectStore = CEObjectStoreUtil.getDefaultCEObjectStore();
            SearchSQL sql = new SearchSQL();
            sql.setSelectList("*");
            sql.setFromClauseInitialValue("Document", null, true);
            //sql.setWhereClause(" IsCurrentVersion = TRUE ");
            int pageSize = 50;
            PropertyFilter filter = new PropertyFilter();
            filter.setMaxRecursion(0);
            filter.addIncludeType(new FilterElement(null, null, null, FilteredPropertyType.ANY, null));

            SearchScope search = new SearchScope(ceObjectStore.getOs());
            IndependentObjectSet objects = search.fetchObjects(sql, pageSize, filter, true);
            PageIterator pageIterator = objects.pageIterator();

            while (pageIterator.nextPage() == true) {
                Object[] currentPage = pageIterator.getCurrentPage();
                for (Object o : currentPage) {
                    Document document = (Document) o;
                    // 你在这里加统计
                    try {
                        if (downloadLock.lock(document.get_Id().toString())) {
                            if (!isDownloaded(((Document) o).get_Id().toString())) {
                                this.saveDocToTemp(document);
                                this.updateTransferStatus(document);
                            }
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                        log.error(e);
                        log.error("出错文档id：" + document.get_Id().toString());
                    } finally {
                        downloadLock.releaseLock(document.get_Id().toString());
                    }
                }
                /*index++;
                if (index >= 100000) {
                    log.info("10万条文档已下载完毕，如需继续，请重新启动应用");
                    break;
                }*/
            }
        } catch (Exception e) {
            e.printStackTrace();
            log.error(e);
        }
    }

    private boolean isDownloaded(String docId) {
        String sql = "select count(1) from t_transfer where doc_id = ? and status = 1";
        String sql2 = "select count(1) from t_transfer2 where doc_id = ? and status = 1";
        int cnt = jdbcTemplate.queryForObject(sql, new Object[]{docId}, Integer.class);
        int cnt2 = jdbcTemplate.queryForObject(sql2, new Object[]{docId}, Integer.class);
        if (cnt > 0 || cnt2 > 0) {
            return true;
        } else {
            return false;
        }
    }

    private void saveDocToTemp(Document doc) throws TransformerException, ParserConfigurationException, IOException {
        String id = doc.get_Id().toString();
        // 获取文档所有属性
        List<PropertyInfo> properties = this.getDocumentProperties(doc);
        String documentTitle = doc.getProperties().getStringValue("DocumentTitle");
        String mimeType = doc.get_MimeType();

        this.generateXml(id, properties, doc.get_ClassDescription().get_SymbolicName(), documentTitle, mimeType);
        this.saveFile(doc);
    }

    public void reloadNotExistFileFormDb() throws Exception {
/*        String sql1 = "select doc_id from t_transfer";
        List<String> docIdList = this.jdbcTemplate.queryForList(sql1, String.class);
        String sql2 = "select doc_id from t_transfer2";
        List<String> docIdList2 = this.jdbcTemplate.queryForList(sql2, String.class);

        docIdList.addAll(docIdList2);
        Path startingDir = Paths.get(DOC_PATH);

        for(String docId : docIdList){
            Document document = CEHelper.getDocumentInfo(CEObjectStoreUtil.getDefaultCEObjectStore().getOs(), docId);
            int fileNums = document.get_ContentElements().size();
            String pattern = docId.replace("{", "\\{").replace("}", "\\}");
            pattern = "^(" + pattern + ").*$";
            FindFileVisitor filterFilesVisitor= new FindFileVisitor(pattern);
            Files.walkFileTree(startingDir, filterFilesVisitor);
            List<Path> searchResults = filterFilesVisitor.getFilenameList();

            if(searchResults == null || searchResults.size() < fileNums){
                // reload
                clearDataFromDBAndFileSystem(docId, searchResults);
                this.saveDocToTemp(document);
                this.updateTransferStatus(document);
            }

           // List<File> fileList = this.getFileFromTemp(docId, fileNums, retrievalNames);
        }*/
        List<Object> toDoData = Lists.newArrayList();
        int totalNums = this.countTransferData();
        int start = 0;
        int end = 100;
        while (totalNums >= 0) {
            List<Map<String, Object>> docList = queryTransferData(start, end);
            for (Map<String, Object> row : docList) {
                String docId = (String) row.get("doc_id");

                try {
                    Integer fileNums = ((BigDecimal) row.get("file_nums")).intValue();
                    String retrievalNames = (String) row.get("retrieval_Names");
                    List<File> fileList = this.getFileFromTemp(docId, fileNums, retrievalNames);
                    for(File file : fileList){
                        boolean isExists = file.exists();
                        if(!isExists){
                            this.markThisData(toDoData, docId);
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                    log.error(e);
                }
            }
            start += 100;
            end += 100;
            totalNums -= 100;
        }

        for(Object docId : toDoData){
            clearDataFromDBAndFileSystem((String)docId);
            Document document = CEHelper.getDocumentInfo(CEObjectStoreUtil.getDefaultCEObjectStore().getOs(), docId.toString());
            this.saveDocToTemp(document);
            this.updateTransferStatus(document);
        }
    }

    private void markThisData(List<Object> toDoData, Object mark){
        toDoData.add(mark);
    }

    private void clearDataFromDBAndFileSystem(String docId) throws Exception {
        String deleteSql1 = "delete from t_transfer where doc_id = ?";
        this.jdbcTemplate.update(deleteSql1, docId);

        String deleteSql2 = "delete from t_transfer2 where doc_id = ?";
        this.jdbcTemplate.update(deleteSql2, docId);

        Path startingDir = Paths.get(DOC_PATH);
        String pattern = docId.replace("{", "\\{").replace("}", "\\}");
        pattern = "^(" + pattern + ").*$";
        FindFileVisitor filterFilesVisitor= new FindFileVisitor(pattern);
        Files.walkFileTree(startingDir, filterFilesVisitor);
        List<Path> searchResults = filterFilesVisitor.getFilenameList();

        if(searchResults != null){
            for(Path file : searchResults){
                Files.deleteIfExists(file);
            }
        }
    }

    private void saveFile(Document document) throws IOException {
        String documentId = document.get_Id().toString();
        String verseriesId = document.get_VersionSeries().get_Id().toString();
        int majorVersionNo = document.get_MajorVersionNumber();
        int minorVersionNo = document.get_MinorVersionNumber();
        ContentElementList contentElements = document.get_ContentElements();
        int index = 0;
        List<String> retrievalNames = Collections.synchronizedList(new ArrayList<>());
        List<String> contentTypes = Collections.synchronizedList(new ArrayList<>());
        if (contentElements != null && !contentElements.isEmpty()) {
            Iterator<?> contentElementIterator = contentElements.iterator();
            while (contentElementIterator.hasNext()) {
                ContentTransfer contentTransfer = (ContentTransfer) contentElementIterator.next();
                String orgFileName = contentTransfer.get_RetrievalName();
                String suffix = orgFileName.substring(orgFileName.lastIndexOf(".") + 1);
                String filePath = DOC_PATH + documentId + "_" + index + "." + suffix;
                File file = new File(filePath);
                if (!file.exists()) {
                    InputStream accessContentStream = contentTransfer.accessContentStream();
                    String contentType = contentTransfer.get_ContentType();
                    String retrievalName = contentTransfer.get_RetrievalName();
                    retrievalName = retrievalName.replace(',','_');
                    retrievalNames.add(retrievalName);
                    contentTypes.add(contentType);

                    BufferedOutputStream bufferedOutputStream = new BufferedOutputStream(new FileOutputStream(new File(filePath)));
                    IOUtils.copy(accessContentStream, bufferedOutputStream);
                    bufferedOutputStream.close();
                    accessContentStream.close();
                }
                index++;
            }
        }
        this.saveDocIdToDb(documentId, index, retrievalNames, contentTypes,verseriesId, majorVersionNo, minorVersionNo);
    }

    private void generateXml(String docId, List<PropertyInfo> properties, String symbolicName, String documentTitle, String mimeType) throws ParserConfigurationException, TransformerException {
        File xmlFile = new File(XML_PATH + docId + ".xml");
        if (!xmlFile.exists()) {
            DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
            DocumentBuilder db = factory.newDocumentBuilder();
            org.w3c.dom.Document document = db.newDocument();
            document.setXmlStandalone(true);
            Element rootEl = document.createElement("document");

            Element idEl = document.createElement("id");
            idEl.setTextContent(docId);

            Element symbolicNameEl = document.createElement("symbolicName");
            symbolicNameEl.setTextContent(symbolicName);

            Element documentTitleEl = document.createElement("documentTitle");
            documentTitleEl.setTextContent(documentTitle);

            Element mimeTypeEl = document.createElement("mimeType");
            mimeTypeEl.setTextContent(mimeType);

            Element propsEl = document.createElement("props");
            for (PropertyInfo propertyInfo : properties) {
                Integer dataType = propertyInfo.getDataType();
                Integer cardinality = propertyInfo.getCardinality();

                Element propertyEl = document.createElement("property");
                propertyEl.setAttribute("key", propertyInfo.getSymbolicName());
                propertyEl.setAttribute("dataType", String.valueOf(dataType));
                propertyEl.setAttribute("cardinality", String.valueOf(cardinality));
                switch (dataType) {
                    case 2:
                        if (cardinality == 0) {
                            Boolean propertyBoolean = propertyInfo.getPropertyBoolean();
                            if (propertyBoolean != null) {
                                propertyEl.setAttribute("value", String.valueOf(propertyBoolean));
                            }
                        } else {
                            List<Boolean> propertyBooleans = propertyInfo.getPropertyBooleans();
                            if (propertyBooleans != null) {
                                propertyEl.setAttribute("value", StringUtils.join(propertyBooleans, ","));
                            }
                        }
                        break;
                    case 3:
                        if (cardinality == 0) {
                            Date propertyDateTime = propertyInfo.getPropertyDateTime();
                            if (propertyDateTime != null) {
                                propertyEl.setAttribute("value", DateFormatUtils.format(propertyDateTime, DateFormatUtils.ISO_8601_EXTENDED_DATETIME_FORMAT.getPattern()));
                            }
                        } else {
                            Date[] propertyDateTimes = propertyInfo.getPropertyDateTimes();
                            List<String> dateTimeList = new ArrayList<>();
                            for (Date propertyDateTime : propertyDateTimes) {
                                String dateTimeStr = DateFormatUtils.format(propertyDateTime, DateFormatUtils.ISO_8601_EXTENDED_DATETIME_FORMAT.getPattern());
                                dateTimeList.add(dateTimeStr);
                            }
                            if (propertyDateTimes != null) {
                                propertyEl.setAttribute("value", StringUtils.join(dateTimeList, ","));
                            }
                        }
                        break;
                    case 4:
                        if (cardinality == 0) {
                            Double propertyFloat64 = propertyInfo.getPropertyFloat64();
                            if (propertyFloat64 != null) {
                                propertyEl.setAttribute("value", String.valueOf(propertyFloat64));
                            }
                        } else {
                            List<Double> propertyFloat64s = propertyInfo.getPropertyFloat64s();
                            propertyEl.setAttribute("value", StringUtils.join(propertyFloat64s, ","));
                        }
                        break;
                    case 6:
                        if (cardinality == 0) {
                            Integer propertyInteger32 = propertyInfo.getPropertyInteger32();
                            if (propertyInteger32 != null) {
                                propertyEl.setAttribute("value", String.valueOf(propertyInteger32));
                            }
                        } else {
                            List<Integer> propertyInteger32s = propertyInfo.getPropertyInteger32s();
                            propertyEl.setAttribute("value", StringUtils.join(propertyInteger32s, ","));
                        }
                        break;
                    case 8:
                        if (cardinality == 0) {
                            String propertyString = propertyInfo.getPropertyString();
                            if (propertyString != null) {
                                propertyEl.setAttribute("value", propertyString);
                            }
                        } else {
                            List<String> propertyStrings = propertyInfo.getPropertyStrings();
                            propertyEl.setAttribute("value", StringUtils.join(propertyStrings, ","));
                        }
                        break;
                }
                propsEl.appendChild(propertyEl);
            }
            rootEl.appendChild(idEl);
            rootEl.appendChild(symbolicNameEl);
            rootEl.appendChild(propsEl);
            rootEl.appendChild(mimeTypeEl);
            rootEl.appendChild(documentTitleEl);

            document.appendChild(rootEl);
            TransformerFactory tff = TransformerFactory.newInstance();
            Transformer tf = tff.newTransformer();
            tf.setOutputProperty(OutputKeys.INDENT, "yes");
            tf.transform(new DOMSource(document), new StreamResult(xmlFile));
        }
    }

    private List<PropertyInfo> getDocumentProperties(Document doc) {
        return ClassPropertiesUtil.getDocumentPropertyDescriptions(doc);
    }

    private void saveDocIdToDb(String docId, int fileNums, List<String> retrievalNameList, List<String> contentTypeList,String verseriesId, int majorVersionNo, int minorVersionNo) {
        String retrievalNames = StringUtils.join(retrievalNameList, ",");
        String contentTypes = StringUtils.join(contentTypeList, ",");

        if (fileNums > 10) {
            String sql = "insert into t_transfer2(doc_id,status,file_nums,retrieval_Names,content_Types,version_id, majaorVersion_no, minorversion_no) values(?,?,?,?,?,?,?,?)";
            LobHandler lobHandler = new DefaultLobHandler();
            jdbcTemplate.execute(sql,
                    new AbstractLobCreatingPreparedStatementCallback(lobHandler) {
                        protected void setValues(PreparedStatement ps, LobCreator lobCreator) throws SQLException {
                            ps.setString(1, docId);
                            ps.setInt(2, 0);
                            ps.setInt(3, fileNums);
                            lobCreator.setClobAsString(ps, 4, retrievalNames);
                            ps.setString(5, contentTypes);
                            ps.setString(6, verseriesId);
                            ps.setInt(7, majorVersionNo);
                            ps.setInt(8, minorVersionNo);
                        }
                    }
            );
        } else {
            String sql = "insert into t_transfer(doc_id,status,file_nums,retrieval_Names,content_Types,version_id, majaorVersion_no, minorversion_no) values(?,?,?,?,?,?,?,?)";
            jdbcTemplate.update(new PreparedStatementCreator() {
                @Override
                public PreparedStatement createPreparedStatement(Connection connection) throws SQLException {
                    PreparedStatement ps = connection.prepareStatement(sql);
                    ps.setString(1, docId);
                    ps.setInt(2, 0);
                    ps.setInt(3, fileNums);
                    ps.setString(4, retrievalNames);
                    ps.setString(5, contentTypes);
                    ps.setString(6, verseriesId);
                    ps.setInt(7, majorVersionNo);
                    ps.setInt(8, minorVersionNo);
                    return ps;
                }
            });
        }
    }

    private void updateTransferStatus(Document doc) throws Exception {
        String sql = "update t_transfer set status = 1 where doc_id = ? ";
        jdbcTemplate.update(sql, doc.get_Id().toString());

        String sql2 = "update t_transfer2 set status = 1 where doc_id = ?";
        jdbcTemplate.update(sql2, doc.get_Id().toString());
    }

    public List<Map<String, Object>> queryTransferData(int start, int end) {
        String sql1 = "select * from (select doc_id,file_nums,retrieval_Names,content_types,status,version_id, majaorVersion_no, minorversion_no,rownum r from t_transfer where status = 1 and rownum <= ?) t where r > ?";
        List<Map<String, Object>> resultSet1 = jdbcTemplate.queryForList(sql1, end, start);
        String sql2 = "select * from (select doc_id,status,file_nums,retrieval_Names,content_types,version_id, majaorVersion_no, minorversion_no,rownum r from t_transfer2 where status = 1 and rownum <= ?) t where r > ?";
        final LobHandler lobHandler = new DefaultLobHandler();  // reusable object
        final List<Map<String, Object>> resultset2 = new ArrayList<>();
        jdbcTemplate.query(
                sql2, new Object[] {end,start},
                new AbstractLobStreamingResultSetExtractor() {
                    public void streamData(ResultSet rs) throws SQLException, IOException {
                        do{
                            Map<String, Object> row = new HashMap<>();
                            row.put("doc_id", rs.getString(1));
                            row.put("status", rs.getString(2));
                            row.put("file_nums", rs.getBigDecimal(3));
                            row.put("retrieval_Names", lobHandler.getClobAsString(rs,4));
                            row.put("content_types", rs.getString(5));
                            row.put("version_id", rs.getString(6));
                            row.put("majaorVersion_no", rs.getBigDecimal(7));
                            row.put("minorversion_no", rs.getBigDecimal(8));
                            resultset2.add(row);
                        }while(rs.next());
                    }
                }
        );

        resultSet1.addAll(resultset2);
        return resultSet1;
    }

    public int countTransferData() {
        String sql = "select count(1) from t_transfer where status = 1 ";
        String sql2 = "select count(1) from t_tansfer2 where status = 1";
        int total = jdbcTemplate.queryForObject(sql, Integer.class) + jdbcTemplate.queryForObject(sql2, Integer.class);
        return total;
    }

    private Map<String, Object> getPropertiesFromXml(String docId) throws ParserConfigurationException, IOException, SAXException, ParseException {
        Map<String, Object> properties;
        List<PropertyInfo> propertyInfoList = Collections.synchronizedList(new ArrayList<>());
        File propertyFile = new File(XML_PATH + docId + ".xml");

        DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
        DocumentBuilder db = dbf.newDocumentBuilder();
        org.w3c.dom.Document doc = db.parse(propertyFile);

        Node propsNode = doc.getElementsByTagName("props").item(0);
        String id = doc.getElementsByTagName("id").item(0).getTextContent();

        NodeList propertyNodeList = propsNode.getChildNodes();
        for (int i = 0; i < propertyNodeList.getLength(); i++) {

            Node propertyNode = propertyNodeList.item(i);
            String nodeName = propertyNode.getNodeName();
            if (nodeName.equals("property")) {
                PropertyInfo propertyInfo = new PropertyInfo();

                Element propertyEl = (Element) propertyNode;
                String propertyName = propertyEl.getAttribute("key");
                int dataType = Integer.valueOf(propertyEl.getAttribute("dataType")).intValue();
                int cardinality = Integer.valueOf(propertyEl.getAttribute("cardinality")).intValue();

                propertyInfo.setSymbolicName(propertyName);
                propertyInfo.setDataType(dataType);
                propertyInfo.setCardinality(cardinality);
                switch (dataType) {
                    case 2:
                        String propertyBooleanStr = propertyEl.getAttribute("value");
                        if (StringUtils.isNotEmpty(propertyBooleanStr)) {
                            if (cardinality == 0) {
                                Boolean propertyBoolean = Boolean.valueOf(propertyBooleanStr);
                                propertyInfo.setPropertyBoolean(propertyBoolean);
                            } else {
                                List<Boolean> propertyBooleans = new ArrayList(Arrays.asList(StringUtils.split(propertyBooleanStr, ",")));
                                propertyInfo.setPropertyBooleans(propertyBooleans);
                            }
                        }
                        break;
                    case 3:
                        String propertyDateTimeStr = propertyEl.getAttribute("value");
                        SimpleDateFormat sdf = new SimpleDateFormat(DateFormatUtils.ISO_8601_EXTENDED_DATETIME_FORMAT.getPattern());
                        if (StringUtils.isNotEmpty(propertyDateTimeStr)) {
                            if (cardinality == 0) {
                                Date propertyDateTime = sdf.parse(propertyDateTimeStr);
                                propertyInfo.setPropertyDateTime(propertyDateTime);
                            } else {
                                List<Date> propertyDateTimes = new ArrayList<>();
                                for (String item : StringUtils.split(propertyDateTimeStr, ",")) {
                                    propertyDateTimes.add(sdf.parse(item));
                                }
                                propertyInfo.setPropertyDateTimes(propertyDateTimes.toArray(new Date[]{}));
                            }
                        }
                        break;
                    case 4:
                        String propertyFloat64Str = propertyEl.getAttribute("value");
                        if (StringUtils.isNotEmpty(propertyFloat64Str)) {
                            if (cardinality == 0) {
                                Double propertyFloat64 = Double.valueOf(propertyFloat64Str);
                                propertyInfo.setPropertyFloat64(propertyFloat64);
                            } else {
                                List<Double> propertyFloat64s = new ArrayList(Arrays.asList(StringUtils.split(propertyFloat64Str, ",")));
                                propertyInfo.setPropertyFloat64s(propertyFloat64s);
                            }
                        }
                        break;
                    case 6:
                        String propertyInteger32Str = propertyEl.getAttribute("value");
                        if (StringUtils.isNotEmpty(propertyInteger32Str)) {
                            if (cardinality == 0) {
                                Integer propertyInteger32 = Integer.valueOf(propertyInteger32Str);
                                propertyInfo.setPropertyInteger32(propertyInteger32);
                            } else {
                                List<Integer> propertyInteger32s = new ArrayList(Arrays.asList(StringUtils.split(propertyInteger32Str, ",")));
                                propertyInfo.setPropertyInteger32s(propertyInteger32s);
                            }
                        }
                        break;
                    case 8:
                        String propertyString = propertyEl.getAttribute("value");
                        if (StringUtils.isNotEmpty(propertyString)) {
                            if (cardinality == 0) {
                                propertyInfo.setPropertyString(propertyString);
                            } else {
                                List<String> propertyStrings = new ArrayList(Arrays.asList(StringUtils.split(propertyString, ",")));
                                propertyInfo.setPropertyStrings(propertyStrings);
                            }
                        }
                        break;
                }

                propertyInfoList.add(propertyInfo);
            }
        }
        properties = ClassPropertiesUtil.getPropertyDescriptions(propertyInfoList);
        properties.put("Id", new Id(id));
        return properties;
    }

    private Map<String, Object> getBasicPropertiesFromXml(String docId) throws ParserConfigurationException, IOException, SAXException {
        Map<String, Object> properties = new HashMap<>();
        File propertyFile = new File(XML_PATH + docId + ".xml");

        DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
        DocumentBuilder db = dbf.newDocumentBuilder();
        org.w3c.dom.Document doc = db.parse(propertyFile);

        String docClassName = doc.getElementsByTagName("symbolicName").item(0).getTextContent();
        String mimeType = doc.getElementsByTagName("mimeType").item(0).getTextContent();
        String documentTitle = doc.getElementsByTagName("documentTitle").item(0).getTextContent();
        properties.put("docClassName", docClassName);
        properties.put("documentTitle", documentTitle);
        properties.put("mimeType", mimeType);
        return properties;
    }

    private List<File> getFileFromTemp(String docId, int fileNums, String retrievalNames) {
        List<File> fileList = Collections.synchronizedList(new ArrayList<>());
        for (int i = 0; i < fileNums; i++) {
            String retrievalName = StringUtils.split(retrievalNames, ",")[i];
            String suffix = retrievalName.substring(retrievalName.lastIndexOf(".") + 1);
            String filePath = DOC_PATH + docId + "_" + i + "." + suffix;
            fileList.add(new File(filePath));
        }
        return fileList;
    }

    private void uploadToNewFileNet(Map<String, Object> properties, List<File> files, Map<String, Object> basicProperties, String retrievalNames, String contentTypes) throws Exception {
        CEObjectStore ceObjectStore = CEObjectStoreUtil.getDefaultCEObjectStore();
        Folder contentIntegrateFolder = CEHelper.retrieveFolderNotExistThenCreate(ceObjectStore.getOs(), DEFAULT_CONTENT_INTEGRATE_PATH);

        List<ContentTransferInfo> contents = Collections.synchronizedList(new ArrayList<>());
        for (int i = 0; i < files.size(); i++) {
            File file = files.get(i);
            FileInputStream in = new FileInputStream(file);
            ContentTransferInfo contentTransferInfo = new ContentTransferInfo();
            String retrievalName = StringUtils.split(retrievalNames, ",")[i];
            String contentType = StringUtils.split(contentTypes, ",")[i];
            contentTransferInfo.setRetrievalName(retrievalName);
            contentTransferInfo.setInputStream(in);
            contentTransferInfo.setContentType(contentType);
            contents.add(contentTransferInfo);
        }
        properties.put("DocumentTitle", this.escapeSepcialChars(basicProperties.get("documentTitle").toString()));
        properties.put("MimeType", basicProperties.get("mimeType").toString());

        CEHelper.createDocumentInfo(ceObjectStore.getOs(), basicProperties.get("docClassName").toString(), 0, contentIntegrateFolder, properties, contents);
    }

    private String escapeSepcialChars(String documentTitle){
        char[] illegalCharacters = new char[]{'\\','/', ':', '*', '?', '"', '<' ,'>', '|'};
        char[] documentTitleChars = documentTitle.toCharArray();
        for(char titleChar : documentTitleChars){
            for(char illegalChar : illegalCharacters){
                if(titleChar == illegalChar){
                    documentTitle.replaceAll(illegalChar+"", "\\" + illegalChar);
                }
            }
        }
        return documentTitle;
    }

    public void uploadDocuments() {
        try {
            int totalNums = this.countTransferData();
            int start = 0;
            int end = 100;
            while (totalNums >= 0) {
                List<Map<String, Object>> docList = queryTransferData(start, end);
                for (Map<String, Object> row : docList) {
                    String docId = (String) row.get("doc_id");
                    boolean isLock = uploadLock.lock(docId);
                    try {
                        if (isLock) {
                            if (!isUpload(docId)) {
                                Integer fileNums = ((BigDecimal) row.get("file_nums")).intValue();
                                String retrievalNames = (String) row.get("retrieval_Names");
                                String contentTypes = (String) row.get("content_types");
                                String versionId = (String)row.get("version_id");
                                Map<String, Object> properties = this.getPropertiesFromXml(docId);
                                Map<String, Object> basicProperties = this.getBasicPropertiesFromXml(docId);
                                List<File> fileList = this.getFileFromTemp(docId, fileNums, retrievalNames);

                                boolean isMultipleVersionDocument = this.checkIsMultipleVersionDocument(versionId);
                                if(isMultipleVersionDocument){
                                    versionIdSet.add(versionId);
                                    continue;
                                }

                                this.uploadToNewFileNet(properties, fileList, basicProperties, retrievalNames, contentTypes);
                                this.updateTransferStatus2(docId);
                            }
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                        log.error(e);
                    } finally {
                        uploadLock.releaseLock(docId);
                    }
                }
                start += 100;
                end += 100;
                totalNums -= 100;
            }
            this.uploadMutipleVersionDocument();
        } catch (Exception e) {
            e.printStackTrace();
            log.error(e);
        }
    }

    private Set<String> versionIdSet = Collections.synchronizedSet(new HashSet<>());
    private void uploadMutipleVersionDocument() throws Exception {
        Iterator itt = versionIdSet.iterator();
        while(itt.hasNext()){
            String versionId = (String) itt.next();
            String sql1 = "select doc_id,majaorVersion_no,minorversion_no,version_id,retrieval_Names, content_types,file_nums from t_transfer where status = 1 and versionId = ?";
            String sql2 = "select doc_id,majaorVersion_no,minorversion_no,version_id,retrieval_Names, content_types,file_nums from t_transfer2 where status = 1 and versionId = ?";
            List<Map<String, Object>> resultSet1 = this.jdbcTemplate.queryForList(sql1, new Object[]{versionId});
            LobHandler lobHandler = new DefaultLobHandler();
            final List<Map<String, Object>> resultSet2 = new ArrayList<>();
            this.jdbcTemplate.query(sql2, new Object[]{versionId},new AbstractLobStreamingResultSetExtractor() {
                protected void streamData(ResultSet rs) throws SQLException, DataAccessException {
                    do{
                        Map<String, Object> row = new HashMap<>();
                        row.put("doc_id", rs.getString(1));
                        row.put("majaorVersion_no", rs.getBigDecimal(2));
                        row.put("minorversion_no", rs.getBigDecimal(3));
                        row.put("version_id", rs.getString(4));
                        row.put("retrieval_Names", lobHandler.getClobAsString(rs,5));
                        row.put("content_Types", rs.getString(6));
                        row.put("file_nums", rs.getBigDecimal(7));
                        resultSet2.add(row);
                    }while(rs.next());
                }
            });
            resultSet1.addAll(resultSet2);
            resultSet1.sort(new Comparator<Map<String, Object>>() {
                @Override
                public int compare(Map<String, Object> o1, Map<String, Object> o2) {
                    if(((BigDecimal)o1.get("majaorVersion_no")).intValue() > ((BigDecimal)o2.get("majaorVersion_no")).intValue()){
                        return 1;
                    }else if(((BigDecimal)o1.get("majaorVersion_no")).intValue() == ((BigDecimal)o2.get("majaorVersion_no")).intValue()){
                        return 0;
                    }else{
                        return -1;
                    }
                }
            });
            int index = 0;
            for(Map<String, Object> row : resultSet1){
                index++;
                String docId = (String)row.get("doc_id");
                int majaorVersionNo =((BigDecimal)row.get("majaorVersion_no")).intValue();
                String retrievalNames = (String)row.get("retrieval_Names");
                String contentTypes = (String)row.get("content_types");
                int fileNums = ((BigDecimal)row.get("file_nums")).intValue();
                Map<String, Object> properties = this.getPropertiesFromXml(docId);
                Map<String, Object> basicProperties = this.getBasicPropertiesFromXml(docId);
                List<File> fileList = this.getFileFromTemp(docId, fileNums, retrievalNames);
                if(index == 1){
                    this.uploadToNewFileNet(properties, fileList, basicProperties, retrievalNames, contentTypes);
                    continue;
                }
                int previousVersionNo = majaorVersionNo - 1;
                // 找到之前的版本文档，进行检入操作
                String versionSql1 = "select doc_id from t_transfer where  version_id = ? and status = 1 and majaorVersion_no = ?";
                String versionSql2 = "select doc_id from t_transfer2 where  version_id = ? and status = 1 and majaorVersion_no = ?";
                String previousDocId1 = this.jdbcTemplate.queryForObject(versionSql1, new Object[]{versionId, previousVersionNo},String.class);
                String previousDocId2 = this.jdbcTemplate.queryForObject(versionSql2, new Object[]{versionId, previousVersionNo},String.class);

                String previousDocId = StringUtils.isEmpty(previousDocId1) ? previousDocId2 : previousDocId1;
                Document previousDocument = CEHelper.getDocumentInfo(CEObjectStoreUtil.getDefaultCEObjectStore().getOs(), previousDocId);


                List<ContentTransferInfo> contents = Collections.synchronizedList(new ArrayList<>());
                for (int i = 0; i < fileList.size(); i++) {
                    File file = fileList.get(i);
                    FileInputStream in = new FileInputStream(file);
                    ContentTransferInfo contentTransferInfo = new ContentTransferInfo();
                    String retrievalName = StringUtils.split(retrievalNames, ",")[i];
                    String contentType = StringUtils.split(contentTypes, ",")[i];
                    contentTransferInfo.setRetrievalName(retrievalName);
                    contentTransferInfo.setInputStream(in);
                    contentTransferInfo.setContentType(contentType);
                    contents.add(contentTransferInfo);
                }
                properties.put("DocumentTitle", basicProperties.get("documentTitle").toString());
                properties.put("MimeType", basicProperties.get("mimeType").toString());
                properties.remove("id");
                CEHelper.checkinWithId(previousDocument, properties, contents,new Id(docId));
                this.updateTransferStatus2(docId);
            }
        }
    }

    private boolean checkIsMultipleVersionDocument(String versionId){
        String sql1 = "select count(1) from t_transfer where version_id = ? ";
        int countNo1 = this.jdbcTemplate.queryForObject(sql1, new Object[]{versionId}, Integer.class);

        String sql2 = "select count(1) from t_transfer2 where version_id = ?";
        int countNo2 = this.jdbcTemplate.queryForObject(sql2, new Object[]{versionId}, Integer.class);

        if(countNo1 > 1 || countNo2 > 1){
            return true;
        }else{
            return false;
        }
    }

    private Boolean isUpload(String docId) {
        String sql = "select count(1) from t_transfer where doc_id = ? and status = 2";
        String sql2 = "select count(1) from t_transfer where doc_id = ? and status = 2";
        int count = this.jdbcTemplate.queryForObject(sql, new Object[]{docId}, Integer.class);
        int count2 = this.jdbcTemplate.queryForObject(sql2, new Object[]{docId}, Integer.class);
        if (count > 0 || count2 > 0) {
            return true;
        } else {
            return false;
        }
    }

    private void updateTransferStatus2(String docId) {
        String sql = "update t_transfer set status = 2 where doc_id = ? ";
        jdbcTemplate.update(sql, docId);

        String sql2 = "update t_transfer2 set status = 2 where doc_id = ? ";
        jdbcTemplate.update(sql2, docId);
    }

    public void checkInConsistant() throws IOException {
        Path path=Paths.get(XML_PATH);

        Files.walkFileTree(path,new SimpleFileVisitor<Path>(){
            @Override
            public FileVisitResult visitFile(Path file, BasicFileAttributes attrs) throws IOException {
                if (file.toString().endsWith(".xml")) {
                    String fileName = file.getFileName().toString();
                    int endPos = fileName.indexOf(".");
                    String docId = fileName.substring(0, endPos);

                    String sql = "select count(1) from t_transfer where doc_id = ?";
                    int cnt = jdbcTemplate.queryForObject(sql, new Object[]{docId}, Integer.class);
                    // 说明xml中的id在数据库中找不到,记录下来
                    if (cnt <= 0) {
                        log.error("该文档id在数据库找不到，但存在xml:" + docId);
                        Document doc = Factory.Document.fetchInstance(CEObjectStoreUtil.getDefaultCEObjectStore().getOs(), new Id(docId), null);
                        int fileSize = doc.get_ContentElements().size();
                        String docTitle = doc.getProperties().getStringValue("DocumentTitle");
                        log.error("附件数量："+fileSize+"&标题："+docTitle);
                        System.out.println("该文档id在数据库找不到，但存在xml:" + docId);
                    }
                }
                return FileVisitResult.CONTINUE;
            }
        });
    }

    /**
     * 获取所有版本号大于1的文档,查询多版本文档
     * 获取老板本，老的版本号记录下来，版本序列号记录下来
     * 通过版本序列号将所有的文档逐个进行升级
     */
    public void bugfixVersionDocument(){
        try {
            int index = 0;
            CEObjectStore ceObjectStore = CEObjectStoreUtil.getDefaultCEObjectStore();
            SearchSQL sql = new SearchSQL();
            sql.setSelectList("*");
            sql.setFromClauseInitialValue("Document", null, true);
            sql.setWhereClause(" IsCurrentVersion = TRUE ");
            int pageSize = 50;
            PropertyFilter filter = new PropertyFilter();
            filter.setMaxRecursion(0);
            filter.addIncludeType(new FilterElement(null, null, null, FilteredPropertyType.ANY, null));

            SearchScope search = new SearchScope(ceObjectStore.getOs());
            IndependentObjectSet objects = search.fetchObjects(sql, pageSize, filter, true);
            PageIterator pageIterator = objects.pageIterator();

            while (pageIterator.nextPage() == true) {
                Object[] currentPage = pageIterator.getCurrentPage();
                for (Object o : currentPage) {
                    Document document = (Document) o;
                    try {
                        if (downloadLock.lock(document.get_Id().toString())) {
                            this.updateTransferVersion(document);
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                        log.error(e);
                        log.error("出错文档id：" + document.get_Id().toString());
                    } finally {
                        downloadLock.releaseLock(document.get_Id().toString());
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
            log.error(e);
        }
    }

    public void bugfixVersionDocument2() throws Exception {

        String sql = "select doc_id from t_transfer where status = 1 AND (version_id is null or majaorVersion_no is null)";
        List<String> resultList = this.jdbcTemplate.queryForList(sql, String.class);
        for(String docId : resultList){
            try{
            Document document = CEHelper.getDocumentInfo(CEObjectStoreUtil.getDefaultCEObjectStore().getOs(), docId);
            this.updateTransferVersion(document);
            }catch(Exception e){
                e.printStackTrace();
                log.error(e);
            }

        }
        String sql2 = "select doc_id from t_transfer2 where status = 1 AND (version_id is null or majaorVersion_no) is null";
        List<String> resultList2 = this.jdbcTemplate.queryForList(sql2, String.class);
        for(String docId : resultList2){
            try{
                Document document = CEHelper.getDocumentInfo(CEObjectStoreUtil.getDefaultCEObjectStore().getOs(), docId);
                this.updateTransferVersion2(document);
            }catch(Exception e){
                e.printStackTrace();
                log.error(e);
            }

        }
    }

    private void updateTransferVersion2(Document doc) throws Exception {
        String versionId = doc.get_VersionSeries().get_Id().toString();
        int majorVersionNo = doc.get_MajorVersionNumber();
        int minorVersionNo = doc.get_MinorVersionNumber();
        String sql = "update t_transfer2 set status = 1,version_id=?,majorVersion_no =?,minorversion_no=? where doc_id = ? ";

        jdbcTemplate.update(sql, versionId, majorVersionNo, minorVersionNo, doc.get_Id().toString());
    }

    private void updateTransferVersion(Document doc) throws Exception {
        String versionId = doc.get_VersionSeries().get_Id().toString();
        int majorVersionNo = doc.get_MajorVersionNumber();
        int minorVersionNo = doc.get_MinorVersionNumber();
        String sql = "update t_transfer set status = 1,version_id=?,majorVersion_no =?,minorversion_no=? where doc_id = ? ";

        jdbcTemplate.update(sql, versionId, majorVersionNo, minorVersionNo, doc.get_Id().toString());
    }

    public void resetDocumentRetrievalName() throws Exception {
        String sql = "select doc_id from t_transfer where status = 1 and (retrieval_Names is null or retrieval_Names = '')";
        List<String> resultList = this.jdbcTemplate.queryForList(sql, String.class);
        for(String docId : resultList){
            Document doc = CEHelper.getDocumentInfo(CEObjectStoreUtil.getDefaultCEObjectStore().getOs(), docId);
            ContentElementList contentElements = doc.get_ContentElements();

            List<String> retrievalNames = Collections.synchronizedList(new ArrayList<>());
            List<String> contentTypes = Collections.synchronizedList(new ArrayList<>());
            if (contentElements != null && !contentElements.isEmpty()) {
                Iterator<?> contentElementIterator = contentElements.iterator();
                while (contentElementIterator.hasNext()) {
                    ContentTransfer contentTransfer = (ContentTransfer) contentElementIterator.next();
                    String contentType = contentTransfer.get_ContentType();
                    String retrievalName = contentTransfer.get_RetrievalName();
                    retrievalNames.add(retrievalName);
                    contentTypes.add(contentType);
                }
                String fixSql = "update t_transfer set retrieval_Names = ?, content_Types = ? where doc_id = ?";
                jdbcTemplate.update(new PreparedStatementCreator() {
                    @Override
                    public PreparedStatement createPreparedStatement(Connection connection) throws SQLException {
                        PreparedStatement ps = connection.prepareStatement(fixSql);
                        ps.setString(1, StringUtils.join(retrievalNames, ","));
                        ps.setString(2, StringUtils.join(contentTypes, ","));
                        ps.setString(3, docId);
                        return ps;
                    }
                });
            }
        }
    }

    public void resetDocumentRetrievalName2() throws Exception {
        String sql = "select doc_id from t_transfer2 where (retrieval_Names is null or retrieval_Names = '')";
        List<String> resultList = this.jdbcTemplate.queryForList(sql, String.class);
        for(String docId : resultList){
            Document doc = CEHelper.getDocumentInfo(CEObjectStoreUtil.getDefaultCEObjectStore().getOs(), docId);
            ContentElementList contentElements = doc.get_ContentElements();

            List<String> retrievalNames = Collections.synchronizedList(new ArrayList<>());
            List<String> contentTypes = Collections.synchronizedList(new ArrayList<>());
            if (contentElements != null && !contentElements.isEmpty()) {
                Iterator<?> contentElementIterator = contentElements.iterator();
                while (contentElementIterator.hasNext()) {
                    ContentTransfer contentTransfer = (ContentTransfer) contentElementIterator.next();
                    String contentType = contentTransfer.get_ContentType();
                    String retrievalName = contentTransfer.get_RetrievalName();
                    retrievalNames.add(retrievalName);
                    contentTypes.add(contentType);
                }
                final LobHandler lobHandler = new DefaultLobHandler();
                String fixSql = "update t_transfer2 set retrieval_Names = ?, content_Types = ? where doc_id = ?";
                jdbcTemplate.execute(fixSql,
                        new AbstractLobCreatingPreparedStatementCallback(lobHandler) {
                            protected void setValues(PreparedStatement ps, LobCreator lobCreator) throws SQLException {
                                //ps.setInt(1, lobHandler.get);
                                lobCreator.setClobAsString(ps, 1, StringUtils.join(retrievalNames,","));
                                lobCreator.setClobAsString(ps, 2, StringUtils.join(contentTypes,","));
                                ps.setString(3, docId);
                            }
                        }
                );
            }
        }
    }

    public void queryNotExistFile() throws Exception {
        String sql = "select doc_id from t_transfer where status = 1";
        List<String> resultList = this.jdbcTemplate.queryForList(sql, String.class);
        for(String docId : resultList){
            try{
                Document doc = CEHelper.getDocumentInfo(CEObjectStoreUtil.getDefaultCEObjectStore().getOs(), docId);
            }catch(EngineRuntimeException e){
                if(e.getExceptionCode().equals("FNRCE0051E")){
                    String msg = "文档id为："+docId+"的文档对应的附件已不在FileNet中";
                    log.error(msg);
                    System.out.println(msg);
                }
            }
        }
    }

    public void clearErrorData(){
        String sql = "delete from t_transfer where status = 1 and retrieval_Names is null and file_nums >=1";
        this.jdbcTemplate.update(sql);
        String sql2 = "delete from t_transfer2 where status = 0 and (retrieval_Names is null or dbms_lob.getlength(retrieval_Names)=0) and file_nums >=1";
        this.jdbcTemplate.update(sql2);
        String sql3 = "DELETE from t_transfer\n" +
                " WHERE (doc_id) IN\n" +
                "       (SELECT doc_id\n" +
                "          FROM t_transfer\n" +
                "         GROUP BY doc_id, status, file_nums, retrieval_Names, content_types\n" +
                "        HAVING COUNT(1) > 1)\n" +
                "   AND ROWID NOT IN\n" +
                "       (SELECT MIN(ROWID)\n" +
                "          FROM t_transfer\n" +
                "         GROUP BY doc_id, status, file_nums, retrieval_Names, content_types\n" +
                "        HAVING COUNT(*) > 1)";
        this.jdbcTemplate.update(sql3);
        String sql4="DELETE from t_transfer2\n" +
                " WHERE (doc_id) IN\n" +
                "       (SELECT doc_id\n" +
                "          FROM t_transfer2\n" +
                "         GROUP BY doc_id, status, file_nums, content_types\n" +
                "        HAVING COUNT(1) > 1)\n" +
                "   AND ROWID NOT IN\n" +
                "       (SELECT MIN(ROWID)\n" +
                "          FROM t_transfer2\n" +
                "         GROUP BY doc_id, status, file_nums, content_types\n" +
                "        HAVING COUNT(*) > 1)";
        this.jdbcTemplate.update(sql4);
    }

    public void mergeData1(){
        String sql = "select doc_id from t_transfer group by doc_id having count(1) >1 ";
        List<String> docIds = this.jdbcTemplate.queryForList(sql, String.class);

        for(String docId : docIds){
            String sql2 = "update t_transfer set flag = 'D' where doc_id = ?";
            this.jdbcTemplate.update(sql2, docId);

            String sql3 = "select doc_id,status,file_nums,retrieval_Names,content_Types,version_id, majaorVersion_no, minorversion_no from t_transfer where doc_id = ?";
            List<Map<String,Object>> results = this.jdbcTemplate.queryForList(sql3,docId);

            List<String> temp1 = new ArrayList<>();
            List<String> temp2 = new ArrayList<>();
            int fileNums = 0;
            int majaorVersionNo =0;
            int minorversionNo = 0;
            String verseriesId = "";

            for(Map<String, Object> item : results){
                String contentType = (String)item.get("content_types");
                String retrievalNames = (String)item.get("retrieval_Names");
                verseriesId = (String)item.get("version_id");
                majaorVersionNo = ((BigDecimal)item.get("majaorVersion_no")).intValue();
                minorversionNo = ((BigDecimal)item.get("minorversion_no")).intValue();
                fileNums = ((BigDecimal)item.get("file_nums")).intValue();
                temp1.add(contentType);
                temp2.add(retrievalNames);
            }
            String contentTypes = StringUtils.join(temp1,",");
            String retrievalNames = StringUtils.join(temp2,",");

            String sql4 = "insert into t_transfer(doc_id,status,file_nums,retrieval_Names,content_Types,version_id, majaorVersion_no, minorversion_no) values(?,?,?,?,?,?,?,?)";
            this.jdbcTemplate.update(sql4,docId,1,fileNums,retrievalNames,contentTypes,verseriesId,majaorVersionNo,minorversionNo);

            String sql5 = "delete from t_transfer where flag = 'D' ";
            this.jdbcTemplate.update(sql5);
        }
    }

    public void mergeData2(){
        String sql = "select doc_id from t_transfer2 group by doc_id having count(1) >1 ";
        List<String> docIds = this.jdbcTemplate.queryForList(sql, String.class);

        for(String docId : docIds){
            String sql2 = "update t_transfer2 set flag = 'D' where doc_id = ?";
            this.jdbcTemplate.update(sql2, docId);

            LobHandler lobHandler = new DefaultLobHandler();
            String sql3 = "select doc_id,status,file_nums,retrieval_Names,content_Types,version_id, majaorVersion_no, minorversion_no from t_transfer2 where doc_id = ?";
            final List<Map<String, Object>> resultset = new ArrayList<>();
            this.jdbcTemplate.query(sql3, new Object[]{docId},new AbstractLobStreamingResultSetExtractor() {
                protected void streamData(ResultSet rs) throws SQLException, DataAccessException {
                    do{
                        Map<String, Object> row = new HashMap<>();
                        row.put("doc_id", rs.getString(1));
                        row.put("status", rs.getString(2));
                        row.put("file_nums", rs.getBigDecimal(3));
                        row.put("retrieval_Names", lobHandler.getClobAsString(rs,4));
                        row.put("content_Types", rs.getString(5));
                        row.put("version_id", rs.getString(6));
                        row.put("majaorVersion_no", rs.getBigDecimal(7));
                        row.put("minorversion_no", rs.getBigDecimal(8));

                        resultset.add(row);
                    }while(rs.next());
                }
            });

            List<String> temp1 = new ArrayList<>();
            List<String> temp2 = new ArrayList<>();
            int fileNums = 0;
            int majaorVersionNo =0;
            int minorversionNo = 0;
            String verseriesId = "";

            for(Map<String, Object> item : resultset){
                String contentType = (String)item.get("content_types");
                String retrievalNames = (String)item.get("retrieval_Names");
                verseriesId = (String)item.get("version_id");
                majaorVersionNo = ((BigDecimal)item.get("majaorVersion_no")).intValue();
                minorversionNo = ((BigDecimal)item.get("minorversion_no")).intValue();
                fileNums = ((BigDecimal)item.get("file_nums")).intValue();
                temp1.add(contentType);
                temp2.add(retrievalNames);
            }
            String contentTypes = StringUtils.join(temp1,",");
            String retrievalNames = StringUtils.join(temp2,",");

            String sql4 = "insert into t_transfer2(doc_id,status,file_nums,content_Types,version_id, majaorVersion_no, minorversion_no, flag) values(?,?,?,?,?,?,?,?)";
            this.jdbcTemplate.update(sql4,docId,1,fileNums,contentTypes,verseriesId,majaorVersionNo,minorversionNo,"W");

            String sql5 = "update t_transfer2 set retrieval_Names = ? where doc_id = ? and flag = 'W'";
            LobHandler lobHandler2 = new DefaultLobHandler();
            jdbcTemplate.execute(sql5,
                    new AbstractLobCreatingPreparedStatementCallback(lobHandler2) {
                        protected void setValues(PreparedStatement ps, LobCreator lobCreator) throws SQLException {
                            lobCreator.setClobAsString(ps, 1, retrievalNames);
                            ps.setString(2, docId);
                        }
                    }
            );

            String sql6 = "delete from t_transfer2 where flag = 'D' ";
            this.jdbcTemplate.update(sql6);
        }
    }

    public void clearContentRepeatData(){
        String sql = "select * from t_transfer";
        List<Map<String, Object>> rows = this.jdbcTemplate.queryForList(sql);
        for(Map<String, Object> row : rows){
            String retrievalNames = (String)row.get("retrieval_Names");
            String[] retrievalNamesArray = StringUtils.split(retrievalNames,",");
            List<String> retrievalNamesList = Arrays.asList(retrievalNamesArray);
            List<String> retrievalNamesWithoutDuplicatesList = retrievalNamesList.stream().distinct().collect(Collectors.toList());
            String retrievalNamesWithoutDuplicatesStr = StringUtils.join(retrievalNamesWithoutDuplicatesList, ",");
            String docId = (String)row.get("doc_id");
            String sql2 = "update t_transfer set retrieval_Names = ? where doc_id = ?";
            this.jdbcTemplate.update(sql2, new Object[]{retrievalNamesWithoutDuplicatesStr, docId});
        }

        String sql3 = "select * from t_transfer2";
        List<Map<String, Object>> rows2 = Lists.newArrayList();

        LobHandler lobHandler = new DefaultLobHandler();
        jdbcTemplate.query(sql3,
                new AbstractLobStreamingResultSetExtractor() {
                    public void streamData(ResultSet rs) throws SQLException, IOException {
                        do{
                            Map<String, Object> row = new HashMap<>();
                            row.put("doc_id", rs.getString(1));
                            row.put("status", rs.getString(2));
                            row.put("file_nums", rs.getBigDecimal(3));
                            row.put("retrieval_Names", lobHandler.getClobAsString(rs,4));
                            row.put("content_Types", rs.getString(5));
                            row.put("version_id", rs.getString(6));
                            row.put("majaorVersion_no", rs.getBigDecimal(7));
                            row.put("minorversion_no", rs.getBigDecimal(8));
                            rows2.add(row);
                        }while(rs.next());
                    }
                }
        );

        for(Map<String, Object> row : rows2){
            String retrievalNames = (String)row.get("retrieval_Names");
            String[] retrievalNamesArray = StringUtils.split(retrievalNames,",");
            List<String> retrievalNamesList = Arrays.asList(retrievalNamesArray);
            List<String> retrievalNamesWithoutDuplicatesList = retrievalNamesList.stream().distinct().collect(Collectors.toList());
            String retrievalNamesWithoutDuplicatesStr = StringUtils.join(retrievalNamesWithoutDuplicatesList, ",");
            String docId = (String)row.get("doc_id");

            String sql4 = "update t_transfer2 set retrieval_Names = ? where doc_id = ?";
            //this.jdbcTemplate.update(sql4, new Object[]{retrievalNamesWithoutDuplicatesStr, docId});
            //tring sql = "insert into t_transfer2(doc_id,status,file_nums,retrieval_Names,content_Types,version_id, majaorVersion_no, minorversion_no) values(?,?,?,?,?,?,?,?)";
            LobHandler lobHandler2 = new DefaultLobHandler();
            jdbcTemplate.execute(sql4,
                    new AbstractLobCreatingPreparedStatementCallback(lobHandler2) {
                        protected void setValues(PreparedStatement ps, LobCreator lobCreator) throws SQLException {
                            lobCreator.setClobAsString(ps, 1, retrievalNamesWithoutDuplicatesStr);
                            ps.setString(2, docId);
                        }
                    }
            );
        }
    }
}
