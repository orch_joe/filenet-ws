package com.zhjoe.ccpws.service;

import com.alibaba.fastjson.JSON;
import com.filenet.api.admin.*;
import com.filenet.api.collection.ClassDefinitionSet;
import com.filenet.api.collection.LocalizedStringList;
import com.filenet.api.collection.PropertyDefinitionList;
import com.filenet.api.constants.RefreshMode;
import com.filenet.api.core.Connection;
import com.filenet.api.core.Domain;
import com.filenet.api.core.Factory;
import com.filenet.api.core.ObjectStore;
import com.filenet.api.exception.EngineRuntimeException;
import com.filenet.api.util.Id;
import com.filenet.api.util.UserContext;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.google.common.collect.Sets;
import com.zhjoe.ccpws.vo.TreeNode;
import com.zhjoe.strongit.filenet.service.ce.CEObjectStore;
import com.zhjoe.strongit.filenet.service.datatype.ClassDefinitionInfo;
import com.zhjoe.strongit.filenet.service.util.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.security.auth.Subject;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class DocumentClassTransferService2 {
	private static Logger logger = LoggerFactory.getLogger(DocumentClassTransferService2.class);
	private static String ExtName = "2";
	/**
	 * 1.按照根类型查找老的FileNet中所有的文档类
	 * 2.循环每个文档及其子类，获取文档类的符号名称，显示名称
	 * 3.进入新的FileNet根据符号名称和显示名称，转移文档类
	 * 4.获取老的FileNet下所有文档类的属性信息，属性中包含choiceList
	 * 5.给新的FileNet对应的文档类转移属性信息
	 */
	//忽略同步的class文档类
	private static Set<String> denyClassNameList = Sets.newHashSet();
	//所有需要同步的文档类定义
	private static List<ClassDefinitionInfo> classDefinitionInfos = Lists.newArrayList();
	//所有类定义中的属性信息
	private static Map<String, Set<PropertyTemplate>> properties = Maps.newHashMap();

	public static void main(String[] args) {
		logger.info("------------------------文档类同步开始----------------------------");
		DocumentClassTransferService2 service = new DocumentClassTransferService2();
		CEObjectStore oldCeObjectStore = service.getOldOs();
		ObjectStore oldOs = oldCeObjectStore.getOs();
		//oldCeObjectStore.getUc().pushSubject(oldCeObjectStore.getSubject());
		//获取所有老的filenet的文档类定义
		logger.info("****获取老filenet中的所有文档定义信息开始****");
		service.queryAllClassDefinition("Document", oldOs);
		List<ClassDefinitionInfo> classDefinitionInfoTree = TreeUtils.generateTrees(classDefinitionInfos);
		logger.info(JSON.toJSONString(classDefinitionInfoTree));
		logger.info("****获取老filenet中的所有文档定义信息结束****");
		//释放当前线程中的userContext
		oldCeObjectStore.getUc().popSubject();


		//开始在新的filenet中放入信息
		CEObjectStore newCeObjectStore = service.getNewOs();
		ObjectStore newOs = newCeObjectStore.getOs();
		//newCeObjectStore.getUc().pushSubject(newCeObjectStore.getSubject());
		logger.info("****在新的filenet中同步老filenet属性开始****");
		service.createNewClassDefinitonInNewFileNet(classDefinitionInfoTree, newOs);
		logger.info("****在新的filenet中同步老filenet属性开始****");
		newCeObjectStore.getUc().popSubject();
		logger.info("------------------------文档类同步结束----------------------------");
	}

	//开始同步新的filenet信息classDefinition信息
	private void createNewClassDefinitonInNewFileNet(List<ClassDefinitionInfo> classDefinitionInfoTree, ObjectStore newOs) {
		ClassDefinitionInfo classDefinitionInfoRoot = TreeUtils.getRoot(classDefinitionInfoTree);
		List<ClassDefinitionInfo> classDefinitionInfos = Lists.newArrayList();
		if (null == classDefinitionInfoRoot) {
			logger.info("根节点的文档类没有找到,不在继续执行同步操作");
			return;
		}
		logger.info("因为根节点为Document所以不用同步,因为Document为文档根");
		List<? extends TreeNode> children = classDefinitionInfoRoot.getChildren();
		if (null == children || children.isEmpty()) {
			logger.info("根类中没有找到子类信息,不再进行同步操作");
			return;
		}
		logger.info("从根类开始递归调用子类中的信息同步,第一次同步根节点下面的全部子类信息");
		classDefinitionInfos = (List<ClassDefinitionInfo>) classDefinitionInfoRoot.getChildren();
		this.recursionCreateClassDefinition(classDefinitionInfos, newOs);
	}

	private void recursionCreateClassDefinition(List<ClassDefinitionInfo> childNodes, ObjectStore newOs) {
		for (ClassDefinitionInfo childClassDefinitionInfo : childNodes) {
			//获取父类名称
			String parentClassName = childClassDefinitionInfo.getParentClassName();
			//获取子类的名称
			String subClassName = childClassDefinitionInfo.getSymbolicName();
			//获取子类显示名称
			String subDisplayName = childClassDefinitionInfo.getName();
			//定义子类的信息
			ClassDefinition childClassDefinition = null;
			//从新的filenet中按照className获取父类信息
			ClassDefinition parentClassDefinition = Factory.ClassDefinition.fetchInstance(newOs, parentClassName, null);
			try {
				childClassDefinition = Factory.ClassDefinition.fetchInstance(newOs, subClassName, null);
				logger.info("在父类:{}---中获取子类信息:{}", parentClassDefinition, subClassName);
			} catch (EngineRuntimeException e) {
				logger.info("在父类:{}---中没有获取到获取子类信息:{}", parentClassDefinition, subClassName);
			}
			//如果父类存在,子类不存在,那么在父类中创建子类信息
			if (childClassDefinition == null && parentClassDefinition != null) {
				logger.info("开始在父类:{}---中创建子类信息:{}", parentClassDefinition, subClassName);
				createSubClass(newOs, parentClassDefinition, subClassName, subDisplayName);
				logger.info("结束在父类:{}---中创建子类信息:{}", parentClassDefinition, subClassName);
			}
			//如果子类中还有子节点那么递归调用
			if (childClassDefinitionInfo.getChildren() != null && childClassDefinitionInfo.getChildren().size() > 0) {
				recursionCreateClassDefinition((List<ClassDefinitionInfo>) childClassDefinitionInfo.getChildren(), newOs);
			}
		}
	}

	private void createSubClass(ObjectStore newOs, ClassDefinition parentClassDefinition, String subClassName, String subDisplayName) {
		//在新的filenet中创建子类
		ClassDefinition subClassDefinition = parentClassDefinition.createSubclass();
		//设置子类属性
		subClassDefinition.set_SymbolicName(subClassName);
		LocalizedString ls = Factory.LocalizedString.createInstance();
		ls.set_LocalizedText(subDisplayName);
		ls.set_LocaleName("zh_cn");
		LocalizedStringList lsl = Factory.LocalizedString.createList();
		lsl.add(ls);
		subClassDefinition.set_DisplayNames(lsl);
		//获取自定的定义信息
		PropertyDefinitionList propertyDefinitionList = subClassDefinition.get_PropertyDefinitions();
		//在老的filenet中根据子类的名称获取子类中对应的属性信息
		Set<PropertyTemplate> subPropertyTemplates = properties.get(subClassName);
		//遍历所有子类的属性信息
		for (PropertyTemplate subPropertyTemplate : subPropertyTemplates) {
			//在新的filenet中创建子类的propertyTemplate,并返回
			logger.info("开始构建ClassDefinition:{}信息,并开始同步PropertyTemplate");
			PropertyTemplate newPropertyTemplate = buildNewPropertyTemplate(newOs, subPropertyTemplate, subClassName);
			logger.info("结束构建ClassDefinition:{}信息,并开始同步PropertyTemplate");
			PropertyDefinition propertyDefinition = newPropertyTemplate.createClassProperty();
			propertyDefinitionList.add(propertyDefinition);
		}
		//保存子类的信息
		subClassDefinition.save(RefreshMode.REFRESH);
	}

	private PropertyTemplate buildNewPropertyTemplate(ObjectStore newOs, PropertyTemplate subPropertyTemplate, String subClassName) {
		PropertyTemplate newPropertyTemplate = null;
		Id propId = subPropertyTemplate.get_Id();
		try {
			newPropertyTemplate = Factory.PropertyTemplate.fetchInstance(newOs, propId, null);
		} catch (Exception e) {
			logger.info("在新的FileNet中没有找到id:{}的PropertyTemplate,自动创建", propId.toString());
		}
		if (null != newPropertyTemplate) {
			return newPropertyTemplate;
		}
		int typeId = subPropertyTemplate.get_DataType().getValue();
		logger.info("同步文档类:{}---中的PropertyTemplate信息SymbolicName:{}", subClassName, subPropertyTemplate.get_SymbolicName());
		if (typeId == 1) {
			//double
			newPropertyTemplate = Factory.PropertyTemplateFloat64.createInstance(newOs);
		} else if (typeId == 2) {
			//guid
			newPropertyTemplate = Factory.PropertyTemplateId.createInstance(newOs);

		} else if (typeId == 3) {
			//boolean
			newPropertyTemplate = Factory.PropertyTemplateBoolean.createInstance(newOs);

		} else if (typeId == 4) {
			//BINARY
			newPropertyTemplate = Factory.PropertyTemplateBinary.createInstance(newOs);

		} else if (typeId == 5) {
			//object
			newPropertyTemplate = Factory.PropertyTemplateObject.createInstance(newOs);

		} else if (typeId == 6) {
			//date
			newPropertyTemplate = Factory.PropertyTemplateDateTime.createInstance(newOs);

		} else if (typeId == 7) {
			//long
			newPropertyTemplate = Factory.PropertyTemplateInteger32.createInstance(newOs);

		} else if (typeId == 8) {
			//String
			newPropertyTemplate = Factory.PropertyTemplateString.createInstance(newOs);
		}
		//构建一个新的choiceList

		ChoiceList newChoiceList = buildNewChoiceList(newOs, subPropertyTemplate, subClassName);
		if (null != newChoiceList) {
			newPropertyTemplate.set_ChoiceList(newChoiceList);
		}
		newPropertyTemplate.set_IsHidden(subPropertyTemplate.get_IsHidden());
		LocalizedString locStr = Factory.LocalizedString.createInstance();
		locStr.set_LocalizedText(subPropertyTemplate.get_Name());
		locStr.set_LocaleName(newOs.get_LocaleName());
		newPropertyTemplate.set_DisplayNames(Factory.LocalizedString.createList());
		newPropertyTemplate.get_DisplayNames().add(locStr);
		newPropertyTemplate.set_Cardinality(subPropertyTemplate.get_Cardinality());
		newPropertyTemplate.set_Settability(subPropertyTemplate.get_Settability());
		newPropertyTemplate.set_IsValueRequired(subPropertyTemplate.get_IsValueRequired());
		newPropertyTemplate.set_PersistenceType(subPropertyTemplate.get_PersistenceType());
		newPropertyTemplate.set_IsNameProperty(subPropertyTemplate.get_IsNameProperty());
		newPropertyTemplate.set_RequiresUniqueElements(subPropertyTemplate.get_RequiresUniqueElements());
		newPropertyTemplate.set_SymbolicName(subPropertyTemplate.get_SymbolicName() + ExtName);
		newPropertyTemplate.set_PropertyDisplayCategory(subPropertyTemplate.get_PropertyDisplayCategory());
		newPropertyTemplate.save(RefreshMode.REFRESH);
		return newPropertyTemplate;
	}

	private ChoiceList buildNewChoiceList(ObjectStore newOs, PropertyTemplate subPropertyTemplate, String subClassName) {
		ChoiceList oldChoiceList = subPropertyTemplate.get_ChoiceList();
		if (null == oldChoiceList) {
			return null;
		}
		ChoiceList newChoiceList = null;
		try {
			newChoiceList = Factory.ChoiceList.fetchInstance(newOs, oldChoiceList.get_Id(), null);
		} catch (Exception e) {
			logger.info("在新的FileNet中没有找到id:{}的choiceList,自动创建", oldChoiceList.get_Id().toString());
		}
		if (null != newChoiceList) {
			return newChoiceList;
		}
		logger.info("同步文档类:{}---中的PropertyTemplate信息SymbolicName:{}----choiceList信息DisplayName:{}", subClassName, subPropertyTemplate.get_SymbolicName(), oldChoiceList.get_DisplayName());
		newChoiceList = Factory.ChoiceList.createInstance(newOs);
		newChoiceList.set_DataType(oldChoiceList.get_DataType());
		newChoiceList.set_DisplayName(oldChoiceList.get_DisplayName() + ExtName);
		newChoiceList.set_DescriptiveText(oldChoiceList.get_DescriptiveText() + ExtName);
		com.filenet.api.collection.ChoiceList oldChoiceListChoiceValues = oldChoiceList.get_ChoiceValues();
		if (null != oldChoiceListChoiceValues && !oldChoiceListChoiceValues.isEmpty()) {
			buildNewChoiceValues(newOs, newChoiceList, oldChoiceListChoiceValues);
		}
		newChoiceList.save(RefreshMode.REFRESH);
		return newChoiceList;
	}

	private void buildNewChoiceValues(ObjectStore newOs, ChoiceList newChoiceList, com.filenet.api.collection.ChoiceList oldChoiceListChoiceValues) {
		Iterator<Choice> oldChoiceListIterator = oldChoiceListChoiceValues.iterator();
		if (oldChoiceListIterator.hasNext()) {
			newChoiceList.set_ChoiceValues(Factory.Choice.createList());
		} else {
			return;
		}
		while (oldChoiceListIterator.hasNext()) {
			Choice oldChoice = oldChoiceListIterator.next();
			//创建新的choiceList
			Choice newChoice = Factory.Choice.createInstance(newOs);
			newChoice.set_DisplayName(oldChoice.get_DisplayName() + ExtName);
			newChoice.set_DisplayNames(oldChoice.get_DisplayNames());
			newChoice.set_ChoiceType(oldChoice.get_ChoiceType());
			newChoice.set_ChoiceIntegerValue(oldChoice.get_ChoiceIntegerValue());
			newChoice.set_ChoiceStringValue(oldChoice.get_ChoiceStringValue());
			newChoiceList.get_ChoiceValues().add(newChoice);
		}
	}

	//构造函数设置初始的内容
	public DocumentClassTransferService2() {
		String denyClassNames = FileNetConfig.getValue("DenyClassNames");
		String[] denyClassNamesArray = denyClassNames.split(",");
		for (String denyClassName : denyClassNamesArray) {
			denyClassNameList.add(denyClassName);
		}
		//根节点的文档类Document
		ClassDefinitionInfo rootClassDefinitionInfo = new ClassDefinitionInfo();
		rootClassDefinitionInfo.setSymbolicName("Document");
		rootClassDefinitionInfo.setId(FileNetConfig.getValue("RootClassId"));
		rootClassDefinitionInfo.setName("文档");
		rootClassDefinitionInfo.setOsName(FileNetConfig.getValue("objectStoreName"));
		this.classDefinitionInfos.add(rootClassDefinitionInfo);
	}

	//获取所有老的filenet的文档类定义
	private void queryAllClassDefinition(String documentClass, ObjectStore oldOs) {

		//获取文档的类信息
		ClassDefinition documentClassDefinition = Factory.ClassDefinition.fetchInstance(oldOs, documentClass, null);
		//获取库名称
		String osName = oldOs.get_SymbolicName();
		//获取所有直接子类的类信息
		ClassDefinitionSet childClassDefinitionSet = documentClassDefinition.get_ImmediateSubclassDefinitions();
		Iterator it = childClassDefinitionSet.iterator();
		//遍历子类信息
		while (it.hasNext()) {
			//获取直接子类定义信息
			ClassDefinition childClassDefinition = (ClassDefinition) it.next();
			String symbolicName = childClassDefinition.get_SymbolicName();
			String displayName = childClassDefinition.get_DisplayName();
			String classId = childClassDefinition.get_Id().toString();
			//如果在
			if (denyClassNameList.contains(symbolicName)) {
				continue;
			}
			ClassDefinitionInfo classDefinitionInfo = new ClassDefinitionInfo();
			classDefinitionInfo.setSymbolicName(symbolicName);
			classDefinitionInfo.setId(classId);
			classDefinitionInfo.setName(displayName);
			classDefinitionInfo.setOsName(osName);
			classDefinitionInfo.setParentClassName(documentClass);
			classDefinitionInfos.add(classDefinitionInfo);
			//从老的filenet中获取所有的propertyTemplate
			List<PropertyTemplate> propertyTemplates = queryPropertyTemplate(childClassDefinition, documentClassDefinition);
			Set<PropertyTemplate> classProperTemplates = Sets.newHashSet();
			if (null == propertyTemplates || propertyTemplates.isEmpty()) {
				continue;
			}
			logger.info("开始获取文档类:{}----下面的文档类名称:{}---一共私有属性为:{}", documentClass, displayName, propertyTemplates.size());
			for (PropertyTemplate propertyTemplate : propertyTemplates) {
				if (null == propertyTemplate) {
					continue;
				}
				String propId = propertyTemplate.get_Id().toString();
				logger.info("子文档类名称为:{}------属性id:{}---属性名称:{}", displayName, propId, propertyTemplate.get_DisplayName());
				classProperTemplates.add(propertyTemplate);
			}
			logger.info("开始获取文档类:{}----下面的文档类名称:{},属性获取完毕", documentClass, displayName);
			properties.put(symbolicName, classProperTemplates);
			//如果子类定义中还是子类,那么开始递归调用
			boolean isEmpty = childClassDefinition.get_ImmediateSubclassDefinitions().isEmpty();
			if (!isEmpty) {
				queryAllClassDefinition(symbolicName, oldOs);
			}
		}
	}

	private List<PropertyTemplate> queryPropertyTemplate(ClassDefinition childClassDefinition, ClassDefinition documentClassDefinition) {
		List<PropertyTemplate> propertyTemplates = Lists.newArrayList();
		//获取文档类定义中的属性信息
		PropertyDefinitionList propertyDefinitionList = childClassDefinition.get_PropertyDefinitions();
		//遍历
		Iterator it = propertyDefinitionList.iterator();
		while (it.hasNext()) {
			PropertyDefinition childPropertyDefinition = (PropertyDefinition) it.next();
			childPropertyDefinition.get_IsSystemOwned();
			childPropertyDefinition.get_IsNameProperty();
			//传入
			if (!childPropertyDefinition.get_IsSystemOwned() && !childPropertyDefinition.get_IsHidden() && !existParentProperty(childPropertyDefinition, documentClassDefinition)) {
				PropertyTemplate propertyTemplate = childPropertyDefinition.get_PropertyTemplate();
				propertyTemplates.add(propertyTemplate);
			}
		}
		return propertyTemplates;
	}

	//查看父类中的属性,在子类中是否存在,存在返回true,不存在返回false
	private boolean existParentProperty(PropertyDefinition childPropertyDefinition, ClassDefinition documentClassDefinition) {
		//获取父类定义的所有的属性
		PropertyDefinitionList parentPropertyDefinitionList = documentClassDefinition.get_PropertyDefinitions();
		Iterator it = parentPropertyDefinitionList.iterator();
		while (it.hasNext()) {
			PropertyDefinition parentPropertyDefinition = (PropertyDefinition) it.next();
			//如果父类中定义的属性在子类中出现,那么返回true
			if (parentPropertyDefinition.get_SymbolicName().equals(childPropertyDefinition.get_SymbolicName())) {
				return true;
			}
		}
		return false;
	}


	private CEObjectStore getOldOs() {
		FileNetConfigUtil.config();
		CEObjectStore ceos = new CEObjectStore();
		Connection con = CEConnectionUtil.getConnection();
		ceos.setCon(con);
		Subject subject = UserContext.createSubject(con, FileNetConfig.ADMIN_USER, FileNetConfig.ADMIN_PASSWORD, FileNetConfig.DEFAULT_JAAS_STANZA);
		ceos.setSubject(subject);
		ceos.getUc().pushSubject(subject);
		Domain domain = Factory.Domain.fetchInstance(con, null, null);
		ceos.setDomain(domain);
		String domainName = domain.get_Name();
		ceos.setDomainName(domainName);
		ceos.setOset(domain.get_ObjectStores());
		ObjectStore os = null;
		os = Factory.ObjectStore.fetchInstance(domain, FileNetConfig.OBJECT_STORE_NAME, null);
		ceos.setOs(os);
		ceos.setConnected(true);
		return ceos;
	}

	private CEObjectStore getNewOs() {
		FileNetConfigUtil.config(FileNetConfig.getValue("NewContentEngineUrl"));
		String newAdminPassword = FileNetConfig.getValue("newAdminPassword");
		String newAdminUser = FileNetConfig.getValue("newAdminUser");
		CEObjectStore ceos = new CEObjectStore();
		Connection con = NewCEConnectionUtil.getConnection();
		ceos.setCon(con);
		Subject subject = UserContext.createSubject(con, newAdminUser, newAdminPassword, FileNetConfig.DEFAULT_JAAS_STANZA);
		ceos.setSubject(subject);
		ceos.getUc().pushSubject(subject);
		Domain domain = Factory.Domain.fetchInstance(con, null, null);
		ceos.setDomain(domain);
		String domainName = domain.get_Name();
		ceos.setDomainName(domainName);
		ceos.setOset(domain.get_ObjectStores());
		ObjectStore os = null;
		String newOsName = FileNetConfig.getValue("NEW_ObjectStoreName");
		os = Factory.ObjectStore.fetchInstance(domain, newOsName, null);
		ceos.setOs(os);
		ceos.setConnected(true);
		return ceos;
	}

}

