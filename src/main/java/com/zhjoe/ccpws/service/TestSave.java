package com.zhjoe.ccpws.service;

import com.filenet.api.admin.Choice;
import com.filenet.api.admin.ChoiceList;
import com.filenet.api.collection.ChoiceListSet;
import com.filenet.api.constants.ChoiceType;
import com.filenet.api.constants.RefreshMode;
import com.filenet.api.constants.TypeID;
import com.filenet.api.core.Factory;
import com.filenet.api.core.ObjectStore;
import com.filenet.api.util.Id;
import com.zhjoe.strongit.filenet.service.ce.CEObjectStore;
import com.zhjoe.strongit.filenet.service.util.NewCEObjectStoreUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Iterator;

public class TestSave {
	private static Logger logger = LoggerFactory.getLogger(TestSave.class);

	public static void main(String[] args) {
		CEObjectStore ceObjectStore = NewCEObjectStoreUtil.getDefaultCEObjectStore();
		ObjectStore os = ceObjectStore.getOs();
		TestSave testSave = new TestSave();
		//testSave.buildNewChoiceList(os);

		testSave.deleteAllPropertyAndChoiceList(os);
	}

	private ChoiceList buildNewChoiceList(ObjectStore newOs) {
		ChoiceList newChoiceList = null;
		try {
			newChoiceList = Factory.ChoiceList.fetchInstance(newOs, new Id("{123456}"), null);
		} catch (Exception e) {
			logger.info("在新的FileNet中没有找到id:{}的choiceList", "{123456}");
		}
		if (null != newChoiceList) {
			return newChoiceList;
		}
		newChoiceList = Factory.ChoiceList.createInstance(newOs);
		newChoiceList.set_DataType(TypeID.STRING);
		newChoiceList.set_DisplayName("self_test");
		newChoiceList.set_DescriptiveText("description");
		buildNewChoiceValues(newChoiceList, newOs);
		newChoiceList.save(RefreshMode.REFRESH);
		return newChoiceList;
	}

	private void buildNewChoiceValues(ChoiceList newChoiceList, ObjectStore newOs) {
		newChoiceList.set_ChoiceValues(Factory.Choice.createList());
		for (int i = 0; i < 5; i++) {
			//创建新的choiceList
			Choice newChoice = Factory.Choice.createInstance(newOs);
			newChoice.set_DisplayName("self_test_choice");
			newChoice.set_ChoiceType(ChoiceType.STRING);
			newChoice.set_ChoiceStringValue("selft_test_choice" + i);
			newChoiceList.get_ChoiceValues().add(newChoice);
		}
	}

	private void deleteAllPropertyAndChoiceList(ObjectStore newOs){
		ChoiceListSet choiceListSet = newOs.get_ChoiceLists();
		Iterator<ChoiceList> choiceListIterator = choiceListSet.iterator();
		while (choiceListIterator.hasNext()){
			ChoiceList choiceList = choiceListIterator.next();
			choiceList.delete();
			//choiceList.save(RefreshMode.REFRESH);
		}
	}
}
