package com.zhjoe.ccpws.service.exportbean;

import lombok.Data;

@Data
public class PropertyTemplateRoot {
	private String displayNames;
	private Boolean valueRequired;
	private Boolean hidden;
	private Boolean nameProperty;
	private Boolean requiresUniqueElements;
	private ChoiceListBean choiceList;
	private String symbolicName;
	private Integer dataType;
	private String id;
	private String name;

	private String propertyDisplayCategory;

	private int cardinality;


}
