package com.zhjoe.ccpws.service.exportbean;

import lombok.Data;

import java.util.List;

@Data
public class ChoiceListBean {
	private String displayName;
	private String symbolicName;
	private Integer dataType;
	private String id;

	private String descriptiveText;
	private String name;

	private List<ChoiceBean> choiceBeans;

}
