package com.zhjoe.ccpws.service.exportbean;

import lombok.Data;

@Data
public class ChoiceBean {
	private String displayName;
	private Integer choiceType;

	private Integer choiceIntegerValue;

	private String choiceStringValue;

	private String id;
}
