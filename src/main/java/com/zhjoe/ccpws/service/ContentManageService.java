package com.zhjoe.ccpws.service;

import com.filenet.api.core.Folder;
import com.filenet.api.core.ObjectStore;
import com.zhjoe.ccpws.constant.Constants;
import com.zhjoe.ccpws.vo.Node;
import com.zhjoe.strongit.filenet.service.ce.CEHelper;
import com.zhjoe.strongit.filenet.service.comparator.ComparatorFolder;
import com.zhjoe.strongit.filenet.service.datatype.BaseInfo;
import com.zhjoe.strongit.filenet.service.datatype.FolderInfo;
import com.zhjoe.strongit.filenet.service.datatype.Pagination;
import com.zhjoe.strongit.filenet.service.datatype.RetrievalResult;
import com.zhjoe.strongit.filenet.service.util.ConvertFolder2FolderInfoUtil;
import org.springframework.stereotype.Service;

import java.util.*;

@Service
public class ContentManageService {

    /**
     *
     * @Title: fullTextSearchSearch
     * @Description: 全文检索
     * @param @param userVo
     * @param @param keyWord
     * @param @param pagination
     * @param @return
     * @param @throws Exception    设定文件
     * @return RetrievalResult    返回类型
     */
    public RetrievalResult fullTextSearchSearch(ObjectStore os, String keyWord, Pagination pagination) throws Exception{
        RetrievalResult result = CEHelper.retrieveDocumentInfosContentSummary(os, Constants.ROOT_CLASS, true, "id,ContentSummary,DocumentTitle", keyWord, "IsCurrentVersion = TRUE", "DocumentTitle", pagination);
        return result;
    }
    /**
     * @throws Exception
     * @Title: findTreeChilds
     * @Description: 查询档案中心的文件夹并以树节点的形式返回
     * @param @param os
     * @param @param path
     * @return List<Node>    返回类型
     */
    public List<Node> findTreeChilds(ObjectStore os, String path) throws Exception{
        List<Node> childs = new ArrayList<Node>();
        Folder folder = CEHelper.retrieveFolder(os, path);
        List<BaseInfo> folderList = searchChildFolders(folder, null, null);
        folderList2NodeList(folderList, childs);
        return childs;
    }
    /**
     * @throws Exception
     * @Title: searchChildFolders
     * @Description: 获取文件夹对象下的文件夹列表
     * @param @param parentFolder
     * @param @return    设定文件
     * @return List<BaseInfo>    返回类型
     */
    public List<BaseInfo> searchChildFolders(Folder parentFolder, String sort, String order) throws Exception {
        List<BaseInfo> list = new ArrayList<BaseInfo>();
        ComparatorFolder comparator = new ComparatorFolder();
        comparator.setOrder(order == null ? "asc" : order);
        comparator.setProperty(sort == null ? "FolderName" : sort);
        comparator.setType(1);
        List<Folder> subFolderList = CEHelper.retrieveSubFolderInfos(parentFolder, comparator);
        Iterator<Folder> folerListIterator = subFolderList.iterator();
        while (folerListIterator.hasNext()) {
            Folder folder = folerListIterator.next();
            FolderInfo folderInfo = ConvertFolder2FolderInfoUtil.convertFolder2FolderInfo(folder);
            list.add(folderInfo);
        }
        return list;
    }
    /**
     * @throws Exception
     * @Title: folderList2NodeList
     * @Description:树节点的转换
     * @param @param folderList
     * @param @param childs
     * @return void    返回类型
     */
    public void folderList2NodeList(List<BaseInfo> folderList,List<Node> childs){
        Node node = null;
        Map<String,Object> attributes = null;
        for(BaseInfo base : folderList){
            node = new Node();
            attributes = new HashMap<String, Object>();
            node.setId(base.getId());
            node.setText(base.getName());
            node.setState("open");
            node.setPath(base.getPath());
            childs.add(node);
        }
    }
}
