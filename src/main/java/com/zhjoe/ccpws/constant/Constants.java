/**
 * @Title: Constants.java
 * @Package com.ccpws.constant
 * @Description: TODO(用一句话描述该文件做什么)
 * @author Changling Jiang
 * @date 2013年11月26日 下午7:31:01
 * @version V1.0
 */
package com.zhjoe.ccpws.constant;


import com.zhjoe.strongit.filenet.service.util.FileNetConfig;

/**
 * @ClassName: Constants
 * @Description: TODO(这里用一句话描述这个类的作用)
 * @author Changling Jiang
 * @date 2013年11月26日 下午7:31:01
 *
 */
public class Constants {
	public static final String regularDocClass = FileNetConfig.DOCUMENTCLASS;
	public static final String folderClass = FileNetConfig.getValue("FolderFolderClass");
	public static final String ROOT_CLASS = FileNetConfig.DOCUMENTCLASS;
	public static final String ROOT_PATH = FileNetConfig.DEFAULT_FOLDER_PATH;
	/**
	 * 属性修改
	 */
	public static final String AUDIT_EVENT_ATTRIBUTES_CHANGED = "attributesChanged";
	/**
	 * 创建
	 */
	public static final String AUDIT_EVENT_CREATED = "created";
	/**
	 * 创建发行版
	 */
	public static final String AUDIT_EVENT_REVISION_CREATED = "revisionCreated";
	/**
	 * 版本添加
	 */
	public static final String AUDIT_EVENT_VERSION_ADDED = "versionAdded";
	/**
	 * 删除
	 *
	 */
	public static final String AUDIT_EVENT_DELETED = "deleted";
	/**
	 * 移动
	 */
	public static final String AUDIT_EVENT_MOVED = "moved";
	/**
	 * 复制
	 */
	public static final String AUDIT_EVENT_COPIED = "copied";


}
