/**
 * @Title: ConvertChoiceList2ChoiceInfoListUtil.java
 * @Package com.strongit.filenet.service.util
 * @Description: TODO(用一句话描述该文件做什么)
 * @author Changling Jiang
 * @date 2013年11月19日 下午3:54:28
 * @version V1.0
 */
package com.zhjoe.strongit.filenet.service.util;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import com.zhjoe.strongit.filenet.service.datatype.ChoiceInfo;
import org.apache.log4j.Logger;

import com.filenet.api.admin.Choice;
import com.filenet.api.collection.ChoiceList;

/**
 * @ClassName: ConvertChoiceList2ChoiceInfoListUtil
 * @Description: TODO(这里用一句话描述这个类的作用)
 * @author Changling Jiang
 * @date 2013年11月19日 下午3:54:28
 *
 */
public class ConvertChoiceList2ChoiceInfoListUtil {

	private static final Logger log = Logger.getLogger(ConvertChoiceList2ChoiceInfoListUtil.class);

	/**
	 * @Title: convertChoiceList2ChoiceListInfo
	 * @Description: 把FileNet CE ChoiceList实例转换为 ChoiceInfo对象实例
	 * @param @param choiceList
	 * @param @return
	 * @param @throws Exception    设定文件
	 * @return List<ChoiceInfo>    返回类型
	 */
	public static List<ChoiceInfo> convertChoiceList2ChoiceListInfo(ChoiceList choiceList) throws Exception {
		try {
			List<ChoiceInfo> choiceInfoList = new ArrayList<ChoiceInfo>();
			if (choiceList != null) {
				Iterator iterator = choiceList.iterator();
				while (iterator.hasNext()) {
					Choice choice = (Choice) iterator.next();
					ChoiceInfo choiceInfo = new ChoiceInfo();
					choiceInfo.setDisplayName(choice.get_DisplayName());
					choiceInfo.setValue(choice.get_ChoiceStringValue());
					choiceInfoList.add(choiceInfo);
				}
			}
			return choiceInfoList;
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	/**
	 * @Title: convertChoiceList2Map
	 * @Description: FileNet CE ChoiceList Convert into Map
	 * @param @param choiceList
	 * @param @return
	 * @param @throws Exception    设定文件
	 * @return Map<String, Object>    返回类型
	 */
	public static Map<String, Object> convertChoiceList2Map(ChoiceList choiceList) throws Exception {
		try {
			Map<String, Object> map = new HashMap<String, Object>();
			List<ChoiceInfo> choiceInfoList = new ArrayList<ChoiceInfo>();
			if (choiceList != null) {
				Iterator iterator = choiceList.iterator();
				while (iterator.hasNext()) {
					Choice choice = (Choice) iterator.next();
					map.put(choice.get_DisplayName(), choice.get_DisplayName() + "=" + choice.get_ChoiceStringValue());
				}
			}
			return map;
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

}
