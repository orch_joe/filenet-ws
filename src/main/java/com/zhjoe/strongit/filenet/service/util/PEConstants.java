/**
 * @Title: PEVWSessionUtil.java
 * @Package com.strongit.filenet.service.pe
 * @Description: FileNet PE Constants
 * @author Changling Jiang
 * @date Apr 24, 2012 10:18:33 PM
 * @version V1.0
 */
package com.zhjoe.strongit.filenet.service.util;

/**
 * @ClassName: PEConstants
 * @Description: TODO(这里用一句话描述这个类的作用)
 * @author Changling Jiang
 * @date 2013年11月27日 上午11:16:03
 *
 */
public class PEConstants {

	/**
	 * Work queue "Conductor"
	 */
	public static final String WORK_QUEUE_CONDUCTOR_NAME = "Conductor";

	/**
	 * User queue "Inbox"
	 */
	public static final String USER_QUEUE_INBOX_NAME = "Inbox(0)";

	/**
	 * User queue "Tracker"
	 */
	public static final String USER_QUEUE_TRACKER_NAME = "Tracker(0)";

	/**
	 * Default query buffer size.
	 */
	public static final int DEFAULT_QUERY_BUFFER_SIZE = 50;

	/**
	 * F_Comment
	 */
	public static final String SYSTEM_FIELD_COMMENT = "F_Comment";

	/**
	 * F_BoundUser
	 */
	public static final String SYSTEM_FIELD_BOUND_USER = "F_BoundUser";

	/**
	 * F_BoundUserId
	 */
	public static final String SYSTEM_FIELD_BOUND_USER_ID = "F_BoundUserId";

	public static final String SYSTEM_FIELD_USER_ID = "F_UserId";

	/**
	 * F_Locked
	 */
	public static final String SYSTEM_FIELD_LOCKED = "F_Locked";
	/**
	 * F_Originator
	 */
	public static final String SYSTEM_FIELD_ORIGINATOR = "F_Originator";
	/**
	 * F_StepName
	 */
	public static final String SYSTEM_FIELD_STEP_NAME = "F_StepName";
	/**
	 * F_Subject
	 */
	public static final String SYSTEM_FIELD_SUBJECT = "F_Subject";
	/**
	 * F_WobNum
	 */
	public static final String SYSTEM_FIELD_WOBNUM = "F_WobNum";
	/**
	 * F_Class
	 */
	public static final String SYSTEM_FIELD_WORK_CLASS_NAME = "F_Class";
	/**
	 * F_WorkClassId
	 */
	public static final String SYSTEM_FIELD_WORK_CLASS_ID = "F_WorkClassId";
	/**
	 * F_StartTime
	 */
	public static final String SYSTEM_FIELD_START_TIME = "F_StartTime";
	/**
	 * F_EnqueueTime
	 */
	public static final String SYSTEM_FIELD_RECIVE_TIME = "F_EnqueueTime";
	/**
	 * F_Response
	 */
	public static final String SYSTEM_FIELD_RESPONSE = "F_Response";
	/**
	 * F_WorkFlowNumber
	 */
	public static final String SYSTEM_FIELD_WORK_FLOW_NUMBER = "F_WorkFlowNumber";
	/**
	 * F_EventType
	 */
	public static final String SYSTEM_FIELD_EVENT_TYPE = "F_EventType";

	/**
	 * Event log related.
	 */
	public static final String EVENT_TYPE_WF_TERMINATION_MSG = "165";
	/**
	 * Event log related.
	 */
	public static final String EVENT_TYPE_BEGIN_SERVICE_NORMAL = "350";
	/**
	 * Event log related.
	 */
	public static final String EVENT_TYPE_WORKOBJECT_QUEUED = "352";
	/**
	 * Event log related.
	 */
	public static final String EVENT_TYPE_END_SERVICE_NORMAL = "360";

	/**
	 * Process Engine database related.
	 */
	public static final String PE_DB_SCHEMA = "f_sw";
	/**
	 * Process Engine database related.
	 */
	public static final String PE_DB_LOG_VIEW_PREFIX = "VWVL1_";
	/**
	 * Process Engine database related.
	 */
	public static final String PE_DB_QUEUE_VIEW_PREFIX = "VWVQ1_";
	/**
	 * Process Engine database related.
	 */
	public static final String PE_DB_ROSTER_VIEW_PREFIX = "VWVR1_";

	public static final String PendingAction = "待处理";

	private static final String[] action = {"提交", "办结", "驳回", "确认", "重新提交", "处理", "操作", "开通权限", "同意", "终止", "管理员驳回", "取消", "结束", "完成", "不同意", "商务受理人同意", "商务受理人不同意"};
	public static final String[] workflowStatus = {"submit", "Processing", "Completed", "Cancel", "reject", "Business", "Draft"};
	public static final String PURCHASE_WORKFLOW_STATUS_UNFINISHED = "0";
	public static final String PURCHASE_WORKFLOW_STATUS_FINISHED = "1";

	public static String getSubmit() {
		return action[0];
	}

	public static String getApproval() {
		return action[1];
	}

	public static String getReject() {
		return action[2];
	}

	public static String getConfirm() {
		return action[3];
	}

	public static String getReSubmit() {
		return action[4];
	}

	public static String getProcess() {
		return action[5];
	}

	public static String getOperation() {
		return action[6];
	}

	public static String getPermissionOpened() {
		return action[7];
	}

	public static String getAgree() {
		return action[8];
	}

	public static String getAbort() {
		return action[9];
	}

	public static String getManagerReject() {
		return action[10];
	}

	public static String getCancel() {
		return action[11];
	}

	public static String getStop() {
		return action[12];
	}

	public static String getCompleted() {
		return action[13];
	}

	public static String getDisagree() {
		return action[14];
	}

	public static String getBusinessAgree() {
		return action[15];
	}

	public static String getBusinessDisagree() {
		return action[16];
	}

	public static String getPageByWorkFlowType(String workFlowType, String openType) {
		//如果是采购订单流程(PO)
		if (FileNetConfig.getValue("purchaseWorkflowName").equals(workFlowType)) {
			if ("myTask".equals(openType)) { //从代办事宜打开
				return "/ccp/pe/purchase/po/purchaseAudit.jsp";
			} else if ("rejectTask".equals(openType)) {//从已退回打开
				return "/ccp/pe/purchase/po/purchaseReSubmit.jsp";
			} else if ("doingTask".equals(openType) || "appliedTask".equals(openType)) { //我的在办和已办
				return "/ccp/pe/purchase/po/purchaseReadOnly.jsp";
			} else if ("historyTask".equals(openType)) {
				return "/ccp/pe/purchase/po/purchaseHistory.jsp";
			}
		} else if (FileNetConfig.getValue("purchaseWorkflowNamePR").equals(workFlowType)) {//采购申请审批流程(PR)
			if ("myTask".equals(openType)) { //从代办事宜打开
				return "/ccp/pe/purchase/pr/purchaseAuditPR.jsp";
			} else if ("rejectTask".equals(openType)) {//从已退回打开
				return "/ccp/pe/purchase/pr/purchaseReSubmitPR.jsp";
			} else if ("doingTask".equals(openType) || "appliedTask".equals(openType)) { //我的在办和已办
				return "/ccp/pe/purchase/pr/purchaseReadOnlyPR.jsp";
			} else if ("historyTask".equals(openType)) {//已结束
				return "/ccp/pe/purchase/pr/purchaseHistoryPR.jsp";
			} else if ("myAccceptance".equals(openType)) {
				return "/ccp/pe/purchase/pr/purchaseBusinessPersions.jsp";
			}
		} else if (FileNetConfig.getValue("seaReturnWorkflowName").equals(workFlowType)) { //海返流程
			if ("myTask".equals(openType)) { //从代办事宜打开
				return "/ccp/pe/seaReturn/seareturnaudit.jsp";
			} else if ("doingTask".equals(openType) || "appliedTask".equals(openType)) {
				return "/ccp/pe/seaReturn/seareturnreadonly.jsp";
			} else if ("historyTask".equals(openType)) {//已结束
				return "/ccp/pe/seaReturn/seareturnreadonly.jsp";
			} else if ("rejectTask".equals(openType)) {
				return "/ccp/pe/seaReturn/seareturnresubmit.jsp";
			}
		} else if (FileNetConfig.getValue("purchaseWorkflowNameKC").equals(workFlowType)) {
			if ("myTask".equals(openType)) { //从代办事宜打开
				return "/ccp/pe/kc/purchaseAuditKC.jsp";
			} else if ("rejectTask".equals(openType)) {//从已退回打开
				return "/ccp/pe/kc/purchaseReSubmitKC.jsp";
			} else if ("doingTask".equals(openType) || "appliedTask".equals(openType)) { //我的在办和已办
				return "/ccp/pe/kc/purchaseReadOnlyKC.jsp";
			} else if ("historyTask".equals(openType)) {//已结束
				return "/ccp/pe/kc/purchaseHistoryKC.jsp";
			}
		}
		return null;
	}
}
