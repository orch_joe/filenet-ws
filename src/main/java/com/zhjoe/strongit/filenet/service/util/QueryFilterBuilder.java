/**
 * @Title: QueryFilterBuilder.java
 * @Package com.strongit.filenet.service.util
 * @Description: FileNet CE Query 工具类
 * @author Changling Jiang
 * @date 2013年11月11日 下午12:40:51
 * @version V1.0
 */

package com.zhjoe.strongit.filenet.service.util;

import org.apache.log4j.Logger;

/**
 * @ClassName: QueryFilterBuilder
 * @Description: TODO(这里用一句话描述这个类的作用)
 * @author Changling Jiang
 * @date 2013年11月27日 上午11:15:47
 *
 */
public class QueryFilterBuilder {

	private static final Logger log = Logger.getLogger(QueryFilterBuilder.class);

	private QueryFilterBuilder() {
	}

	private static final String OP_EQUALS = "=";
	private static final String VAR_PREFIX = ":";
	private static final String OP_AND = " AND ";


	/**
	 * buildEqualsFilter:(这里用一句话描述这个方法的作用)
	 * @author cljiang
	 * @param dataFieldNames
	 * @return
	 * String
	 * @date 2012-6-11	上午11:05:58
	 * @version 　Ver 1.0
	 */
	public static String buildEqualsFilter(String... dataFieldNames) {

		StringBuffer filter = new StringBuffer();

		for (int i = 0; i < dataFieldNames.length; i++) {
			String field = dataFieldNames[i];
			filter.append(field + OP_EQUALS + VAR_PREFIX + field);
			if (i < dataFieldNames.length - 1)
				filter.append(OP_AND);
		}

		return filter.toString();
	}
}
