/**
 * @Title: CEObjectStoreUtil.java
 * @Package com.strongit.filenet.service.util
 * @Description: FileNet CE ObjectStore 工具类
 * @author Changling Jiang
 * @date 2013年11月11日 下午12:41:45
 * @version V1.0
 */
package com.zhjoe.strongit.filenet.service.util;


import javax.security.auth.Subject;

import com.filenet.api.core.Connection;
import com.filenet.api.core.Domain;
import com.filenet.api.core.Factory;
import com.filenet.api.core.ObjectStore;
import com.filenet.api.util.UserContext;
import com.zhjoe.strongit.filenet.service.ce.CEObjectStore;

import static com.zhjoe.strongit.filenet.service.util.FileNetConfig.*;

/**
 * @ClassName: CEObjectStoreUtil
 * @Description: FileNet CE ObjectStore 工具类
 * @author Changling Jiang
 * @date 2013年11月11日 下午12:41:45
 *
 */
public class CEObjectStoreUtil {

	/**
	 *
	 * @Title: getCEObjectStore
	 * @Description: 获得 FileNet CEObjectStore 对象
	 * @param @param user		用户名
	 * @param @param password	密码
	 * @param @param fetch
	 * @param @return
	 * @return CEObjectStore
	 */
	public static CEObjectStore getCEObjectStore(String user, String password, boolean fetch) {

		FileNetConfigUtil.config();

		CEObjectStore ceos = new CEObjectStore();

		Connection con = CEConnectionUtil.getConnection();

		ceos.setCon(con);

		Subject subject = UserContext.createSubject(con, user, password, DEFAULT_JAAS_STANZA);

		ceos.setSubject(subject);

		ceos.getUc().pushSubject(subject);

		Domain domain = Factory.Domain.fetchInstance(con, null, null);

		ceos.setDomain(domain);

		String domainName = domain.get_Name();

		ceos.setDomainName(domainName);

		ceos.setOset(domain.get_ObjectStores());


		ObjectStore os = null;

		if (fetch) {

			os = Factory.ObjectStore.fetchInstance(domain, OBJECT_STORE_NAME, null);

		} else {

			os = Factory.ObjectStore.getInstance(domain, OBJECT_STORE_NAME);

		}

		ceos.setOs(os);

		ceos.setConnected(true);

		return ceos;

	}

	/**
	 *
	 * @Title: getDefaultCEObjectStore
	 * @Description: 获得默认的 FileNet CEObjectStore 对象
	 * @param @return
	 * @return CEObjectStore
	 */
	public static CEObjectStore getDefaultCEObjectStore() {
		return getCEObjectStore(ADMIN_USER, ADMIN_PASSWORD, true);
	}
}
