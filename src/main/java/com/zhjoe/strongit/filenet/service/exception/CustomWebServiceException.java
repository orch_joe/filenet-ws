/**
 * @Title: CustomWebServiceException.java
 * @Package com.ccpws.exception
 * @Description: TODO(用一句话描述该文件做什么)
 * @author Changling Jiang
 * @date 2014-2-14 下午03:35:11
 * @version V1.0
 */
package com.zhjoe.strongit.filenet.service.exception;

import javax.xml.ws.WebServiceException;

/**
 * @ClassName: CustomWebServiceException
 * @Description: TODO(这里用一句话描述这个类的作用)
 * @author Changling Jiang
 * @date 2014-2-14 下午03:35:11
 *
 */
public class CustomWebServiceException extends WebServiceException {

	/**
	 * @Fields serialVersionUID : serialVersionUID
	 */
	private static final long serialVersionUID = 1L;

	private String exceptionCode;

	public CustomWebServiceException() {
		super();
	}

	public CustomWebServiceException(String message, Throwable cause) {
		super(message, cause);
	}

	public CustomWebServiceException(String message) {
		super(message);
	}

	public CustomWebServiceException(Throwable cause) {
		super(cause);
	}

	/**
	 * <p>Title: </p>
	 * <p>Description: </p>
	 * @param exceptionCode
	 */
	public CustomWebServiceException(String exceptionCode, String message) {
		super(message);
		this.exceptionCode = exceptionCode;
	}

	/**
	 * @return exceptionCode
	 */
	public String getExceptionCode() {
		return exceptionCode;
	}

	/**
	 * @param exceptionCode exceptionCode to set
	 */
	public void setExceptionCode(String exceptionCode) {
		this.exceptionCode = exceptionCode;
	}
}
