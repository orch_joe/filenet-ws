/**
 * @Title: FileNetConfigUtil.java
 * @Package com.strongit.filenet.service.util
 * @Description: FileNet 环境配置工具
 * @author Changling Jiang
 * @date 2013年11月11日 下午12:42:47
 * @version V1.0
 */
package com.zhjoe.strongit.filenet.service.util;


import java.io.File;
import java.net.URL;

import com.zhjoe.strongit.filenet.service.exception.ConfigException;
import org.apache.log4j.Logger;

/**
 * @ClassName: FileNetConfigUtil
 * @Description: FileNet 环境配置工具
 * @author Changling Jiang
 * @date 2013年11月11日 下午12:42:47
 *
 */
public class FileNetConfigUtil {

	private static final Logger logger = Logger.getLogger(FileNetConfigUtil.class);

	public static void config() throws ConfigException {
		try {
			logger.info("设置filenet webservice环境变量");
			String waspLocation = FileNetConfig.WASP_LOCATION;
			if ((waspLocation != null) && (!waspLocation.equals(""))) {
				URL waspLocationURL = FileNetConfigUtil.class.getResource(waspLocation);
				System.setProperty("wasp.location", waspLocationURL.toString());
			}
			String jaasConfigFile = FileNetConfig.JAAS_CONFIG_FILE;
			URL jaasConfigURL = FileNetConfigUtil.class.getResource(jaasConfigFile);
			String jaasConfigFilePath = jaasConfigURL.getPath();

			System.setProperty("java.security.auth.login.config", jaasConfigFilePath);

			String ceUrl = FileNetConfig.CONTENT_ENGINE_URL;
			System.setProperty("filenet.pe.bootstrap.ceuri", ceUrl);

			logger.debug("java.security.auth.login.config = " + System.getProperty("java.security.auth.login.config"));
			logger.debug("wasp.location = " + System.getProperty("wasp.location"));
			logger.debug("filenet.pe.bootstrap.ceuri = " + System.getProperty("filenet.pe.bootstrap.ceuri"));

		} catch (Throwable t) {
			logger.error(t.getMessage(), t);
			throw new ConfigException(t);
		}
	}
	public static void config(String ceUrl) throws ConfigException {
		try {
			logger.info("设置filenet webservice环境变量");
			String waspLocation = FileNetConfig.WASP_LOCATION;
			if ((waspLocation != null) && (!waspLocation.equals(""))) {
				URL waspLocationURL = FileNetConfigUtil.class.getResource(waspLocation);
				System.setProperty("wasp.location", waspLocationURL.toString());
			}
			String jaasConfigFile = FileNetConfig.JAAS_CONFIG_FILE;
			URL jaasConfigURL = FileNetConfigUtil.class.getResource(jaasConfigFile);
			String jaasConfigFilePath = jaasConfigURL.getPath();

			System.setProperty("java.security.auth.login.config", jaasConfigFilePath);

			System.setProperty("filenet.pe.bootstrap.ceuri", ceUrl);

			logger.debug("java.security.auth.login.config = " + System.getProperty("java.security.auth.login.config"));
			logger.debug("wasp.location = " + System.getProperty("wasp.location"));
			logger.debug("filenet.pe.bootstrap.ceuri = " + System.getProperty("filenet.pe.bootstrap.ceuri"));

		} catch (Throwable t) {
			logger.error(t.getMessage(), t);
			throw new ConfigException(t);
		}
	}

	private static String toFilePath(URL url) {
		if ((url == null) || (!url.getProtocol().equals("file"))) {
			return null;
		}
		String filename = url.getFile().replace('/', File.separatorChar);
		int position = 0;
		while ((position = filename.indexOf('%', position)) >= 0) {
			if (position + 2 < filename.length()) {
				String hexStr = filename.substring(position + 1, position + 3);
				char ch = (char) Integer.parseInt(hexStr, 16);
				filename = filename.substring(0, position) + ch + filename.substring(position + 3);
			}
		}
		return new File(filename).getPath();
	}
}
