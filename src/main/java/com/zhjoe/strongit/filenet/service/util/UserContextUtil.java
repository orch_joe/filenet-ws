/**
 * @Title: UserContextUtil.java
 * @Package com.strongit.filenet.service.util
 * @Description: TODO(用一句话描述该文件做什么)
 * @author Changling Jiang
 * @date 2014-2-14 上午11:26:20
 * @version V1.0
 */
package com.zhjoe.strongit.filenet.service.util;

import com.filenet.api.core.Connection;
import com.filenet.api.util.UserContext;

import javax.security.auth.Subject;

/**
 * @ClassName: UserContextUtil
 * @Description: TODO(这里用一句话描述这个类的作用)
 * @author Changling Jiang
 * @date 2014-2-14 上午11:26:20
 *
 */
public class UserContextUtil {

	public UserContextUtil() {
	}

	public static Subject authenticate(Connection connection, String userName, String password, String stanzaName) {
		Subject subject = UserContext.createSubject(connection, userName, password, stanzaName);
		return subject;
	}

	public static void pushSubject(Subject subject) {
		UserContext uc = UserContext.get();
		uc.pushSubject(subject);
	}

	public static Subject popSubject() {
		UserContext uc = UserContext.get();
		return uc.popSubject();
	}
}
