/**
 * @Title: ComparatorSecurity.java
 * @Package com.strongit.filenet.service.comparator
 * @Description: 表格排序类（文件夹）
 * @author Ke Cui
 * @date 2014年4月22日 上午11:06:06
 * @version V1.0
 */
package com.zhjoe.strongit.filenet.service.comparator;

import com.zhjoe.strongit.filenet.service.datatype.Grantee;

import java.util.Comparator;


/**
 * @ClassName: ComparatorSecurity
 * @Description: 对表格进行排序（文件夹）
 * @author Ke Cui
 * @date 2014年4月22日 上午11:06:06
 *
 */
public class ComparatorSecurity implements Comparator {

	protected String property;// 排序字段
	protected String order;// 升序||降序
	protected int type;// 字段类型(现只提供 字符串:1)

	public int compare(Object o1, Object o2) {

		Grantee grantee1 = (Grantee) o1;
		Grantee grantee2 = (Grantee) o2;

		if (type == 1) {
			String property1 = grantee1.getShortName();
			String property2 = grantee2.getShortName();
			if (property1 == "" || property1 == null) {
				return 1;
			} else if (property2 == "" || property2 == null) {
				return -1;
			}
			if ("desc".equals(order)) {
				return property2.compareTo(property1);
			} else {
				return property1.compareTo(property2);
			}
		} else {
			return 0;
		}
	}

	/**
	 * @return property
	 */
	public String getProperty() {
		return property;
	}

	/**
	 * @param property
	 *           property to set
	 */
	public void setProperty(String property) {
		this.property = property;
	}

	/**
	 * @return order
	 */
	public String getOrder() {
		return order;
	}

	/**
	 * @param order
	 *           order to set
	 */
	public void setOrder(String order) {
		this.order = order;
	}

	/**
	 * @return type
	 */
	public int getType() {
		return type;
	}

	/**
	 * @param type
	 *           type to set
	 */
	public void setType(int type) {
		this.type = type;
	}

}
