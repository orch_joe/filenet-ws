package com.zhjoe.strongit.filenet.service.datatype;

import com.google.common.collect.TreeMultimap;

/**
 * @author QiaoYu
 */
public class GlobelClassDefine {
    public static GlobelClassDefine globelClassDefine = null;

    public GlobelClassDefine() {

    }

    public static GlobelClassDefine getInstance() {
        if (globelClassDefine == null) {
            globelClassDefine = new GlobelClassDefine();
        }
        return globelClassDefine;
    }

    private TreeMultimap<String, String> treeMap = null;

    /**
     * @return 返回 treeMap
     */
    public TreeMultimap<String, String> getTreeMap() {
        return treeMap;
    }

    /**
     * @setParam 设置treeMap
     */
    public void setTreeMap(TreeMultimap<String, String> treeMap) {
        this.treeMap = treeMap;
    }


}

