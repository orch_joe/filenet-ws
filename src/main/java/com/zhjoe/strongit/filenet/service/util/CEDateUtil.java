/**
 * @Title: CEDateUtil.java
 * @Package com.strongit.filenet.service.util
 * @Description: TODO(用一句话描述该文件做什么)
 * @author Changling Jiang
 * @date 2014-1-21 下午01:34:51
 * @version V1.0
 */
package com.zhjoe.strongit.filenet.service.util;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

/**
 * @ClassName: CEDateUtil
 * @Description: TODO(这里用一句话描述这个类的作用)
 * @author Changling Jiang
 * @date 2014-1-21 下午01:34:51
 *
 */
public class CEDateUtil {

	/**
	 * CE SQL 时间为条件查询时 格式化时间使用
	 */
	public static DateFormat sdf = new SimpleDateFormat("yyyyMMdd'T'HHmmss'Z'");
	public static DateFormat sdf2 = new SimpleDateFormat("yyyyMMdd");
	public static Calendar calendar = Calendar.getInstance();

	/**
	 * @Title: convertDate2String
	 * @Description: CE SQL 时间为条件查询时调用（例：DateCreated >= 20121010T101010Z 不加引号 注：未考虑时区进去，若中国需减去8个小时，值20121010T021010Z）
	 * @param @param date
	 * @param @return    设定文件
	 * @return String    返回类型
	 */
	public static String convertDate2String(Date date) {
		if (date != null) {
			return sdf.format(date);
		}
		return null;
	}

	/**
	 * @Title: calculateDateFrom
	 * @Description: CE SQL 开始日期20121010为条件查询时调用（例：DateCreated >= 20121010T000000Z 不加引号 注：考虑时区进去，中国减去8个小时，值20121009T160000Z）
	 * @param @param date
	 * @param @return
	 * @param @throws ParseException    设定文件
	 * @return String    返回类型
	 */
	public static String calculateDateFrom(Date date) throws ParseException {
		try {
			if (date != null) {
				String format = sdf2.format(date);
				date = sdf2.parse(format);
				calendar.setTime(date);
				calendar.add(Calendar.DATE, -1);
				calendar.set(Calendar.HOUR, 16);
				calendar.set(Calendar.MINUTE, 0);
				calendar.set(Calendar.SECOND, 0);
				return sdf.format(calendar.getTime());
			}
		} catch (ParseException e) {
			e.printStackTrace();
			throw e;
		}
		return null;
	}

	/**
	 * @Title: calculateDateTo
	 * @Description: CE SQL 结束日期20121010为条件查询时调用（例：DateCreated <= 20121010T000000Z 不加引号 注：考虑时区进去，中国加上16个小时，值20121009T155959Z）
	 * @param @param date
	 * @param @return
	 * @param @throws ParseException    设定文件
	 * @return String    返回类型
	 */
	public static String calculateDateTo(Date date) throws ParseException {
		try {
			if (date != null) {
				String format = sdf2.format(date);
				date = sdf2.parse(format);
				calendar.setTime(date);
				calendar.set(Calendar.HOUR, 15);
				calendar.set(Calendar.MINUTE, 59);
				calendar.set(Calendar.SECOND, 59);
				return sdf.format(calendar.getTime());
			}
		} catch (ParseException e) {
			e.printStackTrace();
			throw e;
		}
		return null;
	}

}
