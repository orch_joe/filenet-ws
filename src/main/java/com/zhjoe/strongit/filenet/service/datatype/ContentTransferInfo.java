/**
 * @Title: ContentTransferInfo.java
 * @Package com.strongit.filenet.service.datatype
 * @Description: TODO(用一句话描述该文件做什么)
 * @author Changling Jiang
 * @date 2013年11月13日 下午6:25:55
 * @version V1.0
 */
package com.zhjoe.strongit.filenet.service.datatype;

import java.io.InputStream;

/**
 * @ClassName: ContentTransferInfo
 * @Description: Document文档ContentTransfer信息
 * @author Changling Jiang
 * @date 2013年11月13日 下午6:25:55
 *
 */
public class ContentTransferInfo {

	private InputStream inputStream;
	private String contentType;
	private String retrievalName;

	/**
	 * @return inputStream
	 */
	public InputStream getInputStream() {
		return inputStream;
	}

	/**
	 * @param inputStream inputStream to set
	 */
	public void setInputStream(InputStream inputStream) {
		this.inputStream = inputStream;
	}

	/**
	 * @return contentType
	 */
	public String getContentType() {
		return contentType;
	}

	/**
	 * @param contentType contentType to set
	 */
	public void setContentType(String contentType) {
		this.contentType = contentType;
	}

	/**
	 * @return retrievalName
	 */
	public String getRetrievalName() {
		return retrievalName;
	}

	/**
	 * @param retrievalName retrievalName to set
	 */
	public void setRetrievalName(String retrievalName) {
		this.retrievalName = retrievalName;
	}

}
