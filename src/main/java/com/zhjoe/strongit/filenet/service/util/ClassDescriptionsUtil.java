/**
 * @Title: ClassDescriptionsUtil.java
 * @Package com.strongit.filenet.service.util
 * @Description: TODO(用一句话描述该文件做什么)
 * @author Changling Jiang
 * @date 2014年9月3日 下午5:45:18
 * @version V1.0
 */
package com.zhjoe.strongit.filenet.service.util;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import com.filenet.api.collection.ClassDescriptionSet;
import com.filenet.api.core.Factory;
import com.filenet.api.core.ObjectStore;
import com.filenet.api.meta.ClassDescription;

/**
 * @ClassName: ClassDescriptionsUtil
 * @Description: TODO(这里用一句话描述这个类的作用)
 * @author Changling Jiang
 * @date 2014年9月3日 下午5:45:18
 *
 */
public class ClassDescriptionsUtil {

	/**
	 * @Title: getImmediateSubclassDescriptions
	 * @Description: 获取文档下的所有直接子类
	 * @param @param os
	 * @param @param symbolicName
	 * @param @return    设定文件
	 * @return List<ClassDescription>    返回类型
	 */
	public static List<ClassDescription> getImmediateSubclassDescriptions(ObjectStore os, String symbolicName) {
		List<ClassDescription> list = new ArrayList<ClassDescription>();
		ClassDescription document = Factory.ClassDescription.fetchInstance(os, symbolicName, null);
		ClassDescriptionSet immediateSubclassDescriptions = document.get_ImmediateSubclassDescriptions();
		Iterator iterator = immediateSubclassDescriptions.iterator();
		while (iterator.hasNext()) {
			ClassDescription classDescription = (ClassDescription) iterator.next();
			//cd.get_DisplayName()
			//cd.get_SymbolicName()
			//cd.get_AllowsInstances()
			if (!classDescription.get_IsHidden()) {
				list.add(classDescription);
			}
		}
		return list;
	}

}
