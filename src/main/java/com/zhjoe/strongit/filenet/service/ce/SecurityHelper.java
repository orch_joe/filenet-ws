/**
 * @Title: SecurityHelper.java
 * @Package com.strongit.filenet.service.ce
 * @Description: FileNet CE 对象权限操作类
 * @author Changling Jiang
 * @date 2013年11月11日 上午11:38:44
 * @version V1.0
 */
package com.zhjoe.strongit.filenet.service.ce;

import java.util.*;

import com.zhjoe.strongit.filenet.service.comparator.ComparatorSecurity;
import com.zhjoe.strongit.filenet.service.datatype.Grantee;
import com.zhjoe.strongit.filenet.service.util.CEObjectCollectionUtil;
import org.apache.log4j.Logger;

import com.filenet.api.collection.AccessPermissionList;
import com.filenet.api.collection.GroupSet;
import com.filenet.api.collection.UserSet;
import com.filenet.api.constants.AccessType;
import com.filenet.api.constants.PrincipalSearchAttribute;
import com.filenet.api.constants.PrincipalSearchSortType;
import com.filenet.api.constants.PrincipalSearchType;
import com.filenet.api.constants.PropertyNames;
import com.filenet.api.constants.RefreshMode;
import com.filenet.api.core.Document;
import com.filenet.api.core.EntireNetwork;
import com.filenet.api.core.Factory;
import com.filenet.api.core.Folder;
import com.filenet.api.core.ObjectStore;
import com.filenet.api.property.PropertyFilter;
import com.filenet.api.security.AccessPermission;
import com.filenet.api.security.Group;
import com.filenet.api.security.Realm;
import com.filenet.api.security.User;

/**
 * @ClassName: SecurityHelper
 * @Description: FileNet CE 对象权限操作类
 * @author Changling Jiang
 * @date 2013年11月11日 上午11:38:44
 *
 */
public class SecurityHelper {

	private static Logger log = Logger.getLogger(SecurityHelper.class);

	/**
	 * @Title: getGrantees
	 * @Description: 获得授权对象信息
	 * @param @param os
	 * @param @param pattern
	 * @param @return
	 * @throws Exception
	 * @return List<Grantee>
	 */
	public static List<Grantee> getGrantees(ObjectStore os, String pattern) throws Exception {
		try {
			List<Grantee> list = new ArrayList<Grantee>();

			PropertyFilter propertyFilter = new PropertyFilter();
			propertyFilter.addIncludeProperty(0, null, null, PropertyNames.MY_REALM);
			EntireNetwork entireNetwork = Factory.EntireNetwork.fetchInstance(os.getConnection(), propertyFilter);
			Realm realm = entireNetwork.get_MyRealm();
			GroupSet groups = realm.findGroups(pattern, PrincipalSearchType.PREFIX_MATCH, PrincipalSearchAttribute.SHORT_NAME, PrincipalSearchSortType.NONE, Integer.getInteger("50"), null);
			if (groups != null && !groups.isEmpty()) {
				Iterator it = groups.iterator();
				Group group;
				while (it.hasNext()) {
					group = (Group) it.next();
					Grantee grantee = new Grantee();
					grantee.setName(group.get_Name());
					grantee.setShortName(group.get_ShortName());
					grantee.setDisplayName(group.get_DisplayName());
					grantee.setDistinguishedName(group.get_DistinguishedName());
					grantee.setGranteeType("GROUP");
					list.add(grantee);
				}
			}

			UserSet users = realm.findUsers(pattern, PrincipalSearchType.PREFIX_MATCH, PrincipalSearchAttribute.SHORT_NAME, PrincipalSearchSortType.NONE, Integer.getInteger("50"), null);
			if (users != null && !users.isEmpty()) {
				Iterator it = users.iterator();
				User user;
				while (it.hasNext()) {
					user = (User) it.next();
					Grantee grantee = new Grantee();
					grantee.setName(user.get_Name());
					grantee.setShortName(user.get_ShortName());
					grantee.setDisplayName(user.get_DisplayName());
					grantee.setDistinguishedName(user.get_DistinguishedName());
					grantee.setGranteeType("USER");
					list.add(grantee);
				}
			}
			return list;
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	/**
	 * @Title: getDocumentPermissions
	 * @Description: 获得CE Document对象权限用户
	 * @param @param document
	 * @param @return    设定文件
	 * @throws Exception
	 * @return List<Grantee>    返回类型
	 */
	public static List<Grantee> getDocumentPermissions(Document document, ComparatorSecurity security) throws Exception {
		try {
			List<Grantee> list = new ArrayList<Grantee>();
			if (document != null) {
				AccessPermissionList permissions = document.get_Permissions();
				if (permissions != null && !permissions.isEmpty()) {
					Iterator it = permissions.iterator();
					while (it.hasNext()) {
						AccessPermission permission = (AccessPermission) it.next();
						Grantee grantee = new Grantee();
						grantee.setDistinguishedName(permission.get_GranteeName());
						grantee.setGranteeType(permission.get_GranteeType().toString());
						if ("USER".equalsIgnoreCase(grantee.getGranteeType())) {
							User user = Factory.User.fetchInstance(document.getConnection(), grantee.getDistinguishedName(), null);
							grantee.setName(user.get_Name());
							grantee.setShortName(user.get_ShortName());
							grantee.setDisplayName(user.get_DisplayName());
						} else if ("GROUP".equalsIgnoreCase(grantee.getGranteeType())) {
							if (!"#AUTHENTICATED-USERS".equalsIgnoreCase(grantee.getDistinguishedName())) {
								Group group = Factory.Group.fetchInstance(document.getConnection(), grantee.getDistinguishedName(), null);
								grantee.setName(group.get_Name());
								grantee.setShortName(group.get_ShortName());
								grantee.setDisplayName(group.get_DisplayName());
							} else {
								grantee.setName("#AUTHENTICATED-USERS");
								grantee.setShortName("#AUTHENTICATED-USERS");
								grantee.setDisplayName("#AUTHENTICATED-USERS");
							}
						}
						grantee.setAccessType(permission.get_AccessType().toString());
						grantee.setAccessMask(permission.get_AccessMask());
						grantee.setInheritableDepth(permission.get_InheritableDepth());
						grantee.setPermissionSource(permission.get_PermissionSource().getValue());
						//log.debug(grantee);
						list.add(grantee);
					}
				}
			}
			if (security != null) {
				Collections.sort(list, security);
			}
			return list;
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}


	/**
	 * @Title: getFolderPermissions
	 * @Description: 获得CE Folder对象权限用户
	 * @param @param folder
	 * @param @return    设定文件
	 * @throws Exception
	 * @return List<Grantee>    返回类型
	 */
	public static List<Grantee> getFolderPermissions(Folder folder, ComparatorSecurity security) throws Exception {
		try {
			List<Grantee> list = new ArrayList<Grantee>();
			if (folder != null) {
				AccessPermissionList permissions = folder.get_Permissions();
				if (permissions != null && !permissions.isEmpty()) {
					Iterator it = permissions.iterator();
					while (it.hasNext()) {
						AccessPermission permission = (AccessPermission) it.next();
						Grantee grantee = new Grantee();
						grantee.setGranteeType(permission.get_GranteeType().toString());
						if ("USER".equalsIgnoreCase(grantee.getGranteeType())) {
							User user = Factory.User.fetchInstance(folder.getConnection(), permission.get_GranteeName(), null);
							grantee.setName(user.get_Name());
							grantee.setShortName(user.get_ShortName());
							grantee.setDistinguishedName(user.get_DistinguishedName());
							grantee.setDisplayName(user.get_DisplayName());
						} else if ("GROUP".equalsIgnoreCase(grantee.getGranteeType())) {
							if (!"#AUTHENTICATED-USERS".equalsIgnoreCase(permission.get_GranteeName())) {
								Group group = Factory.Group.fetchInstance(folder.getConnection(), permission.get_GranteeName(), null);
								grantee.setName(group.get_Name());
								grantee.setShortName(group.get_ShortName());
								grantee.setDistinguishedName(group.get_DistinguishedName());
								grantee.setDisplayName(group.get_DisplayName());
							} else {
								grantee.setName("#AUTHENTICATED-USERS");
								grantee.setShortName("#AUTHENTICATED-USERS");
								grantee.setDisplayName("#AUTHENTICATED-USERS");
							}
						}
						grantee.setAccessType(permission.get_AccessType().toString());
						grantee.setAccessMask(permission.get_AccessMask());
						grantee.setInheritableDepth(permission.get_InheritableDepth());
						grantee.setPermissionSource(permission.get_PermissionSource().getValue());
						//log.debug(grantee);
						list.add(grantee);
					}
				}
			}
			if (security != null) {
				Collections.sort(list, security);
			}
			return list;
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	/**
	 * @Title: setFolderPermissions
	 * @Description: 设置 FileNet CE Folder的权限用户
	 * @param @param folder
	 * @param @param granteeList
	 * @param @return    设定文件
	 * @throws Exception
	 * @return boolean    返回类型
	 */
	public static boolean setFolderPermissions(Folder folder, List<Grantee> granteeList) throws Exception {
		try {
			AccessPermissionList permissions = folder.get_Permissions();
			setPermissions(permissions, granteeList);
			folder.set_Permissions(permissions);
			folder.save(RefreshMode.REFRESH);
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	public static boolean addDocumentPermissions(Document document, List<Grantee> granteeList) throws Exception {
		//添加权限
		try {
			//Retrieves this object from the server and replaces existing information in the property cache with the retrieved property information.
			//class com.filenet.api.exception.EngineRuntimeException:The object {A52B7B45-5D5E-4707-8FBE-4A9E20DD15AC}  has been modified since it was retrieved. Update sequence number mismatch; requested USN = 1, database USN = 2.
			document.refresh();
			AccessPermissionList permissions = document.get_Permissions();
			for (int i = 0; i < granteeList.size(); i++) {
				Grantee grantee = granteeList.get(i);
				grantee.setAccessType("ALLOW");
				AccessPermission permission;
				permission = getPermission(grantee);
				permissions.add(permission);
			}
			document.set_Permissions(permissions);
			document.save(RefreshMode.REFRESH);
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	/**
	 * @Title: setDocumentPermissions
	 * @Description: 设置 FileNet CE Document的权限用户
	 * @param @param document
	 * @param @param granteeList
	 * @param @return    设定文件
	 * @throws Exception
	 * @return boolean    返回类型
	 */
	public static boolean setDocumentPermissions(Document document, List<Grantee> granteeList) throws Exception {
		try {
			AccessPermissionList permissions = document.get_Permissions();
			setPermissions(permissions, granteeList);
			document.set_Permissions(permissions);
			document.save(RefreshMode.REFRESH);
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	/**
	 * @Title: setPermissions
	 * @Description: 设置权限集合
	 * @param @param permissions
	 * @param @param granteeList
	 * @param @throws Exception    设定文件
	 * @return void    返回类型
	 */
	public static void setPermissions(AccessPermissionList permissions, List<Grantee> granteeList) throws Exception {
		try {
			Map<String, List<Grantee>> map = convertGranteeList2GranteeMap(granteeList);
			if (permissions != null) {
				//去掉非文件夹继承的权限
				for (int i = permissions.size() - 1; i >= 0; i--) {
					AccessPermission accessPermission = (AccessPermission) permissions.get(i);
					int permissionSource = accessPermission.get_PermissionSource().getValue();
					if (permissionSource == 3) {
						//因从文件夹继承，不做任何处理
					} else {
						String granteeName = accessPermission.get_GranteeName();
						if (map.containsKey(granteeName)) {
							permissions.remove(i);
						}
					}
				}
				//添加权限
				for (int i = 0; i < granteeList.size(); i++) {
					Grantee grantee = granteeList.get(i);
					grantee.setAccessType("ALLOW");
					AccessPermission permission = getPermission(grantee);
					permissions.add(permission);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	/**
	 * @Title: convert2Map
	 * @Description: List<Grantee> 转换成 Map<name, List<Grantee>>
	 * @param @param granteeList
	 * @param @return    设定文件
	 * @throws Exception
	 * @return Map<String, List < Grantee>>    返回类型
	 */
	public static Map<String, List<Grantee>> convertGranteeList2GranteeMap(List<Grantee> granteeList) throws Exception {
		try {
			Map<String, List<Grantee>> map = new HashMap<String, List<Grantee>>();
			if (granteeList != null && !granteeList.isEmpty()) {
				for (int i = 0; i < granteeList.size(); i++) {
					Grantee grantee = granteeList.get(i);
					String name = grantee.getName();
					if (map.containsKey(name)) {
						map.get(name).add(grantee);
					} else {
						List<Grantee> list = new ArrayList<Grantee>();
						list.add(grantee);
						map.put(name, list);
					}
				}
			}
			return map;
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}


	/**
	 * @Title: removeDocumentPermission
	 * @Description: 取消文档用户权限
	 * @param @param document
	 * @param @param grantee
	 * @param @return    设定文件
	 * @throws Exception
	 * @return boolean    返回类型
	 */
	public static boolean removeDocumentPermission(Document document, Grantee grantee) throws Exception {
		try {
			AccessPermissionList permissions = document.get_Permissions();
			AccessPermission removePermission = null;
			if (permissions != null && !permissions.isEmpty()) {
				Iterator it = permissions.iterator();
				while (it.hasNext()) {
					AccessPermission permission = (AccessPermission) it.next();
					if (permission.get_GranteeName() != null && permission.get_PermissionSource().getValue() != 3 && permission.get_GranteeName().indexOf(grantee.getName()) >= 0) {
						removePermission = permission;
						break;
					}
				}
			}
			if (removePermission != null) {
				permissions.remove(removePermission);
				document.set_Permissions(permissions);
				document.save(RefreshMode.REFRESH);
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	/**
	 * @Title: removeFolderPermission
	 * @Description: 取消文件夹用户权限
	 * @param @param folder
	 * @param @param grantee
	 * @param @return    设定文件
	 * @throws Exception
	 * @return boolean    返回类型
	 */
	public static boolean removeFolderPermission(Folder folder, Grantee grantee) throws Exception {
		try {
			AccessPermissionList permissions = folder.get_Permissions();
			AccessPermission removePermission = null;
			if (permissions != null && !permissions.isEmpty()) {
				Iterator it = permissions.iterator();
				while (it.hasNext()) {
					AccessPermission permission = (AccessPermission) it.next();
					if (permission.get_GranteeName() != null && permission.get_PermissionSource().getValue() != 3 && permission.get_GranteeName().indexOf(grantee.getName()) >= 0) {
						removePermission = permission;
						break;
					}
				}
			}
			if (removePermission != null) {
				permissions.remove(removePermission);
				folder.set_Permissions(permissions);
				folder.save(RefreshMode.REFRESH);
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	/**
	 * @Title: getPermission
	 * @Description: 设置AccessPermission实例
	 * @param @param grantee
	 * @param @return
	 * @param @throws Exception    设定文件
	 * @return AccessPermission    返回类型
	 */
	private static AccessPermission getPermission(Grantee grantee) throws Exception {
		try {
			AccessPermission permission = Factory.AccessPermission.createInstance();
			permission.set_GranteeName(grantee.getName());
			if ("ALLOW".equalsIgnoreCase(grantee.getAccessType())) {
				permission.set_AccessType(AccessType.ALLOW);
			} else {
				permission.set_AccessType(AccessType.DENY);
			}
			permission.set_InheritableDepth(grantee.getInheritableDepth());
			permission.set_AccessMask(grantee.getAccessMask());
			return permission;
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	/**
	 * @Title: getMemberOfGroups
	 * @Description: LDAP中用户所属组
	 * @param @param os
	 * @param @return    设定文件
	 * @throws Exception
	 * @return List<String>    返回类型
	 */
	public static List<String> getMemberOfGroups(User user) {
		log.debug("查看用户所属组");
		List<String> list = new ArrayList<String>();
		log.debug(user.get_Name() + " : 所属组");
		GroupSet groups = user.get_MemberOfGroups();
		if (groups != null && !groups.isEmpty()) {
			Iterator iterator = groups.iterator();
			while (iterator.hasNext()) {
				Group group = (Group) iterator.next();
				list.add(group.get_ShortName());
			}
		}
		log.debug("查看用户所属组");
		return list;
	}

	/**
	 * @Title: getUser
	 * @Description: 获得ObjectStore当前用户实例
	 * @param @param os
	 * @param @return
	 * @param @throws Exception    设定文件
	 * @return User    返回类型
	 */
	public static User getUser(ObjectStore os) throws Exception {
		try {
			return Factory.User.fetchCurrent(os.getConnection(), null);
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	/**
	 * @Title: getDisplayName
	 * @Description: 根据ShortName，获得DisplayName（若未查询到返回ShortName）
	 * @param @param os
	 * @param @param name
	 * @param @return    设定文件
	 * @return String    返回类型
	 */
	public static String getDisplayName(ObjectStore os, String name) {
		String displayName = name;
		try {
			User user = Factory.User.fetchInstance(os.getConnection(), name, null);
			displayName = user.get_DisplayName();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return displayName;
	}

	/**
	 * @Title: findUser
	 * @Description: 获得指定用户名称（shortname）
	 * @param @param os
	 * @param @param name
	 * @param @return
	 * @param @throws Exception    设定文件
	 * @return User    返回类型
	 */
	public static User findUser(ObjectStore os, String name) throws Exception {
		try {
			User user = Factory.User.fetchInstance(os.getConnection(), name, null);
			return user;
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	/**
	 * @Title: findUser
	 * @Description: 获得指定用户名称（shortname）
	 * @param @param os
	 * @param @param name
	 * @param @return
	 * @param @throws Exception    设定文件
	 * @return User    返回类型
	 */
	public static Group findGroup(ObjectStore os, String groupName) throws Exception {
		try {
			Group group = Factory.Group.fetchInstance(os.getConnection(), groupName, null);
			return group;
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	/**
	 * @Title: findUsers
	 * @Description: 通过OS对象查询用户对象列表
	 * @param @param os
	 * @param @param pattern
	 * @param @return    设定文件
	 * @throws Exception
	 * @return List<User>    返回类型
	 */
	public static List<User> findUsers(ObjectStore os, String pattern) throws Exception {
		try {
			List<User> list = new ArrayList<User>();
			UserSet userSet = Factory.Realm.fetchCurrent(os.getConnection(), null).findUsers(pattern, PrincipalSearchType.PREFIX_MATCH, PrincipalSearchAttribute.DISPLAY_NAME, PrincipalSearchSortType.ASCENDING, 0, null);
			if (userSet != null) {
				for (User user : CEObjectCollectionUtil.c(userSet, User.class)) {
					list.add(user);
				}
			}
			return list;
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	/**
	 * @Title: findGroups
	 * @Description: 通过OS对象查询用户组对象列表
	 * @param @param os
	 * @param @param pattern
	 * @param @return
	 * @param @throws Exception    设定文件
	 * @return List<Group>    返回类型
	 */
	public static List<Group> findGroups(ObjectStore os, String pattern) throws Exception {
		try {
			List<Group> list = new ArrayList<Group>();
			GroupSet groupSet = Factory.Realm.fetchCurrent(os.getConnection(), null).findGroups(pattern, PrincipalSearchType.PREFIX_MATCH, PrincipalSearchAttribute.SHORT_NAME, PrincipalSearchSortType.ASCENDING, 0, null);
			if (groupSet != null) {
				for (Group group : CEObjectCollectionUtil.c(groupSet, Group.class)) {
					list.add(group);
				}
			}
			return list;
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	public static int calculateGranteeMask(List<Grantee> grantees, com.zhjoe.ccpws.vo.UserVo userVo) throws Exception{
		int accessMask = 0;
		if(grantees != null && grantees.size() > 0){
			for(int i = 0; i < grantees.size(); i++){
				Grantee grantee = grantees.get(i);
				//如果是用户
				if("USER".equals(grantee.getGranteeType())){
					if(grantee.getShortName().equals(userVo.getShortName()) && grantee.getAccessMask() > accessMask) {//当前登录用户 根据权限标识码，取最大值
						accessMask = grantee.getAccessMask();
					}
				}else if("GROUP".equals(grantee.getGranteeType())){
					if("#AUTHENTICATED-USERS".equals(grantee.getShortName()) && grantee.getAccessMask() > accessMask){//FileNet默认的用户组。非LDAP
						accessMask = grantee.getAccessMask();
					}
					if(userVo.getMemberOfGroups().indexOf(grantee.getShortName()) > -1 && grantee.getAccessMask() > accessMask){//当前登录用户所属组 根据权限标识码，取最大值
						accessMask = grantee.getAccessMask();
					}
				}
			}
		}
		return accessMask;
	}
}
