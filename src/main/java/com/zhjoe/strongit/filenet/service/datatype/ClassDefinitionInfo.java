package com.zhjoe.strongit.filenet.service.datatype;

import com.filenet.api.admin.ClassDefinition;
import com.filenet.api.constants.PropertyNames;
import com.filenet.api.core.ObjectStore;
import com.filenet.api.property.FilterElement;
import com.filenet.api.property.PropertyFilter;
import com.zhjoe.ccpws.vo.TreeNode;

import java.util.List;
import java.util.Objects;

public class ClassDefinitionInfo implements TreeNode {
	private String id;
	private String type;
	private String name;
	private String symbolicName;
	private String description;
	private String parentClassName;
	private String osName;
	private List<ClassDefinitionInfo> childrens;

	public String getOsName() {
		return osName;
	}

	public void setOsName(String osName) {
		this.osName = osName;
	}

	public String getParentClassName() {
		return parentClassName;
	}

	public void setParentClassName(String parentClassName) {
		this.parentClassName = parentClassName;
	}

	public String getId() {
		return id;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getSymbolicName() {
		return symbolicName;
	}

	public void setSymbolicName(String symbolicName) {
		this.symbolicName = symbolicName;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public void fromClassDefinition(ClassDefinition classDefinition) {
		setId(classDefinition.get_Id().toString());
		setName(classDefinition.get_Name());
		setSymbolicName(classDefinition.get_SymbolicName());
		setDescription(classDefinition.get_DescriptiveText());

		ObjectStore os = classDefinition.getObjectStore();
		PropertyFilter pf = new PropertyFilter();
		pf.addIncludeProperty(new FilterElement(0, null, Boolean.TRUE, PropertyNames.SYMBOLIC_NAME, null));
		os.fetchProperties(pf);
		String symbolicName = os.get_SymbolicName();
		setOsName(symbolicName);
	}

	@Override
	public Object id() {
		return this.symbolicName;
	}

	@Override
	public Object parentId() {
		return this.parentClassName;
	}

	@Override
	public boolean root() {
		return Objects.equals(this.symbolicName, "Document");
	}

	@Override
	public List<ClassDefinitionInfo> getChildren() {
		return this.childrens;
	}

	@Override
	public void setChildren(List children) {
		this.childrens = children;
	}
}
