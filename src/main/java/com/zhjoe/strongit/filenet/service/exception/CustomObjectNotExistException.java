/**
 * @Title: CustomObjectNotExistException.java
 * @Package com.strongit.filenet.service.exception
 * @Description: 自定义CustomObjectNotExistException异常
 * @author Changling Jiang
 * @date 2013年11月11日 下午12:37:49
 * @version V1.0
 */
package com.zhjoe.strongit.filenet.service.exception;

/**
 * @ClassName: CustomObjectNotExistException
 * @Description: TODO(这里用一句话描述这个类的作用)
 * @author Changling Jiang
 * @date 2013年11月27日 上午11:08:32
 *
 */
public class CustomObjectNotExistException extends Exception {

	/**
	 * @Fields serialVersionUID : TODO
	 */
	private static final long serialVersionUID = -9147437675796282784L;

	public CustomObjectNotExistException() {
		super();
	}

	public CustomObjectNotExistException(String errorMessage) {
		super(errorMessage);
	}

}
