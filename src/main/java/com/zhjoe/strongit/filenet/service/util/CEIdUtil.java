/**
 * @Title: CEIdUtil.java
 * @Package com.strongit.filenet.service.util
 * @Description: TODO(用一句话描述该文件做什么)
 * @author Changling Jiang
 * @date 2014-1-7 上午10:27:09
 * @version V1.0
 */
package com.zhjoe.strongit.filenet.service.util;

/**
 * @ClassName: CEIdUtil
 * @Description: TODO(这里用一句话描述这个类的作用)
 * @author Changling Jiang
 * @date 2014-1-7 上午10:27:09
 *
 */
public class CEIdUtil {

	/**
	 * @Title: convertApiId2DBId
	 * @Description: 把数据API Id转换成CE数据库 ID
	 * @param @param id
	 * @param @return    设定文件
	 * @return String    返回类型
	 */
	public static String convertApiId2DBId(String id) {
		String result = null;
		if (id != null) {
			id = id.replaceAll("\\W", "");
			result = id.substring(6, 8) + id.substring(4, 6) + id.substring(2, 4) + id.substring(0, 2) + id.substring(10, 12) + id.substring(8, 10) + id.substring(14, 16) + id.substring(12, 14) + id.substring(16, 20) + id.substring(20, 32);
		}
		return result;
	}

	/**
	 * @Title: convertDBId2ApiId
	 * @Description: 把CE数据库Id转换成API ID
	 * @param @param id
	 * @param @return    设定文件
	 * @return String    返回类型
	 */
	public static String convertDBId2ApiId(String id) {
		String result = null;
		if (id != null && !"".equalsIgnoreCase(id.trim())) {
			result = "{" + id.substring(6, 8) + id.substring(4, 6) + id.substring(2, 4) + id.substring(0, 2) + "-" + id.substring(10, 12) + id.substring(8, 10) + "-" + id.substring(14, 16) + id.substring(12, 14) + "-" + id.substring(16, 20) + "-" + id.substring(20, 32) + "}";
		}
		return result;
	}


	public static void main(String[] args) {
		String id = convertApiId2DBId(" {17D00B30-842B-4938-BB8C-4F2B5C7F237B}");
		System.out.println(id);

	}


}
