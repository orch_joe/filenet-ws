/**
 * @Title: ConvertFolder2FolderInfoUtil.java
 * @Package com.strongit.filenet.service.util
 * @Description: CE DocumentClass 和Web DocumentClass工具类
 * @author Changling Jiang
 * @date 2013年11月15日 下午5:06:14
 * @version V1.0
 */
package com.zhjoe.strongit.filenet.service.util;

import com.filenet.api.core.Folder;
import com.zhjoe.strongit.filenet.service.datatype.FolderInfo;

/**
 * @ClassName: ConvertFolder2FolderInfoUtil
 * @Description: CE DocumentClass 和Web DocumentClass工具类
 * @author Changling Jiang
 * @date 2013年11月15日 下午5:06:14
 *
 */
public class ConvertFolder2FolderInfoUtil {
	/**
	 * @Title: convertFolder2FolderInfo
	 * @Description: 把FileNet CE Folder实例转换为 FolderInfo对象实例
	 * @param @param folder
	 * @param @return
	 * @param @throws Exception    设定文件
	 * @return FolderInfo    返回类型
	 */
	public static FolderInfo convertFolder2FolderInfo(Folder folder) throws Exception {
		try {
			FolderInfo folderInfo = null;
			String symbolicName = folder.get_ClassDescription().get_SymbolicName();
			String dynamicClassName = FileNetConfig.getValue(symbolicName + "FolderInfo");
			if (dynamicClassName != null && !"".equals(dynamicClassName)) {
				folderInfo = (FolderInfo) (Class.forName(dynamicClassName).newInstance());
				folderInfo.fromFolder(folder);
			}
			if (folder.get_SubFolders().isEmpty()) {
				folderInfo.setIsChild(FolderInfo.IS_CHILD_T);
			} else {
				folderInfo.setIsChild(FolderInfo.IS_CHILD_F);
			}
			return folderInfo;
		} catch (InstantiationException e) {
			e.printStackTrace();
			throw e;
		} catch (IllegalAccessException e) {
			e.printStackTrace();
			throw e;
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
			throw e;
		}
	}

}
