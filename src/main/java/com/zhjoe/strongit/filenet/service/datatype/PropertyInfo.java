/**
 * @Title: PropertyInfo.java
 * @Package com.strongit.filenet.service.datatype
 * @Description: TODO(用一句话描述该文件做什么)
 * @author Changling Jiang
 * @date 2014年8月22日 下午2:47:24
 * @version V1.0
 */
package com.zhjoe.strongit.filenet.service.datatype;

import java.util.Date;
import java.util.List;

/**
 * @ClassName: PropertyInfo
 * @Description: TODO(这里用一句话描述这个类的作用)
 * @author Changling Jiang
 * @date 2014年8月22日 下午2:47:24
 *
 */
public class PropertyInfo {

	private String className;
	private String type; //folder document
	private String symbolicName;
	private String displayName;
	private String descriptiveText;
	private Integer cardinality;//0单一,2数组
	private Integer dataType;//BINARY 1 ,BOOLEAN 2 ,DATETIME 3 ,FLOAT 4 ,GUID 5 ,INTEGER 6 ,OBJECT 7 ,STRING 8
	private Boolean isValueRequired;
	private Boolean isReadOnly;
	private Boolean requiresUniqueElements;
	private String choiceListName;
	private String choiceListId;
	private Boolean hasHierarchy;

	//String
	private String propertyString;
	private String propertyDefaultString;
	private Integer maximumLengthString;
	private List<String> propertyStrings;

	//DateTime
	private Date propertyDateTime;
	private Date propertyDefaultDateTime;
	private Date propertyMaximumDateTime;
	private Date propertyMinimumDateTime;
	private Date[] propertyDateTimes;

	//Float
	private Double propertyFloat64;
	private Double propertyDefaultFloat64;
	private Double propertyMaximumFloat64;
	private Double propertyMinimumFloat64;
	private List<Double> propertyFloat64s;


	//Boolean
	private Boolean propertyBoolean;
	private Boolean propertyDefaultBoolean;
	private List<Boolean> propertyBooleans;

	//Integer
	private Integer propertyInteger32;
	private Integer propertyDefaultInteger32;
	private Integer propertyMaximumInteger32;
	private Integer propertyMinimumInteger32;
	private List<Integer> propertyInteger32s;

	/**
	 * @return className
	 */
	public String getClassName() {
		return className;
	}

	/**
	 * @param className className to set
	 */
	public void setClassName(String className) {
		this.className = className;
	}

	/**
	 * @return type
	 */
	public String getType() {
		return type;
	}

	/**
	 * @param type type to set
	 */
	public void setType(String type) {
		this.type = type;
	}

	/**
	 * @return symbolicName
	 */
	public String getSymbolicName() {
		return symbolicName;
	}

	/**
	 * @param symbolicName symbolicName to set
	 */
	public void setSymbolicName(String symbolicName) {
		this.symbolicName = symbolicName;
	}

	/**
	 * @return displayName
	 */
	public String getDisplayName() {
		return displayName;
	}

	/**
	 * @param displayName displayName to set
	 */
	public void setDisplayName(String displayName) {
		this.displayName = displayName;
	}

	/**
	 * @return descriptiveText
	 */
	public String getDescriptiveText() {
		return descriptiveText;
	}

	/**
	 * @param descriptiveText descriptiveText to set
	 */
	public void setDescriptiveText(String descriptiveText) {
		this.descriptiveText = descriptiveText;
	}

	/**
	 * @return cardinality
	 */
	public Integer getCardinality() {
		return cardinality;
	}

	/**
	 * @param cardinality cardinality to set
	 */
	public void setCardinality(Integer cardinality) {
		this.cardinality = cardinality;
	}

	/**
	 * @return dataType
	 */
	public Integer getDataType() {
		return dataType;
	}

	/**
	 * @param dataType dataType to set
	 */
	public void setDataType(Integer dataType) {
		this.dataType = dataType;
	}

	/**
	 * @return isValueRequired
	 */
	public Boolean getIsValueRequired() {
		return isValueRequired;
	}

	/**
	 * @param isValueRequired isValueRequired to set
	 */
	public void setIsValueRequired(Boolean isValueRequired) {
		this.isValueRequired = isValueRequired;
	}

	/**
	 * @return isReadOnly
	 */
	public Boolean getIsReadOnly() {
		return isReadOnly;
	}

	/**
	 * @param isReadOnly isReadOnly to set
	 */
	public void setIsReadOnly(Boolean isReadOnly) {
		this.isReadOnly = isReadOnly;
	}

	/**
	 * @return requiresUniqueElements
	 */
	public Boolean getRequiresUniqueElements() {
		return requiresUniqueElements;
	}

	/**
	 * @param requiresUniqueElements requiresUniqueElements to set
	 */
	public void setRequiresUniqueElements(Boolean requiresUniqueElements) {
		this.requiresUniqueElements = requiresUniqueElements;
	}

	/**
	 * @return choiceListName
	 */
	public String getChoiceListName() {
		return choiceListName;
	}

	/**
	 * @param choiceListName choiceListName to set
	 */
	public void setChoiceListName(String choiceListName) {
		this.choiceListName = choiceListName;
	}

	/**
	 * @return choiceListId
	 */
	public String getChoiceListId() {
		return choiceListId;
	}

	/**
	 * @param choiceListId choiceListId to set
	 */
	public void setChoiceListId(String choiceListId) {
		this.choiceListId = choiceListId;
	}

	/**
	 * @return hasHierarchy
	 */
	public Boolean getHasHierarchy() {
		return hasHierarchy;
	}

	/**
	 * @param hasHierarchy hasHierarchy to set
	 */
	public void setHasHierarchy(Boolean hasHierarchy) {
		this.hasHierarchy = hasHierarchy;
	}

	/**
	 * @return propertyString
	 */
	public String getPropertyString() {
		return propertyString;
	}

	/**
	 * @param propertyString propertyString to set
	 */
	public void setPropertyString(String propertyString) {
		this.propertyString = propertyString;
	}

	/**
	 * @return propertyDefaultString
	 */
	public String getPropertyDefaultString() {
		return propertyDefaultString;
	}

	/**
	 * @param propertyDefaultString propertyDefaultString to set
	 */
	public void setPropertyDefaultString(String propertyDefaultString) {
		this.propertyDefaultString = propertyDefaultString;
	}

	/**
	 * @return maximumLengthString
	 */
	public Integer getMaximumLengthString() {
		return maximumLengthString;
	}

	/**
	 * @param maximumLengthString maximumLengthString to set
	 */
	public void setMaximumLengthString(Integer maximumLengthString) {
		this.maximumLengthString = maximumLengthString;
	}

	/**
	 * @return propertyStrings
	 */
	public List<String> getPropertyStrings() {
		return propertyStrings;
	}

	/**
	 * @param propertyStrings propertyStrings to set
	 */
	public void setPropertyStrings(List<String> propertyStrings) {
		this.propertyStrings = propertyStrings;
	}

	/**
	 * @return propertyDateTime
	 */

	public Date getPropertyDateTime() {
		return propertyDateTime;
	}

	/**
	 * @param propertyDateTime propertyDateTime to set
	 */
	public void setPropertyDateTime(Date propertyDateTime) {
		this.propertyDateTime = propertyDateTime;
	}

	/**
	 * @return propertyDefaultDateTime
	 */

	public Date getPropertyDefaultDateTime() {
		return propertyDefaultDateTime;
	}

	/**
	 * @param propertyDefaultDateTime propertyDefaultDateTime to set
	 */
	public void setPropertyDefaultDateTime(Date propertyDefaultDateTime) {
		this.propertyDefaultDateTime = propertyDefaultDateTime;
	}

	/**
	 * @return propertyMaximumDateTime
	 */
	//在前台利用JS校验时 可以直接使用new Date(propertyMaximumDateTime)
	public Date getPropertyMaximumDateTime() {
		return propertyMaximumDateTime;
	}

	/**
	 * @param propertyMaximumDateTime propertyMaximumDateTime to set
	 */
	public void setPropertyMaximumDateTime(Date propertyMaximumDateTime) {
		this.propertyMaximumDateTime = propertyMaximumDateTime;
	}

	/**
	 * @return propertyMinimumDateTime
	 */

	public Date getPropertyMinimumDateTime() {
		return propertyMinimumDateTime;
	}

	/**
	 * @param propertyMinimumDateTime propertyMinimumDateTime to set
	 */
	public void setPropertyMinimumDateTime(Date propertyMinimumDateTime) {
		this.propertyMinimumDateTime = propertyMinimumDateTime;
	}

	/**
	 * @return propertyDateTimes
	 */
	public Date[] getPropertyDateTimes() {
		return propertyDateTimes;
	}

	/**
	 * @param propertyDateTimes propertyDateTimes to set
	 */
	public void setPropertyDateTimes(Date[] propertyDateTimes) {
		this.propertyDateTimes = propertyDateTimes;
	}

	/**
	 * @return propertyFloat64
	 */
	public Double getPropertyFloat64() {
		return propertyFloat64;
	}

	/**
	 * @param propertyFloat64 propertyFloat64 to set
	 */
	public void setPropertyFloat64(Double propertyFloat64) {
		this.propertyFloat64 = propertyFloat64;
	}

	/**
	 * @return propertyDefaultFloat64
	 */
	public Double getPropertyDefaultFloat64() {
		return propertyDefaultFloat64;
	}

	/**
	 * @param propertyDefaultFloat64 propertyDefaultFloat64 to set
	 */
	public void setPropertyDefaultFloat64(Double propertyDefaultFloat64) {
		this.propertyDefaultFloat64 = propertyDefaultFloat64;
	}

	/**
	 * @return propertyMaximumFloat64
	 */
	public Double getPropertyMaximumFloat64() {
		return propertyMaximumFloat64;
	}

	/**
	 * @param propertyMaximumFloat64 propertyMaximumFloat64 to set
	 */
	public void setPropertyMaximumFloat64(Double propertyMaximumFloat64) {
		this.propertyMaximumFloat64 = propertyMaximumFloat64;
	}

	/**
	 * @return propertyMinimumFloat64
	 */
	public Double getPropertyMinimumFloat64() {
		return propertyMinimumFloat64;
	}

	/**
	 * @param propertyMinimumFloat64 propertyMinimumFloat64 to set
	 */
	public void setPropertyMinimumFloat64(Double propertyMinimumFloat64) {
		this.propertyMinimumFloat64 = propertyMinimumFloat64;
	}

	/**
	 * @return propertyFloat64s
	 */
	public List<Double> getPropertyFloat64s() {
		return propertyFloat64s;
	}

	/**
	 * @param propertyFloat64s propertyFloat64s to set
	 */
	public void setPropertyFloat64s(List<Double> propertyFloat64s) {
		this.propertyFloat64s = propertyFloat64s;
	}

	/**
	 * @return propertyBoolean
	 */
	public Boolean getPropertyBoolean() {
		return propertyBoolean;
	}

	/**
	 * @param propertyBoolean propertyBoolean to set
	 */
	public void setPropertyBoolean(Boolean propertyBoolean) {
		this.propertyBoolean = propertyBoolean;
	}

	/**
	 * @return propertyDefaultBoolean
	 */
	public Boolean getPropertyDefaultBoolean() {
		return propertyDefaultBoolean;
	}

	/**
	 * @param propertyDefaultBoolean propertyDefaultBoolean to set
	 */
	public void setPropertyDefaultBoolean(Boolean propertyDefaultBoolean) {
		this.propertyDefaultBoolean = propertyDefaultBoolean;
	}

	/**
	 * @return propertyBooleans
	 */
	public List<Boolean> getPropertyBooleans() {
		return propertyBooleans;
	}

	/**
	 * @param propertyBooleans propertyBooleans to set
	 */
	public void setPropertyBooleans(List<Boolean> propertyBooleans) {
		this.propertyBooleans = propertyBooleans;
	}

	/**
	 * @return propertyInteger32
	 */
	public Integer getPropertyInteger32() {
		return propertyInteger32;
	}

	/**
	 * @param propertyInteger32 propertyInteger32 to set
	 */
	public void setPropertyInteger32(Integer propertyInteger32) {
		this.propertyInteger32 = propertyInteger32;
	}

	/**
	 * @return propertyDefaultInteger32
	 */
	public Integer getPropertyDefaultInteger32() {
		return propertyDefaultInteger32;
	}

	/**
	 * @param propertyDefaultInteger32 propertyDefaultInteger32 to set
	 */
	public void setPropertyDefaultInteger32(Integer propertyDefaultInteger32) {
		this.propertyDefaultInteger32 = propertyDefaultInteger32;
	}

	/**
	 * @return propertyMaximumInteger32
	 */
	public Integer getPropertyMaximumInteger32() {
		return propertyMaximumInteger32;
	}

	/**
	 * @param propertyMaximumInteger32 propertyMaximumInteger32 to set
	 */
	public void setPropertyMaximumInteger32(Integer propertyMaximumInteger32) {
		this.propertyMaximumInteger32 = propertyMaximumInteger32;
	}

	/**
	 * @return propertyMinimumInteger32
	 */
	public Integer getPropertyMinimumInteger32() {
		return propertyMinimumInteger32;
	}

	/**
	 * @param propertyMinimumInteger32 propertyMinimumInteger32 to set
	 */
	public void setPropertyMinimumInteger32(Integer propertyMinimumInteger32) {
		this.propertyMinimumInteger32 = propertyMinimumInteger32;
	}

	/**
	 * @return propertyInteger32s
	 */
	public List<Integer> getPropertyInteger32s() {
		return propertyInteger32s;
	}

	/**
	 * @param propertyInteger32s propertyInteger32s to set
	 */
	public void setPropertyInteger32s(List<Integer> propertyInteger32s) {
		this.propertyInteger32s = propertyInteger32s;
	}


}
