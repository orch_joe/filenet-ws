/**
 * @Title: Pagination.java
 * @Package com.strongit.filenet.service.datatype
 * @Description: 分页信息
 * @author Changling Jiang
 * @date 2013年11月11日 上午11:47:22
 * @version V1.0
 */
package com.zhjoe.strongit.filenet.service.datatype;


/**
 * @ClassName: Pagination
 * @Description: 分页信息
 * @author Changling Jiang
 * @date 2013年11月11日 上午11:47:22
 *
 */
public class Pagination {

	// 当前页数
	private int currentPage;

	// 每页记录数
	private int objectsPerpage;

	// 总页数
	private int totalPages;

	// 总记录数
	private int totalObjects;

	private String sortProperty;
	private String sortOrderBy;

	/**
	 * @return currentPage
	 */
	public int getCurrentPage() {
		return currentPage;
	}

	/**
	 * @param currentPage
	 *            currentPage to set
	 */
	public void setCurrentPage(int currentPage) {
		this.currentPage = currentPage;
	}

	/**
	 * @return objectsPerpage
	 */
	public int getObjectsPerpage() {
		return objectsPerpage;
	}

	/**
	 * @param objectsPerpage
	 *            objectsPerpage to set
	 */
	public void setObjectsPerpage(int objectsPerpage) {
		this.objectsPerpage = objectsPerpage;
	}

	/**
	 * @return totalPages
	 */
	public int getTotalPages() {
		return totalPages;
	}

	/**
	 * @param totalPages
	 *            totalPages to set
	 */
	public void setTotalPages(int totalPages) {
		this.totalPages = totalPages;
	}

	/**
	 * @return totalObjects
	 */
	public int getTotalObjects() {
		return totalObjects;
	}

	/**
	 * @param totalObjects
	 *            totalObjects to set
	 */
	public void setTotalObjects(int totalObjects) {
		this.totalObjects = totalObjects;
	}

	public String getSortProperty() {
		return sortProperty;
	}

	public void setSortProperty(String sortProperty) {
		this.sortProperty = sortProperty;
	}

	public String getSortOrderBy() {
		return sortOrderBy;
	}

	public void setSortOrderBy(String sortOrderBy) {
		this.sortOrderBy = sortOrderBy;
	}

}
