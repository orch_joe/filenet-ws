package com.zhjoe.strongit.filenet.service.util;

import com.filenet.api.core.Connection;
import com.filenet.api.core.Factory;

import static com.zhjoe.strongit.filenet.service.util.FileNetConfig.CONTENT_ENGINE_URL;

public class NewCEConnectionUtil {
    public static Connection getConnection() {
        String contentEngineUrl = FileNetConfig.getValue("NewContentEngineUrl");
        return Factory.Connection.getConnection(contentEngineUrl);
    }
}
