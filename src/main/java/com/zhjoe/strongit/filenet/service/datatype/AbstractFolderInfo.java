/**
 * @Title: FolderInfo.java
 * @Package com.strongit.filenet.service.datatype
 * @Description: 封装FileNet CE Folder对象类
 * @author Changling Jiang
 * @date 2013年11月11日 上午11:46:19
 * @version V1.0
 */
package com.zhjoe.strongit.filenet.service.datatype;

import java.util.Map;

import com.filenet.api.constants.PropertyNames;
import com.filenet.api.core.Folder;
import com.filenet.api.core.ObjectStore;
import com.filenet.api.property.FilterElement;
import com.filenet.api.property.PropertyFilter;

/**
 * @ClassName: FolderInfo
 * @Description: 封装FileNet CE Folder对象类
 * @author Changling Jiang
 * @date 2013年11月11日 上午11:46:19
 *
 */
public abstract class AbstractFolderInfo extends BaseInfo {

	public AbstractFolderInfo fromFolder(Folder folder) throws Exception {
		setId(folder.get_Id().toString());
		setName(folder.get_Name());
		setClassName(folder.getClassName());
		setPath(folder.get_PathName());
		setCreater(folder.get_Creator());
		setDateCreated(folder.get_DateCreated());
		setLastModifier(folder.get_LastModifier());
		setDateLastModified(folder.get_DateLastModified());
		setType("folder");

		ObjectStore os = folder.getObjectStore();
		PropertyFilter pf = new PropertyFilter();
		pf.addIncludeProperty(new FilterElement(0, null, Boolean.TRUE, PropertyNames.SYMBOLIC_NAME, null));
		os.fetchProperties(pf);
		String symbolicName = os.get_SymbolicName();
		setOsName(symbolicName);
		fromCustom(folder);
		return this;
	}

	protected abstract void fromCustom(Folder paramFolder);

	protected void fromVo(Map<String, Object> map) {
		if (getName() != null) {
			map.put("FolderName", getName());
		}
	}


}
