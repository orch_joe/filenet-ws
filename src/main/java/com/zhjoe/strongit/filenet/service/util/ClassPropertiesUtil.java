package com.zhjoe.strongit.filenet.service.util;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import com.filenet.api.collection.BooleanList;
import com.filenet.api.collection.DateTimeList;
import com.filenet.api.collection.Float64List;
import com.filenet.api.collection.Integer32List;
import com.filenet.api.collection.PropertyDescriptionList;
import com.filenet.api.collection.StringList;
import com.filenet.api.constants.PropertySettability;
import com.filenet.api.constants.RefreshMode;
import com.filenet.api.core.Document;
import com.filenet.api.core.Factory;
import com.filenet.api.core.ObjectStore;
import com.filenet.api.meta.ClassDescription;
import com.filenet.api.meta.PropertyDescription;
import com.filenet.api.property.Properties;
import com.zhjoe.strongit.filenet.service.datatype.PropertyInfo;

/**
 * @ClassName: ClassPropertiesUtil
 * @Description: 类属性动态相关
 * @author Changling Jiang
 * @date 2014年8月28日 下午12:25:13
 *
 */
public class ClassPropertiesUtil {

	public static List<PropertyInfo> getClassPropertyDescriptions(ObjectStore os, String className) {
		ClassDescription classDescription = Factory.ClassDescription.fetchInstance(os, className, null);
		return getClassPropertyDescriptions(classDescription);
	}

	public static Map<String, Object> getPropertyDescriptions(List<PropertyInfo> propertyInfos) {
		Map<String, Object> map = new HashMap<String, Object>();

		for (int i = 0; i < propertyInfos.size(); i++) {
			PropertyInfo propertyInfo = propertyInfos.get(i);
			Integer dataType = propertyInfo.getDataType();
			Integer cardinality = propertyInfo.getCardinality();
			String symbolicName = propertyInfo.getSymbolicName();
			switch (dataType) {
				case 2:
					if (cardinality == 0) {
						Boolean propertyBoolean = propertyInfo.getPropertyBoolean();
						if (propertyBoolean != null) {
							map.put(symbolicName, propertyBoolean);
						}
					} else {
						List<Boolean> propertyBooleans = propertyInfo.getPropertyBooleans();
						if (propertyBooleans != null) {
							BooleanList booleanList = Factory.BooleanList.createList();
							if (!propertyBooleans.isEmpty()) {
								booleanList.addAll(propertyBooleans);
							}
							map.put(symbolicName, booleanList);
						}
					}
					break;
				case 3:
					if (cardinality == 0) {
						Date propertyDateTime = propertyInfo.getPropertyDateTime();
						if (propertyDateTime != null) {
							map.put(symbolicName, propertyDateTime);
						}
					} else {
						Date[] propertyDateTimes = propertyInfo.getPropertyDateTimes();
						DateTimeList dateTimeList = Factory.DateTimeList.createList();
						if (propertyDateTimes != null) {
							dateTimeList.addAll(Arrays.asList(propertyDateTimes));
						}
						map.put(symbolicName, dateTimeList);
					}
					break;
				case 4:
					if (cardinality == 0) {
						Double propertyFloat64 = propertyInfo.getPropertyFloat64();
						if (propertyFloat64 != null) {
							map.put(symbolicName, propertyFloat64);
						}
					} else {
						List<Double> propertyFloat64s = propertyInfo.getPropertyFloat64s();
						if (propertyFloat64s != null && !propertyFloat64s.isEmpty()) {
							Float64List float64List = Factory.Float64List.createList();
							if (!propertyFloat64s.isEmpty()) {
								for (int j = 0; j < propertyFloat64s.size(); j++) {
									float64List.add(Double.parseDouble(propertyFloat64s.get(j) + ""));
								}
							}
							map.put(symbolicName, float64List);
						}
					}
					break;
				case 6:
					if (cardinality == 0) {
						Integer propertyInteger32 = propertyInfo.getPropertyInteger32();
						if (propertyInteger32 != null) {
							map.put(symbolicName, propertyInteger32);
						}
					} else {
						List<Integer> propertyInteger32s = propertyInfo.getPropertyInteger32s();
						if (propertyInteger32s != null) {
							Integer32List integer32List = Factory.Integer32List.createList();
							if (!propertyInteger32s.isEmpty()) {
								integer32List.addAll(propertyInteger32s);
							}
							map.put(symbolicName, integer32List);
						}
					}
					break;
				case 8:
					if (cardinality == 0) {
						String propertyString = propertyInfo.getPropertyString();
						if (propertyString != null) {
							map.put(symbolicName, propertyString);
						}
					} else {
						List<String> propertyStrings = propertyInfo.getPropertyStrings();
						if (propertyStrings != null) {
							StringList stringList = Factory.StringList.createList();
							if (!propertyStrings.isEmpty()) {
								stringList.addAll(propertyStrings);
							}
							map.put(symbolicName, stringList);
						}
					}
					break;
			}
		}
		return map;
	}

	public static boolean updateProperties(Document document, List<PropertyInfo> propertyInfos) {
		Properties properties = document.getProperties();
		for (int i = 0; i < propertyInfos.size(); i++) {
			PropertyInfo propertyInfo = propertyInfos.get(i);
			Integer dataType = propertyInfo.getDataType();
			Integer cardinality = propertyInfo.getCardinality();
			String symbolicName = propertyInfo.getSymbolicName();
			switch (dataType) {
				case 2:
					if (cardinality == 0) {
						Boolean propertyBoolean = propertyInfo.getPropertyBoolean();
						properties.putValue(symbolicName, propertyBoolean);
					} else {
						List<Boolean> propertyBooleans = propertyInfo.getPropertyBooleans();
						BooleanList booleanList = Factory.BooleanList.createList();
						if (propertyBooleans != null && !propertyBooleans.isEmpty()) {
							booleanList.addAll(propertyBooleans);
						}
						properties.putValue(symbolicName, booleanList);
					}
					break;
				case 3:
					if (cardinality == 0) {
						Date propertyDateTime = propertyInfo.getPropertyDateTime();
						properties.putValue(symbolicName, propertyDateTime);
					} else {
						Date[] propertyDateTimes = propertyInfo.getPropertyDateTimes();
						DateTimeList dateTimeList = Factory.DateTimeList.createList();
						if (propertyDateTimes != null) {
							dateTimeList.addAll(Arrays.asList(propertyDateTimes));
						}
						properties.putValue(symbolicName, dateTimeList);
					}
					break;
				case 4:
					if (cardinality == 0) {
						Double propertyFloat64 = propertyInfo.getPropertyFloat64();
						properties.putValue(symbolicName, propertyFloat64);
					} else {
						List<Double> propertyFloat64s = propertyInfo.getPropertyFloat64s();
						Float64List float64List = Factory.Float64List.createList();
						if (propertyFloat64s != null && !propertyFloat64s.isEmpty()) {
							for (int j = 0; j < propertyFloat64s.size(); j++) {
								float64List.add(Double.parseDouble(propertyFloat64s.get(j) + ""));
							}
						}
						properties.putValue(symbolicName, float64List);
					}
					break;
				case 6:
					if (cardinality == 0) {
						Integer propertyInteger32 = propertyInfo.getPropertyInteger32();
						properties.putValue(symbolicName, propertyInteger32);
					} else {
						List<Integer> propertyInteger32s = propertyInfo.getPropertyInteger32s();
						Integer32List integer32List = Factory.Integer32List.createList();
						if (propertyInteger32s != null && !propertyInteger32s.isEmpty()) {
							integer32List.addAll(propertyInteger32s);
						}
						properties.putValue(symbolicName, integer32List);
					}
					break;
				case 8:
					if (cardinality == 0) {
						String propertyString = propertyInfo.getPropertyString();
						properties.putValue(symbolicName, propertyString);
					} else {
						List<String> propertyStrings = propertyInfo.getPropertyStrings();
						StringList stringList = Factory.StringList.createList();
						if (propertyStrings != null && !propertyStrings.isEmpty()) {
							stringList.addAll(propertyStrings);
						}
						properties.putValue(symbolicName, stringList);
					}
					break;
			}
		}
		document.save(RefreshMode.REFRESH);
		return true;
	}


	public static List<PropertyInfo> getDocumentPropertyDescriptions(Document document) {
		List<PropertyInfo> descriptions = getClassPropertyDescriptions(document.get_ClassDescription());
		Properties properties = document.getProperties();
		for (int i = 0; i < descriptions.size(); i++) {
			PropertyInfo propertyInfo = descriptions.get(i);
			Integer dataType = propertyInfo.getDataType();
			Integer cardinality = propertyInfo.getCardinality();
			String symbolicName = propertyInfo.getSymbolicName();
			switch (dataType) {
				case 2:
					if (cardinality == 0) {
						Boolean propertyBoolean = properties.getBooleanValue(symbolicName);
						propertyInfo.setPropertyBoolean(propertyBoolean);
					} else {
						BooleanList booleanListValue = properties.getBooleanListValue(symbolicName);
						List<Boolean> propertyBooleans = new ArrayList<Boolean>();
						if (!booleanListValue.isEmpty()) {
							propertyBooleans.addAll(booleanListValue);
						}
						propertyInfo.setPropertyBooleans(propertyBooleans);
					}
					break;
				case 3:
					if (cardinality == 0) {
						Date propertyDateTime = properties.getDateTimeValue(symbolicName);
						propertyInfo.setPropertyDateTime(propertyDateTime);
					} else {
						DateTimeList dateTimeListValue = properties.getDateTimeListValue(symbolicName);
						if (!dateTimeListValue.isEmpty()) {
							Date[] propertyDateTimes = new Date[dateTimeListValue.size()];
							dateTimeListValue.toArray(propertyDateTimes);
							propertyInfo.setPropertyDateTimes(propertyDateTimes);
						}
					}
					break;
				case 4:
					if (cardinality == 0) {
						Double propertyFloat64 = properties.getFloat64Value(symbolicName);
						if (propertyFloat64 != null) {
							propertyInfo.setPropertyFloat64(propertyFloat64);
						}
					} else {
						Float64List float64ListValue = properties.getFloat64ListValue(symbolicName);
						List<Double> propertyFloat64s = new ArrayList<Double>();
						if (!float64ListValue.isEmpty()) {
							propertyFloat64s.addAll(float64ListValue);
						}
						propertyInfo.setPropertyFloat64s(propertyFloat64s);
					}
					break;
				case 6:
					if (cardinality == 0) {
						Integer propertyInteger32 = properties.getInteger32Value(symbolicName);
						propertyInfo.setPropertyInteger32(propertyInteger32);
					} else {
						Integer32List integer32ListValue = properties.getInteger32ListValue(symbolicName);
						List<Integer> propertyInteger32s = new ArrayList<Integer>();
						if (propertyInteger32s.isEmpty()) {
							propertyInteger32s.addAll(integer32ListValue);
						}
						propertyInfo.setPropertyInteger32s(propertyInteger32s);
					}
					break;
				case 8:
					if (cardinality == 0) {
						String propertyString = properties.getStringValue(symbolicName);
						propertyInfo.setPropertyString(propertyString);
					} else {
						StringList stringListValue = properties.getStringListValue(symbolicName);
						List<String> propertyStrings = new ArrayList<String>();
						if (!stringListValue.isEmpty()) {
							propertyStrings.addAll(stringListValue);
						}
						propertyInfo.setPropertyStrings(propertyStrings);
					}
					break;
			}
		}
		return descriptions;
	}


	public static List<PropertyInfo> getClassPropertyDescriptions(ClassDescription classDescription) {
		List<PropertyInfo> list = new ArrayList<PropertyInfo>();
		PropertyDescriptionList propertyDescriptionList = classDescription.get_PropertyDescriptions();
		PropertyDescription propertyDescription;
		Iterator iterator = propertyDescriptionList.iterator();
		while (iterator.hasNext()) {
			propertyDescription = (PropertyDescription) iterator.next();
			if (!propertyDescription.get_IsSystemOwned() && !propertyDescription.get_IsSystemGenerated() && !propertyDescription.get_IsHidden()) {
				PropertyInfo propertyInfo = new PropertyInfo();

				String symbolicName = propertyDescription.get_SymbolicName();
				String displayName = propertyDescription.get_DisplayName();
				String descriptiveText = propertyDescription.get_DescriptiveText();
				int cardinality = propertyDescription.get_Cardinality().getValue();
				int dataType = propertyDescription.get_DataType().getValue();
				if (dataType == 1 || dataType == 5 || dataType == 7) {
					continue;
				}


				Boolean isValueRequired = propertyDescription.get_IsValueRequired();
				Boolean isReadOnly = propertyDescription.get_IsReadOnly();

				PropertySettability p = propertyDescription.get_Settability();
				int i = p.getValue();
				Boolean requiresUniqueElements = propertyDescription.get_RequiresUniqueElements();
				String choiceListName = propertyDescription.get_ChoiceList() == null ? null : propertyDescription.get_ChoiceList().get_Name();
				String choiceListId = propertyDescription.get_ChoiceList() == null ? null : propertyDescription.get_ChoiceList().get_Id().toString();
				Boolean hasHierarchy = propertyDescription.get_ChoiceList() == null ? false : propertyDescription.get_ChoiceList().get_HasHierarchy();
				propertyInfo.setSymbolicName(symbolicName);
				propertyInfo.setDisplayName(displayName);
				propertyInfo.setDescriptiveText(descriptiveText);
				propertyInfo.setCardinality(cardinality);
				propertyInfo.setDataType(dataType);
				propertyInfo.setIsValueRequired(isValueRequired);
				propertyInfo.setIsReadOnly(isReadOnly);
				propertyInfo.setRequiresUniqueElements(requiresUniqueElements);
				propertyInfo.setChoiceListName(choiceListName);
				propertyInfo.setChoiceListId(choiceListId);
				propertyInfo.setHasHierarchy(hasHierarchy);

				Properties properties = propertyDescription.getProperties();
				//BINARY 1 ,BOOLEAN 2 ,DATE 3 ,DOUBLE 4 ,GUID 5 ,LONG 6 ,OBJECT 7 ,STRING 8
				switch (dataType) {
					case 2:
						Boolean propertyDefaultBoolean = properties.getBooleanValue("PropertyDefaultBoolean");
						propertyInfo.setPropertyDefaultBoolean(propertyDefaultBoolean);
						break;
					case 3:
						Date propertyDefaultDateTime = properties.getDateTimeValue("PropertyDefaultDateTime");
						Date propertyMaximumDateTime = properties.getDateTimeValue("PropertyMaximumDateTime");
						Date propertyMinimumDateTime = properties.getDateTimeValue("PropertyMinimumDateTime");
						propertyInfo.setPropertyDefaultDateTime(propertyDefaultDateTime);
						propertyInfo.setPropertyMaximumDateTime(propertyMaximumDateTime(propertyMaximumDateTime));
						propertyInfo.setPropertyMinimumDateTime(propertyMinimumDateTime);
						break;
					case 4:
						Double propertyDefaultFloat64 = properties.getFloat64Value("PropertyDefaultFloat64");
						Double propertyMaximumFloat64 = properties.getFloat64Value("PropertyMaximumFloat64");
						Double propertyMinimumFloat64 = properties.getFloat64Value("PropertyMinimumFloat64");
						propertyInfo.setPropertyDefaultFloat64(propertyDefaultFloat64);
						propertyInfo.setPropertyMaximumFloat64(propertyMaximumFloat64);
						propertyInfo.setPropertyMinimumFloat64(propertyMinimumFloat64);
						break;
					case 6:
						Integer propertyDefaultInteger32 = properties.getInteger32Value("PropertyDefaultInteger32");
						Integer propertyMaximumInteger32 = properties.getInteger32Value("PropertyMaximumInteger32");
						Integer propertyMinimumInteger32 = properties.getInteger32Value("PropertyMinimumInteger32");
						propertyInfo.setPropertyDefaultInteger32(propertyDefaultInteger32);
						propertyInfo.setPropertyMaximumInteger32(propertyMaximumInteger32);
						propertyInfo.setPropertyMinimumInteger32(propertyMinimumInteger32);
						break;
					case 8:
						String propertyDefaultString = properties.getStringValue("PropertyDefaultString");
						Integer maximumLengthString = properties.getInteger32Value("MaximumLengthString");
						propertyInfo.setPropertyDefaultString(propertyDefaultString);
						propertyInfo.setMaximumLengthString(maximumLengthString);
						break;
				}
				list.add(propertyInfo);
			}
		}
		return list;
	}

	;


	private static Date propertyMaximumDateTime(Date date) {
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(date);
		int year = calendar.get(Calendar.YEAR);
		if (year > 9999) {
			calendar.set(9999, 11, 31, 23, 59, 59);
			return calendar.getTime();
		}
		return date;
	}
}
