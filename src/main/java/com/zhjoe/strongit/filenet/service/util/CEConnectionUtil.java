/**
 * @Title: CEConnectionUtil.java
 * @Package com.strongit.filenet.service.util
 * @Description: FileNet CE Connection 工具类
 * @author Changling Jiang
 * @date 2013年11月11日 下午12:40:51
 * @version V1.0
 */
package com.zhjoe.strongit.filenet.service.util;

import com.filenet.api.core.Connection;
import com.filenet.api.core.Factory;

import static com.zhjoe.strongit.filenet.service.util.FileNetConfig.*;

/**
 * @ClassName: CEConnectionUtil
 * @Description: FileNet CE Connection 工具类
 * @author Changling Jiang
 * @date 2013年11月11日 下午12:40:51
 *
 */
public class CEConnectionUtil {

	public static Connection getConnection() {
		return Factory.Connection.getConnection(CONTENT_ENGINE_URL);
	}
}
