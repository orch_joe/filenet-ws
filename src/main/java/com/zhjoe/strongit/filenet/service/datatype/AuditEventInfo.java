/**
 * @Title: AuditEventInfo.java
 * @Package com.strongit.filenet.service.datatype
 * @Description: AuditEvent
 * @author Changling Jiang
 * @date 2013-12-21 下午02:41:50
 * @version V1.0
 */
package com.zhjoe.strongit.filenet.service.datatype;

/**
 * @ClassName: AuditEventInfo
 * @Description: AuditEvent
 * @author Changling Jiang
 * @date 2013-12-21 下午02:41:50
 *
 */
public class AuditEventInfo extends BaseInfo {

	private String initiatingUser;
	private String className;
	private String eventStatus;

	/**
	 * @return initiatingUser
	 */
	public String getInitiatingUser() {
		return initiatingUser;
	}

	/**
	 * @param initiatingUser initiatingUser to set
	 */
	public void setInitiatingUser(String initiatingUser) {
		this.initiatingUser = initiatingUser;
	}

	/**
	 * @return className
	 */
	public String getClassName() {
		return className;
	}

	/**
	 * @param className className to set
	 */
	public void setClassName(String className) {
		this.className = className;
	}

	/**
	 * @return eventStatus
	 */
	public String getEventStatus() {
		return eventStatus;
	}

	/**
	 * @param eventStatus eventStatus to set
	 */
	public void setEventStatus(String eventStatus) {
		this.eventStatus = eventStatus;
	}

}
