/**
 * @Title: ChoiceInfo.java
 * @Package com.strongit.filenet.service.datatype
 * @Description: TODO(用一句话描述该文件做什么)
 * @author Changling Jiang
 * @date 2013年11月19日 下午3:44:24
 * @version V1.0
 */
package com.zhjoe.strongit.filenet.service.datatype;

import java.util.List;

/**
 * @ClassName: ChoiceInfo
 * @Description: TODO(这里用一句话描述这个类的作用)
 * @author Changling Jiang
 * @date 2013年11月19日 下午3:44:24
 *
 */
public class ChoiceInfo {
	private String displayName;
	private String value;

	/**
	 * @return displayName
	 */
	public String getDisplayName() {
		return displayName;
	}

	/**
	 * @param displayName displayName to set
	 */
	public void setDisplayName(String displayName) {
		this.displayName = displayName;
	}

	/**
	 * @return value
	 */
	public String getValue() {
		return value;
	}

	/**
	 * @param value value to set
	 */
	public void setValue(String value) {
		this.value = value;
	}

}
