/**
 * @Title: FolderNotExistException.java
 * @Package com.strongit.filenet.service.exception
 * @Description: 自定义文件夹不存在异常
 * @author Changling Jiang
 * @date 2013年11月11日 下午12:39:24
 * @version V1.0
 */
package com.zhjoe.strongit.filenet.service.exception;

/**
 * @ClassName: FolderNotExistException
 * @Description: 自定义文件夹不存在异常
 * @author Changling Jiang
 * @date 2013年11月11日 下午12:39:24
 *
 */
public class FolderNotExistException extends Exception {

	/**
	 * @Fields serialVersionUID : TODO(用一句话描述这个变量表示什么)
	 */
	private static final long serialVersionUID = 1L;

	public FolderNotExistException() {
		super();
	}

	public FolderNotExistException(String errorMessage) {
		super(errorMessage);
	}
}
