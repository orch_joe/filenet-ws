/**
 * @Title: DocumentNotExistException.java
 * @Package com.strongit.filenet.service.exception
 * @Description: 自定义文档不存在异常
 * @author Changling Jiang
 * @date 2013年11月11日 下午12:38:51
 * @version V1.0
 */
package com.zhjoe.strongit.filenet.service.exception;

/**
 * @ClassName: DocumentNotExistException
 * @Description: 自定义文档不存在异常
 * @author Changling Jiang
 * @date 2013年11月11日 下午12:38:51
 *
 */
public class DocumentNotExistException extends Exception {

	/**
	 * @Fields serialVersionUID : TODO(用一句话描述这个变量表示什么)
	 */
	private static final long serialVersionUID = 1L;

	public DocumentNotExistException() {
		super();
	}

	public DocumentNotExistException(String errorMessage) {
		super(errorMessage);
	}
}
