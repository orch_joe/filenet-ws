/**
 * @Title: CEBulkUtil.java
 * @Package com.strongit.filenet.service.util
 * @Description: TODO(用一句话描述该文件做什么)
 * @author Changling Jiang
 * @date 2013年11月11日 下午12:40:26
 * @version V1.0
 */
package com.zhjoe.strongit.filenet.service.util;

import com.filenet.api.constants.RefreshMode;
import com.filenet.api.core.Domain;
import com.filenet.api.core.IndependentlyPersistableObject;
import com.filenet.api.core.UpdatingBatch;

/**
 * @ClassName: CEBulkUtil
 * @Description: TODO(这里用一句话描述这个类的作用)
 * @author Changling Jiang
 * @date 2013年11月11日 下午12:40:26
 *
 */
public class CEBulkUtil {

	private UpdatingBatch ub;

	public CEBulkUtil() {
	}

	public void createBatch(Domain domain) {
		this.ub = UpdatingBatch.createUpdatingBatchInstance(domain, RefreshMode.REFRESH);
	}

	public void populateBatch(IndependentlyPersistableObject object) {
		this.ub.add(object, null);
	}

	public void updateBatch() {
		this.ub.updateBatch();
	}
}
