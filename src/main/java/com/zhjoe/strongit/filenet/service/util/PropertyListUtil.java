/**
 * @Title: PropertyListUtil.java
 * @Package com.strongit.filenet.service.util
 * @Description: TODO(用一句话描述该文件做什么)
 * @author Changling Jiang
 * @date 2014-3-10 下午12:02:19
 * @version V1.0
 */
package com.zhjoe.strongit.filenet.service.util;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import com.filenet.api.collection.BooleanList;
import com.filenet.api.collection.Integer32List;
import com.filenet.api.collection.StringList;
import com.filenet.api.core.Factory;


/**
 * @ClassName: PropertyListUtil
 * @Description: TODO(这里用一句话描述这个类的作用)
 * @author Changling Jiang
 * @date 2014-3-10 下午12:02:19
 *
 */
public class PropertyListUtil {

	public static Object list2PropertyList(Set set) {
		if (set != null && !set.isEmpty()) {

			List list = new ArrayList();
			list.addAll(set);

			Object object = list.get(0);
			if (object instanceof String) {
				return list2StringList(list);
			} else if (object instanceof Integer) {
				return list2IntegerList(list);
			} else if (object instanceof Boolean) {
				return list2BooleanList(list);
			}
		}
		return null;
	}


	public static StringList list2StringList(List list) {
		StringList stringList = Factory.StringList.createList();
		if (list != null && !list.isEmpty()) {
			for (int i = 0; i < list.size(); i++) {
				stringList.add(list.get(i));
			}
		}
		return stringList;
	}

	public static Integer32List list2IntegerList(List list) {
		Integer32List integerList = Factory.Integer32List.createList();
		if (list != null && !list.isEmpty()) {
			for (int i = 0; i < list.size(); i++) {
				integerList.add(list.get(i));
			}
		}
		return integerList;
	}

	public static BooleanList list2BooleanList(List list) {
		BooleanList booleanList = Factory.BooleanList.createList();
		if (list != null && !list.isEmpty()) {
			for (int i = 0; i < list.size(); i++) {
				booleanList.add(list.get(i));
			}
		}
		return booleanList;
	}

}
