/**
 * @Title: CEHelper.java
 * @Package com.strongit.filenet.service.ce
 * @Description: FileNet CE 操作帮助类
 * @author Changling Jiang
 * @date 2013年11月11日 上午11:37:29
 * @version V1.0
 */
package com.zhjoe.strongit.filenet.service.ce;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import com.filenet.api.collection.*;
import com.filenet.api.meta.ClassDescription;
import com.filenet.api.meta.PropertyDescription;
import com.zhjoe.strongit.filenet.service.comparator.ComparatorDocument;
import com.zhjoe.strongit.filenet.service.comparator.ComparatorFolder;
import com.zhjoe.strongit.filenet.service.datatype.ContentTransferInfo;
import com.zhjoe.strongit.filenet.service.datatype.Pagination;
import com.zhjoe.strongit.filenet.service.datatype.RetrievalResult;
import com.zhjoe.strongit.filenet.service.util.CEObjectCollectionUtil;
import com.zhjoe.strongit.filenet.service.util.CEObjectStoreUtil;
import com.zhjoe.strongit.filenet.service.util.FileNetConfig;
import com.zhjoe.strongit.filenet.service.util.NewCEObjectStoreUtil;
import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;

import com.filenet.api.admin.ChoiceList;
import com.filenet.api.admin.ClassDefinition;
import com.filenet.api.constants.AutoClassify;
import com.filenet.api.constants.AutoUniqueName;
import com.filenet.api.constants.CheckinType;
import com.filenet.api.constants.ComponentRelationshipType;
import com.filenet.api.constants.CompoundDocumentState;
import com.filenet.api.constants.DefineSecurityParentage;
import com.filenet.api.constants.RefreshMode;
import com.filenet.api.constants.ReservationType;
import com.filenet.api.constants.VersionBindType;
import com.filenet.api.constants.VersionStatus;
import com.filenet.api.core.ComponentRelationship;
import com.filenet.api.core.Containable;
import com.filenet.api.core.ContentTransfer;
import com.filenet.api.core.CustomObject;
import com.filenet.api.core.Document;
import com.filenet.api.core.Factory;
import com.filenet.api.core.Folder;
import com.filenet.api.core.ObjectStore;
import com.filenet.api.core.ReferentialContainmentRelationship;
import com.filenet.api.core.VersionSeries;
import com.filenet.api.events.Event;
import com.filenet.api.exception.EngineRuntimeException;
import com.filenet.api.exception.ExceptionCode;
import com.filenet.api.property.Properties;
import com.filenet.api.util.Id;

/**
 * @ClassName: CEHelper
 * @Description: FileNet CE 操作帮助类
 * @author Changling Jiang
 * @date 2013年11月11日 上午11:37:29
 *
 */
public class CEHelper {

	private static Logger log = Logger.getLogger(CEHelper.class);

	/**
	 * @Title: getFolder
	 * @Description: 获得目录
	 * @param @param os
	 * @param @param folderKey
	 * @param @return
	 * @param @throws Exception    设定文件
	 * @return Folder    返回类型
	 */
	public static Folder getFolder(ObjectStore os, String folderKey) throws Exception {
		try {
			return Factory.Folder.fetchInstance(os, FileNetConfig.getValue(folderKey), null);
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	/**
	 * @Title: retrieveFolder
	 * @Description: 通过指定路径获取Folder对象
	 * @param @param os
	 * @param @param path
	 * @param @return    设定文件
	 * @return Folder    返回类型
	 */
	public static Folder retrieveFolder(ObjectStore os, String path) throws Exception {
		try {
			return Factory.Folder.fetchInstance(os, path, null);
		} catch (EngineRuntimeException e) {
			if (e.getExceptionCode() == ExceptionCode.E_OBJECT_NOT_FOUND) {
				return null;
			} else {
				e.printStackTrace();
				throw e;
			}
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}


	/**
	 * @Title: retrieveFolderNotExistThenCreate
	 * @Description: 通过指定路径获取Folder对象
	 * @param @param os
	 * @param @param path
	 * @param @return    设定文件
	 * @return Folder    返回类型
	 */
	public static Folder retrieveFolderNotExistThenCreate(ObjectStore os, String path) throws Exception {
		try {
			Folder folder = retrieveFolder(os, path);
			if (folder == null) {
				Folder parentFolder = null;
				String[] paths = path.split("/");
				String childPath = "";
				for (int i = 0; i < paths.length; i++) {
					if (childPath.equals("/")) {
						childPath += paths[i];
					} else {
						childPath += "/" + paths[i];
					}


					folder = CEHelper.retrieveFolder(os, childPath);
					if (folder == null) {
						folder = saveFolderInfo(parentFolder.createSubFolder(paths[i]));
					}
					parentFolder = folder;
				}
				return folder;
			}
			return folder;
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}


	/**
	 * @Title: createFolderInfo
	 * @Description: 创建FolderInfo对象实例(未保存)
	 * @param @param os
	 * @param @param className
	 * @param @return
	 * @throws Exception
	 * @return Folder
	 */
	public static Folder createFolderInfo(ObjectStore os, String className) throws Exception {
		try {
			return Factory.Folder.createInstance(os, className);
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	/**
	 * @Title: createFolderInfo
	 * @Description: 创建子文件夹
	 * @param @param os 文档库
	 * @param @param parent 父文件夹
	 * @param @param className 文件夹类
	 * @param @param props 文件夹属性
	 * @param @return
	 * @param @throws Exception    设定文件
	 * @return Folder    返回类型
	 */
	public static Folder createFolderInfo(ObjectStore os, Folder parent, String className, Map props) throws Exception {
		Folder child = createFolderInfo(os, className);
		child.set_Parent(parent);
		child = updateFolderProperties(child, props);
		return child;
	}


	/**
	 * @Title: saveFolderInfo
	 * @Description: 保存FolderInfo对象实例
	 * @param @param folder
	 * @param @return
	 * @throws Exception
	 * @return Folder
	 */
	public static Folder saveFolderInfo(Folder folder) throws Exception {
		try {
			folder.save(RefreshMode.REFRESH);
			return folder;
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	/**
	 * @Title: getFolderInfo
	 * @Description: 获得FolderInfo对象
	 * @param @param os
	 * @param @param id
	 * @param @return
	 * @throws Exception
	 * @return Folder
	 */
	public static Folder getFolderInfo(ObjectStore os, String id) throws Exception {
		try {
			Folder folder = Factory.Folder.fetchInstance(os, new Id(id), null);
			return folder;
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	/**
	 * @Title: createSubFolderInfo
	 * @Description: 为子文件指定父文件夹
	 * @param @param parent
	 * @param @param child
	 * @param @return
	 * @param @throws Exception    设定文件
	 * @return Folder    返回类型
	 */
	public static Folder createSubFolderInfo(Folder parent, Folder child) throws Exception {
		try {
			child.set_Parent(parent);
			child.save(RefreshMode.REFRESH);
			return child;
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	/**
	 * @Title: createSubFolderInfo
	 * @Description: 通过指定路径创建子文件夹实例
	 * @param @param os
	 * @param @param parent
	 * @param @param child
	 * @param @return    设定文件
	 * @throws Exception
	 * @return Folder    返回类型
	 */
	public static Folder createSubFolderInfo(ObjectStore os, String parent, String child) throws Exception {
		return saveFolderInfo(retrieveFolder(os, parent).createSubFolder(child));
	}


	/**
	 * @Title: deleteFolder
	 * @Description: 删除Folder对象
	 * @param @param folder
	 * @param @throws Exception    设定文件
	 * @return void    返回类型
	 */
	public static void deleteFolder(Folder folder) throws Exception {
		try {
			folder.delete();
			folder.save(RefreshMode.REFRESH);
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	/**
	 * @Title: deleteDocument
	 * @Description: 删除文档对象(删除文档的某一版本)
	 * @param @param document    设定文件
	 * @throws Exception
	 * @return void    返回类型
	 */
	public static void deleteDocument(Document document) throws Exception {
		try {
			log.debug("删除文档，文档Id = " + document.get_Id() + " " + document.get_MajorVersionNumber() + "." + document.get_MinorVersionNumber());
			document.delete();
			document.save(RefreshMode.REFRESH);
			log.debug("删除文档成功");
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	/**
	 * @Title: deleteComponentRelationship
	 * @Description: 删除关联关系
	 * @param @param parentDocument
	 * @param @param childDocument
	 * @param @param flag 标识是否删除该子文档    设定文件
	 * @return void    返回类型
	 */
	public static void deleteComponentRelationship(Document parentDocument, Document childDocument, boolean flag) throws Exception {
		try {
			ComponentRelationshipSet componentRelationshipSet = parentDocument.get_ChildRelationships();
			if (componentRelationshipSet != null && !componentRelationshipSet.isEmpty()) {
				Iterator iterator = componentRelationshipSet.iterator();
				while (iterator.hasNext()) {
					ComponentRelationship componentRelationship = (ComponentRelationship) iterator.next();
					if (componentRelationship.get_ChildComponent() != null) {
						Document document = componentRelationship.get_ChildComponent();
						if (document.get_Id().toString().equals(childDocument.get_Id().toString())) {
							componentRelationship.delete();
							componentRelationship.save(RefreshMode.REFRESH);
							if (flag) {
								CEHelper.deleteDocumentWithAllVersions(childDocument);
							}
						}
					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	/**
	 * @Title: deleteAllComponentRelationship
	 * @Description: 删除全部复合文档关系
	 * @param @param coupoundDocument
	 * @param @param flag 是否也删除关联的文档
	 * @param @throws Exception    设定文件
	 * @return void    返回类型
	 */
	public static void deleteAllComponentRelationship(Document coupoundDocument, boolean flag) throws Exception {
		try {
			ComponentRelationshipSet componentRelationshipSet = coupoundDocument.get_ChildRelationships();
			if (componentRelationshipSet != null && !componentRelationshipSet.isEmpty()) {
				Iterator iterator = componentRelationshipSet.iterator();
				while (iterator.hasNext()) {
					ComponentRelationship componentRelationship = (ComponentRelationship) iterator.next();
					if (componentRelationship.get_ChildComponent() != null) {
						Document document = componentRelationship.get_ChildComponent();
						componentRelationship.delete();
						componentRelationship.save(RefreshMode.REFRESH);
						if (flag) {
							CEHelper.deleteDocumentWithAllVersions(document);
						}
					} else {
						componentRelationship.delete();
						componentRelationship.save(RefreshMode.REFRESH);
					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	/**
	 * @Title: deleteDocumentWithAllVersions
	 * @Description: 删除文档的所有版本
	 * @param @param document
	 * @param @throws Exception    设定文件
	 * @return void    返回类型
	 */
	public static void deleteDocumentWithAllVersions(Document document) throws Exception {
		try {
			VersionSeries verSeries = document.get_VersionSeries();
			verSeries.delete();
			verSeries.save(RefreshMode.REFRESH);
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	/**
	 * @Title: checkout
	 * @Description: 检出文档
	 * @param @param document
	 * @param @throws Exception    设定文件
	 * @return void    返回类型
	 */
	public static void checkout(Document document) throws Exception {
		try {
			if (!document.get_IsReserved()) {
				document.checkout(ReservationType.OBJECT_STORE_DEFAULT, null, null, null);
				document.save(RefreshMode.REFRESH);
			}
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	/**
	 * @Title: cancelCheckout
	 * @Description: 文档取消检出
	 * @param @param document
	 * @param @throws Exception    设定文件
	 * @return void    返回类型
	 */
	public static void cancelCheckout(Document document) throws Exception {
		try {
			Document reservation = (Document) document.get_Reservation();
			document.cancelCheckout();
			reservation.save(RefreshMode.REFRESH);
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}



	/**
	 * @Title: checkin
	 * @Description: 检入文档
	 * @param @param document
	 * @param @param propMap
	 * @param @param contents
	 * @param @throws Exception    设定文件
	 * @return void    返回类型
	 */
	public static void checkin(Document document, Map<String, Object> propMap, List<ContentTransferInfo> contents) throws Exception {
		try {
			if (document != null) {
				if (document.get_IsCurrentVersion().booleanValue() == false) {
					document = (Document) document.get_CurrentVersion();
				}
				if (document.get_IsReserved().booleanValue() == false) {
					document.checkout(ReservationType.EXCLUSIVE, null, null, null);
					document.save(RefreshMode.REFRESH);
				}

				if (document.get_IsCurrentVersion().booleanValue() == false) {
					document = (Document) document.get_CurrentVersion();
				}

				if (document.get_IsReserved().booleanValue() == true && (document.get_VersionStatus().getValue() != VersionStatus.RESERVATION_AS_INT)) {
					Document reservation = (Document) document.get_Reservation();
					document = updateDocumentProperties(reservation, propMap);
					ContentElementList contentList = Factory.ContentElement.createList();
					if (contents != null) {
						for (int i = 0; i < contents.size(); i++) {
							ContentTransferInfo contentTransferInfo = contents.get(i);
							ContentTransfer contentTransfer = Factory.ContentTransfer.createInstance();
							if (contentTransferInfo.getInputStream() != null) {
								contentTransfer.setCaptureSource(contentTransferInfo.getInputStream());
							}
							if (contentTransferInfo.getContentType() != null) {
								contentTransfer.set_ContentType(contentTransferInfo.getContentType());
							}
							if (contentTransferInfo.getRetrievalName() != null) {
								contentTransfer.set_RetrievalName(contentTransferInfo.getRetrievalName());
							}
							contentList.add(contentTransfer);
						}
					}
					reservation.set_ContentElements(contentList);
					reservation.checkin(AutoClassify.DO_NOT_AUTO_CLASSIFY, CheckinType.MAJOR_VERSION);
					reservation.save(RefreshMode.REFRESH);

				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	public static void checkinWithId(Document document, Map<String, Object> propMap, List<ContentTransferInfo> contents, Id id) throws Exception {
		try {
			if (document != null) {
				if (document.get_IsCurrentVersion().booleanValue() == false) {
					document = (Document) document.get_CurrentVersion();
				}
				if (document.get_IsReserved().booleanValue() == false) {
					document.checkout(ReservationType.EXCLUSIVE, id, null, null);
					document.save(RefreshMode.REFRESH);
				}

				if (document.get_IsCurrentVersion().booleanValue() == false) {
					document = (Document) document.get_CurrentVersion();
				}

				if (document.get_IsReserved().booleanValue() == true && (document.get_VersionStatus().getValue() != VersionStatus.RESERVATION_AS_INT)) {
					Document reservation = (Document) document.get_Reservation();
					document = updateDocumentProperties(reservation, propMap);
					ContentElementList contentList = Factory.ContentElement.createList();
					if (contents != null) {
						for (int i = 0; i < contents.size(); i++) {
							ContentTransferInfo contentTransferInfo = contents.get(i);
							ContentTransfer contentTransfer = Factory.ContentTransfer.createInstance();
							if (contentTransferInfo.getInputStream() != null) {
								contentTransfer.setCaptureSource(contentTransferInfo.getInputStream());
							}
							if (contentTransferInfo.getContentType() != null) {
								contentTransfer.set_ContentType(contentTransferInfo.getContentType());
							}
							if (contentTransferInfo.getRetrievalName() != null) {
								contentTransfer.set_RetrievalName(contentTransferInfo.getRetrievalName());
							}
							contentList.add(contentTransfer);
						}
					}
					reservation.set_ContentElements(contentList);
					reservation.checkin(AutoClassify.DO_NOT_AUTO_CLASSIFY, CheckinType.MAJOR_VERSION);
					reservation.save(RefreshMode.REFRESH);

				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}
	/**
	 * @Title: retrieveFolderInfos
	 * @Description: 检索文件夹信息
	 * @param @param os
	 * @param @param className
	 * @param @param includeSubclasses
	 * @param @param selectList
	 * @param @param whereClause
	 * @param @param orderByClause
	 * @param @param pagination
	 * @param @return
	 * @param @throws Exception    设定文件
	 * @return RetrievalResult    返回类型
	 */
	public static RetrievalResult retrieveFolderInfos(ObjectStore os, String className, boolean includeSubclasses, String selectList, String whereClause, String orderByClause, Pagination pagination) throws Exception {
		try {
			CESearch search = new CESearch();
			search.setScope(os);
			search.setObjectSql(className, includeSubclasses, selectList, null, whereClause, orderByClause);
			RetrievalResult result = search.fetchFolders(os, pagination);
			return result;
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	/**
	 * @Title: retrieveAllInFolderInfos
	 * @Description: 查询文件下所有对象
	 * @param @param os
	 * @param @param documentSymbolicClassName
	 * @param @param documentIncludeSubclasses
	 * @param @param documentSelectList
	 * @param @param documentWhereClause
	 * @param @param documentOrderByClause
	 * @param @param folderSymbolicClassName
	 * @param @param folderIncludeSubclasses
	 * @param @param folderSelectList
	 * @param @param folderWhereClause
	 * @param @param folderOrderByClause
	 * @param @param pagination
	 * @param @return
	 * @param @throws Exception    设定文件
	 * @return RetrievalResult    返回类型
	 */
	public static RetrievalResult retrieveAllInFolderInfos(ObjectStore os, String documentSymbolicClassName, boolean documentIncludeSubclasses, String documentSelectList, String documentWhereClause, String documentOrderByClause, String folderSymbolicClassName, boolean folderIncludeSubclasses, String folderSelectList, String folderWhereClause, String folderOrderByClause, Pagination pagination)
			throws Exception {
		try {
			CESearch search = new CESearch();
			search.setScope(os);
			RetrievalResult result = search.fetchAllInFolder(os, documentSymbolicClassName, documentIncludeSubclasses, documentSelectList, documentWhereClause, documentOrderByClause, folderSymbolicClassName, folderIncludeSubclasses, folderSelectList, folderWhereClause, folderOrderByClause, pagination);
			return result;
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	/**
	 * @Title: retrieveDocumentInfos
	 * @Description: 检索文档信息(支持全文检索)
	 * @param @param os
	 * @param @param className
	 * @param @param includeSubclasses
	 * @param @param selectList
	 * @param @param text 全文检索标识
	 * @param @param whereClause
	 * @param @param orderByClause
	 * @param @param pagination
	 * @param @return
	 * @param @throws Exception    设定文件
	 * @return RetrievalResult    返回类型
	 */
	public static RetrievalResult retrieveDocumentInfos(ObjectStore os, String className, boolean includeSubclasses, String selectList, String text, String whereClause, String orderByClause, Pagination pagination) throws Exception {
		try {
			CESearch search = new CESearch();
			search.setScope(os);
			search.setObjectSql(className, includeSubclasses, selectList, text, whereClause, orderByClause);
			RetrievalResult result = search.fetchDocuments(os, pagination);
			return result;
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}


	/**
	 * @Title: retrieveDocumentInfosContentSummary
	 * @Description: 检索文档信息(支持全文检索) 全文附近内容高亮显示
	 * @param @param os
	 * @param @param className
	 * @param @param includeSubclasses
	 * @param @param selectList
	 * @param @param text 全文检索标识
	 * @param @param whereClause
	 * @param @param orderByClause
	 * @param @param pagination
	 * @param @return
	 * @param @throws Exception    设定文件
	 * @return RetrievalResult    返回类型
	 */
	public static RetrievalResult retrieveDocumentInfosContentSummary(ObjectStore os, String className, boolean includeSubclasses, String selectList, String text, String whereClause, String orderByClause, Pagination pagination) throws Exception {
		try {
			CESearch search = new CESearch();
			search.setScope(os);
			search.setObjectSql(className, includeSubclasses, selectList, text, whereClause, orderByClause);
			RetrievalResult result = search.fetchDocumentContentSummary(os, pagination);
			return result;
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}


	/**
	 * @Title: retrieveSubFolderInfos
	 * @Description: 通过父文件夹对象获取子文件夹列表
	 * @param @param parent
	 * @param @return    设定文件
	 * @throws Exception
	 * @return List<Folder>    返回类型
	 */
	public static List<Folder> retrieveSubFolderInfos(Folder parent, ComparatorFolder comparator) throws Exception {
		try {
			FolderSet folders = parent.get_SubFolders();
			List<Folder> list = new ArrayList<Folder>();
			for (Folder folder : CEObjectCollectionUtil.c(folders, Folder.class)) {
				list.add(folder);
			}
			if (comparator != null) {
				Collections.sort(list, comparator);
			}
			return list;
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}

	}

	/**
	 * @Title: retrieveContainedDocumentInfos
	 * @Description: 通过父文件夹对象获取文档列表
	 * @param @param os
	 * @param @param path
	 * @param @return    设定文件
	 * @throws Exception
	 * @return List<Document>    返回类型
	 */
	public static List<Document> retrieveContainedDocumentInfos(Folder folder) throws Exception {
		try {
			DocumentSet documents = folder.get_ContainedDocuments();
			List<Document> list = new ArrayList<Document>();
			for (Document document : CEObjectCollectionUtil.c(documents, Document.class)) {
				list.add(document);
			}
			return list;
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}

	}


	/**
	 * @Title: createDocumentInfo
	 * @Description: 创建DocumentInfo对象(包含元数据和内容)
	 * @param @param os
	 * @param @param className
	 * @param @param compoundDocumentState 1 - 复合文档 0 - 标准文档
	 * @param @param folder
	 * @param @param propMap
	 * @param @param contents
	 * @param @return    设定文件
	 * @throws Exception
	 * @return Document    返回类型
	 */
	public static Document createDocumentInfo(ObjectStore os, String className, int compoundDocumentState, Folder folder, Map<String, Object> propMap, List<ContentTransferInfo> contents) throws Exception {
		try {
			Document document = createDocumentInfo(os, className);
			//设置文档类型 1 - 复合文档 0 - 标准文档
			if (compoundDocumentState == CompoundDocumentState.COMPOUND_DOCUMENT_AS_INT) {
				document.set_CompoundDocumentState(CompoundDocumentState.COMPOUND_DOCUMENT);
			} else {
				document.set_CompoundDocumentState(CompoundDocumentState.STANDARD_DOCUMENT);
			}
			document = updateDocumentProperties(document, propMap);
			ContentElementList contentList = Factory.ContentElement.createList();
			if (contents != null) {
				for (int i = 0; i < contents.size(); i++) {
					ContentTransferInfo contentTransferInfo = contents.get(i);
					ContentTransfer contentTransfer = Factory.ContentTransfer.createInstance();
					if (contentTransferInfo.getInputStream() != null) {
						contentTransfer.setCaptureSource(contentTransferInfo.getInputStream());
					}
					if (contentTransferInfo.getContentType() != null) {
						contentTransfer.set_ContentType(contentTransferInfo.getContentType());
					}
					if (contentTransferInfo.getRetrievalName() != null) {
						contentTransfer.set_RetrievalName(contentTransferInfo.getRetrievalName());
					}
					contentList.add(contentTransfer);
				}
			}

			document.set_ContentElements(contentList);
			//document.set_MimeType("application/msword");
			document.checkin(AutoClassify.DO_NOT_AUTO_CLASSIFY, CheckinType.MAJOR_VERSION);
			document.set_SecurityFolder(folder);
			document.save(RefreshMode.REFRESH);
			ReferentialContainmentRelationship rel = folder.file(document, AutoUniqueName.AUTO_UNIQUE, document.getProperties().getStringValue("DocumentTitle"), DefineSecurityParentage.DEFINE_SECURITY_PARENTAGE);
			rel.save(RefreshMode.REFRESH);
			return document;
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	/**
	 * @Title: createDocumentInfo
	 * @Description: 创建DocumentInfo对象
	 * @param @param os
	 * @param @param className
	 * @param @return
	 * @throws Exception
	 * @return Document
	 */
	private static Document createDocumentInfo(ObjectStore os, String className) throws Exception {
		try {
			Document document = Factory.Document.createInstance(os, className);
			return document;
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	/**
	 * @Title: saveDocumentInfo
	 * @Description: 保存DocumentInfo对象
	 * @param @param document
	 * @param @return
	 * @throws Exception
	 * @return Document
	 */
	public static Document saveDocumentInfo(Document document) throws Exception {
		try {
			document.save(RefreshMode.REFRESH);
			return document;
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}

	}

	/**
	 * @Title: getFoldersFiledIn
	 * @Description: 获取文档FoldersFileIn列表
	 * @param @param document
	 * @param @return    设定文件
	 * @throws Exception
	 * @return List<Folder>    返回类型
	 */
	public static List<Folder> getFoldersFiledIn(Document document) throws Exception {
		try {
			List<Folder> folders = new ArrayList<Folder>();
			FolderSet folderSet = document.get_FoldersFiledIn();
			if (folderSet != null && !folderSet.isEmpty()) {
				Iterator iterator = folderSet.iterator();
				while (iterator.hasNext()) {
					Folder folder = (Folder) iterator.next();
					folders.add(folder);
				}
			}
			return folders;
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	public static String getFoldersPathFiledIn(Document document) throws Exception {
		try {
			String path = "";
			List<Folder> folders = new ArrayList<Folder>();
			FolderSet folderSet = document.get_FoldersFiledIn();
			if (folderSet != null && !folderSet.isEmpty()) {
				Iterator iterator = folderSet.iterator();
				while (iterator.hasNext()) {
					Folder folder = (Folder) iterator.next();
					String pathName = folder.get_PathName();
					if (StringUtils.isNotEmpty(path)) {
						path += ";" + pathName;
					} else {
						path += pathName;
					}
				}
			}
			return path;
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	/**
	 * @Title: getDocumentInfo
	 * @Description: 获得DocumentInfo对象
	 * @param @param os
	 * @param @param id
	 * @param @return
	 * @throws Exception
	 * @return Document
	 */
	public static Document getDocumentInfo(ObjectStore os, String id) throws Exception {
		try {
			Document document = Factory.Document.fetchInstance(os, new Id(id), null);
			return document;
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}


	/**
	 * @Title: retrieveChoiceList
	 * @Description: 获得com.filenet.api.admin.ChoiceList对象实例
	 * @param @param os
	 * @param @param id
	 * @param @return    设定文件
	 * @throws Exception
	 * @return ChoiceList    返回类型
	 */
	public static ChoiceList retrieveChoiceList(ObjectStore os, String id) throws Exception {
		try {
			return Factory.ChoiceList.fetchInstance(os, new Id(id), null);
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	/**
	 * @Title: createCustomObjectInfo
	 * @Description: 创建CustomObjectInfo对象实例
	 * @param @param os
	 * @param @param className
	 * @param @return
	 * @throws Exception
	 * @return CustomObject
	 */
	public static CustomObject createCustomObjectInfo(ObjectStore os, String className) throws Exception {
		try {
			CustomObject customObject = Factory.CustomObject.createInstance(os, className);
			return customObject;
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	/**
	 * @Title: saveCustomObjectInfo
	 * @Description: 保存CustomObjectInfo对象实例
	 * @param @param customObject
	 * @param @return
	 * @throws Exception
	 * @return CustomObject
	 */
	public static CustomObject saveCustomObjectInfo(CustomObject customObject) throws Exception {
		try {
			customObject.save(RefreshMode.REFRESH);
			return customObject;
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}


	/**
	 * @Title: getCustomObjectInfo
	 * @Description: 获得CustomObjectInfo对象
	 * @param @param os
	 * @param @param id
	 * @param @return
	 * @throws Exception
	 * @return CustomObject
	 */
	public static CustomObject getCustomObjectInfo(ObjectStore os, String id) throws Exception {
		try {
			return Factory.CustomObject.fetchInstance(os, new Id(id), null);
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	/**
	 * @Title: createCompoundDocument
	 * @Description: 为文档添加子文档，形成复合文档
	 * @param @param document 已有普通文档
	 * @param @param documents 子文档集合
	 * @param @param componentRelationshipType 关联关系为 动态 或 静态 ComponentRelationshipType.DYNAMIC_CR || ComponentRelationshipType.DYNAMIC_LABEL_CR
	 * @param @param versionBindType LATEST_MAJOR_VERSION or LATEST_VERSION
	 * @param @return    设定文件
	 * @throws Exception
	 * @return Document    返回类型
	 */
	public static Document createCompoundDocument(Document document, List<Document> documents, ComponentRelationshipType componentRelationshipType, VersionBindType versionBindType) throws Exception {
		try {
			if (document.get_CompoundDocumentState().getValue() == 0) {
				document.set_CompoundDocumentState(CompoundDocumentState.COMPOUND_DOCUMENT);
				document.save(RefreshMode.REFRESH);
			}
			if (documents != null && !documents.isEmpty()) {
				for (int i = 0; i < documents.size(); i++) {
					Document childDocument = documents.get(i);
					ComponentRelationship componentRelationship = Factory.ComponentRelationship.createInstance(document.getObjectStore(), null, componentRelationshipType, document, "ComponentRelation" + i);
					componentRelationship.set_ChildComponent(childDocument);
					componentRelationship.set_ComponentSortOrder(new Integer(i));
					if (componentRelationshipType != null && (componentRelationshipType == ComponentRelationshipType.DYNAMIC_CR || componentRelationshipType == ComponentRelationshipType.DYNAMIC_LABEL_CR)) {
						componentRelationship.set_VersionBindType(versionBindType);
					}
					componentRelationship.save(RefreshMode.REFRESH);
				}
			}
			return document;
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	/**
	 * @Title: dynamicBindCompoundDocument
	 * @Description: 文档和复合文档动态绑定
	 * @param @param os
	 * @param @param parent 复合文档
	 * @param @param child 子文档
	 * @param @return
	 * @param @throws Exception    设定文件
	 * @return ComponentRelationship    返回类型
	 */
	public static ComponentRelationship dynamicBindCompoundDocument(ObjectStore os, Document parent, Document child) throws Exception {
		try {
			ComponentRelationship componentRelationship = Factory.ComponentRelationship.createInstance(os, null);
			componentRelationship.set_ParentComponent(parent);
			componentRelationship.set_ChildComponent(child);
			componentRelationship.set_ComponentSortOrder(1);
			componentRelationship.set_ComponentRelationshipType(ComponentRelationshipType.DYNAMIC_CR);
			componentRelationship.set_VersionBindType(VersionBindType.LATEST_MAJOR_VERSION);
			componentRelationship.save(RefreshMode.REFRESH);
			return componentRelationship;
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	/**
	 * @Title: getDocumentChildDocuments
	 * @Description: 获取复合文档的子文档列表
	 * @param @param document
	 * @param @return
	 * @param @throws Exception    设定文件
	 * @return List<Document>    返回类型
	 */
	public static List<Document> getDocumentChildDocuments(Document document) throws Exception {
		try {
			List<Document> list = new ArrayList<Document>();
			ComponentRelationshipSet childRelationships = document.get_ChildRelationships();
			Iterator iterator = childRelationships.iterator();
			while (iterator.hasNext()) {
				ComponentRelationship next = (ComponentRelationship) iterator.next();
				Document childDocument = next.get_ChildComponent();
				if (childDocument != null) {
					list.add(childDocument);
				} else {
					next.delete();
					next.save(RefreshMode.REFRESH);
				}
			}
			return list;
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}


	/**
	 * @Title: getDocumentChildDocuments
	 * @Description: 获取复合文档的子文档列表
	 * @param @param document
	 * @param @param pagination
	 * @param @return
	 * @param @throws Exception    设定文件
	 * @return List<Document>    返回类型
	 */
	public static RetrievalResult getDocumentChildDocuments(Document document, Pagination pagination, ComparatorDocument comparator) throws Exception {
		try {
			RetrievalResult result = new RetrievalResult();
			List<Document> list = CEHelper.getDocumentChildDocuments(document);
			if (comparator != null) {
				Collections.sort(list, comparator);
			}
			//子文档的数量
			int totalObjects = list.size();
			int totalPages = 0;
			int from = (pagination.getCurrentPage() - 1) * pagination.getObjectsPerpage();
			int to = pagination.getCurrentPage() * pagination.getObjectsPerpage();

			if (from >= totalObjects) {
				from = 0;
				to = pagination.getObjectsPerpage();
			}
			if (totalObjects <= to) {
				to = totalObjects;
			}
			pagination.setTotalObjects(totalObjects);
			if (totalObjects == 0) {
				totalPages = 0;
			} else {
				totalPages = totalObjects / pagination.getObjectsPerpage() + 1;
			}
			pagination.setTotalPages(totalPages);
			result.setPagination(pagination);
			result.setObjectList(list.subList(from, to));
			return result;
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}


	/**
	 * @Title: pagination
	 * @Description: 分页方法
	 * @param @param list
	 * @param @param pagination
	 * @param @return    设定文件
	 * @throws Exception
	 * @return RetrievalResult    返回类型
	 */
	public static RetrievalResult pagination(List list, Pagination pagination) throws Exception {
		try {
			RetrievalResult result = new RetrievalResult();
			int currentPage = pagination.getCurrentPage();
			int objectsPerpage = pagination.getObjectsPerpage();
			int totalObjects = list.size();
			pagination.setTotalObjects(totalObjects);
			pagination.setTotalPages((totalObjects + objectsPerpage - 1) / objectsPerpage);
			int fromIndex = (currentPage - 1) * objectsPerpage;
			if (fromIndex < 0) {
				fromIndex = 0;
			}
			int toIndex = fromIndex + objectsPerpage;
			if (toIndex > totalObjects - 1) {
				toIndex = totalObjects;
			}
			if (fromIndex < toIndex) {
				list = list.subList(fromIndex, toIndex);
			} else {
				pagination.setCurrentPage(pagination.getTotalPages() != 0 ? pagination.getTotalPages() : 1);
			}
			result.setObjectList(list);
			result.setPagination(pagination);
			return result;
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	/**
	 * @Title: updateDocumentSecurityFolder
	 * @Description: 设置文档的安全性文件夹
	 * @param @param document
	 * @param @param folder 安全性文件夹
	 * @param @return    设定文件
	 * @return Document    返回类型
	 */
	public static Document updateDocumentSecurityFolder(Document document, Folder folder) {
		document.set_SecurityFolder(folder);
		document.save(RefreshMode.REFRESH);
		return document;
	}

	/**
	 * @Title: updateDocumentProperties
	 * @Description: 更新文档属性
	 * @param @param document
	 * @param @param propMap
	 * @param @return    设定文件
	 * @throws Exception
	 * @return Document    返回类型
	 */
	public static Document updateDocumentProperties(Document document, Map<String, Object> propMap) throws Exception {
		try {
			String className = document.getClassName();
			ClassDescription classDescription = Factory.ClassDescription.fetchInstance(CEObjectStoreUtil.getDefaultCEObjectStore().getOs(), className, null);
			PropertyDescriptionList propertyDescriptionList = classDescription.get_PropertyDescriptions();
			PropertyDescription propertyDescription;
			Iterator iterator = propertyDescriptionList.iterator();
			while (iterator.hasNext()) {
				propertyDescription = (PropertyDescription) iterator.next();
				if (propertyDescription.get_IsSystemOwned()
						|| propertyDescription.get_IsSystemGenerated()) {
					String propKey = propertyDescription.get_SymbolicName();
					if(propMap != null){
						if(propMap.containsKey(propKey)){
							propMap.remove(propKey);
						}
					}
				}
			}

			if (propMap != null) {
				Properties props = document.getProperties();
				Iterator<String> it = propMap.keySet().iterator();
				while (it.hasNext()) {
					String key = it.next();
					Object value = propMap.get(key);
					log.debug(key + "=" + value);
					props.putObjectValue(key, value);
				}
			}
			document.save(RefreshMode.REFRESH);
			return document;
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}


	/**
	 * @Title: updateFolderProperties
	 * @Description: 更新文件夹属性
	 * @param @param folder
	 * @param @param propMap
	 * @param @return    设定文件
	 * @throws Exception
	 * @return Folder    返回类型
	 */
	public static Folder updateFolderProperties(Folder folder, Map<String, Object> propMap) throws Exception {
		try {
			Properties props = folder.getProperties();
			if (propMap != null) {
				Iterator<String> it = propMap.keySet().iterator();
				while (it.hasNext()) {
					String key = it.next();
					Object value = propMap.get(key);
					props.putObjectValue(key, value);
				}
			}
			folder.save(RefreshMode.REFRESH);
			return folder;
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}


	/**
	 * @Title: file
	 * @Description: 把文件对象file到另外一个文件目录
	 * @param @param folder
	 * @param @param document
	 * @param @return
	 * @param @throws Exception    设定文件
	 * @return boolean    返回类型
	 */
	public static boolean file(Folder folder, Document document) throws Exception {
		try {
			document.set_SecurityFolder(folder);
			ReferentialContainmentRelationship rel = folder.file(document, AutoUniqueName.AUTO_UNIQUE, document.get_Name(), DefineSecurityParentage.DEFINE_SECURITY_PARENTAGE);
			rel.save(RefreshMode.REFRESH);
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	/**
	 * @Title: unfile
	 * @Description: 把文件对象unfile到另外一个文件目录
	 * @param @param folder
	 * @param @param document
	 * @param @return    设定文件
	 * @throws Exception
	 * @return boolean    返回类型
	 */
	public static boolean unfile(Folder folder, Document document) throws Exception {
		try {
			ReferentialContainmentRelationship rel = folder.unfile(document);
			rel.save(RefreshMode.REFRESH);
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	/**
	 * @Title: move
	 * @Description: 文件夹移动
	 * @param @param targetFolder
	 * @param @param currentFolder
	 * @param @return
	 * @param @throws Exception    设定文件
	 * @return boolean    返回类型
	 */
	public static boolean move(Folder targetFolder, Folder currentFolder) throws Exception {
		try {
			currentFolder.move(targetFolder);
			currentFolder.save(RefreshMode.REFRESH);
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	/**
	 * @Title: auditEvents
	 * @Description: FileNet Containable AuditEvent
	 * @param @param containable
	 * @param @return
	 * @param @throws Exception    设定文件
	 * @return List<Event>    返回类型
	 */
	public static List<Event> auditEvents(Containable containable) throws Exception {
		try {
			List<Event> eventList = new ArrayList<Event>();
			if (containable != null) {
				EventSet auditedEvents = containable.get_AuditedEvents();
				Iterator eventIt = auditedEvents.iterator();
				while (eventIt.hasNext()) {
					Event event = (Event) eventIt.next();
					eventList.add(event);
				}
			}
			return eventList;
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	/**
	 * @Title: getVersionsDocumentInfos
	 * @Description: 查询文档所有版本信息
	 * @param @param document
	 * @param @return
	 * @param @throws Exception    设定文件
	 * @return List<Document>    返回类型
	 */
	public static List<Document> getVersionsDocumentInfos(Document document) throws Exception {
		try {
			List<Document> list = new ArrayList<Document>();
			VersionableSet versionableSet = document.get_Versions();
			Iterator versionableSetItr = versionableSet.iterator();
			Document documentVersion;
			while (versionableSetItr.hasNext()) {
				documentVersion = (Document) versionableSetItr.next();
				list.add(documentVersion);
			}
			return list;
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	/**
	 * @throws Exception
	 * @Title: customUnfile
	 * @Description: file到指定目录，并unfile掉指定目录以外的所有目录
	 * @param @param document
	 * @param @param folder
	 * @param @return    设定文件
	 * @return boolean    返回类型
	 */
	public static boolean customfile(Document document, Folder folder) throws Exception {
		CEHelper.file(folder, document);
		List<Folder> list = CEHelper.getFoldersFiledIn(document);
		for (int i = 0; i < list.size(); i++) {
			Folder f = list.get(i);
			if (folder.get_Id().toString().equals(f.get_Id().toString())) {
			} else {
				CEHelper.unfile(f, document);
			}
		}
		return true;
	}

	public static boolean customfile(List<Document> documents, Folder folder) throws Exception {
		for (int i = 0; i < documents.size(); i++) {
			Document document = documents.get(i);
			customfile(document, folder);
		}
		return true;
	}

	public static void getParentFolder(List<Folder> folderList, Folder currentFolder) {
		Folder parent = currentFolder.get_Parent();
		if (parent != null) {
			folderList.add(parent);
			getParentFolder(folderList, parent);
		}
	}

	public static ClassDefinition getClassDefinitionById(ObjectStore os, String id) {
		return Factory.ClassDefinition.fetchInstance(os, new Id(id), null);
	}

}
