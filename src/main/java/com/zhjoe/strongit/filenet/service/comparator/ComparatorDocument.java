/**
 * @Title: ComparatorDocument.java
 * @Package com.strongit.filenet.service.comparator
 * @Description: 表格排序类（文档）
 * @author Ke Cui
 * @date 2014年4月22日 上午11:06:06
 * @version V1.0
 */
package com.zhjoe.strongit.filenet.service.comparator;

import java.util.Comparator;
import java.util.Date;

import com.filenet.api.core.Document;

/**
 * @ClassName: ComparatorDocument
 * @Description: 对表格进行排序（文档）
 * @author Ke Cui
 * @date 2014年4月22日 上午11:06:06
 *
 */
public class ComparatorDocument implements Comparator {

	protected String property;// 排序字段
	protected String order;// 升序||降序
	protected int type;// 字段类型(现只提供 字符串:1 数字:2 时间:3)

	public int compare(Object o1, Object o2) {

		Document document1 = (Document) o1;
		Document document2 = (Document) o2;

		if (type == 1) {
			String property1 = document1.getProperties().getStringValue(property);
			String property2 = document2.getProperties().getStringValue(property);
			if (property1 == "" || property1 == null) {
				return 1;
			} else if (property2 == "" || property2 == null) {
				return -1;
			}
			if ("desc".equals(order)) {
				return property2.compareTo(property1);
			} else {
				return property1.compareTo(property2);
			}
		} else if (type == 2) {
			String property1 = document1.getProperties().getInteger32Value(property) == null ? "" : document1.getProperties().getInteger32Value(property).toString();
			String property2 = document2.getProperties().getInteger32Value(property) == null ? "" : document2.getProperties().getInteger32Value(property).toString();
			if ("desc".equals(order)) {
				return property2.compareTo(property1);
			} else {
				return property1.compareTo(property2);
			}
		} else if (type == 3) {
			Date property1 = document1.get_DateLastModified();
			Date property2 = document2.get_DateLastModified();
			if (property1 == null) {
				return 1;
			} else if (property2 == null) {
				return -1;
			}
			if ("desc".equals(order)) {
				if (property1.before(property2)) {// 降序
					return 1;
				} else {
					return -1;
				}
			} else {
				if (property1.after(property2)) {// 升序
					return 1;
				} else {
					return -1;
				}
			}
		} else {
			return 0;
		}
	}

	/**
	 * @return property
	 */
	public String getProperty() {
		return property;
	}

	/**
	 * @param property
	 *           property to set
	 */
	public void setProperty(String property) {
		this.property = property;
	}

	/**
	 * @return order
	 */
	public String getOrder() {
		return order;
	}

	/**
	 * @param order
	 *           order to set
	 */
	public void setOrder(String order) {
		this.order = order;
	}

	/**
	 * @return type
	 */
	public int getType() {
		return type;
	}

	/**
	 * @param type
	 *           type to set
	 */
	public void setType(int type) {
		this.type = type;
	}

}
