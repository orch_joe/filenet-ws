/**
 * @Title: ConvertDocument2DocumentInfoUtil.java
 * @Package com.strongit.filenet.service.util
 * @Description: CE DocumentClass 和Web DocumentClass工具类
 * @author Changling Jiang
 * @date 2013年11月15日 下午5:06:14
 * @version V1.0
 */
package com.zhjoe.strongit.filenet.service.util;

import com.filenet.api.core.Document;
import com.zhjoe.strongit.filenet.service.datatype.DocumentInfo;

/**
 * @ClassName: ConvertDocument2DocumentInfoUtil
 * @Description: CE DocumentClass 和Web DocumentClass工具类
 * @author Changling Jiang
 * @date 2013年11月15日 下午5:06:14
 *
 */
public class ConvertDocument2DocumentInfoUtil {

	/**
	 * @Title: convertDocument2DocumentInfo
	 * @Description: 把FileNet CE Document实例转换为 DocumentInfo对象实例
	 * @param @param document
	 * @param @return
	 * @param @throws Exception    设定文件
	 * @return DocumentInfo    返回类型
	 */
	public static DocumentInfo convertDocument2DocumentInfo(Document document) throws Exception {
		try {
			DocumentInfo documentInfo = null;
			String symbolicName = document.get_ClassDescription().get_SymbolicName();
			String dynamicClassName = FileNetConfig.getValue(symbolicName + "DocumentInfo");
			if (dynamicClassName != null && !"".equals(dynamicClassName)) {
				documentInfo = (DocumentInfo) (Class.forName(dynamicClassName).newInstance());
				documentInfo.fromDocument(document);
			}
			return documentInfo;
		} catch (InstantiationException e) {
			e.printStackTrace();
			throw e;
		} catch (IllegalAccessException e) {
			e.printStackTrace();
			throw e;
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
			throw e;
		}
	}

}
