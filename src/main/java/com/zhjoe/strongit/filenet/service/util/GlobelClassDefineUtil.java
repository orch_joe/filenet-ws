package com.zhjoe.strongit.filenet.service.util;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map.Entry;

import javax.annotation.PostConstruct;

import com.zhjoe.strongit.filenet.service.datatype.GlobelClassDefine;
import com.zhjoe.strongit.filenet.service.exception.ConfigException;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Service;

import com.filenet.api.admin.ClassDefinition;
import com.filenet.api.collection.ClassDefinitionSet;
import com.filenet.api.core.Factory;
import com.filenet.api.core.ObjectStore;
import com.google.common.collect.TreeMultimap;

/**
 * @author  QiaoYu
 */
@Service
public class GlobelClassDefineUtil {
    private static Logger logger = Logger.getLogger(GlobelClassDefineUtil.class);
    @PostConstruct
    public void startMethod(){
        GlobelClassDefine globelClassDefine = GlobelClassDefine.getInstance();
        TreeMultimap<String, String> treeMap = TreeMultimap.create();
        try {
            ObjectStore os = CEObjectStoreUtil.getDefaultCEObjectStore().getOs();
            ClassDefinition classDefinition = Factory.ClassDefinition.fetchInstance(os, "Document", null);
            Iterator<ClassDefinition> classes = classDefinition.get_ImmediateSubclassDefinitions().iterator();
            while (classes.hasNext()) {
                ClassDefinition clasdefin = (ClassDefinition) classes.next();
                treeMap.put("Document", clasdefin.get_SymbolicName());
                getImdiateSubClass(clasdefin.get_ImmediateSubclassDefinitions(),clasdefin.get_SymbolicName(),treeMap);
            }
            logger.info("=====================================初始化TreeClassDefine完成=====================================================");
            Iterator<Entry<String, String>> iterator = treeMap.entries().iterator();
            while (iterator.hasNext()) {
                Entry<String, String> entry = (Entry<String, String>) iterator.next();
                logger.info(entry.getKey()+"---"+entry.getValue());
            }
            globelClassDefine.setTreeMap(treeMap);
        } catch (ConfigException e) {
            logger.error("",e);
            e.printStackTrace();
        }
    }

    /**
     * @Description:  TODO
     * @CreateName:  QiaoYu
     * @CreateDate:  2017年7月5日 上午11:32:23
     */
    private static void getImdiateSubClass(ClassDefinitionSet classDefinitionSet, String symbolicName, TreeMultimap<String, String> treeMap) {
        Iterator<ClassDefinition> classes = classDefinitionSet.iterator();
        while (classes.hasNext()) {
            ClassDefinition clasdefin = (ClassDefinition) classes.next();
            treeMap.put(symbolicName, clasdefin.get_SymbolicName());
            if (null!=clasdefin.get_ImmediateSubclassDefinitions() && !clasdefin.get_ImmediateSubclassDefinitions().isEmpty()) {
                getImdiateSubClass(clasdefin.get_ImmediateSubclassDefinitions(), clasdefin.get_SymbolicName(), treeMap);
            }
        }
    }

    public List<String> getAllParentClass(String className){
        List<String> parentsClasses = new ArrayList<String>();
        GlobelClassDefine globelClassDefine = GlobelClassDefine.getInstance();
        TreeMultimap<String, String> treeMap = globelClassDefine.getTreeMap();
        Iterator<Entry<String, String>> iterator = treeMap.entries().iterator();
        while (iterator.hasNext()) {
            Entry<String, String> entry = (Entry<String, String>) iterator.next();
            if (className.equals(entry.getValue())) {
                String key = entry.getKey();
                parentsClasses.add(key);
                getAllParentClass(key,treeMap,parentsClasses);
            }
        }
        return parentsClasses;
    }

    /**
     * @Description:  TODO
     * @CreateName:  QiaoYu
     * @CreateDate:  2017年7月5日 下午1:00:42
     */
    private void getAllParentClass(String className, TreeMultimap<String, String> treeMap, List<String> parentsClasses) {
        Iterator<Entry<String, String>> iterator = treeMap.entries().iterator();
        while (iterator.hasNext()) {
            Entry<String, String> entry = (Entry<String, String>) iterator.next();
            if (className.equals(entry.getValue())) {
                String key = entry.getKey();
                parentsClasses.add(key);
                getAllParentClass(key,treeMap,parentsClasses);
            }
        }
    }
    public static void main(String[] args) {
        GlobelClassDefineUtil globelClassDefineUtil = new GlobelClassDefineUtil();
        globelClassDefineUtil.startMethod();
        List<String> classes = globelClassDefineUtil.getAllParentClass("ContractFile");
        for (String string : classes) {
            System.out.println(string);
        }
    }
}
