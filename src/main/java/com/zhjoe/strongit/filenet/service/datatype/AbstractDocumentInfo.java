/**
 * @Title: AbstractDocumentInfo.java
 * @Package com.strongit.filenet.service.datatype
 * @Description: 封装FileNet CE Document对象类
 * @author Changling Jiang
 * @date 2013年11月11日 上午11:45:14
 * @version V1.0
 */
package com.zhjoe.strongit.filenet.service.datatype;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import com.filenet.api.collection.FolderSet;
import com.filenet.api.core.Document;
import com.filenet.api.core.Folder;
import com.zhjoe.strongit.filenet.service.util.ConvertFolder2FolderInfoUtil;

/**
 * @ClassName: AbstractDocumentInfo
 * @Description: 封装FileNet CE Document对象类
 * @author Changling Jiang
 * @date 2013年11月11日 上午11:45:14
 *
 */
public abstract class AbstractDocumentInfo extends BaseInfo {

	private String documentTitle;
	private Integer compoundDocumentState;//Standerd||Compound Document
	private String mimeType;
	private Double contentSize;
	private String versionSeriesId;
	private Integer majorVersionNumber;
	private Integer minorVersionNumber;
	private Boolean isReserved;
	private String contentSummary;
	private List<FolderInfo> folderInfos = new ArrayList<FolderInfo>();

	public AbstractDocumentInfo fromDocument(Document document) throws Exception {
		try {
			setType("document");
			setId(document.get_Id().toString());
			setName(document.get_Name());
			setClassName(document.getClassName());
			setCreater(document.get_Creator());
			setDateCreated(document.get_DateCreated());
			setLastModifier(document.get_LastModifier());
			setDateLastModified(document.get_DateLastModified());
			setDocumentTitle(document.getProperties().getStringValue("DocumentTitle"));
			setMimeType(document.getProperties().getStringValue("MimeType"));
			setContentSize(document.get_ContentSize());
			setVersionSeriesId(document.get_VersionSeries().get_Id().toString());
			setMajorVersionNumber(document.get_MajorVersionNumber());
			setMinorVersionNumber(document.get_MinorVersionNumber());
			setCompoundDocumentState(document.get_CompoundDocumentState().getValue());
			setIsReserved(document.get_IsReserved());
			FolderSet folderSet = document.get_FoldersFiledIn();
			for (Iterator<Folder> itFolder = folderSet.iterator(); itFolder.hasNext(); ) {
				Folder folder = itFolder.next();
				FolderInfo folderInfo = ConvertFolder2FolderInfoUtil.convertFolder2FolderInfo(folder);
				folderInfos.add(folderInfo);
			}
			fromCustom(document);
			return this;
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	protected abstract void fromCustom(Document document);

	public void fromVo(Map<String, Object> map) {
		if (getDocumentTitle() != null) {
			map.put("DocumentTitle", getDocumentTitle());
		}
	}

	/**
	 * @return documentTitle
	 */
	public String getDocumentTitle() {
		return documentTitle;
	}

	/**
	 * @param documentTitle documentTitle to set
	 */
	public void setDocumentTitle(String documentTitle) {
		this.documentTitle = documentTitle;
	}

	/**
	 * @return compoundDocumentState
	 */
	public Integer getCompoundDocumentState() {
		return compoundDocumentState;
	}

	/**
	 * @param compoundDocumentState compoundDocumentState to set
	 */
	public void setCompoundDocumentState(Integer compoundDocumentState) {
		this.compoundDocumentState = compoundDocumentState;
	}

	/**
	 * @return mimeType
	 */
	public String getMimeType() {
		return mimeType;
	}

	/**
	 * @param mimeType mimeType to set
	 */
	public void setMimeType(String mimeType) {
		this.mimeType = mimeType;
	}

	/**
	 * @return contentSize
	 */
	public Double getContentSize() {
		return contentSize;
	}

	/**
	 * @param contentSize contentSize to set
	 */
	public void setContentSize(Double contentSize) {
		this.contentSize = contentSize;
	}

	/**
	 * @return versionSeriesId
	 */
	public String getVersionSeriesId() {
		return versionSeriesId;
	}

	/**
	 * @param versionSeriesId versionSeriesId to set
	 */
	public void setVersionSeriesId(String versionSeriesId) {
		this.versionSeriesId = versionSeriesId;
	}

	/**
	 * @return majorVersionNumber
	 */
	public Integer getMajorVersionNumber() {
		return majorVersionNumber;
	}

	/**
	 * @param majorVersionNumber majorVersionNumber to set
	 */
	public void setMajorVersionNumber(Integer majorVersionNumber) {
		this.majorVersionNumber = majorVersionNumber;
	}

	/**
	 * @return minorVersionNumber
	 */
	public Integer getMinorVersionNumber() {
		return minorVersionNumber;
	}

	/**
	 * @param minorVersionNumber minorVersionNumber to set
	 */
	public void setMinorVersionNumber(Integer minorVersionNumber) {
		this.minorVersionNumber = minorVersionNumber;
	}

	/**
	 * @return isReserved
	 */
	public Boolean getIsReserved() {
		return isReserved;
	}

	/**
	 * @param isReserved isReserved to set
	 */
	public void setIsReserved(Boolean isReserved) {
		this.isReserved = isReserved;
	}

	/**
	 * @return contentSummary
	 */
	public String getContentSummary() {
		return contentSummary;
	}

	/**
	 * @param contentSummary contentSummary to set
	 */
	public void setContentSummary(String contentSummary) {
		this.contentSummary = contentSummary;
	}

	public List<FolderInfo> getFolderInfos() {
		return folderInfos;
	}

	public void setFolderInfos(List<FolderInfo> folderInfos) {
		this.folderInfos = folderInfos;
	}
}
