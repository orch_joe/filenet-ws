package com.zhjoe.strongit.filenet.service.util;

import java.io.*;

public class DiskUtil {

	public static void writTextToDisk(String text, String path) {
		File file = null;
		try {
			file = new File(path);
			makeDirs(file.getPath());
			FileOutputStream writerStream = new FileOutputStream(path);
			BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(writerStream, "UTF-8"));
			writer.write(text);
			writer.flush();
			writer.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	private static void makeDirs(String path) {
		File file = new File(path);
		String filePathStr = file.getParent();
		File filePath = new File(filePathStr);
		if (!filePath.exists()) {
			filePath.mkdirs();
		}
	}

	public static String readTextFromDisk(String path) {
		StringBuilder result = new StringBuilder();
		try {
			FileInputStream fis = new FileInputStream(path);
			InputStreamReader isr = new InputStreamReader(fis, "UTF-8");
			BufferedReader br = new BufferedReader(isr);
			String line = null;
			while ((line = br.readLine()) != null) {
				result.append(line);
				result.append("\r\n");
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
		return result.toString();
	}
}
