/**
 * @Title: CESearch.java
 * @Package com.strongit.filenet.service.ce
 * @Description: FileNet CE Search 封装类
 * @author Changling Jiang
 * @date 2013年11月11日 上午11:38:25
 * @version V1.0
 */
package com.zhjoe.strongit.filenet.service.ce;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.zhjoe.strongit.filenet.service.datatype.DocumentInfo;
import com.zhjoe.strongit.filenet.service.datatype.Pagination;
import com.zhjoe.strongit.filenet.service.datatype.RetrievalResult;
import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;

import com.filenet.api.collection.IndependentObjectSet;
import com.filenet.api.collection.PageIterator;
import com.filenet.api.collection.PageMark;
import com.filenet.api.collection.RepositoryRowSet;
import com.filenet.api.constants.FilteredPropertyType;
import com.filenet.api.core.Document;
import com.filenet.api.core.Folder;
import com.filenet.api.core.ObjectStore;
import com.filenet.api.property.FilterElement;
import com.filenet.api.property.PropertyFilter;
import com.filenet.api.query.RepositoryRow;
import com.filenet.api.query.SearchSQL;
import com.filenet.api.query.SearchScope;

/**
 * @ClassName: CESearch
 * @Description: FileNet CE Search 封装类
 * @author Changling Jiang
 * @date 2013年11月11日 上午11:38:25
 *
 */
public class CESearch {

	private static final Logger log = Logger.getLogger(CESearch.class);

	private SearchSQL sql;

	private SearchScope search;

	private PropertyFilter filter;

	private static int pageSize = 100000;

	private static boolean continuable = true;

	public CESearch() {
	}

	public void setScope(ObjectStore os) {

		this.search = new SearchScope(os);
		// Specify a property filter to use for the filter parameter, if needed.This can be null if you are not filtering properties.
		this.filter = new PropertyFilter();
		this.filter.setMaxRecursion(0);
		this.filter.addIncludeType(new FilterElement(null, null, null, FilteredPropertyType.ANY, null));
	}

	public void setObjectSql(String className, boolean includeSubclasses, String selectList, String text, String whereClause, String orderByClause) {

		this.sql = new SearchSQL();
		this.sql.setSelectList(selectList);
		this.sql.setFromClauseInitialValue(className, null, includeSubclasses);

		if (!StringUtils.isEmpty(text)) {
			this.sql.setContainsRestriction(className, text);
		}
		if (!StringUtils.isEmpty(whereClause)) {
			this.sql.setWhereClause(whereClause);
		}

		if (!StringUtils.isEmpty(orderByClause)) {
			this.sql.setOrderByClause(orderByClause);
		}
		log.debug("sql:" + this.sql.toString());

	}

	/**
	 * @Title: fetchAllInFolder
	 * @Description: 查询文件下所有对象
	 * @param @param os
	 * @param @param documentSymbolicClassName
	 * @param @param documentIncludeSubclasses
	 * @param @param documentSelectList
	 * @param @param documentWhereClause
	 * @param @param documentOrderByClause
	 * @param @param folderSymbolicClassName
	 * @param @param folderIncludeSubclasses
	 * @param @param folderSelectList
	 * @param @param folderWhereClause
	 * @param @param folderOrderByClause
	 * @param @param pagination
	 * @param @return
	 * @param @throws Exception    设定文件
	 * @return RetrievalResult    返回类型
	 */
	public RetrievalResult fetchAllInFolder(ObjectStore os, String documentSymbolicClassName, boolean documentIncludeSubclasses, String documentSelectList, String documentWhereClause, String documentOrderByClause, String folderSymbolicClassName, boolean folderIncludeSubclasses, String folderSelectList, String folderWhereClause, String folderOrderByClause, Pagination pagination) throws Exception {

		RetrievalResult result = new RetrievalResult();
		List list = new ArrayList();
		result.setPagination(pagination);
		result.setObjectList(list);

		int from = (pagination.getCurrentPage() - 1) * pagination.getObjectsPerpage() + 1;
		int to = pagination.getCurrentPage() * pagination.getObjectsPerpage();
		int index = 0;

		SearchSQL folderSearchSQL = new SearchSQL();
		folderSearchSQL.setSelectList(folderSelectList);
		folderSearchSQL.setFromClauseInitialValue(folderSymbolicClassName, null, folderIncludeSubclasses);
		if (!StringUtils.isEmpty(folderWhereClause)) {
			folderSearchSQL.setWhereClause(folderWhereClause);
		}
		if (!StringUtils.isEmpty(folderOrderByClause)) {
			folderSearchSQL.setOrderByClause(folderOrderByClause);
		}

		Date folderSearchTimeFrom = new Date();
		log.debug("开始查询文件夹：" + folderSearchTimeFrom);
		IndependentObjectSet folderObjects = this.search.fetchObjects(folderSearchSQL, Integer.valueOf(this.pageSize), this.filter, Boolean.valueOf(this.continuable));
		log.debug("查询IndependentObjectSet完成");
		PageIterator folderPageIterator = folderObjects.pageIterator();
		int folderTotalObjects = 0;
		List<PageMark> folderPageMarksList = new ArrayList<PageMark>();


		log.debug("开始计算文件夹总页数");
		while (folderPageIterator.nextPage() == true) {
			log.debug("开始获取FolderPageMark");
			folderPageMarksList.add(folderPageIterator.getPageMark());
			log.debug("获取当前FolderPageMark结束");
			log.debug("开始获取当前页Folder条目数");
			int currentPageMarkCount = folderPageIterator.getElementCount();
			folderTotalObjects += currentPageMarkCount;
			log.debug("获取当前页Folder条目数结束 ： " + currentPageMarkCount);

			if (index >= to || (index + currentPageMarkCount) < from) {
				index += currentPageMarkCount;
			} else {
				Object[] currentPage = folderPageIterator.getCurrentPage();

				int tempIndex = index;
				for (int i = 0; i < currentPage.length; i++) {
					index++;
					if (index < from) {
					} else if (index >= from && index <= to) {
						Folder folder = (Folder) currentPage[i];
						//folder = CEHelper.getFolderInfo(os, folder.get_Id().toString());
						list.add(folder);//添加
					} else if (index > to) {
						index = tempIndex + currentPageMarkCount;
						break;
					}
				}
			}
		}
		log.debug("FolderPageMark总数量：" + folderPageMarksList.size());
		log.debug("文件夹总数量：" + folderTotalObjects);
		Date folderSearchTimeTo = new Date();
		log.debug("查询文件夹结束：" + folderSearchTimeTo + ",耗时 ：" + (folderSearchTimeTo.getTime() - folderSearchTimeFrom.getTime()) / 1000 + " 秒");


		SearchSQL documentSearchSQL = new SearchSQL();
		documentSearchSQL.setSelectList(documentSelectList);
		documentSearchSQL.setFromClauseInitialValue(documentSymbolicClassName, null, documentIncludeSubclasses);
		if (!StringUtils.isEmpty(documentWhereClause)) {
			documentSearchSQL.setWhereClause(documentWhereClause);
		}

		if (!StringUtils.isEmpty(documentOrderByClause)) {
			documentSearchSQL.setOrderByClause(documentOrderByClause);
		}
		Date documentSearchTimeFrom = new Date();
		log.debug("开始查询文档：" + documentSearchTimeFrom);
		IndependentObjectSet documentObjects = this.search.fetchObjects(documentSearchSQL, Integer.valueOf(this.pageSize), this.filter, Boolean.valueOf(this.continuable));
		log.debug("查询IndependentObjectSet完成");
		PageIterator documentPageIterator = documentObjects.pageIterator();
		int documentTotalObjects = 0;
		List<PageMark> documentPageMarksList = new ArrayList<PageMark>();

		log.debug("开始计算文档总页数");
		while (documentPageIterator.nextPage() == true) {
			log.debug("开始获取DocumentPageMark");
			documentPageMarksList.add(documentPageIterator.getPageMark());
			log.debug("获取当前DocumentPageMark结束");
			log.debug("开始获取Document当前页条目数");
			int currentPageMarkCount = documentPageIterator.getElementCount();
			documentTotalObjects += currentPageMarkCount;
			log.debug("获取当前页Document条目数结束 ： " + currentPageMarkCount);

			log.debug("按分页条件获取文档");
			if (index >= to || (index + currentPageMarkCount) < from) {
				index += currentPageMarkCount;
			} else {
				Object[] currentPage = documentPageIterator.getCurrentPage();

				int tempIndex = index;
				for (int i = 0; i < currentPage.length; i++) {
					index++;
					if (index < from) {
					} else if (index >= from && index <= to) {
						Document document = (Document) currentPage[i];
						//document = CEHelper.getDocumentInfo(os, document.get_Id().toString());
						list.add(document);//添加
					} else if (index > to) {
						index = tempIndex + currentPageMarkCount;
						break;
					}
				}
			}
			log.debug("按分页条件获取文档结束");

		}
		log.debug("DocumentPageMark总数量：" + documentPageMarksList.size());
		log.debug("文档总数量：" + documentTotalObjects);
		Date documentSearchTimeTo = new Date();
		log.debug("查询文档结束：" + documentSearchTimeTo + ",耗时 ：" + (documentSearchTimeTo.getTime() - documentSearchTimeFrom.getTime()) / 1000 + " 秒");


		int totalObjects = folderTotalObjects + documentTotalObjects;
		pagination.setTotalObjects(totalObjects);
		int totalPages = (totalObjects == 0) ? 0 : (totalObjects % pagination.getObjectsPerpage() == 0 ? totalObjects / pagination.getObjectsPerpage() : totalObjects / pagination.getObjectsPerpage() + 1);
		pagination.setTotalPages(totalPages);
		log.debug("文件总数量：" + totalObjects);
		log.debug("文件总页数：" + totalPages);

		return result;
	}

	public RetrievalResult fetchFolders(ObjectStore os, Pagination pagination) throws Exception {

		RetrievalResult result = new RetrievalResult();
		List<Folder> folders = new ArrayList<Folder>();
		result.setPagination(pagination);
		result.setObjectList(folders);

		Date searchTimeFrom = new Date();
		log.debug("开始查询文件夹：" + searchTimeFrom);
		IndependentObjectSet objects = this.search.fetchObjects(this.sql, Integer.valueOf(this.pageSize), this.filter, Boolean.valueOf(this.continuable));
		log.debug("查询IndependentObjectSet完成");
		PageIterator pageIterator = objects.pageIterator();
		int totalObjects = 0;
		List<PageMark> pageMarksList = new ArrayList<PageMark>();


		int from = (pagination.getCurrentPage() - 1) * pagination.getObjectsPerpage() + 1;
		int to = pagination.getCurrentPage() * pagination.getObjectsPerpage();
		int index = 0;

		log.debug("开始计算文件夹总页数");
		while (pageIterator.nextPage() == true) {
			log.debug("开始获取PageMark");
			pageMarksList.add(pageIterator.getPageMark());
			log.debug("获取当前PageMark结束");
			log.debug("开始获取当前页条目数");
			int currentPageMarkCount = pageIterator.getElementCount();
			totalObjects += currentPageMarkCount;
			log.debug("获取当前页条目数结束 ： " + currentPageMarkCount);

			if (index >= to || (index + currentPageMarkCount) < from) {
				index += currentPageMarkCount;
			} else {
				Object[] currentPage = pageIterator.getCurrentPage();

				int tempIndex = index;
				for (int i = 0; i < currentPage.length; i++) {
					index++;
					if (index < from) {
					} else if (index >= from && index <= to) {
						Folder folder = (Folder) currentPage[i];
						//folder = CEHelper.getFolderInfo(os, folder.get_Id().toString());
						folders.add(folder);//添加
					} else if (index > to) {
						index = tempIndex + currentPageMarkCount;
						break;
					}
				}
			}
		}
		log.debug("PageMark总数量：" + pageMarksList.size());
		log.debug("文件夹总数量：" + totalObjects);
		pagination.setTotalObjects(totalObjects);
		int totalPages = (totalObjects == 0) ? 0 : (totalObjects % pagination.getObjectsPerpage() == 0 ? totalObjects / pagination.getObjectsPerpage() : totalObjects / pagination.getObjectsPerpage() + 1);
		pagination.setTotalPages(totalPages);
		log.debug("文件夹总页数：" + totalPages);
		Date searchTimeTo = new Date();
		log.debug("查询文件夹结束：" + searchTimeTo + ",耗时 ：" + (searchTimeTo.getTime() - searchTimeFrom.getTime()) / 1000 + " 秒");
		return result;
	}

	public RetrievalResult fetchDocuments(ObjectStore os, Pagination pagination) throws Exception {

		RetrievalResult result = new RetrievalResult();
		List<Document> documents = new ArrayList<Document>();
		result.setPagination(pagination);
		result.setObjectList(documents);

		Date searchTimeFrom = new Date();
		log.debug("开始查询文档：" + searchTimeFrom);
		IndependentObjectSet objects = this.search.fetchObjects(this.sql, Integer.valueOf(this.pageSize), this.filter, Boolean.valueOf(this.continuable));
		log.debug("查询IndependentObjectSet完成");
		PageIterator pageIterator = objects.pageIterator();
		int totalObjects = 0;
		List<PageMark> pageMarksList = new ArrayList<PageMark>();


		int from = (pagination.getCurrentPage() - 1) * pagination.getObjectsPerpage() + 1;
		int to = pagination.getCurrentPage() * pagination.getObjectsPerpage();
		int index = 0;

		log.debug("开始计算文档总页数");
		while (pageIterator.nextPage() == true) {
			log.debug("开始获取PageMark");
			pageMarksList.add(pageIterator.getPageMark());
			log.debug("获取当前PageMark结束");
			log.debug("开始获取当前页条目数");
			int currentPageMarkCount = pageIterator.getElementCount();
			totalObjects += currentPageMarkCount;
			log.debug("获取当前页条目数结束 ： " + currentPageMarkCount);

			log.debug("按分页条件获取文档");
			if (index >= to || (index + currentPageMarkCount) < from) {
				index += currentPageMarkCount;
			} else {
				Object[] currentPage = pageIterator.getCurrentPage();

				int tempIndex = index;
				for (int i = 0; i < currentPage.length; i++) {
					index++;
					if (index < from) {
					} else if (index >= from && index <= to) {
						Document document = (Document) currentPage[i];
						//document = CEHelper.getDocumentInfo(os, document.get_Id().toString());
						documents.add(document);//添加
					} else if (index > to) {
						index = tempIndex + currentPageMarkCount;
						break;
					}
				}
			}
			log.debug("按分页条件获取文档结束");

		}
		log.debug("PageMark总数量：" + pageMarksList.size());
		log.debug("文档总数量：" + totalObjects);
		pagination.setTotalObjects(totalObjects);
		int totalPages = (totalObjects == 0) ? 0 : (totalObjects % pagination.getObjectsPerpage() == 0 ? totalObjects / pagination.getObjectsPerpage() : totalObjects / pagination.getObjectsPerpage() + 1);
		pagination.setTotalPages(totalPages);
		log.debug("文档总页数：" + totalPages);
		Date searchTimeTo = new Date();
		log.debug("查询文档结束：" + searchTimeTo + ",耗时 ：" + (searchTimeTo.getTime() - searchTimeFrom.getTime()) / 1000 + " 秒");
		return result;
	}

	public RetrievalResult fetchDocumentContentSummary(ObjectStore os, Pagination pagination) throws Exception {

		RetrievalResult result = new RetrievalResult();
		List<DocumentInfo> documents = new ArrayList<DocumentInfo>();
		result.setPagination(pagination);
		result.setObjectList(documents);

		Date searchTimeFrom = new Date();
		log.debug("开始查询文档：" + searchTimeFrom);
		RepositoryRowSet objects = this.search.fetchRows(this.sql, Integer.valueOf(this.pageSize), this.filter, Boolean.valueOf(this.continuable));
		log.debug("查询IndependentObjectSet完成");
		PageIterator pageIterator = objects.pageIterator();
		int totalObjects = 0;
		List<PageMark> pageMarksList = new ArrayList<PageMark>();


		int from = (pagination.getCurrentPage() - 1) * pagination.getObjectsPerpage() + 1;
		int to = pagination.getCurrentPage() * pagination.getObjectsPerpage();
		int index = 0;

		log.debug("开始计算文档总页数");
		while (pageIterator.nextPage() == true) {
			log.debug("开始获取PageMark");
			pageMarksList.add(pageIterator.getPageMark());
			log.debug("获取当前PageMark结束");
			log.debug("开始获取当前页条目数");
			int currentPageMarkCount = pageIterator.getElementCount();
			totalObjects += currentPageMarkCount;
			log.debug("获取当前页条目数结束 ： " + currentPageMarkCount);

			log.debug("按分页条件获取文档");
			if (index >= to || (index + currentPageMarkCount) < from) {
				index += currentPageMarkCount;
			} else {
				Object[] currentPage = pageIterator.getCurrentPage();

				int tempIndex = index;
				for (int i = 0; i < currentPage.length; i++) {
					index++;
					if (index < from) {
					} else if (index >= from && index <= to) {
						DocumentInfo documentInfo = new DocumentInfo();
						RepositoryRow document = (RepositoryRow) currentPage[i];
						String documentId = document.getProperties().get("Id").getIdValue().toString();
						Document doc = CEHelper.getDocumentInfo(os, documentId);
						documentInfo.fromDocument(doc);
						documentInfo.setPath(CEHelper.getFoldersPathFiledIn(doc));
						documentInfo.setContentSummary(document.getProperties().getStringValue("ContentSummary"));
						documents.add(documentInfo);//添加
					} else if (index > to) {
						index = tempIndex + currentPageMarkCount;
						break;
					}
				}
			}
			log.debug("按分页条件获取文档结束");

		}
		log.debug("PageMark总数量：" + pageMarksList.size());
		log.debug("文档总数量：" + totalObjects);
		pagination.setTotalObjects(totalObjects);
		int totalPages = (totalObjects == 0) ? 0 : (totalObjects % pagination.getObjectsPerpage() == 0 ? totalObjects / pagination.getObjectsPerpage() : totalObjects / pagination.getObjectsPerpage() + 1);
		pagination.setTotalPages(totalPages);
		log.debug("文档总页数：" + totalPages);
		Date searchTimeTo = new Date();
		log.debug("查询文档结束：" + searchTimeTo + ",耗时 ：" + (searchTimeTo.getTime() - searchTimeFrom.getTime()) / 1000 + " 秒");
		return result;
	}
}
