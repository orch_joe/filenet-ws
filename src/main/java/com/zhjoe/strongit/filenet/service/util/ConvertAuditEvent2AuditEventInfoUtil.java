/**
 * @Title: ConvertAuditEvent2AuditEventInfoUtil.java
 * @Package com.strongit.filenet.service.util
 * @Description: TODO(用一句话描述该文件做什么)
 * @author Changling Jiang
 * @date 2013-12-21 下午02:54:09
 * @version V1.0
 */
package com.zhjoe.strongit.filenet.service.util;

import com.filenet.api.events.Event;
import com.zhjoe.strongit.filenet.service.datatype.AuditEventInfo;

/**
 * @ClassName: ConvertAuditEvent2AuditEventInfoUtil
 * @Description: AuditEvent转换
 * @author Changling Jiang
 * @date 2013-12-21 下午02:54:09
 *
 */
public class ConvertAuditEvent2AuditEventInfoUtil {

	/**
	 * @Title: convertDocument2DocumentInfo
	 * @Description: 把FileNet CE AuditEvent 转为AuditEventInfo对象
	 * @param @param event
	 * @param @return    设定文件
	 * @throws Exception
	 * @return AuditEventInfo    返回类型
	 */
	public static AuditEventInfo convertDocument2DocumentInfo(Event event) throws Exception {
		try {
			AuditEventInfo auditEventInfo = new AuditEventInfo();
			if (event != null) {
				auditEventInfo.setInitiatingUser(event.get_InitiatingUser());
				auditEventInfo.setClassName(event.getClassName());
				auditEventInfo.setDateCreated(event.get_DateCreated());
				auditEventInfo.setEventStatus(event.get_EventStatus().toString());
			}
			return auditEventInfo;
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}
}
