/**
 * @Title: RetrievalResult.java
 * @Package com.strongit.filenet.service.datatype
 * @Description: 检索结果
 * @author Changling Jiang
 * @date 2013年11月11日 上午11:47:53
 * @version V1.0
 */
package com.zhjoe.strongit.filenet.service.datatype;

import java.util.List;


/**
 * @ClassName: RetrievalResult
 * @Description: 检索结果
 * @author Changling Jiang
 * @date 2013年11月11日 上午11:47:53
 *
 */
public class RetrievalResult {

	private List<?> objectList;

	private Pagination pagination;

	/**
	 * @return objectList
	 */
	public List<?> getObjectList() {
		return objectList;
	}

	/**
	 * @param objectList objectList to set
	 */
	public void setObjectList(List<?> objectList) {
		this.objectList = objectList;
	}

	/**
	 * @return pagination
	 */
	public Pagination getPagination() {
		return pagination;
	}

	/**
	 * @param pagination pagination to set
	 */
	public void setPagination(Pagination pagination) {
		this.pagination = pagination;
	}
}
