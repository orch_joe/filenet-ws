/**
 * @Title: ConfigException.java
 * @Package com.strongit.filenet.service.exception
 * @Description: 自定义配置异常
 * @author Changling Jiang
 * @date 2013年11月11日 下午12:37:49
 * @version V1.0
 */
package com.zhjoe.strongit.filenet.service.exception;

/**
 * @ClassName: ConfigException
 * @Description: 自定义配置异常
 * @author Changling Jiang
 * @date 2013年11月11日 下午12:37:49
 *
 */
public class ConfigException extends RuntimeException {

	/**
	 * @Fields serialVersionUID : TODO(用一句话描述这个变量表示什么)
	 */
	private static final long serialVersionUID = 1L;

	public ConfigException() {

	}

	public ConfigException(Throwable cause) {
		super(cause);
	}

	public ConfigException(String message) {
		super(message);
	}
}
