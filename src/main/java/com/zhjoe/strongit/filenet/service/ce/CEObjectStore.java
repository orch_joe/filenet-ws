/**
 * @Title: CEObjectStore.java
 * @Package com.strongit.filenet.service.ce
 * @Description: FileNet CE ObjectStore 封装对象
 * @author Changling Jiang
 * @date 2013年11月11日 上午11:37:55
 * @version V1.0
 */
package com.zhjoe.strongit.filenet.service.ce;

import com.filenet.api.collection.ObjectStoreSet;
import com.filenet.api.core.Connection;
import com.filenet.api.core.Domain;
import com.filenet.api.core.Factory;
import com.filenet.api.core.ObjectStore;
import com.filenet.api.util.UserContext;

import javax.security.auth.Subject;
import java.util.Iterator;
import java.util.Vector;

/**
 * @ClassName: CEObjectStore
 * @Description: FileNet CE ObjectStore 封装对象
 * @author Changling Jiang
 * @date 2013年11月11日 上午11:37:55
 *
 */
public class CEObjectStore {

	private Connection con;
	private Domain domain;
	private String domainName;
	private ObjectStoreSet oset;
	private Vector<String> osnames;
	private boolean isConnected;
	private UserContext uc;
	private ObjectStore os;
	private Subject subject;
	private boolean sso;

	public CEObjectStore() {
		con = null;
		domain = null;
		domainName = null;
		oset = null;
		osnames = new Vector<String>();
		isConnected = false;
		uc = UserContext.get();
		os = null;
		subject = null;
		sso = false;

	}

	public CEObjectStore(UserContext userContext) {
		this.uc = userContext;
	}

	/**
	 * @return con
	 */
	public Connection getCon() {
		return con;
	}

	/**
	 * @param con con to set
	 */
	public void setCon(Connection con) {
		this.con = con;
	}

	/**
	 * @return domain
	 */
	public Domain getDomain() {
		return domain;
	}

	/**
	 * @param domain domain to set
	 */
	public void setDomain(Domain domain) {
		this.domain = domain;
	}

	/**
	 * @return domainName
	 */
	public String getDomainName() {
		return domainName;
	}

	/**
	 * @param domainName domainName to set
	 */
	public void setDomainName(String domainName) {
		this.domainName = domainName;
	}

	/**
	 * @return oset
	 */
	public ObjectStoreSet getOset() {
		return oset;
	}

	/**
	 * @param oset oset to set
	 */
	public void setOset(ObjectStoreSet oset) {
		this.oset = oset;
	}

	/**
	 * @return osnames
	 */
	public Vector<String> getOsnames() {

		if (osnames.isEmpty()) {
			Iterator it = oset.iterator();
			while (it.hasNext()) {
				ObjectStore os = (ObjectStore) it.next();
				osnames.add(os.get_DisplayName());
			}
		}
		return osnames;
	}

	/**
	 * @param osnames osnames to set
	 */
	public void setOsnames(Vector<String> osnames) {
		this.osnames = osnames;
	}

	/**
	 * @return isConnected
	 */
	public boolean isConnected() {
		return isConnected;
	}

	/**
	 * @param isConnected isConnected to set
	 */
	public void setConnected(boolean isConnected) {
		this.isConnected = isConnected;
	}

	/**
	 * @return uc
	 */
	public UserContext getUc() {
		return uc;
	}

	/**
	 * @param uc uc to set
	 */
	public void setUc(UserContext uc) {
		this.uc = uc;
	}

	/**
	 * @param os os to set
	 */
	public void setOs(ObjectStore os) {
		this.os = os;
	}

	/**
	 * @param subject subject to set
	 */
	public void setSubject(Subject subject) {
		this.subject = subject;
	}

	/**
	 * @return os
	 */
	public ObjectStore getOs() {
		if (!sso) {
			UserContext.get().pushSubject(subject);
		}
		return os;
	}

	/**
	 * @return subject
	 */
	public Subject getSubject() {
		return subject;
	}

	/**
	 *
	 * @Title: fetchOS
	 * @Description: Returns ObjectStore object for supplied
	 * @param @param name	object store name
	 * @param @return
	 * @return ObjectStore
	 */
	public ObjectStore fetchOS(String name) {
		UserContext.get().pushSubject(subject);
		return Factory.ObjectStore.fetchInstance(domain, name, null);
	}

	/**
	 * @return sso
	 */
	public boolean isSso() {
		return sso;
	}

	/**
	 * @param sso sso to set
	 */
	public void setSso(boolean sso) {
		this.sso = sso;
	}


}
