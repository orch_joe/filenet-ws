/**
 * @Title: FolderInfo.java
 * @Package com.strongit.filenet.service.datatype
 * @Description: TODO(用一句话描述该文件做什么)
 * @author Changling Jiang
 * @date 2013年11月21日 下午6:12:12
 * @version V1.0
 */
package com.zhjoe.strongit.filenet.service.datatype;

import java.util.Map;

import com.filenet.api.core.Folder;

/**
 * @ClassName: FolderInfo
 * @Description: TODO(这里用一句话描述这个类的作用)
 * @author Changling Jiang
 * @date 2013年11月21日 下午6:12:12
 *
 */
public class FolderInfo extends AbstractFolderInfo {

	/* (非 Javadoc)
	 * <p>Title: fromCustom</p>
	 * <p>Description: </p>
	 * @param paramFolder
	 * @see com.strongit.filenet.service.datatype.AbstractFolderInfo#fromCustom(com.filenet.api.core.Folder)
	 */
	@Override
	protected void fromCustom(Folder paramFolder) {
	}

	@Override
	public void fromVo(Map<String, Object> map) {
		super.fromVo(map);
	}

	public static final String IS_CHILD_T = "T";
	public static final String IS_CHILD_F = "F";
}
