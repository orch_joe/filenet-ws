/**
 * @Title: Grantee.java
 * @Package com.strongit.filenet.service.datatype
 * @Description: 封装FileNet CE 权限对象类
 * @author Changling Jiang
 * @date 2013年11月11日 上午11:46:53
 * @version V1.0
 */
package com.zhjoe.strongit.filenet.service.datatype;

import org.apache.commons.lang3.builder.ReflectionToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

/**
 * @ClassName: Grantee
 * @Description: 封装FileNet CE 权限对象类
 * @author Changling Jiang
 * @date 2013年11月11日 上午11:46:53
 *
 */
public class Grantee {

	private String name;//带有后缀p8admin@global.cnooc.corp
	private String shortName;//p8admin
	private String displayName;//显示名称
	private String distinguishedName;//完整的串
	private String granteeType;//GROUP||USER
	private String accessType;//ALLOW||DENY
	private int accessMask;
	private int inheritableDepth;//0-No inheritance||1-Immediate children only||-1-All children (infinite levels deep)
	private int permissionSource; // 0-SOURCE_DIRECT_AS_INT||3-SOURCE_PARENT_AS_INT

	public String toString() {
		return ReflectionToStringBuilder.toString(this, ToStringStyle.MULTI_LINE_STYLE);
	}

	/**
	 * @return name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param name name to set
	 */
	public void setName(String name) {
		this.name = name;
	}


	/**
	 * @return granteeType
	 */
	public String getGranteeType() {
		return granteeType;
	}

	/**
	 * @param granteeType granteeType to set
	 */
	public void setGranteeType(String granteeType) {
		this.granteeType = granteeType;
	}

	/**
	 * @return accessType
	 */
	public String getAccessType() {
		return accessType;
	}

	/**
	 * @param accessType accessType to set
	 */
	public void setAccessType(String accessType) {
		this.accessType = accessType;
	}

	/**
	 * @return displayName
	 */
	public String getDisplayName() {
		return displayName;
	}

	/**
	 * @param displayName displayName to set
	 */
	public void setDisplayName(String displayName) {
		this.displayName = displayName;
	}

	/**
	 * @return distinguishedName
	 */
	public String getDistinguishedName() {
		return distinguishedName;
	}

	/**
	 * @param distinguishedName distinguishedName to set
	 */
	public void setDistinguishedName(String distinguishedName) {
		this.distinguishedName = distinguishedName;
	}

	/**
	 * @return shortName
	 */
	public String getShortName() {
		return shortName;
	}

	/**
	 * @param shortName shortName to set
	 */
	public void setShortName(String shortName) {
		this.shortName = shortName;
	}

	/**
	 * @return accessMask
	 */
	public int getAccessMask() {
		return accessMask;
	}

	/**
	 * @param accessMask accessMask to set
	 */
	public void setAccessMask(int accessMask) {
		this.accessMask = accessMask;
	}

	/**
	 * @return inheritableDepth
	 */
	public int getInheritableDepth() {
		return inheritableDepth;
	}

	/**
	 * @param inheritableDepth inheritableDepth to set
	 */
	public void setInheritableDepth(int inheritableDepth) {
		this.inheritableDepth = inheritableDepth;
	}

	/**
	 * @return permissionSource
	 */
	public int getPermissionSource() {
		return permissionSource;
	}

	/**
	 * @param permissionSource permissionSource to set
	 */
	public void setPermissionSource(int permissionSource) {
		this.permissionSource = permissionSource;
	}

}
