/**
 * @Title: BaseInfo.java
 * @Package com.strongit.filenet.service.datatype
 * @Description: FileNet CE/PE 数据对象基础对象
 * @author Changling Jiang
 * @date 2013年11月11日 上午11:39:46
 * @version V1.0
 */
package com.zhjoe.strongit.filenet.service.datatype;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ReflectionToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import java.util.Date;


/**
 * @ClassName: BaseInfo
 * @Description: FileNet CE/PE 数据对象基础对象
 * @author Changling Jiang
 * @date 2013年11月11日 上午11:39:46
 *
 */
public abstract class BaseInfo {

	protected String id;
	protected String name;
	protected String className;
	protected String creater;
	protected Date dateCreated;
	protected String lastModifier;
	protected Date dateLastModified;
	protected String type;//CE Object type:document || folder
	protected String path;//所在路径
	protected String isChild; // 是否是子节点，应用于延迟加载Folder树
	protected String osName;

	public String getOsName() {
		return osName;
	}

	public void setOsName(String osName) {
		this.osName = osName;
	}

	public String getIsChild() {
		return isChild;
	}

	public void setIsChild(String isChild) {
		this.isChild = isChild;
	}

	/**
	 * @return id
	 */
	public String getId() {
		return id;
	}

	/**
	 * @param id id to set
	 */
	public void setId(String id) {
		this.id = id;
	}

	/**
	 * @return name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param name name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @return className
	 */
	public String getClassName() {
		return className;
	}

	/**
	 * @param className className to set
	 */
	public void setClassName(String className) {
		this.className = className;
	}

	/**
	 * @return creater
	 */
	public String getCreater() {
		return creater;
	}

	/**
	 * @param creater creater to set
	 */
	public void setCreater(String creater) {
		this.creater = creater;
	}

	/**
	 * @return dateCreated
	 */

	public Date getDateCreated() {
		return dateCreated;
	}

	/**
	 * @param dateCreated dateCreated to set
	 */
	public void setDateCreated(Date dateCreated) {
		this.dateCreated = dateCreated;
	}

	/**
	 * @return lastModifier
	 */
	public String getLastModifier() {
		return lastModifier;
	}

	/**
	 * @param lastModifier lastModifier to set
	 */
	public void setLastModifier(String lastModifier) {
		this.lastModifier = lastModifier;
	}

	/**
	 * @return dateLastModified
	 */

	public Date getDateLastModified() {
		return dateLastModified;
	}

	/**
	 * @return type
	 */
	public String getType() {
		return type;
	}

	/**
	 * @param type type to set
	 */
	public void setType(String type) {
		this.type = type;
	}

	/**
	 * @param dateLastModified dateLastModified to set
	 */
	public void setDateLastModified(Date dateLastModified) {
		this.dateLastModified = dateLastModified;
	}

	public String toString() {
		return ReflectionToStringBuilder.toString(this, ToStringStyle.MULTI_LINE_STYLE);
	}

	public boolean equals(Object o) {
		return EqualsBuilder.reflectionEquals(this, o);
	}

	public int hashCode() {
		return HashCodeBuilder.reflectionHashCode(this);
	}

	/**
	 * @return path
	 */
	public String getPath() {
		return path;
	}

	/**
	 * @param path path to set
	 */
	public void setPath(String path) {
		this.path = path;
	}

}
