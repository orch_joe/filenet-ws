package com.zhjoe.strongit.filenet.service.util;

import com.filenet.api.core.Connection;
import com.filenet.api.core.Domain;
import com.filenet.api.core.Factory;
import com.filenet.api.core.ObjectStore;
import com.filenet.api.util.UserContext;
import com.zhjoe.strongit.filenet.service.ce.CEObjectStore;

import javax.security.auth.Subject;

import static com.zhjoe.strongit.filenet.service.util.FileNetConfig.*;
import static com.zhjoe.strongit.filenet.service.util.FileNetConfig.ADMIN_PASSWORD;

public class NewCEObjectStoreUtil {
    /**
     *
     * @Title: getCEObjectStore
     * @Description: 获得 FileNet CEObjectStore 对象
     * @param @param user		用户名
     * @param @param password	密码
     * @param @param fetch
     * @param @return
     * @return CEObjectStore
     */
    public static CEObjectStore getCEObjectStore(String user, String password, boolean fetch) {

        FileNetConfigUtil.config();

        CEObjectStore ceos = new CEObjectStore();

        Connection con = NewCEConnectionUtil.getConnection();

        ceos.setCon(con);

        Subject subject = UserContext.createSubject(con, user, password, DEFAULT_JAAS_STANZA);

        ceos.setSubject(subject);

        ceos.getUc().pushSubject(subject);

        Domain domain = Factory.Domain.fetchInstance(con, null, null);

        ceos.setDomain(domain);

        String domainName = domain.get_Name();

        ceos.setDomainName(domainName);

        ceos.setOset(domain.get_ObjectStores());


        ObjectStore os = null;
        String newOsName = FileNetConfig.getValue("NEW_ObjectStoreName");
        if (fetch) {

            os = Factory.ObjectStore.fetchInstance(domain, newOsName, null);

        } else {

            os = Factory.ObjectStore.getInstance(domain, newOsName);

        }

        ceos.setOs(os);

        ceos.setConnected(true);

        return ceos;

    }

    /**
     *
     * @Title: getDefaultCEObjectStore
     * @Description: 获得默认的 FileNet CEObjectStore 对象
     * @param @return
     * @return CEObjectStore
     */
    public static CEObjectStore getDefaultCEObjectStore() {
        String newAdminPassword = FileNetConfig.getValue("newAdminPassword");
        String newAdminUser = FileNetConfig.getValue("newAdminUser");
        return getCEObjectStore(newAdminUser, newAdminPassword, true);
    }
}
