/**
 * @Title: DocumentInfo.java
 * @Package com.strongit.filenet.service.datatype
 * @Description: 标准Document类(非自定义)
 * @author Changling Jiang
 * @date 2013年11月15日 下午5:39:38
 * @version V1.0
 */
package com.zhjoe.strongit.filenet.service.datatype;

import java.util.List;
import java.util.Map;

import com.filenet.api.core.Document;

/**
 * @ClassName: DocumentInfo
 * @Description: 标准Document类(非自定义)
 * @author Changling Jiang
 * @date 2013年11月15日 下午5:39:38
 *
 */
public class DocumentInfo extends AbstractDocumentInfo {
	private List<Grantee> granteeList;
	private String curUserAccessMask;
	private String parentFolderId;

	public String getParentFolderId() {
		return parentFolderId;
	}

	public void setParentFolderId(String parentFolderId) {
		this.parentFolderId = parentFolderId;
	}

	public List<Grantee> getGranteeList() {
		return granteeList;
	}

	public void setGranteeList(List<Grantee> granteeList) {
		this.granteeList = granteeList;
	}

	/* (非 Javadoc)
	 * <p>Title: fromCustom4CommonDisplay</p>
	 * <p>Description: </p>
	 * @param document
	 * @see com.strongit.filenet.service.datatype.AbstractDocumentInfo#fromCustom(com.filenet.api.core.Document)
	 */
	@Override
	protected void fromCustom(Document document) {
	}

	/* (非 Javadoc)
	 * <p>Title: convertProperties2Map</p>
	 * <p>Description: </p>
	 * @param map
	 * @param abstractDocumentInfo
	 * @see com.strongit.filenet.service.datatype.AbstractDocumentInfo#fromVo(java.util.Map, com.strongit.filenet.service.datatype.AbstractDocumentInfo)
	 */
	@Override
	public void fromVo(Map<String, Object> map) {
		super.fromVo(map);
	}

	public String getCurUserAccessMask() {
		return curUserAccessMask;
	}

	public void setCurUserAccessMask(String curUserAccessMask) {
		this.curUserAccessMask = curUserAccessMask;
	}

}
