/**
 * @Title: FileNetConfig.java
 * @Package com.strongit.filenet.service.util
 * @Description: FileNet CE 连接配置信息类
 * @author Changling Jiang
 * @date 2013年11月11日 下午12:42:17
 * @version V1.0
 */
package com.zhjoe.strongit.filenet.service.util;

import java.util.ResourceBundle;

/**
 * @ClassName: FileNetConfig
 * @Description: FileNet CE 连接配置信息类
 * @author Changling Jiang
 * @date 2013年11月11日 下午12:42:17
 *
 */
public class FileNetConfig {

	public static final String DEFAULT_JAAS_STANZA = "FileNetP8WSI";

	private static final ResourceBundle CONFIG_BUNDLE = ResourceBundle.getBundle("config/filenet/login");

	public static final String JAAS_CONFIG_FILE = CONFIG_BUNDLE.getString("JAAS_ConfigFile");

	public static final String WASP_LOCATION = CONFIG_BUNDLE.getString("WASP_Location");

	public static final String CONTENT_ENGINE_URL = CONFIG_BUNDLE.getString("ContentEngineURL");

	public static final String OBJECT_STORE_NAME = CONFIG_BUNDLE.getString("objectStoreName");

	public static final String ADMIN_USER = CONFIG_BUNDLE.getString("admin_user");

	public static final String ADMIN_PASSWORD = CONFIG_BUNDLE.getString("admin_password");

	public static final String CONNECTION_POINT_NAME = CONFIG_BUNDLE.getString("ConnectionPointName");


	public static final String DOCUMENTCLASS = CONFIG_BUNDLE.getString("DocumentClass");

	public static final String DEFAULT_FOLDER_PATH = CONFIG_BUNDLE.getString("RootPath");


	public static String getValue(String key) {

		return CONFIG_BUNDLE.getString(key);

	}
}
