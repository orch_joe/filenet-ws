<!DOCTYPE html>
<html>
<head lang="en">
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>文件预览[${title}]</title>
    <script type="text/javascript" src="${onlyofficeUrl}"></script>
    <script type="text/javascript" src="/document/static/resource/js/jquery/jquery-2.2.4.min.js"></script>
    <style>
        body {
            padding: 0 0 0 0;
            margin: 0 0 0 0;
        }

        html, body {
            height: 100%;
            overflow: hidden;
        }
    </style>

</head>
<body>
<div id="placeholder" style="width:100%;height:100%;overflow:auto"></div>
<script type="text/javascript">
    config = {
        "documentType": "${onlyofficeFileType}",
        "type": "desktop",//mobile
        "document": {
            "fileType": "${fileType}",
            "key": "${key}",
            "title": "${title}",
            "url": "${url}",
            "permissions": {
                "reader": true,
                "download": true,
                "edit": false,
                "print": true,
                "review": "edit",
                "rename": false,
                "changeHistory": false
            }
        },
        "editorConfig": {
            "mode": "${mode}",
            "canAutosave": true,
            "canCoAuthoring": true,
            "callbackUrl": "$saveurl",
            "createUrl": '',
            "lang": "zh",
            "region": "zh-CN",
            "user": {
                id: 'anonymous',
                firstname: '匿名',
                name: "匿名"
            }
            , "customization": {
                "chat": false,
                "comments": false,
                "goback": false,
                "compactToolbar": true,
                "about": false,
                "leftMenu": true,
                "rightMenu": true,
                "toolbar": true,
                "header": false,
                "autosave": true
            }
        },
        "events": {
            'onReady': function () {
            },
            'onBack': function () {
            },
            'onError': function () {
            },
            'onSave': function () {
            }
        }
    };
    jQuery(document).ready(function () {
        var docEditor = new DocsAPI.DocEditor("placeholder", config);
    });
</script>
</body>
</html>
